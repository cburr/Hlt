# =============================================================================
## @file
#  Configuration of Hlt Lines for commissioning
#  @author Gerhard Raven Gerhard.Raven@nikhef.nl
#  @date 2008-12-02
# =============================================================================
"""
 script to configure Hlt lines for commissioning
"""
# =============================================================================
__author__  = "Gerhard Raven Gerhard.Raven@nikhef.nl"
__version__ = "CVS Tag $Name: not supported by cvs2svn $, $Revision: 1.13 $"
# =============================================================================

from HltLine.HltLinesConfigurableUser import *
from GaudiKernel.SystemOfUnits import GeV, MeV, mm


class Hlt1CommissioningLinesConf(HltLinesConfigurableUser):
    __slots__ = {
        'Prescale': {
            'Hlt1ODINPhysics': 0.000001,
            'Hlt1ODINTechnical': 0.000001,  # @OnlineEnv.AcceptRate
            'Hlt1Tell1Error': 1,
            'Hlt1VeloClosingMicroBias': 1,
            'Hlt1VeloClosingEnhanced': 1,
            'Hlt1VeloClosingPV': 1,
        },
        'Postscale': {
            'Hlt1Incident': 'RATE(1)',
            'Hlt1Tell1Error': 'RATE(10)',
            'Hlt1ErrorEvent': 'RATE(0.01)',
            'Hlt1NZSVelo': 'RATE(1)',
            'Hlt1GEC.*': 'RATE(1)',
            'Hlt1VeloClosingMicroBias': 'RATE(500)',
            'Hlt1VeloClosingEnhanced': 'RATE(500)',
            'Hlt1VeloClosingPV': 'RATE(500)',
        },
        'GEC': 'Loose',
        'ODINPhysics': 'ODIN_PASS(LHCb.ODIN.Physics)',
        'ODINTechnical': '( ODIN_TRGTYP == LHCb.ODIN.TechnicalTrigger )',  # TODO how to replace TRGTYP?
        'ODINVeloClosing': 'scale(ODIN_PASS(LHCb.ODIN.VeloOpen), RATE(10000))',
        'ODIN': {
            'VeloClosingEnhanced': 'ODIN_BXTYP == LHCb.ODIN.BeamCrossing',
            'VeloClosingPV': 'ODIN_BXTYP == LHCb.ODIN.BeamCrossing',
        },
        'L0': {
            'VeloClosingEnhanced': "L0_DECISION(LHCb.L0DUDecision.Any)",
            'VeloClosingPV': "L0_DECISION(LHCb.L0DUDecision.Any)",
        },
        'VeloClosingEnhanced': {
            'ZMin': -150 * mm,
            'ZMax': 150 * mm,
            'MinBackwardTracks': 1,
            'MinForwardTracks': 1,
        },
        'VeloClosingPV': {
            'ZMin': -150 * mm,
            'ZMax': 150 * mm,
            'MinBackwardTracks': 1,
            'MinForwardTracks': 1,
        },
    }

    def _createVeloClosingEnhanced(self, name='VeloClosingEnhanced'):
        from HltLine.HltLine import Hlt1Line as Line
        from Configurables import LoKi__VoidFilter as VoidFilter
        from HltTracking.HltSharedTracking import MinimalVelo
        from Hlt1Lines.Hlt1GECs import Hlt1GECUnit

        props = self.getProp(name).copy()
        zcut = 'in_range({ZMin}, TrSTATEZ(LHCb.State.ClosestToBeam), {ZMax})'.format(**props)
        code = ("(TrNUM('{container}', TrBACKWARD & ({zcut})) >= {MinBackwardTracks}) & "
                "(TrNUM('{container}', ~TrBACKWARD & ({zcut})) >= {MinForwardTracks})"
                .format(container=MinimalVelo.outputSelection(), zcut=zcut, **props))
        Line(
            name,
            prescale=self.prescale,
            ODIN=self.getProp('ODIN').get(name),
            L0DU=self.getProp('L0').get(name),
            algos=[
                Hlt1GECUnit(self.getProp('GEC')),
                MinimalVelo,
                VoidFilter(name + 'Filter', Code=code)
            ],
            postscale=self.postscale
        )

    def _creteVeloClosingPV(self, name='VeloClosingPV'):
        from Configurables import LoKi__VoidFilter as VoidFilter
        from Configurables import LoKi__HltUnit as HltUnit
        from HltLine.HltLine import Hlt1Line as Line
        from Hlt1Lines.Hlt1GECs import Hlt1GECUnit
        from HltTracking.HltPVs import PV3D

        pv = PV3D('Hlt1')
        props = self.getProp(name).copy()
        predicate = ("in_range({ZMin}, VZ, {ZMax}) & "
                     "(RV_TrNUM(TrBACKWARD) >= {MinBackwardTracks}) & "
                     "(RV_TrNUM(~TrBACKWARD) >= {MinForwardTracks})"
                     .format(**props))
        code = "VSOURCE('{}', {}) >> ~VEMPTY".format(pv.output, predicate)

        Line(
            name,
            prescale=self.prescale,
            ODIN=self.getProp('ODIN').get(name),
            L0DU=self.getProp('L0').get(name),
            algos=[
                Hlt1GECUnit(self.getProp('GEC')),
                pv,
                VoidFilter('Hlt1{}Filter'.format(name), Code=code)
            ],
            postscale=self.postscale
        )

    def __apply_configuration__(self):
        from HltLine.HltLine import Hlt1Line   as Line
        #from Hlt1GECs import Hlt1_GEC
        #for i in [ 'VELO','IT','OT' ] :
        #    # WARNING: these imply we always decode Velo & IT
        #    Line('GECPassThrough%s'  % i
        #        , L0DU = 'L0_DECISION_PHYSICS'
        #        , prescale = self.prescale
        #        , postscale = self.postscale
        #        , algos = [Hlt1_GEC(i,reject=False)]
        #        )
        Line('ODINPhysics',   ODIN = self.getProp('ODINPhysics')
            , prescale = self.prescale
            , postscale = self.postscale
            )
        Line('ODINTechnical', ODIN = self.getProp('ODINTechnical')
            , prescale = self.prescale
            , postscale = self.postscale
            )
        from Configurables import FilterByBankType
        Line('Tell1Error'
            , algos = [ FilterByBankType('Hlt1Tell1ErrorDecision'
                                        , PassSelectedEvents = True
                                        , BankNames = [ ".*Error" ]
                                        )
                      ]
            , prescale = self.prescale
            , postscale = self.postscale
            )
        Line('NZSVelo'
            , algos = [ FilterByBankType('Hlt1NZSVeloDecision'
                                        , PassSelectedEvents = True
                                        , BankNames = [ "VeloFull" ]
                                        )
                      ]
            , prescale = self.prescale
            , postscale = self.postscale
            )
        from Configurables import HltIncidentFilter
        Line('Incident'
            , algos = [ HltIncidentFilter('Hlt1IncidentDecision') ]
            , prescale = self.prescale
            , postscale = self.postscale
            , priority = 254
            )
        from DAQSys.Decoders import DecoderDB
        from Configurables import LoKi__HDRFilter   as HDRFilter
        decoder = DecoderDB["HltDecReportsDecoder/Hlt1DecReportsDecoder"]
        # TODO: just want presence, so HLT_ERRORBITS(0xffff) would be nice to have...
        Line('ErrorEvent',prescale = self.prescale, postscale = self.postscale
            , algos = [HDRFilter('Hlt1ErrorEventCounter' ,
                                  Code = "HLT_COUNT_ERRORBITS_RE('^Hlt1.*',0xffff) > 0",
                                  Location = decoder.listOutputs()[0])]
            , priority = 254
            )
        from HltTracking.HltSharedTracking import MinimalVelo
        from HltLine.HltLine import Hlt1Member   as Member
        from Hlt1Lines.Hlt1GECs import Hlt1GECUnit
        Line ( 'VeloClosingMicroBias'
                    , prescale = self.prescale
                    , ODIN = self.getProp('ODINVeloClosing')
                    , algos = [Hlt1GECUnit(self.getProp('GEC')) 
                              ,MinimalVelo
                              , Member( 'Hlt::TrackFilter','All'
                                      , Code = [ 'TrALL' ]
                                      , InputSelection = 'TES:%s' % MinimalVelo.outputSelection()
                                      , OutputSelection = '%Decision'
                                      )
                              ]
                    , postscale = self.postscale
                    )

        self._createVeloClosingEnhanced()
        self._creteVeloClosingPV()
