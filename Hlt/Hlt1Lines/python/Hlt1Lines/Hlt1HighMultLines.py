# =============================================================================
## @file
#  @author Sascha Stahl
#  @date 2015-04-08
#  script to configure Hlt lines for high multiplicity
# =============================================================================

from HltLine.HltLinesConfigurableUser import *


class Hlt1HighMultLinesConf(HltLinesConfigurableUser):
    
    __slots__ = { 'VeloClustersRanges'     : [3000, 4000, 99999], # gets overruled by GEC cut
                  'MinVeloHits_PV'         : 2400,
                  'MaxVeloHits_PV'         : 99999,                  
                  'nPVs'                   :  1 ,
                  'ODIN'                   : '',
                  'L0'                     : 'L0_DECISION_PHYSICS',
                  'GEC'                    : 'HeavyIons'
                  }

    def __filterVeloHits(self,name,minVeloHits,maxVeloHits):
        from Configurables import LoKi__VoidFilter
        fltr_veloHits =  LoKi__VoidFilter ( "Hlt1" + name + "Decision"
                                            , Preambulo = ['from LoKiPhys.decorators import *','from LoKiCore.functions import *']
                                            , Code = "in_range( %(MinVeloHits)s , CONTAINS('Raw/Velo/LiteClusters') , %(MaxVeloHits)s )" % {"MinVeloHits":minVeloHits,"MaxVeloHits":maxVeloHits-1})
        return fltr_veloHits
    
    def __apply_configuration__(self):
        
        from HltLine.HltDecodeRaw import DecodeOT
        from HltLine.HltLine import Hlt1Line
        from Configurables import HltCosmicsOT
        from Hlt1Lines.Hlt1GECs import Hlt1GECUnit
        
        from HltTracking.HltSharedTracking import MinimalVelo
        from HltLine.HltLine import Hlt1Member 
        from HltLine.HltLine import Hlt1Line
        from HltLine.HltDecodeRaw import DecodeVELO
        from Configurables import LoKi__VoidFilter
        from HltTracking.HltPVs import PV3D
        from Hlt1Lines.Hlt1GECs import Hlt1GECUnit

        veloClusterRanges = self.getProp("VeloClustersRanges")

        # Select on n PVs
        pvReco = PV3D('Hlt1')
        fltr_nPVs =  LoKi__VoidFilter ( 'HighVeloMult_NPVsFilter'
                                        , Code = " CONTAINS('%(pvLoc)s')==%(nPVs)s " \
                                        % {"pvLoc" : pvReco.output, "nPVs": self.getProp("nPVs")})
        fltr_veloHits_PV = self.__filterVeloHits("HighVeloMultSinglePV",self.getProp("MinVeloHits_PV"), self.getProp("MaxVeloHits_PV"))

        for i in range(1,len(veloClusterRanges) ):
            name = "HighVeloMult"+str(i)
            fltr_veloHits = self.__filterVeloHits( name ,veloClusterRanges[i-1], veloClusterRanges[i])
            Hlt1Line(name,
                     prescale  = self.prescale,
                     postscale = self.postscale,
                     ODIN      = self.getProp('ODIN'),
                     L0DU      = self.getProp('L0'),
                     algos     = [ Hlt1GECUnit(self.getProp('GEC'))
                                   , fltr_nPVs             # TODO: Add PV reco as an algorithm, works without because TrackMVA runs first.
                                   , fltr_veloHits         # select high velo multiplicity events
                                   ]
                     )
        
        for i in range(1,len(veloClusterRanges) ):
            name = "HighVeloMultMultiPV"+str(i)
            fltr_veloHits = self.__filterVeloHits( name ,veloClusterRanges[i-1], veloClusterRanges[i])
            Hlt1Line(name,
                     prescale  = self.prescale,
                     postscale = self.postscale,
                     ODIN      = self.getProp('ODIN'),
                     L0DU      = self.getProp('L0'),
                     algos     = [ Hlt1GECUnit(self.getProp('GEC'))
                                   , fltr_veloHits         # select high velo multiplicity events
                                   ]
                     )
        

        
