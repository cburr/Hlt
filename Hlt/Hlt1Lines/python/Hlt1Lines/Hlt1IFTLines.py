"""Trivial Hlt1 lines for Ion and Fixed Targed physics.

Based on Sascha Stahl's Hlt1HeavyIonLines.py
"""


from HltLine.HltLinesConfigurableUser import HltLinesConfigurableUser
from HltLine.HltLine import Hlt1Line


class Hlt1IFTLinesConf(HltLinesConfigurableUser):
    __slots__ = {
        'ODIN': {
            'BENoBias': 'ODIN_PASS(LHCb.ODIN.NoBias)',
            'EBNoBias': 'ODIN_PASS(LHCb.ODIN.NoBias)',
            'BBNoBias': 'ODIN_PASS(LHCb.ODIN.NoBias)',
        },
        'L0': {
        },
        'GEC': {
            'BEMicroBiasVelo': 'HeavyIons',
            'EBMicroBiasVelo': 'HeavyIons',
            'BEMicroBiasLowMultVelo': 'HeavyIons',
            'BBMicroBiasVelo': 'HeavyIons',
            'BBMicroBiasLowMultVelo': 'HeavyIons',
            'BBHighMult': 'HeavyIons',
        },
        'MaxVeloTracks': {
            'BEMicroBiasLowMultVelo': 10,
            'BBMicroBiasLowMultVelo': 10,
        },
        'MinVeloTracks': {
            'BEMicroBiasVelo': 1,
            'EBMicroBiasVelo': 1,
            'BEMicroBiasLowMultVelo': 1,
            'BBMicroBiasVelo': 1,
            'BBMicroBiasLowMultVelo': 1,
        },
    }

    __odin_bxtype_filters = {
        'BB': 'ODIN_BXTYP == LHCb.ODIN.BeamCrossing',
        'BE': 'ODIN_BXTYP == LHCb.ODIN.Beam1',
        'EB': 'ODIN_BXTYP == LHCb.ODIN.Beam2',
    }

    def __odin(self, bxtype, line):
        bx_filter = self.__odin_bxtype_filters[bxtype]
        line_filter = self.getProp("ODIN").get(line, None)
        if line_filter:
            return '({}) & ({})'.format(bx_filter, line_filter)
        else:
            return bx_filter

    def __l0(self, line):
        return self.getProp("L0").get(line, None)

    def __gec_algos(self, line, inverted=False):
        from Hlt1Lines.Hlt1GECs import Hlt1GECUnit
        gec = self.getProp("GEC").get(line, None)
        return [Hlt1GECUnit(gec, accept=not inverted)] if gec else []

    def __min_velo_tracks(self, line):
        return self.getProp("MinVeloTracks").get(line)

    def __max_velo_tracks(self, line):
        return self.getProp("MaxVeloTracks").get(line)

    def __create_nobias_line(self, bxtype, line):
        return Hlt1Line(
            line,
            prescale=self.prescale,
            ODIN=self.__odin(bxtype, line),
            L0DU=self.__l0(line),
            postscale=self.postscale,
        )

    def __create_microbias_line(self, bxtype, line):
        from HltTracking.HltSharedTracking import MinimalVelo
        from Configurables import LoKi__VoidFilter

        filter_code = "CONTAINS('{}') >= {}".format(
            MinimalVelo.outputSelection(), self.__min_velo_tracks(line))
        algos = self.__gec_algos(line) + [
            MinimalVelo,
            LoKi__VoidFilter('Hlt1{}Decision'.format(line), Code=filter_code)
        ]
        return Hlt1Line(
            line,
            prescale=self.prescale,
            ODIN=self.__odin(bxtype, line),
            L0DU=self.__l0(line),
            algos=algos,
            postscale=self.postscale
        )

    def __create_highmult_line(self, bxtype, line):
        from Configurables import LoKi__VoidFilter
        inverted_gecs = self.__gec_algos(line, inverted=True)
        algos = inverted_gecs + [
            # Dummy filter is needed to avoid the segfault issue (the gec
            # algorithm instance being destroyed multiple times)
            LoKi__VoidFilter('Hlt1{}Decision'.format(line), Code="FALL")
        ]
        return Hlt1Line(
            line,
            prescale=self.prescale,
            ODIN=self.__odin(bxtype, line),
            L0DU=self.__l0(line),
            algos=algos,
            postscale=self.postscale
        )

    def __create_lowmult_line(self, bxtype, line):
        from Configurables import LoKi__HltUnit as HltUnit
        from HltTracking.Hlt1Tracking import VeloCandidates
        filter_code = "VeloCandidates >> in_range({}, TC_SIZE, {})".format(
            self.__min_velo_tracks(line),
            self.__max_velo_tracks(line))
        algos = self.__gec_algos(line) + [
            HltUnit('Hlt1{}Unit'.format(line),
                    Preambulo=[VeloCandidates(line)],
                    Code=filter_code)
        ]
        return Hlt1Line(
            line,
            prescale=self.prescale,
            ODIN=self.__odin(bxtype, line),
            L0DU=self.__l0(line),
            algos=algos,
            postscale=self.postscale
        )

    def __apply_configuration__(self):
        self.__create_nobias_line('BE', 'BENoBias')
        self.__create_nobias_line('EB', 'EBNoBias')
        self.__create_nobias_line('BB', 'BBNoBias')

        self.__create_microbias_line('BE', 'BEMicroBiasVelo')
        self.__create_microbias_line('EB', 'EBMicroBiasVelo')
        self.__create_microbias_line('BB', 'BBMicroBiasVelo')

        self.__create_lowmult_line('BE', 'BEMicroBiasLowMultVelo')
        self.__create_lowmult_line('BB', 'BBMicroBiasLowMultVelo')

        self.__create_highmult_line('BB', "BBHighMult")
