# -*- coding: utf-8 -*-
## @file
#  Set of Hlt1-lines for study of low multiplicity processes (CEP) 
#  Modified L0 input lines to include HRCDiHadron,lowMult
#  @author Dan Johnson daniel.johnson@cern.ch, Paolo Gandini paolo.gandini@cern.ch
#=============================================================================
#=============================================================================
#__author__  = "Dan Johnson daniel.johnson@cern.ch, Paolo Gandini paolo.gandini@cern.ch"

from Gaudi.Configuration import * 
from HltLine.HltLinesConfigurableUser import *

class Hlt1LowMultLinesConf( HltLinesConfigurableUser ):
    __slots__ = {
        'Prescale'                                  : {
                                                        #
                                                        'Hlt1LowMultPassThrough'             : 0.01,
                                                        'Hlt1NoBiasNonBeamBeam'              : 0.20,
                                                        #
                                                        'Hlt1LowMult'                        : 0.00,
                                                        'Hlt1LowMultMuon'                    : 1.00,
                                                        'Hlt1LowMultVeloCut_Hadrons'         : 1.00,
                                                        'Hlt1LowMultVeloCut_Leptons'         : 1.00,
                                                        'Hlt1LowMultMaxVeloCut'              : 1.00,                                                        
                                                        #
                                                        'Hlt1LowMultHerschel'                : 0.00,
                                                        'Hlt1LowMultVeloAndHerschel_Hadrons' : 1.00,
                                                        'Hlt1LowMultVeloAndHerschel_Leptons' : 1.00,
                                                        'Hlt1LowMultMaxVeloAndHerschel'      : 1.00,
                                                        }
        #
        , 'SpdMult'                                 :   100.   # dimensionless
        , 'VeloCut_Hadrons_MinVelo'                 :     2    # dimensionless
        , 'VeloCut_Hadrons_MaxVelo'                 :     8    # dimensionless
        , 'VeloCut_Hadrons_SpdMult'                 :    20    # dimensionless
        , 'VeloCut_Leptons_MinVelo'                 :     0    # dimensionless
        , 'VeloCut_Leptons_MaxVelo'                 :    10    # dimensionless
        , 'MaxVeloCut_MaxVelo'                      :     8    # dimensionless
        , 'MaxNVelo'                                :    10    # dimensionless for generic Hlt1LowMult 
        , 'MinNVelo'                                :     0    # dimensionless for generic Hlt1LowMult
        #
        , 'MaxHRCADC_B2'                            :   5000    # dimensionless, swithching off the cut
        , 'MaxHRCADC_B1'                            :   5000    # dimensionless, swithching off the cut
        , 'MaxHRCADC_B0'                            :   5000    # dimensionless, swithching off the cut
        , 'MaxHRCADC_F1'                            :   5000    # dimensionless, swithching off the cut
        , 'MaxHRCADC_F2'                            :   5000    # dimensionless, swithching off the cut
        #       
        , 'TrChi2'                                  :     5.   # dimensionless, 
        , 'PT'                                      :   500.   # MeV
        , 'P'                                       :     0.   # MeV 
        #        
        , 'LowMultLineL0Dependency'                 : "( L0_CHANNEL_RE('.*(?<!Photon),lowMult') )"
        , 'LowMultVeloCut_HadronsLineL0Dependency'  : "( L0_CHANNEL_RE('DiHadron,lowMult') | L0_CHANNEL_RE('HRCDiHadron,lowMult') )"
        , 'LowMultVeloCut_LeptonsLineL0Dependency'  : "( L0_CHANNEL_RE('Muon,lowMult') | L0_CHANNEL_RE('DiMuon,lowMult') | L0_CHANNEL_RE('Electron,lowMult') )" 
        , 'LowMultMaxVeloCutLineL0Dependency'       : "( L0_CHANNEL_RE('Photon,lowMult') | L0_CHANNEL_RE('DiEM,lowMult') )"
        , 'LowMultPassThroughLineL0Dependency'      : "L0_CHANNEL_RE('.*lowMult')"
        , 'NoBiasNonBeamBeamODIN'                   : "ODIN_PASS(LHCb.ODIN.Lumi) & ~(ODIN_BXTYP == LHCb.ODIN.BeamCrossing)"
        }
    
    def preambulo( self ):
        ## define some "common" preambulo 
        from HltTracking.Hlt1Tracking import TrackCandidates, FitTrack, IsMuon
        prefix = "LowMult"
        Preambulo = [ FitTrack,
                      TrackCandidates( prefix ),
                      IsMuon
                      ]
        return Preambulo
   
    def streamer_VeloCut_Hadrons( self, name ):
        props = self.getProps()

        ## VoidFilter to cut on the number of Velo tracks.
        from Configurables import LoKi__VoidFilter as VoidFilter
        from HltTracking.HltSharedTracking import MinimalVelo
        props['Velo'] = MinimalVelo.outputSelection()
        code = "in_range( %(VeloCut_Hadrons_MinVelo)s, CONTAINS('%(Velo)s'), %(VeloCut_Hadrons_MaxVelo)s ) "% props
        veloFilter = VoidFilter('Hlt1'+name+'Decision', Code = code)
        return [veloFilter]
    
    def streamer_VeloCut_Leptons( self, name ):
        props = self.getProps()

        ## VoidFilter to cut on the number of Velo tracks.
        from Configurables import LoKi__VoidFilter as VoidFilter
        from HltTracking.HltSharedTracking import MinimalVelo
        props['Velo'] = MinimalVelo.outputSelection()
        code = "in_range( %(VeloCut_Leptons_MinVelo)s, CONTAINS('%(Velo)s'), %(VeloCut_Leptons_MaxVelo)s ) "% props
        veloFilter = VoidFilter('Hlt1'+name+'Decision', Code = code)
        return [veloFilter]
    
    def streamer_MaxVeloCut( self, name ):
        props = self.getProps()

        ## VoidFilter to cut on the number of Velo tracks.
        from Configurables import LoKi__VoidFilter as VoidFilter
        from HltTracking.HltSharedTracking import MinimalVelo
        props['Velo'] = MinimalVelo.outputSelection()
        code = "in_range( 0, CONTAINS('%(Velo)s'), %(MaxVeloCut_MaxVelo)s ) "% props
        veloFilter = VoidFilter('Hlt1'+name+'Decision', Code = code)
        return [veloFilter]

    def streamer( self ):
        props = self.getProps()

        ## VoidFilter to cut on the number of Velo tracks.
        from Configurables import LoKi__VoidFilter as VoidFilter
        from HltTracking.HltSharedTracking import MinimalVelo
        props['Velo'] = MinimalVelo.outputSelection()
        code = "in_range( %(MinNVelo)s, CONTAINS('%(Velo)s'), %(MaxNVelo)s ) "% props
        veloFilter = VoidFilter('Hlt1LowMultNVeloFilter', Code = code)

        ## Streamer
        from Configurables import LoKi__HltUnit as HltUnit
        unit = HltUnit(
            'Hlt1LowMultStreamer',
            ##OutputLevel = 1 ,
            Preambulo = self.preambulo(),
            Code = """
            TrackCandidates
            >>  FitTrack
            >>  tee  ( monitor( TC_SIZE > 0, '# pass TrackFit', LoKi.Monitoring.ContextSvc ) )
            >>  tee  ( monitor( TC_SIZE    , 'nFit' , LoKi.Monitoring.ContextSvc ) ) 
            >>  ( TrCHI2PDOF < %(TrChi2)s )
            >>  ( ( TrPT > %(PT)s * MeV ) & ( TrP  > %(P)s * MeV ) )
            >>  tee  ( monitor( TC_SIZE > 0, '# pass PT', LoKi.Monitoring.ContextSvc ) )
            >>  tee  ( monitor( TC_SIZE , 'nPT' , LoKi.Monitoring.ContextSvc ) )               
            >>  SINK('Hlt1LowMultDecision')
            >> ~TC_EMPTY
            """ % props
            )        
        return [veloFilter, unit]

    def streamerMuon( self ):
        props = self.getProps()
        
        ## VoidFilter to cut on the number of Velo tracks.
        from Configurables import LoKi__VoidFilter as VoidFilter
        from HltTracking.HltSharedTracking import MinimalVelo
        props['Velo'] = MinimalVelo.outputSelection()
        code = "in_range( %(MinNVelo)s, CONTAINS('%(Velo)s'), %(MaxNVelo)s ) "% props
        veloFilterMuon = VoidFilter('Hlt1LowMultNVeloFilter', Code = code)
        
        ## Streamer
        from Configurables import LoKi__HltUnit as HltUnit
        unit = HltUnit(
            'Hlt1LowMultMuonStreamer',
            ##OutputLevel = 1 ,
            Preambulo = self.preambulo(),
            Code = """
            TrackCandidates
            >>  FitTrack
            >>  tee  ( monitor( TC_SIZE > 0, '# pass TrackFit', LoKi.Monitoring.ContextSvc ) )
            >>  tee  ( monitor( TC_SIZE    , 'nFit' , LoKi.Monitoring.ContextSvc ) )
            >>  ( TrCHI2PDOF < %(TrChi2)s )
            >>  ( ( TrPT > %(PT)s * MeV ) & ( TrP  > %(P)s * MeV ) )
            >>  tee  ( monitor( TC_SIZE > 0, '# pass PT', LoKi.Monitoring.ContextSvc ) )
            >>  tee  ( monitor( TC_SIZE , 'nPT' , LoKi.Monitoring.ContextSvc ) )
            >>  IsMuon
            >>  tee  ( monitor( TC_SIZE > 0, '# pass IsMuon', LoKi.Monitoring.ContextSvc ) )
            >>  tee  ( monitor( TC_SIZE , 'nIsMuon' , LoKi.Monitoring.ContextSvc ) )
            >>  SINK('Hlt1LowMultMuonDecision')
            >> ~TC_EMPTY
            """ % props
            )
        return [veloFilterMuon, unit]
    

    
    def streamer_HerschelActivityLimit( self, name ):
        props = self.getProps()

        ## VoidFilter to cut on the ADC sum returned from LoKi functor.
        from Configurables import LoKi__VoidFilter as VoidFilter
        code = " ( HRCSUMADC('Raw/HC/Sums','B2') < %(MaxHRCADC_B2)s ) & \
                 ( HRCSUMADC('Raw/HC/Sums','B1') < %(MaxHRCADC_B1)s ) & \
                 ( HRCSUMADC('Raw/HC/Sums','B0') < %(MaxHRCADC_B0)s ) & \
                 ( HRCSUMADC('Raw/HC/Sums','F1') < %(MaxHRCADC_F1)s ) & \
                 ( HRCSUMADC('Raw/HC/Sums','F2') < %(MaxHRCADC_F2)s ) "% props
        herschelFilter = VoidFilter('Hlt1LowMultHerschelFilter', Code = code)
        return [herschelFilter]
         
    def __apply_configuration__( self ) : 
        from HltLine.HltLine import Hlt1Line
        from HltTracking.HltSharedTracking import MinimalVelo
        from Configurables import HCRawBankDecoderHlt 
        Hlt1LowMult_HCRawBankDecoderHlt = HCRawBankDecoderHlt() 

        #
        Hlt1Line(
            'LowMult',
            ##
            prescale  = self.prescale,
            postscale = self.postscale,
            L0DU = "( ( L0_DATA('Spd(Mult)') < %(SpdMult)s ) & ( %(LowMultLineL0Dependency)s ) )" % self.getProps(),   
            ##
            algos     = [ MinimalVelo ] + self.streamer()
            )
        Hlt1Line(
            'LowMultHerschel',
            ##
            prescale  = self.prescale,
            postscale = self.postscale,
            L0DU = "( ( L0_DATA('Spd(Mult)') < %(SpdMult)s ) & ( %(LowMultLineL0Dependency)s ) )" % self.getProps(),
            ##
            algos     =  [Hlt1LowMult_HCRawBankDecoderHlt ] + self.streamer_HerschelActivityLimit('LowMultHerschel') + [ MinimalVelo ] + self.streamer()
            )
        Hlt1Line(
            'LowMultMuon',
            ##
            prescale  = self.prescale,
            postscale = self.postscale,
            L0DU = "( ( L0_DATA('Spd(Mult)') < %(SpdMult)s ) & ( %(LowMultLineL0Dependency)s ) )" % self.getProps(),
            ##
            algos     = [ MinimalVelo ] + self.streamerMuon()
            )

        #
        Hlt1Line(
            'LowMultVeloCut_Hadrons',
            ##
            prescale  = self.prescale,
            postscale = self.postscale,
            L0DU = "( ( L0_DATA('Spd(Mult)') < %(VeloCut_Hadrons_SpdMult)s ) & ( %(LowMultVeloCut_HadronsLineL0Dependency)s ) )" % self.getProps(),   
            ##
            algos     =  [ MinimalVelo ] + self.streamer_VeloCut_Hadrons('LowMultVeloCut_Hadrons') 
            )
        Hlt1Line(
            'LowMultVeloAndHerschel_Hadrons',
            ##
            prescale  = self.prescale,
            postscale = self.postscale,
            L0DU = "( ( L0_DATA('Spd(Mult)') < %(VeloCut_Hadrons_SpdMult)s ) & ( %(LowMultVeloCut_HadronsLineL0Dependency)s ) )" % self.getProps(),   
            ##
            algos     =  [ Hlt1LowMult_HCRawBankDecoderHlt ] + self.streamer_HerschelActivityLimit('LowMultVeloAndHerschel_Hadrons') + [ MinimalVelo ] + self.streamer_VeloCut_Hadrons('LowMultVeloAndHerschel_Hadrons') 
            )

        #
        Hlt1Line(
            'LowMultVeloCut_Leptons',
            ##
            prescale  = self.prescale,
            postscale = self.postscale,
            L0DU = "( ( L0_DATA('Spd(Mult)') < %(SpdMult)s ) & ( %(LowMultVeloCut_LeptonsLineL0Dependency)s ) )" % self.getProps(),   
            ##
            algos     =  [ MinimalVelo ] + self.streamer_VeloCut_Leptons('LowMultVeloCut_Leptons')
            )
        Hlt1Line(
            'LowMultVeloAndHerschel_Leptons',
            ##
            prescale  = self.prescale,
            postscale = self.postscale,
            L0DU = "( ( L0_DATA('Spd(Mult)') < %(SpdMult)s ) & ( %(LowMultVeloCut_LeptonsLineL0Dependency)s ) )" % self.getProps(),
            ##
            algos     = [ Hlt1LowMult_HCRawBankDecoderHlt ] + self.streamer_HerschelActivityLimit('LowMultVeloAndHerschel_Leptons') + [ MinimalVelo ] + self.streamer_VeloCut_Leptons('LowMultVeloAndHerschel_Leptons')
            )
        
        #
        Hlt1Line(
            'LowMultMaxVeloCut',
            ##
            prescale  = self.prescale,
            postscale = self.postscale,
            L0DU = "( ( L0_DATA('Spd(Mult)') < %(SpdMult)s ) & ( %(LowMultMaxVeloCutLineL0Dependency)s ) )" % self.getProps(),
            ##
            algos     =  [ MinimalVelo ] + self.streamer_MaxVeloCut('LowMultMaxVeloCut')
            )

        Hlt1Line(
            'LowMultMaxVeloAndHerschel',
            ##
            prescale  = self.prescale,
            postscale = self.postscale,
            L0DU = "( ( L0_DATA('Spd(Mult)') < %(SpdMult)s ) & ( %(LowMultMaxVeloCutLineL0Dependency)s ) )" % self.getProps(),
            ##
            algos     =  [ Hlt1LowMult_HCRawBankDecoderHlt ] + self.streamer_HerschelActivityLimit('LowMultMaxVeloCutAndHerschel') + [ MinimalVelo ] + self.streamer_MaxVeloCut('LowMultMaxVeloCutAndHerschel')
            )

        #
        Hlt1Line(
            'NoBiasNonBeamBeam',
            ##
            prescale  = self.prescale,
            postscale = self.postscale,
            ODIN      = self.getProps()['NoBiasNonBeamBeamODIN']
            )
        Hlt1Line(
            'LowMultPassThrough',
            ##
            prescale  = self.prescale,
            postscale = self.postscale,
            L0DU = "( %(LowMultPassThroughLineL0Dependency)s )" % self.getProps(),
            ##
            )
