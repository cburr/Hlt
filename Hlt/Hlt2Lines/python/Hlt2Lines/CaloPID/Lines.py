__author__  = (
    'Carla Marin',
    'Regis Lefevre',
    'Max Chefdeville',
)

from GaudiKernel.SystemOfUnits import MeV,mm,picosecond
from Hlt2Lines.Utilities.Hlt2LinesConfigurableUser import Hlt2LinesConfigurableUser
from HltTracking.HltPVs import PV3D

class CaloPIDLines(Hlt2LinesConfigurableUser):
    __slots__ = {'Prescale'    : {'Hlt2CaloPIDDstD02KPiPi0_MergedPi0TurboCalib'   : 1.00,
                                  'Hlt2CaloPIDDstD02KPiPi0_ResolvedPi0TurboCalib' : 1.00,
                                  'Hlt2CaloPIDDsst2DsGammaTurboCalib' : 1.00,
                                  'Hlt2CaloPIDD2EtapPiTurboCalib'     : 1.00,
                                  },
                 'Common'      : {# The following is for the DstD02KPiPi0 lines
                                  'Track_CHI2DOF_MAX'        :  5. ,
                                  'Track_GHOSTPROB_MAX'      :  0.3,
                                  'Track_MIPCHI2DV_MIN'      : 25. ,
                                  'SoftPion_MIPCHI2DV_MAX'   : 16. ,
                                  'Kaon_PIDK_MIN'            :  0. ,
                                  'Pion_PIDK_MAX'            :  0. ,
                                  'ResolvedPi0_GammaCL_MIN'  : -1000., # i.e. no cut
                                  'D0_PT_MIN'                : 4000. * MeV,
                                  'D0_AM_MIN'                : 1400. * MeV,
                                  'D0_AM_MAX'                : 2300. * MeV,
                                  'D0_M_MIN'                 : 1600. * MeV,
                                  'D0_M_MAX'                 : 2100. * MeV,
                                  'D0_VCHI2PDOF_MAX'         :   9.    ,
                                  'D0_VVDCHI2_MIN'           : 100.    ,
                                  'D0_VIPCHI2_MAX'           :   9.    ,
                                  'D0_DIRA_MIN'              :   0.9999,
                                  'Dst_PT_MIN'               : 4000. * MeV,
                                  'Dst_dAM_MIN'              : (145.421-50.) * MeV,
                                  'Dst_dAM_MAX'              : (145.421+50.) * MeV,
                                  'Dst_dM_MIN'               : (145.421-10.) * MeV,
                                  'Dst_dM_MAX'               : (145.421+10.) * MeV,
                                  'Dst_VCHI2PDOF_MAX'        :   9.    ,
                                  'Dst_VVDCHI2_MAX'          :  16.    ,
                                  'Dst_VIPCHI2_MAX'          :   9.    ,
                                  },

                 'Bd2KstGamma' : {'Photon_PT'         : 2600. * MeV,
                                  'Track_IP_Chi2'     : 55.,
                                  'Track_Chi2ndof'    : 5.,
                                  'Track_P'           : 0. * MeV, # no cut
                                  'Track_PT'          : 0. * MeV, # no cut
                                  'HH_Particle'       : 'K*(892)0',
                                  'HH_Vtx_Chi2ndof'   : 9.,
                                  'HH_Mass_Range'     : 100. * MeV, # to 50
                                  'B_Particle'        : 'B0',
                                  'B_Mass_Range'      : 1500. * MeV,
                                  'B_DIRA'            : 0.02,
                                  'B_IP_Chi2'         : 15.,
                                  'B_PT'              : 3000. * MeV,
                                  'B_FD_Chi2'         : 0.,
                                  },

                 'Bs2PhiGamma' : {'Photon_PT'         : 2600. * MeV,
                                  'Track_IP_Chi2'     : 55.,
                                  'Track_Chi2ndof'    : 5.,
                                  'Track_P'           : 0. * MeV, # no cut
                                  'Track_PT'          : 0. * MeV, # no cut
                                  'HH_Particle'       : 'phi(1020)',
                                  'HH_Vtx_Chi2ndof'   : 9.,
                                  'HH_Mass_Range'     : 15. * MeV,
                                  'B_Particle'        : 'B_s0',
                                  'B_Mass_Range'      : 1500. * MeV,
                                  'B_DIRA'            : 99999., # no cut
                                  'B_IP_Chi2'         : 15.,
                                  'B_PT'              : 3000. * MeV,
                                  'B_FD_Chi2'         : 0.,
                                  },
                 
                 'Dsst2DsGamma' : {'Dsst_Particle'         : 'D*_s+',
                                   'Dsst_Mass_Min'         : 2050. * MeV,
                                   'Dsst_Mass_Max'         : 2250. * MeV,
                                   'Dsst_DIRA'             : 0.999975,
                                   'Dsst_IP'               : 0.05 * mm,
                                   'Dsst_IP_Chi2'          : 10.,
                                   'gamma_PT'              : 500. * MeV,
                                   },

                 'D2EtapPi' : {'Pi_PT'         : 500. * MeV,
                               'Pi_MIPCHI2DV'  : 16.,
                               'Pi_Track_Chi2ndof' : 5.,
                               'Pi_TRGHOSTPROB' : 0.5,
                               'Pi_P' : 1000 * MeV,
                               'Pi_PID' : 0,
                               'gamma_PT' : 1000. * MeV,
                               'Etap_Mass_Min' : 900. * MeV,
                               'Etap_Mass_Max' : 1020. * MeV,
                               'Pi_BACH_PT'      : 600. * MeV,
                               'D_APT' : 2000. * MeV,
                               'D_Mass_Min' : 1700. * MeV,
                               'D_Mass_Max' : 2200. * MeV,
                               'D_Vtx_Chi2ndof' : 4.,
                               'D_BPVLTIME' : 0.25 * picosecond ,                               
                               'D_DIRA'             : 0.999975,
                               'D_IP'               : 0.05 * mm,
                               'D_IP_Chi2'          : 10.,
                               },

                 'D02KPiPi0_MergedPi0'  : {'Pi0_PT_MIN'  : 2000. * MeV,
                                           'Track_PT_MIN':  300. * MeV,
                                           },

                 'D02KPiPi0_ResolvedPi0': {'Pi0_PT_MIN'  : 1000. * MeV,
                                           'Track_PT_MIN':  600. * MeV,
                                           },
                 }

    def __apply_configuration__(self):
        # Dsst and D2Etap
        from Stages import Dsst2DsGammaCombiner, D2EtapPiCombiner
        from Stages import Etap_D2EtapPi
        from Inputs import Hlt2Ds, Hlt2NoPIDsPions, Hlt2Photons
        # B2XGamma
        from Stages import B2XGammaCombiner
        from Stages import photons_B2XGamma
        from Stages import Kstar_Bd2KstGamma, Phi_Bs2PhiGamma
        # D02KPiPi0
        from Stages import D02KPiPi0Combiner
        from Stages import DstD02KPiPi0Combiner
        from Stages import MergedPi0_D02KPiPi0
        from Stages import ResolvedPi0_D02KPiPi0
        from Stages import Kaon_D02KPiPi0  
        from Stages import Pion_D02KPiPi0
        from Stages import SoftPion_DstD02KPiPi0
               
        ### candidates
        # B2XGamma
        bd2kstgamma = B2XGammaCombiner('Bd2KstGamma',
                                       "[B0 -> K*(892)0 gamma]cc",
                                       [photons_B2XGamma,Kstar_Bd2KstGamma],
                                       nickname="Bd2KstGamma")
        
        bs2phigamma = B2XGammaCombiner('Bs2PhiGamma',
                                       "B_s0 -> phi(1020) gamma",
                                       [photons_B2XGamma,Phi_Bs2PhiGamma],
                                       nickname="Bs2PhiGamma") 

        # Dsst and D2Etap
        dsst2dsgamma = Dsst2DsGammaCombiner('Dsst2DsGamma',
                                            "[D*_s+ -> D_s+ gamma]cc",
                                            [Hlt2Photons,Hlt2Ds],
                                            nickname="Dsst2DsGamma")

        d2etappi = D2EtapPiCombiner('D2EtapPi',
                                    "[D+ -> eta_prime pi+]cc",
                                    [Etap_D2EtapPi,Hlt2NoPIDsPions],
                                    nickname="D2EtapPi")

        # D02KPiPi0
        D02KPiPi0_MergedPi0 = D02KPiPi0Combiner(
            'D02KPiPi0_MergedPi0', 
            '[D0 -> K- pi+ pi0]cc',
            [Kaon_D02KPiPi0, Pion_D02KPiPi0, MergedPi0_D02KPiPi0],
            nickname="D02KPiPi0_MergedPi0")

        D02KPiPi0_ResolvedPi0 = D02KPiPi0Combiner(
            'D02KPiPi0_ResolvedPi0', 
            '[D0 -> K- pi+ pi0]cc',
            [Kaon_D02KPiPi0, Pion_D02KPiPi0, ResolvedPi0_D02KPiPi0],
            nickname="D02KPiPi0_ResolvedPi0")

        DstD02KPiPi0_MergedPi0 = DstD02KPiPi0Combiner(
            'DstD02KPiPi0_MergedPi0',
            '[D*(2010)+ -> D0 pi+]cc',
            [D02KPiPi0_MergedPi0, SoftPion_DstD02KPiPi0],
            nickname="DstD02KPiPi0_MergedPi0")
        
        DstD02KPiPi0_ResolvedPi0 = DstD02KPiPi0Combiner(
            'DstD02KPiPi0_ResolvedPi0',
            '[D*(2010)+ -> D0 pi+]cc',
            [D02KPiPi0_ResolvedPi0, SoftPion_DstD02KPiPi0],
            nickname="DstD02KPiPi0_ResolvedPi0")

        # lines
        stages = {'Bd2KstGamma'  : [ PV3D('Hlt2'), bd2kstgamma ],
                  'Bs2PhiGamma'  : [ PV3D('Hlt2'), bs2phigamma ],
                  'Dsst2DsGamma' : [ PV3D('Hlt2'), dsst2dsgamma ],
                  'D2EtapPi'     : [ PV3D('Hlt2'), d2etappi ],
                  'DstD02KPiPi0_MergedPi0'   : [ PV3D('Hlt2'), DstD02KPiPi0_MergedPi0 ],
                  'DstD02KPiPi0_ResolvedPi0' : [ PV3D('Hlt2'), DstD02KPiPi0_ResolvedPi0 ],
                  }

        # finalization
        from HltLine.HltLine import Hlt2Line
        for (nickname, algos) in self.algorithms(stages):
            linename = 'CaloPID' + nickname + 'TurboCalib'
            Hlt2Line(linename,
                     prescale=self.prescale,
                     algos=algos,
                     postscale=self.postscale,
                     Turbo=True
                     )

# EOF
