#!/usr/bin/env python
# =============================================================================
# @file   Stages.py
# @author Carla Marin Benito (carla.marin.benito@cern.ch)
# @author Regis Lefevre (rlefevre@cern.ch)
# @author Max Chefdeville (chefdevi@lapp.in2p3.fr)
# @date   24.02.2017
# =============================================================================
"""Candidate building stages for HLT2 Calo PID lines. Based on:
   - Phys/StrippingSelections/python/StrippingSelections/StrippingRD/StrippingBeauty2XGammaExclusive.py"""

### Build PVs
from HltTracking.HltPVs import PV3D
from GaudiKernel.SystemOfUnits import MeV

### Hlt2ParticleFilter (equivalent to FilterDesktop)
from Hlt2Lines.Utilities.Hlt2Filter import Hlt2ParticleFilter

# B2XGamma: Photon filter
class PhotonFilter(Hlt2ParticleFilter):
    def __init__(self, name, inputs, nickname = None):
        cut = ("(PT > %(Photon_PT)s)")
        Hlt2ParticleFilter.__init__(self, name, cut, inputs,
                                    shared=True,
                                    nickname=nickname)

# B2XGamma: HH filter
class HHFilter(Hlt2ParticleFilter):
    def __init__(self, name, inputs, nickname = None):
        pream = [("goodTrack = ((MIPCHI2DV(PRIMARY) > %(Track_IP_Chi2)s)"
                               " & (TRCHI2DOF < %(Track_Chi2ndof)s )"
                               " & (P  > %(Track_P)s )"
                               " & (PT > %(Track_PT)s ))"),
                 
                 ("goodHH = ((VFASPF(VCHI2/VDOF) < %(HH_Vtx_Chi2ndof)s)"
                            " & (ADMASS('%(HH_Particle)s') < %(HH_Mass_Range)s ))")]
        cut = ("(CHILDCUT( goodTrack, 1 ))"
               " & (CHILDCUT( goodTrack, 2 ))"
               " & goodHH")
        Hlt2ParticleFilter.__init__(self, name, cut, inputs,
                                    Preambulo=pream,
                                    dependencies = [PV3D('Hlt2')],
                                    nickname=nickname)
        
# D02KPiPi0: Pi0 filter
class Pi0Filter(Hlt2ParticleFilter):
    def __init__(self, name, inputs, resolved=False, nickname = None):
        cut = ("(PT > %(Pi0_PT_MIN)s *MeV) & (ID == 'pi0')")
        if resolved:
            cut += "& (CHILD(CL,1) > %(ResolvedPi0_GammaCL_MIN)s)"
            cut += "& (CHILD(CL,2) > %(ResolvedPi0_GammaCL_MIN)s)"
        Hlt2ParticleFilter.__init__(self, name, cut, inputs,
                                    shared=True,
                                    nickname=nickname)

# D02KPiPi0: Track filter
class TrackFilter(Hlt2ParticleFilter):
    def __init__(self, name, inputs, track, nickname = None):
        cut = ("(TRCHI2DOF < %(Track_CHI2DOF_MAX)s)" +
               "& (TRGHOSTPROB < %(Track_GHOSTPROB_MAX)s)")
        if track=="SoftPion":
            cut += "& (MIPCHI2DV(PRIMARY) < %(SoftPion_MIPCHI2DV_MAX)s)"
        else:
            cut += "& (MIPCHI2DV(PRIMARY) > %(Track_MIPCHI2DV_MIN)s)"
        if track=="Pion":
            cut += "& (PIDK < %(Pion_PIDK_MAX)s)"
        elif track=="Kaon":
            cut += "& (PIDK > %(Kaon_PIDK_MIN)s)"
        Hlt2ParticleFilter.__init__(self, name, cut, inputs,
                                    dependencies = [PV3D('Hlt2')],
                                    shared=True,
                                    nickname=nickname)      

### Hlt2Combiner (supports CombineParticles)
from Hlt2Lines.Utilities.Hlt2Combiner import Hlt2Combiner

# B2XGamma combiner
class B2XGammaCombiner(Hlt2Combiner):
    def __init__(self, name, decay, inputs, nickname = None):
        decay = decay
        comb_cuts = ("(ADAMASS('%(B_Particle)s') < 1.5*%(B_Mass_Range)s)")
        moth_cuts = ("(acos(BPVDIRA) < %(B_DIRA)s)"
                     " & (BPVIPCHI2() < %(B_IP_Chi2)s)"
                     " & (PT > %(B_PT)s)"
                     " & (ADMASS('%(B_Particle)s') < %(B_Mass_Range)s)"
                     " & (BPVVDCHI2 > %(B_FD_Chi2)s)")
        Hlt2Combiner.__init__(self, name, decay, inputs,
                              CombinationCut=comb_cuts,
                              MotherCut=moth_cuts,
                              dependencies = [PV3D('Hlt2')],
                              nickname=nickname)

# Dsst2DsGamma combiner
class Dsst2DsGammaCombiner(Hlt2Combiner):
    def __init__(self, name, decay, inputs, nickname = None):
        decay = decay
        daug_cuts = {'gamma' : "(PT > %(gamma_PT)s)"}
        comb_cuts = ("ATRUE")
        moth_cuts = ("(BPVDIRA > %(Dsst_DIRA)s)"
                     " & (BPVIP() < %(Dsst_IP)s)"
                     " & (BPVIPCHI2() < %(Dsst_IP_Chi2)s)"
                     " & (MM > %(Dsst_Mass_Min)s)"
                     " & (MM < %(Dsst_Mass_Max)s)")
        Hlt2Combiner.__init__(self, name, decay, inputs,
                              DaughtersCuts = daug_cuts,
                              CombinationCut=comb_cuts,
                              MotherCut=moth_cuts,
                              dependencies = [PV3D('Hlt2')],
                              nickname=nickname)

# D02KPiPi0 combiner
class D02KPiPi0Combiner(Hlt2Combiner):
    def __init__(self, name, decay, inputs, nickname = None):
        decay = decay
        track_cuts  = ("(PT > %(Track_PT_MIN)s *MeV)")
        daught_cuts = { 'K-' : track_cuts, 'pi+' : track_cuts, 'pi0' : "ALL" }
        comb_cuts = ("(in_range( %(D0_AM_MIN)s *MeV, AM, %(D0_AM_MAX)s *MeV))")
        moth_cuts = ("(PT > %(D0_PT_MIN)s)"+
                     "& (VFASPF(VCHI2PDOF) < %(D0_VCHI2PDOF_MAX)s)"+
                     "& (BPVVDCHI2 > %(D0_VVDCHI2_MIN)s)" +
                     "& (BPVIPCHI2() < %(D0_VIPCHI2_MAX)s)" +
                     "& (BPVDIRA > %(D0_DIRA_MIN)s)" +
                     "& (in_range( %(D0_M_MIN)s *MeV, M, %(D0_M_MAX)s *MeV))")
        Hlt2Combiner.__init__(self, name, decay, inputs,
                              DaughtersCuts = daught_cuts,
                              CombinationCut=comb_cuts,
                              MotherCut=moth_cuts,
                              dependencies = [PV3D('Hlt2')],
                              nickname=nickname)

# Etap2PiPiGamma combiner
class Etap2PiPiGammaCombiner(Hlt2Combiner):
    def __init__(self, name, decay, inputs, nickname = None):
        decay = decay
        pi_daug_cuts = ("(PT > %(Pi_PT)s) & (MIPCHI2DV(PRIMARY) > %(Pi_MIPCHI2DV)s) & (TRCHI2DOF < %(Pi_Track_Chi2ndof)s) & (TRGHOSTPROB < %(Pi_TRGHOSTPROB)s) & (P > %(Pi_P)s) & (PIDK-PIDpi < %(Pi_PID)s)")
        g_daug_cuts = ("(PT > %(gamma_PT)s)")
        daug_cuts = {'pi+' : pi_daug_cuts, 'gamma' : g_daug_cuts}
        comb_cuts = ("ATRUE")
        moth_cuts = ("(MM > %(Etap_Mass_Min)s) & (MM < %(Etap_Mass_Max)s)")
        Hlt2Combiner.__init__(self, name, decay, inputs,
                              DaughtersCuts = daug_cuts,
                              CombinationCut=comb_cuts,
                              MotherCut=moth_cuts,
                              dependencies = [PV3D('Hlt2')],
                              nickname=nickname)

# D2EtapPi combiner
class D2EtapPiCombiner(Hlt2Combiner):
    def __init__(self, name, decay, inputs, nickname = None):
        decay = decay
        etap_daug_cuts = ("ALL")
        pi_daug_cuts = ("(PT > %(Pi_BACH_PT)s) & (MIPCHI2DV(PRIMARY) > %(Pi_MIPCHI2DV)s) & (TRCHI2DOF < %(Pi_Track_Chi2ndof)s) & (TRGHOSTPROB < %(Pi_TRGHOSTPROB)s) & (P > %(Pi_P)s) ")
        daug_cuts = {'eta_prime' : etap_daug_cuts , 'pi+' : pi_daug_cuts}
        comb_cuts = ("(APT > %(D_APT)s) & (in_range( %(D_Mass_Min)s,AM,%(D_Mass_Max)s))")
        moth_cuts = ("(VFASPF(VCHI2PDOF) < %(D_Vtx_Chi2ndof)s) & (BPVLTIME() > %(D_BPVLTIME)s) & (BPVDIRA>%(D_DIRA)s) & (BPVIP()<%(D_IP)s) & (BPVIPCHI2()<%(D_IP_Chi2)s)")
        #moth_cuts = ("(VFASPF(VCHI2PDOF) < %(D_Vtx_Chi2ndof)s) & (BPVLTIME() > %(D_BPVLTIME)s)")
        Hlt2Combiner.__init__(self, name, decay, inputs,
                              DaughtersCuts = daug_cuts,
                              CombinationCut=comb_cuts,
                              MotherCut=moth_cuts,
                              dependencies = [PV3D('Hlt2')],
                              nickname=nickname)

# DstD02KPiPi0 combiner        
class DstD02KPiPi0Combiner(Hlt2Combiner):
    def __init__(self, name, decay, inputs, nickname = None):
        decay = decay
        comb_cuts = ("(in_range( %(Dst_dAM_MIN)s *MeV, (AM - AM1), %(Dst_dAM_MAX)s *MeV))")
        moth_cuts = ("(PT > %(Dst_PT_MIN)s)"+
                     "& (VFASPF(VCHI2PDOF) < %(Dst_VCHI2PDOF_MAX)s)"+
                     "& (BPVVDCHI2 < %(Dst_VVDCHI2_MAX)s)" +
                     "& (BPVIPCHI2() < %(Dst_VIPCHI2_MAX)s)" +
                     "& (in_range( %(Dst_dM_MIN)s *MeV, (M - M1), %(Dst_dM_MAX)s *MeV))")
        Hlt2Combiner.__init__(self, name, decay, inputs,
                              CombinationCut=comb_cuts,
                              MotherCut=moth_cuts,
                              dependencies = [PV3D('Hlt2')],
                              nickname=nickname)

##########
### Build intermediate states (final ones are built in Lines.py)
##########
        
# B2XGamma
from Inputs import Hlt2Photons, Hlt2NoPIDsPions, Hlt2Kstar, Hlt2Phi, Hlt2Ds
photons_B2XGamma = PhotonFilter("photons_B2XGamma",
                                [Hlt2Photons],
                                nickname="Bd2KstGamma")

Kstar_Bd2KstGamma = HHFilter("Kstar_Bd2KstGamma",
                             [Hlt2Kstar],
                             nickname="Bd2KstGamma")

Phi_Bs2PhiGamma = HHFilter("Phi_Bs2PhiGamma",
                           [Hlt2Phi],
                           nickname="Bs2PhiGamma")

# D2EtapPi
Etap_D2EtapPi = Etap2PiPiGammaCombiner("Etap_D2EtapPi",
                                       "[eta_prime -> pi+ pi- gamma]cc",
                                       [Hlt2NoPIDsPions,Hlt2Photons],
                                       nickname="D2EtapPi")


# D02KPiPi0
from Inputs import Hlt2MergedPi0, Hlt2ResolvedPi0, Hlt2LooseKaons, Hlt2LoosePions, Hlt2NoPIDsPions
MergedPi0_D02KPiPi0 = Pi0Filter("MergedPi0_D02KPiPi0",
                                [Hlt2MergedPi0],
                                resolved=False,
                                nickname="D02KPiPi0_MergedPi0")

ResolvedPi0_D02KPiPi0 = Pi0Filter("ResolvedPi0_D02KPiPi0",
                                  [Hlt2ResolvedPi0],
                                  resolved=True,
                                  nickname="D02KPiPi0_ResolvedPi0")

Kaon_D02KPiPi0 = TrackFilter("Kaon_D02KPiPi0",
                             [Hlt2LooseKaons],
                             track="Kaon",
                             nickname="Kaon_D02KPiPi0")

Pion_D02KPiPi0 = TrackFilter("Pion_D02KPiPi0",
                             [Hlt2LoosePions],
                             track="Pion",
                             nickname="Pion_D02KPiPi0")

SoftPion_DstD02KPiPi0 = TrackFilter("SoftPion_DstD02KPiPi0",
                                    [Hlt2NoPIDsPions],
                                    track="SoftPion",
                                    nickname="SoftPion_DstD02KPiPi0")

# EOF
