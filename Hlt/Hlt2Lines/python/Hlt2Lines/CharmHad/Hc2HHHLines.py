from GaudiKernel.SystemOfUnits import GeV, MeV, picosecond, mrad

class CharmHadHc2HHHLines() :
    def localcuts(self) :
        cuts = {
        'PentaPhiPp' : {
                 'Trk_ALL_PT_MIN'           :  250 * MeV,
                 'Trk_ALL_MIPCHI2DV_MIN'    :  3,
                 'AM12_MIN'                 : 1012.0 * MeV,
                 'AM12_MAX'                 : 1028.0 * MeV,
                 'AM_MIN'                   :  2240 * MeV,
                 'AM_MAX'                   :  2800 * MeV,
                 'ASUMPT_MIN'               :  2200.0 * MeV,
                 'ACHI2DOCA_MAX'            :  10.0,
                 'VCHI2PDOF_MAX'            :  4.0,
                 'acosBPVDIRA_MAX'          :  20.0 * mrad,
                 'BPVLTIME_MIN'             :  0.2*picosecond,
                 'PT_MIN'                   :  2000.0 * MeV,
                 'IPCHI2_MAX'               :  4.0,
                 'Mass_M_MIN'               :  2255 * MeV,
                 'Mass_M_MAX'               :  2785 * MeV,
                }
            }
        return cuts

    def locallines(self) :
        from Stages import MassFilter
        from Stages import PentaPhiPp
       
        PentaPhiPpFilt = MassFilter('Filt' , inputs = [ PentaPhiPp ]
                                              , nickname = 'PentaPhiPp')
 
        stages = {
            'PentaToPhiPpTurbo' : [PentaPhiPpFilt],
            }
        return stages
