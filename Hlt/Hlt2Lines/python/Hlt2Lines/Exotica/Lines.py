from GaudiKernel.SystemOfUnits import GeV, MeV, picosecond, mm
from Hlt2Lines.Utilities.Hlt2LinesConfigurableUser import \
    Hlt2LinesConfigurableUser

# Exotica Lines written by Phil Ilten and Mike Williams (mwill@mit.edu), tuned by Constantin Weisser
# Addition of LFVPrompt lines by X. Cid Vidal (xabier.cid.vidal@cern.ch)

from Hlt2Lines.Utilities.Hlt2RelatedInfo import Hlt2RelInfoConeVariables, Hlt2RelInfoVertexIsolation

class ExoticaLines(Hlt2LinesConfigurableUser):
    __slots__ = {'_stages' : {},
                 'Prescale' : {'Hlt2ExoticaDiMuonNoIPTurbo': 0.01,
                               'Hlt2ExoticaDiMuonNoIPSSTurbo':0.01},
                 'Postscale' : {},
                 'Common' : {'GhostProb' : 0.3},
                 'ExoticaDisplPhiPhi' : {'TisTosSpec' : "Hlt1IncPhi.*Decision%TOS" ,
                                         'KPT'  : 500*MeV,
                                         'KIPChi2' : 16,
                                         'KProbNNk' : 0.1,
                                         'PhiPT'  : 1000*MeV,
                                         'PhiMassWindow' : 20*MeV,
                                         'VChi2' : 10,
                                         'FDChi2' : 45},
                 'ExoticaSharedDiMuonNoIP' : {'MuPT' : 500*MeV,
                                              'MuP' : 10000*MeV,
                                              'MuProbNNmu' : 0.2,
                                              'DOCA' : 0.2*mm,
                                              'VChi2' : 10},
                 'ExoticaDiMuonNoIPTurbo' : {'TisTosSpec' : None,
                                             'PT' : 1000*MeV},                                        
                 'ExoticaDiMuonNoIPSSTurbo' : {'TisTosSpec' : [],
                                                'MuPT' : 500*MeV,
                                                'MuP' : 10000*MeV,
                                                'MuProbNNmu' : 0.95,
                                                'DOCA' : 0.2*mm,
                                                'VChi2' : 10,
                                                'PT' : 1000*MeV},
                 'ExoticaQuadMuonNoIP' : {'TisTosSpec' : None,
                                          'PT' : 0,
                                          'VChi2' : 10},       
                 'ExoticaDisplDiMuon' : {'TisTosSpec' : None,
                                         'MSwitch' : 500*MeV,
                                         'MuProbNNmu_lowmass' : 0.5,
                                         'MuProbNNmu_highmass' : 0.8,
                                         'MuIPChi2_lowmass' : 4,
                                         'MuIPChi2_highmass' : 9,
                                         'PT' : 1000*MeV,                                   
                                         'IPChi2' : 16, 
                                         'FDChi2' : 30},
                 'ExoticaDisplDiMuonNoPoint' : {'TisTosSpec' : None,
                                                'MSwitch' : 500*MeV,
                                                'MuProbNNmu_lowmass' : 0.5,
                                                'MuProbNNmu_highmass' : 0.8,
                                                'MuIPChi2_lowmass' : 4,
                                                'MuIPChi2_highmass' : 9,
                                                'PT' : 1000*MeV,                                   
                                                'FDChi2' : 30},
                 'ExoticaPrmptDiMuonTurbo' : {'TisTosSpec' : "Hlt1DiMuonNoIPDecision%TOS",
                                              'MuPT' : 500*MeV,
                                              'MuPTPROD' : 1*GeV*GeV,
                                              'MuP' : 10000*MeV,
                                              'M_switch_ab' : 740*MeV,
                                              'M_switch_bc' : 1100*MeV,
                                              'M_switch_cd' : 3000*MeV,
                                              'M_switch_de' : 3200*MeV,
                                              'M_switch_ef' : 9000*MeV,
                                              'MuProbNNmu_a' : 0.8,
                                              'MuProbNNmu_b' : 0.8,
                                              'MuProbNNmu_c' : 0.95,
                                              'MuProbNNmu_d' : 2.0, #Always False
                                              'MuProbNNmu_e' : 0.95,
                                              'MuProbNNmu_f' : 0.9,
                                              'MuIPChi2' : 6,
                                              'PT' : 1000*MeV,
                                              'FDChi2' : 45},
                 'ExoticaPrmptDiMuonJpsiMassTurbo' : {'TisTosSpec' : "Hlt1DiMuonNoIPDecision%TOS",
                                              'MuPT' : 500*MeV,
                                              'MuPTPROD' : 1*GeV*GeV,
                                              'MuP' : 10000*MeV,
                                              'M_switch_ab' : 740*MeV,
                                              'M_switch_bc' : 1100*MeV,
                                              'M_switch_cd' : 3000*MeV,
                                              'M_switch_de' : 3200*MeV,
                                              'M_switch_ef' : 9000*MeV,
                                              'MuProbNNmu_a' : 2.0, #Always False
                                              'MuProbNNmu_b' : 2.0, #Always False
                                              'MuProbNNmu_c' : 2.0, #Always False
                                              'MuProbNNmu_d' : 0.7,
                                              'MuProbNNmu_e' : 2.0, #Always False
                                              'MuProbNNmu_f' : 2.0, #Always False
                                              'MuIPChi2' : 6,
                                              'PT' : 1000*MeV,
                                              'FDChi2' : 45},
                 'ExoticaPrmptDiMuonSSTurbo' : {'TisTosSpec' : "Hlt1DiMuonNoIPSSDecision%TOS",
                                                'MuPT' : 500*MeV,
                                                'MuPTPROD' : 1*GeV*GeV,
                                                'MuP' : 10000*MeV,
                                                'M_switch_ab' : 740*MeV,
                                                'M_switch_bc' : 1100*MeV,
                                                'M_switch_cd' : 3000*MeV,
                                                'M_switch_de' : 3200*MeV,
                                                'M_switch_ef' : 9000*MeV,
                                                'MuProbNNmu_min' : 0.8,
                                                'MuProbNNmu_a' : 0.8,
                                                'MuProbNNmu_b' : 0.8,
                                                'MuProbNNmu_c' : 0.95,
                                                'MuProbNNmu_d' : 2.0, #Always False
                                                'MuProbNNmu_e' : 0.95,
                                                'MuProbNNmu_f' : 0.9,
                                                'MuIPChi2' : 6,
                                                'DOCA' : 0.2*mm,
                                                'VChi2' : 10,
                                                'PT' : 1000*MeV,                                   
                                                'FDChi2' : 45},
                 'ExoticaPrmptDiMuonSSJpsiMassTurbo' : {'TisTosSpec' : "Hlt1DiMuonNoIPSSDecision%TOS",
                                                'MuPT' : 500*MeV,
                                                'MuPTPROD' : 1*GeV*GeV,
                                                'MuP' : 10000*MeV,
                                                'M_switch_ab' : 740*MeV,
                                                'M_switch_bc' : 1100*MeV,
                                                'M_switch_cd' : 3000*MeV,
                                                'M_switch_de' : 3200*MeV,
                                                'M_switch_ef' : 9000*MeV,
                                                'MuProbNNmu_min' : 0.7,
                                                'MuProbNNmu_a' : 2.0, #Always False
                                                'MuProbNNmu_b' : 2.0, #Always False
                                                'MuProbNNmu_c' : 2.0, #Always False
                                                'MuProbNNmu_d' : 0.9,
                                                'MuProbNNmu_e' : 2.0, #Always False
                                                'MuProbNNmu_f' : 2.0, #Always False
                                                'MuIPChi2' : 6,
                                                'DOCA' : 0.2*mm,
                                                'VChi2' : 10,
                                                'PT' : 1000*MeV,
                                                'FDChi2' : 45},
                 'ExoticaPrmptDiMuonHighMass' : {'TisTosSpec' : "Hlt1DiMuonHighMassDecision%TOS",
                                                 'MuPT' : 500*MeV,
                                                'MuPTPROD' : 1*GeV*GeV,
                                                 'MuP' : 10000*MeV,
                                                 'M'          : 5000*MeV,
                                                 'M_switch_ab' : 740*MeV,
                                                 'M_switch_bc' : 1100*MeV,
                                                 'M_switch_cd' : 3000*MeV,
                                                 'M_switch_de' : 3200*MeV,
                                                 'M_switch_ef' : 9000*MeV,
                                                 'MuProbNNmu_a' : 0.8,
                                                 'MuProbNNmu_b' : 0.8,
                                                 'MuProbNNmu_c' : 0.95,
                                                 'MuProbNNmu_d' : 2.0, #Always False
                                                 'MuProbNNmu_e' : 0.95,
                                                 'MuProbNNmu_f' : 0.9,
                                                 'MuIPChi2' : 6,
                                                 'PT' : 1000*MeV,
                                                 'FDChi2' : 45},
                'ExoticaSharedDiENoIP' : {'EPT' : 500*MeV,
                                              'EP' : 5000*MeV,
                                              'EProbNNe' : 0.1,
                                              'DOCA' : 0.5*mm,
                                              'MM' : 10*MeV,
                                              'VChi2' : 25},
                'ExoticaDisplDiE' : {'TisTosSpec' :  "Hlt1TrackMVA.*Decision%TOS",
                                         'EProbNNe' : 0.1,
                                         'EIPChi2' : 8,
                                         'PT' : 500*MeV,
                                         'IPChi2' : 32,
                                         'FDChi2' : 20},
                 'ExoticaSharedRHNu' : {'TisTosSpec' : None,
                                        'PT' : 500*MeV,
                                        'P' : 10000*MeV,                                                  
                                        'ProbNNmu' : 0.5, 
                                        'IPChi2' : 16,
                                        'VChi2' : 10,
                                        'XIPChi2' : 16,
                                        'M' : 0,
                                        'TAU' : 1*picosecond,
                                        'FDChi2' : 45},
                 'ExoticaRHNu' : {'TisTosSpec' : None,                 
                                  'M' : 0,
                                  'TAU' : 1*picosecond},
                 'ExoticaRHNuHighMass' : {'TisTosSpec' : None,
                                          'ProbNNmu' : 0.5,
                                          'M' : 5000*MeV,
                                          'TAU' : 1*picosecond},
                 'ExoticaDiRHNu' : {'TisTosSpec' : None,                 
                                    'PT' : 0,
                                    'VChi2' : 10},


                 'ExoticaSharedLFVPrmpt' : {'TisTosSpec' : None,
                                            'PT' : 500*MeV,
                                            'P' : 10000*MeV,
                                            
                                            'ProbNNmu' : 0.5,
                                            'ProbNNe' : 0.25,
                                            'IPChi2' : 1,
                                            'VChi2' : 5,
                                            'XIPChi2' : 1,
                                            'M' : 0,
                                            'FDChi2' : 1},
                 
                 'ExoticaLFVPrmptTurbo' : {'TisTosSpec' : None,
                                           'M' : 0},
                 
                 'ExoticaLFVPrmptSSTurbo' : {'TisTosSpec' : None,
                                             'PT' : 500*MeV,
                                             'P' : 10000*MeV,
                                             
                                             'ProbNNmu' : 0.5,
                                             'ProbNNe' : 0.25,
                                             'IPChi2' : 1,
                                             'VChi2' : 5,
                                             'XIPChi2' : 1,
                                             'M' : 0,
                                             'FDChi2' : 1},
                 
                 'ExoticaLFVPrmpt' : {'TisTosSpec' : None,
                                      'M' : 5000*MeV},                                                   
                 
                 
                 'ExoticaSharedDisplacedDiE' : {'EPT' : 200*MeV,
                                                'EP' : 2000*MeV,
                                                'EIPChi2' : 9,
                                                'EProbNNe' : 0.1,
                                                'M' : 0*MeV,
                                                'PT' : 0*MeV,                                   
                                                'FDChi2' : 30,
                                                'VChi2' : 100},
                 'ExoticaSharedDisplacedEta2PiPiXee' :  {'PiPT' : 200*MeV,
                                                         'PiP' : 2000*MeV,
                                                         'PiIPChi2' : 9,
                                                         'PiPIDK' : 0,
                                                         'DM' : 200*MeV,
                                                         'PT' : 500*MeV,                                   
                                                         'FDChi2' : 30,
                                                         'VChi2' : 100},
                 'ExoticaD2PiEta_Eta2PiPiXee' : {'TisTosSpec' : None,
                                                 'PiPT' : 500*MeV,
                                                 'PiP'  : 5000*MeV,
                                                 'PiIPChi2' : 9,
                                                 'PiPIDK' : 0,
                                                 'PT' : 1000*MeV,
                                                 'MMIN' : 1700*MeV,
                                                 'MMAX' : 2100*MeV,
                                                 'VChi2' : 10,
                                                 'FDChi2' : 30},
                 
                 'A1ConeVar05' : { "ConeAngle" : 0.5,
                                   "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                   "Location"  : 'A1ConeVar05' },
                 
                 'A1ConeVar15' : { "ConeAngle" : 1.5,
                                   "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                   "Location" :  'A1ConeVar15'},
                 
                 'A1ConeVar10A1mue' : { "ConeAngle" : 1.0,
                                        "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                        "DaughterLocations" : {"[KS0 -> ^mu+ e-]CC" : "MuConeVar10"}
                                        },
                 
                 'A1ConeVar10A1emu' : { "ConeAngle" : 1.0,
                                        "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                        "DaughterLocations" : {"[KS0 -> mu+ ^e-]CC" : "EConeVar10"}
                                        },

                 'A1ConeVar10A1mueSS' : { "ConeAngle" : 1.0,
                                          "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                          "DaughterLocations" : {"[KS0 -> ^mu+ e+]CC" : "MuConeVar10SS"}
                                          },
                 'A1ConeVar10A1emuSS' : { "ConeAngle" : 1.0,
                                          "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                          "DaughterLocations" : {"[KS0 -> mu+ ^e+]CC" : "EConeVar10SS"}
                                          },
                 
                 'VertexIsolation' : { "Location"  : "VertexIsoInfo" }

             }



    def stages(self, nickname = ''):
        if hasattr(self, '_stages') and self._stages:
            if nickname: return self._stages[nickname]
            else: return self._stages
        else: self._stages = {}
      
        from Stages import DisplPhiPhi, SharedDiMuonNoIP, DiMuonNoIP, QuadMuonNoIP, DisplDiMuon, DisplDiMuonNoPoint, PrmptDiMuon, PrmptDiMuonSS, DiMuonNoIPSS, SharedDiENoIP, DisplDiE, SharedRHNu, RHNu, DiRHNu, RHNuNoIP, SharedLFVPrmpt, LFVPrmpt, LFVPrmptSS, SharedDisplacedDiE, SharedDisplacedEta2PiPiXee, D2PiEta
        sharedDiMuon = [SharedDiMuonNoIP('ExoticaSharedDiMuonNoIP')]
        sharedRHNu = [SharedRHNu('ExoticaSharedRHNu')]
        sharedLFVPrmpt = [SharedLFVPrmpt('ExoticaSharedLFVPrmpt')]
        sharedDiE = [SharedDisplacedDiE('ExoticaSharedDisplacedDiE')]
        sharedEta2PiPiXee = [SharedDisplacedEta2PiPiXee('ExoticaSharedDisplacedEta2PiPiXee',sharedDiE)]

        sharedDiENoIP = [SharedDiENoIP('ExoticaSharedDiENoIP')]

        self._stages = {'ExoticaDisplPhiPhi' : [DisplPhiPhi('ExoticaDisplPhiPhi')],
                        'ExoticaSharedDiMuonNoIP' : sharedDiMuon,
                        'ExoticaSharedDiENoIP' : sharedDiENoIP,
                        'ExoticaDiMuonNoIPTurbo' : [DiMuonNoIP('ExoticaDiMuonNoIPTurbo',sharedDiMuon)],
                        'ExoticaQuadMuonNoIP' : [QuadMuonNoIP('ExoticaQuadMuonNoIP',sharedDiMuon)],
                        'ExoticaDisplDiMuon' : [DisplDiMuon('ExoticaDisplDiMuon',sharedDiMuon)],
                        'ExoticaDisplDiMuonNoPoint' : [DisplDiMuonNoPoint('ExoticaDisplDiMuonNoPoint',sharedDiMuon)],
                        'ExoticaPrmptDiMuonTurbo' : [PrmptDiMuon('ExoticaPrmptDiMuonTurbo',sharedDiMuon)],
                        'ExoticaPrmptDiMuonJpsiMassTurbo' : [PrmptDiMuon('ExoticaPrmptDiMuonJpsiMassTurbo',sharedDiMuon)],
                        'ExoticaPrmptDiMuonSSTurbo' : [PrmptDiMuonSS('ExoticaPrmptDiMuonSSTurbo')],
                        'ExoticaPrmptDiMuonSSJpsiMassTurbo' : [PrmptDiMuonSS('ExoticaPrmptDiMuonSSJpsiMassTurbo')],
                        'ExoticaDiMuonNoIPSSTurbo' : [DiMuonNoIPSS('ExoticaDiMuonNoIPSSTurbo')],
                        'ExoticaPrmptDiMuonHighMass' : [PrmptDiMuon('ExoticaPrmptDiMuonHighMass',sharedDiMuon,True)],
                        'ExoticaDisplDiE' : [DisplDiE('ExoticaDisplDiE',sharedDiENoIP)],
                        'ExoticaSharedRHNu' : sharedRHNu,
                        'ExoticaRHNu' : [RHNu('ExoticaRHNu',sharedRHNu)],
                        'ExoticaRHNuHighMass' : [RHNu('ExoticaRHNuHighMass',sharedRHNu)],
                        'ExoticaDiRHNu' : [DiRHNu('ExoticaDiRHNu',sharedRHNu)],
                        #'ExoticaRHNuNoIPTurbo' : [RHNuNoIP('ExoticaRHNuNoIPTurbo')]}
                        'ExoticaSharedLFVPrmpt': sharedLFVPrmpt,
                        'ExoticaLFVPrmptTurbo' : [LFVPrmpt('ExoticaLFVPrmptTurbo',sharedLFVPrmpt)],
                        'ExoticaLFVPrmptSSTurbo' : [LFVPrmptSS('ExoticaLFVPrmptSSTurbo')],
                        'ExoticaLFVPrmpt' : [LFVPrmpt('ExoticaLFVPrmpt',sharedLFVPrmpt)],

                        'ExoticaSharedDisplacedDiE' : sharedDiE,
                        'ExoticaSharedDisplacedEta2PiPiXee' : sharedEta2PiPiXee,
                        'ExoticaD2PiEta_Eta2PiPiXee' : [D2PiEta('ExoticaD2PiEta_Eta2PiPiXee',sharedEta2PiPiXee)]}


        # Return the stages.
        if nickname: return self._stages[nickname]
        else: return self._stages


    def relatedInfo(self):
        A1ConeVar05 = Hlt2RelInfoConeVariables('A1ConeVar05')
        A1ConeVar15 = Hlt2RelInfoConeVariables('A1ConeVar15')
        A1ConeVar10A1mue = Hlt2RelInfoConeVariables('A1ConeVar10A1mue')
        A1ConeVar10A1emu = Hlt2RelInfoConeVariables('A1ConeVar10A1emu')
        A1VertexIso = Hlt2RelInfoVertexIsolation("VertexIsolation")        
        myl = [A1ConeVar05,A1ConeVar15,A1ConeVar10A1mue,A1ConeVar10A1emu,A1VertexIso]
        
        A1ConeVar10A1mueSS = Hlt2RelInfoConeVariables('A1ConeVar10A1mueSS')
        A1ConeVar10A1emuSS = Hlt2RelInfoConeVariables('A1ConeVar10A1emuSS')
        mylSS = [A1ConeVar05,A1ConeVar15,A1ConeVar10A1mueSS,A1ConeVar10A1emuSS,A1VertexIso]
        
        return {'ExoticaLFVPrmptSSTurbo':mylSS,
                'ExoticaLFVPrmptTurbo':myl}

    
    def __apply_configuration__(self) :
        from HltLine.HltLine import Hlt2Line
        from Configurables import HltANNSvc
        stages = self.stages()
        for (nickname, algos, relInfo) in self.algorithms(stages):
            linename = nickname
            turbo = True if (nickname.find('Turbo') > -1) else False
            persist_reco = True if (nickname.find('Turbo') > -1) else False
            if ("LFV" in nickname) and ("Turbo" in nickname):
                Hlt2Line(linename, prescale = self.prescale, algos = algos, Turbo = turbo, PersistReco = False, RelatedInfo = relInfo, postscale = self.postscale)
            else:
                Hlt2Line(linename, prescale = self.prescale, algos = algos, Turbo = turbo, PersistReco = persist_reco, postscale = self.postscale)


