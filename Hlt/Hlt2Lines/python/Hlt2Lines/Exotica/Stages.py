from Hlt2Lines.Utilities.Hlt2Combiner import Hlt2Combiner
from Hlt2Lines.Utilities.Hlt2Filter import Hlt2ParticleFilter
from HltTracking.HltPVs import PV3D
from Inputs import UnbiasedPhi2KK, Hlt2Muons, Hlt2NoPIDsPions, Hlt2NoPIDsElectrons

displHistos = [("rho", "200 * MeV", "1200 * MeV", "250"),
               ("low", "1.2 * GeV", "3 * GeV", "360"),
               ("psi", "3 * GeV", "4 * GeV", "250")]
promptHistos = (displHistos +
                [("B", "4 * GeV", "9 * GeV", "250"),
                 ("Y", "9 * GeV", "12 * GeV", "300"),
                 ("Z", "12 * GeV", "150 * GeV", "276")])

def monitor(histos):
  preambulo = ['H1D = Gaudi.Histo1DDef']
  fun = ''
  for args in histos:
    preambulo += ["massHisto{0} = H1D('DiMuon_{0}', {1}, {2}, {3})".format(*args)]
    if fun:
      fun += ' >> '
    fun += ("tee(process(in_range({1}, M, {2})) "
            ">> process(monitor(M, massHisto{0}, 'DiMuon_{0}')))".format(*args))
  fun += ' >> ~EMPTY'
  return preambulo, fun

class DisplPhiPhi(Hlt2Combiner):
  def __init__(self, name):

    inputs = [UnbiasedPhi2KK]

    dc = {}
    dc['phi(1020)'] =  ("(PT > %(PhiPT)s) "
                        "& (MINTREE('K+'==ABSID,PT) > %(KPT)s) "
                        "& (MINTREE('K+'==ABSID,BPVIPCHI2()) > %(KIPChi2)s) "
                        "& (MAXTREE('K+'==ABSID,TRGHOSTPROB) < %(GhostProb)s) "
                        "& (MINTREE('K+'==ABSID,PROBNNK) > %(KProbNNk)s) "
                        "& (VFASPF(VCHI2PDOF) < %(VChi2)s) "
                        "& (in_range( PDGM('phi(1020)') - %(PhiMassWindow)s , M , PDGM('phi(1020)') + %(PhiMassWindow)s ) )")

    mc =  ("(HASVERTEX)"
           "& (VFASPF(VCHI2) < %(VChi2)s) "
           "& (BPVVDCHI2 > %(FDChi2)s)")

    Hlt2Combiner.__init__(self, name, "B0 -> phi(1020) phi(1020)", inputs,
                          dependencies = [PV3D('Hlt2')],
                          tistos = 'TisTosSpec',
                          DaughtersCuts  = dc,
                          #CombinationCut = cc,
                          MotherCut      = mc,
                          Preambulo = [])


class SharedDiMuonNoIP(Hlt2Combiner) :
  def __init__(self, name):

    inputs = [Hlt2Muons]

    dc = {}
    dc['mu+'] = ("(PT > %(MuPT)s) "
                 "& (P > %(MuP)s) "
                 "& (TRGHOSTPROB < %(GhostProb)s) "
                 "& (PROBNNmu > %(MuProbNNmu)s) ")
    cc = "(AMAXDOCA('') < %(DOCA)s)"
    mc = "(VFASPF(VCHI2PDOF) < %(VChi2)s) "

    Hlt2Combiner.__init__(self, name, "KS0 -> mu+ mu-", inputs,
                          dependencies = [PV3D('Hlt2')],
                          DaughtersCuts  = dc,
                          CombinationCut = cc,
                          MotherCut      = mc,
                          Preambulo = [],
                          shared = True)




class DiMuonNoIP(Hlt2ParticleFilter):
  def __init__(self, name, inputs):
    code = '(PT > %(PT)s)'
    preambulo, postMonitor = monitor(displHistos)
    Hlt2ParticleFilter.__init__(self, name, code, inputs, dependencies = [PV3D('Hlt2')],
                                Preambulo = preambulo, PostMonitor = postMonitor)

class QuadMuonNoIP(Hlt2Combiner):
  def __init__(self, name, inputs):

    cc = "APT > %(PT)s"
    mc =  ("(HASVERTEX)"
           "& (VFASPF(VCHI2) < %(VChi2)s) ")
    Hlt2Combiner.__init__(self, name, "B0 -> KS0 KS0", inputs,
                          dependencies = [PV3D('Hlt2')],
                          #DaughtersCuts  = dc,
                          CombinationCut = cc,
                          MotherCut      = mc,
                          Preambulo = [])


class DisplDiMuon(Hlt2ParticleFilter):
  def __init__(self, name, inputs):

    code = ("(((MM < %(MSwitch)s) & (MINTREE('mu+'==ABSID,BPVIPCHI2()) > %(MuIPChi2_lowmass)s)) | ((MM > %(MSwitch)s) & (MINTREE('mu+'==ABSID,BPVIPCHI2()) > %(MuIPChi2_highmass)s)))"
            "&(((MM < %(MSwitch)s) & (MINTREE('mu+'==ABSID,PROBNNmu) > %(MuProbNNmu_lowmass)s)) | ((MM > %(MSwitch)s) & (MINTREE('mu+'==ABSID,PROBNNmu) > %(MuProbNNmu_highmass)s)))"
            "& (PT > %(PT)s)"
            "& (HASVERTEX)"
            "& (BPVIPCHI2() < %(IPChi2)s)"
            "& (BPVVDCHI2 > %(FDChi2)s)")
    preambulo, postMonitor = monitor(displHistos)
    Hlt2ParticleFilter.__init__(self, name, code, inputs, dependencies = [PV3D('Hlt2')],
                                Preambulo = preambulo, PostMonitor = postMonitor)


class DisplDiMuonNoPoint(Hlt2ParticleFilter):
  def __init__(self, name, inputs):

    code = ("(((MM < %(MSwitch)s) & (MINTREE('mu+'==ABSID,BPVIPCHI2()) > %(MuIPChi2_lowmass)s)) | ((MM > %(MSwitch)s) & (MINTREE('mu+'==ABSID,BPVIPCHI2()) > %(MuIPChi2_highmass)s)))"
            "&(((MM < %(MSwitch)s) & (MINTREE('mu+'==ABSID,PROBNNmu) > %(MuProbNNmu_lowmass)s)) | ((MM > %(MSwitch)s) & (MINTREE('mu+'==ABSID,PROBNNmu) > %(MuProbNNmu_highmass)s)))"
            "& (PT > %(PT)s)"
            "& (HASVERTEX)"
            "& (BPVVDCHI2 > %(FDChi2)s)")
    preambulo, postMonitor = monitor(displHistos)
    Hlt2ParticleFilter.__init__(self, name, code, inputs, dependencies = [PV3D('Hlt2')],
                                Preambulo = preambulo, PostMonitor = postMonitor)


class PrmptDiMuon(Hlt2ParticleFilter):
  def __init__(self, name, inputs, highmass = False, nregions=6):

    region_names = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']

    code = ''
    if highmass: code = '(M > %(M)s) &'
    code += ("(MINTREE('mu+'==ABSID,PT) > %(MuPT)s) & (MINTREE('mu+'==ABSID,P) > %(MuP)s)"
             "& (MINTREE('mu+'==ABSID,BPVIPCHI2()) < %(MuIPChi2)s)")
    for n in range(nregions):
        if n==0:            code += "& ( ((M < %(M_switch_{1}{2})s) & (MINTREE('mu+'==ABSID,PROBNNmu) > %(MuProbNNmu_{1})s)) ".format(None,region_names[n],region_names[n+1])
        elif n==nregions-1: code += "  | ((M > %(M_switch_{0}{1})s) & (MINTREE('mu+'==ABSID,PROBNNmu) > %(MuProbNNmu_{1})s)) )".format(region_names[n-1], region_names[n], None)
        else:               code += "  | ((M > %(M_switch_{0}{1})s) & (M < %(M_switch_{1}{2})s) & (MINTREE('mu+'==ABSID,PROBNNmu) > %(MuProbNNmu_{1})s))".format(region_names[n-1], region_names[n],region_names[n+1])
    code += ("& (CHILD(PT,1)*CHILD(PT,2) > %(MuPTPROD)s)"
             "& (PT > %(PT)s)"
             "& (HASVERTEX)"
             "& (BPVVDCHI2 < %(FDChi2)s)")
    Hlt2ParticleFilter.__init__(self, name, code, inputs,
        tistos = 'TisTosSpec',
         dependencies = [PV3D('Hlt2')])


class PrmptDiMuon_without_mass_regions(Hlt2ParticleFilter):
  def __init__(self, name, inputs, highmass = False):

    code = ''
    if highmass: code = '(M > %(M)s) &'
    code += ("(MINTREE('mu+'==ABSID,PT) > %(MuPT)s) & (MINTREE('mu+'==ABSID,P) > %(MuP)s)"
             "& (MINTREE('mu+'==ABSID,BPVIPCHI2()) < %(MuIPChi2)s)"
             "& (MINTREE('mu+'==ABSID,PROBNNmu) > %(MuProbNNmu)s)"
             "& (CHILD(PT,1)*CHILD(PT,2) > %(MuPTPROD)s)"
             "& (PT > %(PT)s)"
             "& (HASVERTEX)"
             "& (BPVVDCHI2 < %(FDChi2)s)")
    preambulo, postMonitor = monitor(displHistos)
    Hlt2ParticleFilter.__init__(self, name, code, inputs,
                                tistos = 'TisTosSpec',
                                dependencies = [PV3D('Hlt2')],
                                Preambulo = preambulo,
                                PostMonitor = postMonitor)


class PrmptDiMuonSS(Hlt2Combiner) :
  def __init__(self, name, nregions=6):

    region_names = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']

    inputs = [Hlt2Muons]

    dc = {}
    dc['mu+'] = ("(PT > %(MuPT)s) " 
                 "& (P > %(MuP)s) " 
                 "& (BPVIPCHI2() < %(MuIPChi2)s) " 
                 "& (TRGHOSTPROB < %(GhostProb)s) "
                 "& (PROBNNmu > %(MuProbNNmu_min)s) ") 
    cc = ("(APT > %(PT)s) "
          "& (AMAXDOCA('') < %(DOCA)s) "
          "& (ACHILD(PT,1)*ACHILD(PT,2) > %(MuPTPROD)s)"
          )
    mc = ("(VFASPF(VCHI2PDOF) < %(VChi2)s) "
          "& (PT > %(PT)s)"
          "& (HASVERTEX)"
          "& (BPVVDCHI2 < %(FDChi2)s)")

    for n in range(nregions):
        if n==0:            mc += "& ( ((M < %(M_switch_{1}{2})s) & (MINTREE('mu+'==ABSID,PROBNNmu) > %(MuProbNNmu_{1})s)) ".format(None,region_names[n],region_names[n+1])
        elif n==nregions-1: mc += "  | ((M > %(M_switch_{0}{1})s) & (MINTREE('mu+'==ABSID,PROBNNmu) > %(MuProbNNmu_{1})s)) )".format(region_names[n-1], region_names[n], None)
        else:               mc += "  | ((M > %(M_switch_{0}{1})s) & (M < %(M_switch_{1}{2})s) & (MINTREE('mu+'==ABSID,PROBNNmu) > %(MuProbNNmu_{1})s))".format(region_names[n-1], region_names[n],region_names[n+1])

    preambulo, motherMonitor = monitor(promptHistos)

    Hlt2Combiner.__init__(self, name, "[KS0 -> mu+ mu+]cc", inputs,
                          dependencies = [PV3D('Hlt2')],
                          tistos = 'TisTosSpec',
                          DaughtersCuts  = dc,
                          CombinationCut = cc,
                          MotherCut      = mc,
                          MotherMonitor = motherMonitor,
                          Preambulo = preambulo)

class PrmptDiMuonSS_without_mass_regions(Hlt2Combiner) :
  def __init__(self, name):

    inputs = [Hlt2Muons]

    dc = {}
    dc['mu+'] = ("(PT > %(MuPT)s) "
                 "& (P > %(MuP)s) "
                 "& (BPVIPCHI2() < %(MuIPChi2)s) "
                 "& (TRGHOSTPROB < %(GhostProb)s) "
                 "& (PROBNNmu > %(MuProbNNmu)s) ")
    cc = ("(APT > %(PT)s) "
          "& (AMAXDOCA('') < %(DOCA)s) "
          "& (ACHILD(PT,1)*ACHILD(PT,2) > %(MuPTPROD)s)"
          )
    mc = ("(VFASPF(VCHI2PDOF) < %(VChi2)s) "
          "& (PT > %(PT)s)"
          "& (HASVERTEX)"
          "& (BPVVDCHI2 < %(FDChi2)s)")

    Hlt2Combiner.__init__(self, name, "[KS0 -> mu+ mu+]cc", inputs,
                          dependencies = [PV3D('Hlt2')],
                          tistos = 'TisTosSpec',
                          DaughtersCuts  = dc,
                          CombinationCut = cc,
                          MotherCut      = mc,
                          Preambulo = [])

class DiMuonNoIPSS(Hlt2Combiner) :
  def __init__(self, name):

    inputs = [Hlt2Muons]

    dc = {}
    dc['mu+'] = ("(PT > %(MuPT)s) "
                 "& (P > %(MuP)s) "
                 "& (TRGHOSTPROB < %(GhostProb)s) "
                 "& (PROBNNmu > %(MuProbNNmu)s) ")
    cc = "(APT > %(PT)s) &  (AMAXDOCA('') < %(DOCA)s)"
    mc = ("(VFASPF(VCHI2PDOF) < %(VChi2)s) "
          "& (PT > %(PT)s)"
          "& (HASVERTEX)")

    preambulo, motherMonitor = monitor(promptHistos)

    Hlt2Combiner.__init__(self, name, "[KS0 -> mu+ mu+]cc", inputs,
                          dependencies = [PV3D('Hlt2')],
                          tistos = 'TisTosSpec',
                          DaughtersCuts  = dc,
                          CombinationCut = cc,
                          MotherCut      = mc,
                          Preambulo = preambulo,
                          MotherMonitor = motherMonitor)




class SharedRHNu(Hlt2Combiner) :
  def __init__(self, name, ss = False):

    inputs = [Hlt2Muons,Hlt2NoPIDsPions]

    dc = {}
    dc['mu+'] = ("(PT > %(PT)s) "
                 "& (P > %(P)s) "
                 "& (BPVIPCHI2() > %(IPChi2)s) "
                 "& (TRGHOSTPROB < %(GhostProb)s) "
                 "& (PROBNNmu > %(ProbNNmu)s) ")
    dc['pi+'] = ("(PT > %(PT)s) "
                 "& (P > %(P)s) "
                 "& (BPVIPCHI2() > %(IPChi2)s) "
                 "& (TRGHOSTPROB < %(GhostProb)s)")
    cc = "(APT > 2*%(PT)s) &  (AM > %(M)s) & (ACUTDOCACHI2(%(VChi2)s,''))"
    mc = ("(PT > 2*%(PT)s)"
          "& (HASVERTEX)"
          "& (VFASPF(VCHI2PDOF) < %(VChi2)s) "
          "& (BPVIPCHI2() < %(XIPChi2)s)"
          "& (BPVVDCHI2 > %(FDChi2)s)"
          "& (BPVLTIME() > %(TAU)s)")

    decay = "[KS0 -> mu+ pi-]cc"
    if ss: decay = "[KS0 -> mu+ pi+]cc"

    Hlt2Combiner.__init__(self, name, decay, inputs,
                          dependencies = [PV3D('Hlt2')],
                          DaughtersCuts  = dc,
                          CombinationCut = cc,
                          MotherCut      = mc,
                          Preambulo = [],
                          shared = True)

class RHNu(Hlt2ParticleFilter):
  def __init__(self, name, inputs):

    code = ("(M > %(M)s)"
            "& (HASVERTEX)"
            "& (BPVLTIME() > %(TAU)s)")
    Hlt2ParticleFilter.__init__(self, name, code, inputs, dependencies = [PV3D('Hlt2')])


class DiRHNu(Hlt2Combiner):
  def __init__(self, name, inputs):

    cc = "APT > %(PT)s"
    mc =  ("(HASVERTEX)"
           "& (VFASPF(VCHI2) < %(VChi2)s) ")
    Hlt2Combiner.__init__(self, name, "B0 -> KS0 KS0", inputs,
                          dependencies = [PV3D('Hlt2')],
                          #DaughtersCuts  = dc,
                          CombinationCut = cc,
                          MotherCut      = mc,
                          Preambulo = [])

class RHNuNoIP(Hlt2Combiner) :
  def __init__(self, name, ss = False):

    inputs = [Hlt2Muons,Hlt2NoPIDsPions]

    dc = {}
    dc['mu+'] = ("(PT > %(PT)s) "
                 "& (P > %(P)s) "
                 "& (TRGHOSTPROB < %(GhostProb)s) "
                 "& (PROBNNmu > %(ProbNNmu)s) ")
    dc['pi+'] = ("(PT > %(PT)s) "
                 "& (P > %(P)s) "
                 "& (TRGHOSTPROB < %(GhostProb)s)")
    cc = "(APT > 2*%(PT)s) &  (AM > %(M)s) & (ACUTDOCACHI2(%(VChi2)s,''))"
    mc = ("(PT > 2*%(PT)s)"
          "& (HASVERTEX)"
          "& (VFASPF(VCHI2PDOF) < %(VChi2)s) ")

    decay = "[KS0 -> mu+ pi-]cc"
    if ss: decay = "[KS0 -> mu+ pi+]cc"

    Hlt2Combiner.__init__(self, name, decay, inputs,
                          dependencies = [PV3D('Hlt2')],
                          DaughtersCuts  = dc,
                          CombinationCut = cc,
                          MotherCut      = mc,
                          Preambulo = [])


class SharedLFVPrmpt(Hlt2Combiner) :
  def __init__(self, name, ss = False):

    inputs = [Hlt2Muons,Hlt2NoPIDsElectrons]
    
    dc = {}
    dc['mu+'] = ("(PT > %(PT)s) " 
                 "& (P > %(P)s) " 
                 "& (BPVIPCHI2() < %(IPChi2)s) " 
                 "& (TRGHOSTPROB < %(GhostProb)s) " 
                 "& (PROBNNmu > %(ProbNNmu)s) ") 
    dc['e+'] = ("(PT > %(PT)s) " 
                "& (P > %(P)s) " 
                "& (BPVIPCHI2() < %(IPChi2)s) " 
                "& (TRGHOSTPROB < %(GhostProb)s)"
                "& (PROBNNe > %(ProbNNe)s) ") 

    cc = "(APT > 2*%(PT)s) &  (AM > %(M)s) & (ACUTDOCACHI2(%(VChi2)s,''))" 
    mc = ("(PT > 2*%(PT)s)" 
          "& (HASVERTEX)" 
          "& (VFASPF(VCHI2PDOF) < %(VChi2)s) " 
          "& (BPVIPCHI2() < %(XIPChi2)s)" 
          "& (BPVVDCHI2 < %(FDChi2)s)")

    decay = "[KS0 -> mu+ e-]cc"
    if ss: decay = "[KS0 -> mu+ e+]cc"

    Hlt2Combiner.__init__(self, name, decay, inputs,
                          dependencies = [PV3D('Hlt2')],
                          DaughtersCuts  = dc,
                          CombinationCut = cc,
                          MotherCut      = mc,
                          Preambulo = [],
                          shared = True)


class LFVPrmpt(Hlt2ParticleFilter):
  def __init__(self, name, inputs):

    code = ("(M > %(M)s)"
            "& (HASVERTEX)")
    Hlt2ParticleFilter.__init__(self, name, code, inputs, dependencies = [PV3D('Hlt2')])


class LFVPrmptSS(Hlt2Combiner) :
  def __init__(self, name):

    inputs = [Hlt2Muons,Hlt2NoPIDsElectrons]
    
    dc = {}
    dc['mu+'] = ("(PT > %(PT)s) " 
                 "& (P > %(P)s) " 
                 "& (BPVIPCHI2() < %(IPChi2)s) " 
                 "& (TRGHOSTPROB < %(GhostProb)s) " 
                 "& (PROBNNmu > %(ProbNNmu)s) ") 
    dc['e+'] = ("(PT > %(PT)s) " 
                "& (P > %(P)s) " 
                "& (BPVIPCHI2() < %(IPChi2)s) " 
                "& (TRGHOSTPROB < %(GhostProb)s)"
                "& (PROBNNe > %(ProbNNe)s) ") 

    cc = "(APT > 2*%(PT)s) &  (AM > %(M)s) & (ACUTDOCACHI2(%(VChi2)s,''))" 
    mc = ("(PT > 2*%(PT)s)" 
          "& (HASVERTEX)" 
          "& (VFASPF(VCHI2PDOF) < %(VChi2)s) " 
          "& (BPVIPCHI2() < %(XIPChi2)s)" 
          "& (BPVVDCHI2 < %(FDChi2)s)")

    decay = "[KS0 -> mu+ e+]cc"

    Hlt2Combiner.__init__(self, name, decay, inputs,
                          dependencies = [PV3D('Hlt2')],
                          DaughtersCuts  = dc,
                          CombinationCut = cc,
                          MotherCut      = mc,
                          Preambulo = [],
                          shared = True)


class SharedDiENoIP(Hlt2Combiner) :
  def __init__(self, name):

    inputs = [Hlt2NoPIDsElectrons]

    dc = {}
    dc['e+'] = ("(PT > %(EPT)s) "
                 "& (P > %(EP)s) "
                 "& (TRGHOSTPROB < %(GhostProb)s) "
                 "& (PROBNNe > %(EProbNNe)s) ")
    cc = ("(AMAXDOCA('') < %(DOCA)s)"
          "& (AM > %(MM)s)")
    mc = "(VFASPF(VCHI2PDOF) < %(VChi2)s) "

    Hlt2Combiner.__init__(self, name, "KS0 -> e+ e-", inputs,
                          dependencies = [PV3D('Hlt2')],
                          DaughtersCuts  = dc,
                          CombinationCut = cc,
                          MotherCut      = mc,
                          Preambulo = [],
                          shared = True)


class DisplDiE(Hlt2ParticleFilter):
  def __init__(self, name, inputs):

    code = ("(MINTREE('e+'==ABSID,BPVIPCHI2()) > %(EIPChi2)s)"
            "& (MINTREE('e+'==ABSID,PROBNNe) > %(EProbNNe)s)"
            "& (PT > %(PT)s)"
            "& (HASVERTEX)"
            "& (BPVIPCHI2() < %(IPChi2)s)"
            "& (BPVVDCHI2 > %(FDChi2)s)")
    Hlt2ParticleFilter.__init__(self, name, code, inputs,
        tistos = 'TisTosSpec',
        dependencies = [PV3D('Hlt2')])

class SharedDisplacedDiE(Hlt2Combiner) :
  def __init__(self, name):

    inputs = [Hlt2NoPIDsElectrons]

    dc = {}
    dc['e+'] = ("(PT > %(EPT)s) "
                "& (P > %(EP)s) "
                "& (BPVIPCHI2() > %(EIPChi2)s) "
                "& (PROBNNe > %(EProbNNe)s) ")
    cc = "(ACUTDOCACHI2(%(VChi2)s,''))"
    mc = ("(M > %(M)s)"
          "& (PT > %(PT)s)"
          "& (HASVERTEX)"
          "& (VFASPF(VCHI2PDOF) < %(VChi2)s) "
          "& (BPVVDCHI2 > %(FDChi2)s)")

    Hlt2Combiner.__init__(self, name, "KS0 -> e+ e-", inputs,
                          dependencies = [PV3D('Hlt2')],
                          DaughtersCuts  = dc,
                          CombinationCut = cc,
                          MotherCut      = mc,
                          Preambulo = [],
                          shared = True)

class SharedDisplacedEta2PiPiXee(Hlt2Combiner) :
  def __init__(self, name, x2ee):

    inputs = [Hlt2NoPIDsPions] + x2ee

    dc = {}
    dc['pi+'] = ("(PT > %(PiPT)s) "
                 "& (P > %(PiP)s) "
                 "& (BPVIPCHI2() > %(PiIPChi2)s) "
                 "& (PIDK < %(PiPIDK)s) "
                 "& (TRGHOSTPROB < %(GhostProb)s)")
    cc = ("(ACUTDOCACHI2(%(VChi2)s,''))"
          "& ((ADAMASS('eta') < (%(DM)s+20*MeV)) | (ADAMASS('eta_prime') < (%(DM)s+20*MeV)))")
    mc = ("((ADMASS('eta') < %(DM)s) | (ADMASS('eta_prime') < %(DM)s))"
          "& (PT > %(PT)s)"
          "& (HASVERTEX)"
          "& (VFASPF(VCHI2PDOF) < %(VChi2)s) "
          "& (BPVVDCHI2 > %(FDChi2)s)")

    Hlt2Combiner.__init__(self, name, "eta -> pi+ pi- KS0", inputs,
                          dependencies = [PV3D('Hlt2')],
                          DaughtersCuts  = dc,
                          CombinationCut = cc,
                          MotherCut      = mc,
                          Preambulo = [],
                          shared = True)

class D2PiEta(Hlt2Combiner) :
  def __init__(self, name, eta):

    inputs = [Hlt2NoPIDsPions] + eta

    dc = {}
    dc['pi+'] = ("(PT > %(PiPT)s) "
                 "& (P > %(PiP)s) "
                 "& (BPVIPCHI2() > %(PiIPChi2)s) "
                 "& (PIDK < %(PiPIDK)s)"
                 "& (TRGHOSTPROB < %(GhostProb)s)")
    cc = "(APT > %(PT)s) &  (AM > %(MMIN)s) & (AM < %(MMAX)s) & (ACUTDOCACHI2(%(VChi2)s,''))"
    mc = ("(PT > %(PT)s)"
          "& (HASVERTEX)"
          "& (VFASPF(VCHI2PDOF) < %(VChi2)s) "
          "& (BPVVDCHI2 > %(FDChi2)s)")

    Hlt2Combiner.__init__(self, name, '[D+ -> pi+ eta]cc', inputs,
                          dependencies = [PV3D('Hlt2')],
                          DaughtersCuts  = dc,
                          CombinationCut = cc,
                          MotherCut      = mc,
                          Preambulo = [])
