from GaudiKernel.SystemOfUnits import MeV, GeV
from Hlt2Lines.Utilities.Hlt2LinesConfigurableUser import Hlt2LinesConfigurableUser

# Jet Lines written by Phil Ilten and Mike Williams (mwill@mit.edu),
# with Roel Aaij's significant assistance.

class JetsLines(Hlt2LinesConfigurableUser):
    __slots__ = {
        '_stages': {},
        'Prescale': {
            'Hlt2JetsDiJetLowPt'    : 1e-04,
            'Hlt2JetsDiJetMVLowPt'  : 1e-02,
            'Hlt2JetsDiJetMuMuLowPt': 1e-01,
            'Hlt2JetsDiJetSVLowPt'  : 1e-03,
            'Hlt2JetsDiJetSVMuLowPt': 1e-02,
            'Hlt2JetsDiJetSVSVLowPt': 1e-01,
            'Hlt2JetsJetLowPt'      : 1e-04,
            'Hlt2JetsJetDHLowPt'    : 1e-02,
            'Hlt2JetsJetMuLowPt'    : 1e-03,
            'Hlt2JetsJetSVLowPt'    : 1e-03,
            'Hlt2JetsDiJet'         : 1e-03,
            'Hlt2JetsDiJetSV'       : 1e-02,
            'Hlt2JetsJet'           : 1e-03,
            'Hlt2JetsJetDH'         : 1e-02,
            'Hlt2JetsJetMu'         : 1e-03,
            'Hlt2JetsJetSV'         : 1e-03
            },
        'Postscale': {},
        'Common': {
            'D_TOS'        : 'Hlt1((Two)?Track(Muon)?MVA.*|TrackMuon)Decision%TOS',
            'D_MASS'       : 50*MeV,
            'GHOSTPROB'    : 0.2,
            'DPHI'         : 0,
            'SV_VCHI2'     : 10,
            'SV_TRK_PT'    : 500*MeV,
            'SV_TRK_IPCHI2': 16,
            'SV_FDCHI2'    : 25,
            'MU_PT'        : 1000*MeV,
            'MU_PROBNNMU'  : 0.5,
            'JET_PT'       : 17*GeV,
            },
        'JetBuilder': {
            'Reco'      : 'TURBO',
            'JetPtMin'  : 5*GeV,
            'JetInfo'   : False,
            'JetEcPath' : ''
            },
        'JetsJet'           : {},
        'JetsDiJetLowPt'    : {'JET_PT': 10*GeV},
        'JetsDiJetMVLowPt'  : {'JET_PT': 10*GeV},
        'JetsDiJetMuMuLowPt': {'JET_PT': 10*GeV},
        'JetsDiJetSVLowPt'  : {'JET_PT': 10*GeV},
        'JetsDiJetSVMuLowPt': {'JET_PT': 10*GeV},
        'JetsDiJetSVSVLowPt': {'JET_PT': 10*GeV},
        'JetsJetLowPt'      : {'JET_PT': 10*GeV},
        'JetsJetDHLowPt'    : {'JET_PT': 10*GeV},
        'JetsJetMuLowPt'    : {'JET_PT': 10*GeV},
        'JetsJetSVLowPt'    : {'JET_PT': 10*GeV},
        'JetsDiJetHighPt'   : {'JET_PT': 60*GeV},
        'JetsDiJetSVHighPt' : {'JET_PT': 50*GeV},
        'JetsJetHighPt'     : {'JET_PT': 100*GeV},
        'JetsJetDHHighPt'   : {'JET_PT': 40*GeV},
        'JetsJetMuHighPt'   : {'JET_PT': 60*GeV},
        'JetsJetSVHighPt'   : {'JET_PT': 70*GeV}
        }

    def stages(self, nickname = ''):
        if hasattr(self, '_stages') and self._stages:
            if nickname: return self._stages[nickname]
            else: return self._stages
        else: self._stages = {}

        # Import the topo lines for the 2-body SVs.
        from Hlt2Lines.Topo.Lines import TopoLines
        from Hlt2Lines.Utilities.Hlt2Stage import Hlt2ExternalStage
        from Hlt2Lines.Utilities.Hlt2JetBuilder import Hlt2JetBuilder
        from Stages import (FilterSV, FilterMuon, SVSVCombiner, SVMuCombiner,
                            MuMuCombiner, DHCombiner)
        topo_lines = TopoLines()
        topoCombiner = topo_lines.intermediateStages('Topo2BodyCombos')[0]
        topo_sv = Hlt2ExternalStage(topo_lines, topoCombiner)

        # Create the stage filters.
        sv = [FilterSV([topo_sv])]
        mu = [FilterMuon()]
        #dh = [DHCombiner()]
        svsv = [SVSVCombiner(sv)]
        svmu = [SVMuCombiner(sv + mu)]
        mumu = [MuMuCombiner(mu)]

        # The input tags, 9600 + index (9600 SV, 9601 mu, 9602 charm).
        jets = [Hlt2JetBuilder('DiJetsJetBuilder',
			       #sv + mu + dh,
			       sv + mu,
                               shared = True, reco = self.getProps()
                               ['JetBuilder']['Reco'],
                               nickname = 'JetBuilder')]

        # Create the di-jet stages.
        self.diJetStage('JetsDiJetSVSV', svsv + jets, 'SVSV')
        self.diJetStage('JetsDiJetSVMu', svmu + jets, 'SVMu')
        self.diJetStage('JetsDiJetMuMu', mumu + jets, 'MuMu')
        self.diJetStage('JetsDiJetSV', sv + jets, 'SV')
        self.diJetStage('JetsDiJetMV', sv + mu + jets, 'MV')
        self.diJetStage('JetsDiJet', jets, '')

        # Create the single jet stages.
        self.jetStage('JetsJetSV', sv + jets, 'SV')
        self.jetStage('JetsJetMu', mu + jets, 'Mu')
        #self.jetStage('JetsJetDH', dh + jets, 'DH')
        self.jetStage('JetsJet', jets, '')

        # Return the stages.
        if nickname: return self._stages[nickname]
        else: return {k : v for k, v in self._stages.iteritems()}

    def diJetStage(self, name, inputs, tag):
        from Stages import DiJetCombiner
        props = self.getProps()
        self._stages[name] = [DiJetCombiner(inputs, tag, '')]
        if name + 'LowPt' in props: self._stages[name + 'LowPt'] = [
            DiJetCombiner(inputs, tag, 'LowPt')]
        if name + 'HighPt' in props: self._stages[name + 'HighPt'] = [
            DiJetCombiner(inputs, tag, 'HighPt')]

    def jetStage(self, name, inputs, tag):
        from Stages import FilterJet
        props = self.getProps()
        self._stages[name] = [FilterJet(inputs, tag, '')]
        if name + 'LowPt' in props: self._stages[name + 'LowPt'] = [
            FilterJet(inputs, tag, 'LowPt')]
        if name + 'HighPt' in props: self._stages[name + 'HighPt'] = [
            FilterJet(inputs, tag, 'HighPt')]

    def __apply_configuration__(self) :
        from HltLine.HltLine import Hlt2Line
        from Configurables import HltANNSvc
        stages = self.stages()
        all_cuts = self.getProps()
        for (nickname, algos) in self.algorithms(stages):
            cuts = all_cuts.get(nickname, {})
            linename = nickname
            Hlt2Line(linename, prescale = self.prescale, algos = algos,
                     HLT1 = cuts.get('Hlt1Req', None),
                     postscale = self.postscale)
