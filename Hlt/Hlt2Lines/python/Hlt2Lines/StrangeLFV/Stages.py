# ===============================================================================
# @file:   Set of "stages" for lines devoted to the study of MuonElectron decays
# @author: Miguel Ramos Pernas miguel.ramos.pernas@cern.ch
# @date:   2016-29-04
# ===============================================================================

__author__ = "Miguel Ramos Pernas miguel.ramos.pernas@cern.ch"
__date__   = "2016-29-04"
__all__    = ( 'SoftMuonElectronCombiner' )

from Hlt2Lines.Utilities.Hlt2Combiner import Hlt2Combiner
from HltTracking.HltPVs import PV3D

class SoftMuonElectronCombiner(Hlt2Combiner):
    def __init__(self, name, decay, inputs):

        dc = { 'mu+' : ( "(MIPDV(PRIMARY) > %(MinMuIp)s)" +
                         " & (MIPCHI2DV(PRIMARY) > %(MinMuIpChi2)s)" +
                         " & (PROBNNmu > %(MinProbNNmu)s)" +
                         " & (TRGHOSTPROB < %(MaxMuGP)s)" +
                         " & (PT > %(MinPT)s)" ),
               'e-'  : ( "(MIPDV(PRIMARY) > %(MinElIp)s)" +
                         " & (MIPCHI2DV(PRIMARY) > %(MinElIpChi2)s)" +
                         " & (PROBNNe > %(MinProbNNe)s)" +
                         " & (TRGHOSTPROB < %(MaxElGP)s)" + 
                         " & (PT > %(MinPT)s)") }

        cc = ( "(ACHILD(TRGHOSTPROB,1) + ACHILD(TRGHOSTPROB,2) < %(SumGP)s)" )
        
        mc = ( "(PCUTA(ALV(1,2) < %(CosAngle)s))" +
               " & (BPVVDZ > %(VDZ)s)" +
               " & ((MIPDV(PRIMARY)/BPVVDZ) < %(IpDzRatio)s)" +
               " & (VFASPF(VX*VX + VY*VY) > %(Rho2)s)" +
               " & (VFASPF(VCHI2PDOF) < %(VertexChi2)s)" +
               " & (DOCAMAX < %(DOCA)s)" +
               " & (VFASPF(VZ) < %(SVZ)s)" +
               " & (MM < %(Mass)s)" +
               " & (BPVDIRA > %(Dira)s)" )

        Hlt2Combiner.__init__(self,
                              name,
                              decay,
                              inputs,
                              DaughtersCuts = dc,
                              CombinationCut = cc,
                              MotherCut = mc,
                              shared = True,
                              dependencies = [PV3D('Hlt2')],
                              UseP2PVRelations = False)

from Inputs import BiKalmanFittedMuons, BiKalmanFittedElectrons
MuonElectronFromKS0 = SoftMuonElectronCombiner("MuonElectronSoft", "[KS0 -> mu+ e-]cc", [BiKalmanFittedMuons, BiKalmanFittedElectrons])
