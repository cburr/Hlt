__author__  = 'Mika Vesterinen'
__date__    = '$Date: 2015-02-23$'
__version__ = '$Revision: 0.0 $'

def D0Preambulo():
    return  [
        'D0_M  = POINTINGMASS ( LoKi.Child.Selector(0), LoKi.Child.Selector(1), LoKi.Child.Selector(2) )']

def DstPreambulo():
    return  [
        'D0_M  = POINTINGMASS ( LoKi.Child.Selector(1), LoKi.Child.Selector(1,1), LoKi.Child.Selector(1,2) )',
        'Dst_M = POINTINGMASS ( LoKi.Child.Selector(1), LoKi.Child.Selector(1,1), LoKi.Child.Selector(1,2), LoKi.Child.Selector(2) )']
