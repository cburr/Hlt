##
#  @author V. V. Gligorov vladimir.gligorov@cern.ch
#
#  Please contact the responsible before editing this file
#
##

from GaudiKernel.SystemOfUnits import GeV, MeV, mm

from Hlt2Lines.Utilities.Hlt2LinesConfigurableUser import Hlt2LinesConfigurableUser
class TrackEffElectronLines(Hlt2LinesConfigurableUser) :
    __slots__ = {'Prescale'             : {},
                 'TrackGEC'             : {'NTRACK_MAX'           : 100},
                 'SharedChild'          : {'TrChi2Mu'   :   5, 
                                           'TrChi2Ele'  :   5,
                                           'TrChi2Kaon' :   5,
                                           'TrChi2Pion' :   5,
                                           'IPMu'       :   0.0 * mm,
                                           'IPEle'      :   0.0 * mm,
                                           'IPKaon'     :   0.0 * mm,
                                           'IPPion'     :   0.0 * mm,
                                           'IPChi2Mu'   :   16,
                                           'IPChi2Ele'  :   16,
                                           'IPChi2Kaon' :   16,
                                           'IPChi2Pion' :   16,
                                           'EtaMinMu'   :   1.8,
                                           'EtaMinEle'  :   1.8,
                                           'EtaMaxMu'   :   3.5,
                                           'EtaMaxEle'  :   3.5,
                                           'ProbNNe'    :   0.5,
                                           'ProbNNmu'   :   0.5,    
                                           'ProbNNk'    :   0.5,
                                           'ProbNNpi'   :   0.8,
                                           'PtMu'       :   2400 * MeV,
                                           'PtEle'      :   2400 * MeV,
                                           'PtKaon'     :   500 * MeV,
                                           'PtPion'     :   800 * MeV },
                 'DetachedEK'           : {'AM'         :   6000*MeV,
                                           'VCHI2'      :   10,
                                           'VDCHI2'     :   100,
                                           'DIRA'       :   0.995,
                                           'TisTosSpec' :   "Hlt1TrackMVA.*Decision%TOS"
                                          },
                 'DetachedEPi'          : {'AM'         :   2250*MeV,
                                           'VCHI2'      :   10, 
                                           'VDCHI2'     :   100,
                                           'DIRA'       :   0.995,
                                           'TisTosSpec' :   "Hlt1TrackMVA.*Decision%TOS"
                                          },
                 'DetachedMuK'          : {'AM'         :   5500*MeV,
                                           'VCHI2'      :   10, 
                                           'VDCHI2'     :   100,
                                           'DIRA'       :   0.995,
                                           'TisTosSpec' :   "Hlt1Track(Muon)?MVA.*Decision%TOS"
                                          },
                 'DetachedMuPi'         : {'AM'         :   2100*MeV,
                                           'VCHI2'      :   10, 
                                           'VDCHI2'     :   100,
                                           'DIRA'       :   0.995,
                                           'TisTosSpec' :   "Hlt1Track(Muon)?MVA.*Decision%TOS"
                                          },
                 'L0Req'                : {'DetachedEK'   : "L0_CHANNEL('Electron')",
                                           'DetachedEPi'  : "L0_CHANNEL('Electron')",
                                           'DetachedMuK'  : "L0_CHANNEL('Muon')",
                                           'DetachedMuPi' : "L0_CHANNEL('Muon')" },
                 'Hlt1Req'              : {'DetachedEK'   : "HLT_PASS_RE('Hlt1TrackMVA.*Decision')",
                                           'DetachedEPi'  : "HLT_PASS_RE('Hlt1TrackMVA.*Decision')",
                                           'DetachedMuK'  : "HLT_PASS_RE('Hlt1Track(Muon)?MVA.*Decision')",
                                           'DetachedMuPi' : "HLT_PASS_RE('Hlt1Track(Muon)?MVA.*Decision')" }

                 }

    def stages(self, nickname = ""):
        if hasattr(self, '_stages') and self._stages:
            if nickname:
                return self._stages[nickname]
            else:
                return self._stages

        from Stages import (DetachedEKPair, DetachedEPiPair, DetachedMuKPair, DetachedMuPiPair)

        self._stages = {
                        'DetachedEK'       : [DetachedEKPair('DetachedEK')],
                        'DetachedEPi'      : [DetachedEPiPair('DetachedEPi')],
                        'DetachedMuK'      : [DetachedMuKPair('DetachedMuK')],
                        'DetachedMuPi'     : [DetachedMuPiPair('DetachedMuPi')]
                        }
        if nickname:
            return self._stages[nickname]
        else:
            return self._stages

    def __apply_configuration__(self) :
        from HltLine.HltLine import Hlt2Line
        from Configurables import HltANNSvc
        stages = self.stages()
        for (nickname, algos) in self.algorithms(stages):
            Hlt2Line('TrackEffElectron'+nickname+'TurboCalib', prescale = self.prescale,
                     L0DU = self.L0Req[nickname],
                     HLT1 = self.Hlt1Req[nickname],
                     algos = algos, postscale = self.postscale,
                     Turbo=True)
