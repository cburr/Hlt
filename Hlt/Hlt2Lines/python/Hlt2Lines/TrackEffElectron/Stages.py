##
#  @author V. V. Gligorov vladimir.gligorov@cern.ch
# 
#  Please contact the responsible before editing this file
#
##

# Each stage must specify its own inputs
from Hlt2Lines.Utilities.Hlt2Filter import Hlt2ParticleFilter
from Hlt2Lines.Utilities.Hlt2Combiner import Hlt2Combiner
from Hlt2Lines.Utilities.Hlt2Filter import Hlt2VoidFilter

from HltTracking.HltPVs import PV3D
from Inputs import Hlt2Muons,Hlt2Electrons,Hlt2Kaons,Hlt2Pions

class TrackGEC(Hlt2VoidFilter):
    def __init__(self, name):
        from Configurables import LoKi__Hybrid__CoreFactory as Factory
        modules =  Factory().Modules
        for i in [ 'LoKiTrigger.decorators' ] : 
            if i not in modules : modules.append(i)
        from HltTracking.Hlt2TrackingConfigurations import Hlt2BiKalmanFittedForwardTracking as Hlt2LongTracking
        tracks = Hlt2LongTracking().hlt2PrepareTracks()
        code = ("CONTAINS('%s')" % tracks.outputSelection()) + " < %(NTRACK_MAX)s"
        Hlt2VoidFilter.__init__(self, "TrackEffElectron" + name, code, [tracks], shared = True, nickname = 'TrackGEC')

class DetachedMuonFilter(Hlt2ParticleFilter):
    def __init__(self, name, nickname):
        code = ("(MIPDV(PRIMARY)>%(IPMu)s) & (MIPCHI2DV(PRIMARY)>%(IPChi2Mu)s) & (PT> %(PtMu)s) & \
                 (TRCHI2DOF<%(TrChi2Mu)s)  & in_range(%(EtaMinMu)s, ETA, %(EtaMaxMu)s) & (PROBNNmu > %(ProbNNmu)s)")
        inputs = [ Hlt2Muons ]
        Hlt2ParticleFilter.__init__(self, name, code, inputs, 
                                    nickname = nickname,
                                    dependencies = [PV3D('Hlt2')],
                                    shared = True)

class DetachedElectronFilter(Hlt2ParticleFilter):
    def __init__(self, name, nickname):
        code = ("(MIPDV(PRIMARY)>%(IPEle)s) & (MIPCHI2DV(PRIMARY)>%(IPChi2Ele)s) & (PT> %(PtEle)s) & \
                 (TRCHI2DOF<%(TrChi2Ele)s)  & in_range(%(EtaMinEle)s, ETA, %(EtaMaxEle)s) & (PROBNNe > %(ProbNNe)s)") 
        inputs = [ Hlt2Electrons ]
        Hlt2ParticleFilter.__init__(self, name, code, inputs, 
                                    nickname = nickname,
                                    dependencies = [PV3D('Hlt2')],
                                    shared = True)

class DetachedKaonFilter(Hlt2ParticleFilter):
    def __init__(self, name, nickname):
        code = ("(MIPDV(PRIMARY)>%(IPKaon)s) & (MIPCHI2DV(PRIMARY)>%(IPChi2Kaon)s) & (PT> %(PtKaon)s) & \
                 (TRCHI2DOF<%(TrChi2Kaon)s) & (PROBNNk > %(ProbNNk)s)")
        inputs = [ Hlt2Kaons ]
        Hlt2ParticleFilter.__init__(self, name, code, inputs,
                                    nickname = nickname,
                                    dependencies = [PV3D('Hlt2')],
                                    shared = True)

class DetachedPionFilter(Hlt2ParticleFilter):
    def __init__(self, name, nickname):
        code = ("(MIPDV(PRIMARY)>%(IPPion)s) & (MIPCHI2DV(PRIMARY)>%(IPChi2Pion)s) & (PT> %(PtPion)s) & \
                 (TRCHI2DOF<%(TrChi2Pion)s) & (PROBNNpi > %(ProbNNpi)s)")
        inputs = [ Hlt2Pions ]
        Hlt2ParticleFilter.__init__(self, name, code, inputs,
                                    nickname = nickname,
                                    dependencies = [PV3D('Hlt2')],
                                    shared = True)

class DetachedEKPair(Hlt2Combiner) :
    def __init__(self,name):
        dc = {'K+' : "ALL", 'e+' : "ALL"}
        cc = ("(AM < %(AM)s)")
        mc = ("(VFASPF(VCHI2) < %(VCHI2)s) & (BPVDIRA > %(DIRA)s) & (BPVVDCHI2 > %(VDCHI2)s)")
        inputs = [DetachedElectronFilter('ForEleTPDetElectron','SharedChild'),DetachedKaonFilter('ForEleTPDetKaon','SharedChild')]
        from HltTracking.HltPVs import PV3D
        Hlt2Combiner.__init__(self, name, 
                                    ["[J/psi(1S) -> e+ K-]cc","[J/psi(1S) -> e+ K+]cc"], 
                                    inputs,
                                    dependencies = [TrackGEC('TrackGEC'),PV3D('Hlt2')],
                                    tistos = 'TisTosSpec',
                                    DaughtersCuts = dc, CombinationCut = cc,
                                    MotherCut = mc)

class DetachedEPiPair(Hlt2Combiner) :
    def __init__(self,name):
        dc = {'pi+' : "ALL", 'e+' : "ALL"}
        cc = ("(AM < %(AM)s)")
        mc = ("(VFASPF(VCHI2) < %(VCHI2)s) & (BPVDIRA > %(DIRA)s) & (BPVVDCHI2 > %(VDCHI2)s)")
        inputs = [DetachedElectronFilter('ForEleTPDetElectron','SharedChild'),DetachedPionFilter('ForEleTPDetPion','SharedChild')]
        from HltTracking.HltPVs import PV3D
        Hlt2Combiner.__init__(self, name, 
                                    ["[J/psi(1S) -> e+ pi-]cc", "[J/psi(1S) -> e+ pi+]cc"],
                                    inputs,
                                    dependencies = [TrackGEC('TrackGEC'),PV3D('Hlt2')],
                                    tistos = 'TisTosSpec',
                                    DaughtersCuts = dc, CombinationCut = cc, 
                                    MotherCut = mc)

class DetachedMuKPair(Hlt2Combiner) :
    def __init__(self,name):
        dc = {'K+' : "ALL", 'mu+' : "ALL"}
        cc = ("(AM < %(AM)s)")
        mc = ("(VFASPF(VCHI2) < %(VCHI2)s) & (BPVDIRA > %(DIRA)s) & (BPVVDCHI2 > %(VDCHI2)s)")
        inputs = [DetachedMuonFilter('ForEleTPDetMuon','SharedChild'),DetachedKaonFilter('ForEleTPDetKaon','SharedChild')]
        from HltTracking.HltPVs import PV3D
        Hlt2Combiner.__init__(self, name, 
                                    ["[J/psi(1S) -> mu+ K-]cc","[J/psi(1S) -> mu+ K+]cc"], 
                                    inputs,
                                    dependencies = [TrackGEC('TrackGEC'),PV3D('Hlt2')],
                                    tistos = 'TisTosSpec',
                                    DaughtersCuts = dc, CombinationCut = cc, 
                                    MotherCut = mc) 

class DetachedMuPiPair(Hlt2Combiner) :
    def __init__(self,name):
        dc = {'pi+' : "ALL", 'mu+' : "ALL"}
        cc = ("(AM < %(AM)s)")
        mc = ("(VFASPF(VCHI2) < %(VCHI2)s) & (BPVDIRA > %(DIRA)s) & (BPVVDCHI2 > %(VDCHI2)s)")
        inputs = [DetachedMuonFilter('ForEleTPDetMuon','SharedChild'),DetachedPionFilter('ForEleTPDetPion','SharedChild')]
        from HltTracking.HltPVs import PV3D
        Hlt2Combiner.__init__(self, name, 
                                    ["[J/psi(1S) -> mu+ pi-]cc", "[J/psi(1S) -> mu+ pi+]cc"],
                                    inputs,
                                    dependencies = [TrackGEC('TrackGEC'),PV3D('Hlt2')],
                                    tistos = 'TisTosSpec',
                                    DaughtersCuts = dc, CombinationCut = cc, 
                                    MotherCut = mc)  
