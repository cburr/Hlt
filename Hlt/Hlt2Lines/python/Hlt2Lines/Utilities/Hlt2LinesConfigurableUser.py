from Utilities import allCuts
from HltLine.HltLine import Hlt2Member
from HltLine.HltLinesConfigurableUser import HltLinesConfigurableUser
from Gaudi.Configuration import log
from HltTracking.Hlt2TrackingConfigurations import (
    Hlt2BiKalmanFittedForwardTracking, Hlt2BiKalmanFittedDownstreamTracking)
from HltTracking.Hlt2ProbeTrackingConfigurations import (
    Hlt2MuonTTTracking, Hlt2VeloMuonTracking, Hlt2FullDownstreamTracking)
from Configurables import HltJetConf, FilterDesktop
from Hlt2Monitoring import getMonMembers, getMothers, preambulo, streamer


class Hlt2LinesConfigurableUser(HltLinesConfigurableUser):
    # python configurables to be applied before me
    __queried_configurables__ = [
        Hlt2BiKalmanFittedForwardTracking,
        Hlt2BiKalmanFittedDownstreamTracking,
        Hlt2MuonTTTracking,
        Hlt2VeloMuonTracking,
        Hlt2FullDownstreamTracking,
        # ugly trick to ensure instantiation and application:
        lambda *args, **kwargs: HltJetConf(),
        # HltJetConf,
    ]

    __slots__ = {
        '_stages': {},
        '_intermediateStages': {},
        '_relatedInfo': {},
        'Prescale': {},
        'Postscale': {},
    }

    def __stages(self, source):
        # External stage sets the configurable to something that is not self.
        if source._configurable():
            conf = source._configurable()
        else:
            conf = self

        cuts = allCuts(conf)
        deps = []
        stage = source.stage(deps, cuts)
        if stage in deps:
            print 'WARNING: Circular dependency %s %s %s.' % (source._name(),
                                                              source._deps(),
                                                              source._inputs())
        stages = filter(lambda i: bool(i), deps + [stage])
        return stages

    def __relatedInfoSequence(self, stages, algorithms):
        if not stages:
            return ([], stages)

        members = []
        locations = []
        for stage in stages:
            # External stage sets the configurable to something that is not
            # self.
            if stage._configurable():
                conf = stage._configurable()
            else:
                conf = self
            cuts = allCuts(conf)
            infoStage, locs = stage.infoStage(cuts, algorithms[-1])
            members.append(infoStage)
            locations.extend(locs)

        # Consistency check
        if len(locations) != len(set(locations)):
            raise Exception(
                'At least one location in %s appears twice, this will not work!' % locations)

        from HltLine.HltLine import Hlt2SubSequence
        return (locations, [Hlt2SubSequence("RelatedInfoSequence", members,
                                            ModeOR=True, ShortCircuit=False)])

    def algorithms(self, stages):
        from HltLine.HltLine import bindMembers

        def getProp(prop, alg):
            if isinstance(alg, Hlt2Member):
                return alg.Args.get(prop, None)
            else:
                return getattr(alg, prop, None)

        # Flatten the bindMembers to end up with the sequence of
        # algorithms we want. This doesn't flatten everything, for
        # example optional inputs.
        def flatten(l):
            for i in l:
                if isinstance(i, bindMembers):
                    for j in flatten(i.members()):
                        yield j
                else:
                    yield i

        from Hlt2Lines.Utilities.Utilities import uniqueEverseen
        from Hlt2Stage import Hlt2Stage

        sd = {}
        # Loop over the dictionary of {linename : [stages]} that we've
        # been given.
        for name, stg in stages.iteritems():
            sl = []
            for i in stg:
                # If the stage is an Hlt2Stage use it's machinery,
                # otherwise add it directly.
                if isinstance(i, Hlt2Stage):
                    sl.extend(self.__stages(i))
                else:
                    sl.append(i)
            # Flatten the resulting sequence and remove any duplicates.
            allMembers = list(uniqueEverseen(flatten(sl)))
            sd[name] = allMembers

            # We have a list of Hlt2Member here, add some default mass plots.
            # First find the last combiner and successive filters
            members = getMonMembers(allMembers)
            if not members:
                log.debug('Could not find monitoring candidate '
                          'algorithms for %s', name)
            mothers = set()
            if members:
                mothers = getMothers(members[0])

            # Format the preambulo and streamer if the monitoring
            # properties are empty, we only check members of this line
            # (which are Hlt2Member), and skip configurables.
            if mothers and not any('MotherMonitor' in m.Args
                                   or 'PostMonitor' in m.Args
                                   for m in members if isinstance(m, Hlt2Member)):
                # Do default monitoring
                pre, histos, error = preambulo(mothers, members)
                if error:
                    log.debug('%s %s', name, error)
                fun = streamer(histos)
                lastMember = members[-1]
                if not isinstance(lastMember, Hlt2Member):
                    log.debug("Not adding default mass plots to %s, as monitoring "
                              "properties are already set." % name)
                    continue

                # Update an existing preambulo by adding our lines.
                # and add the streamer to the correct property.
                lastMember.Args['Preambulo'] = lastMember.Args.get('Preambulo', []) + pre
                if lastMember.Type == FilterDesktop:
                    lastMember.Args['PostMonitor'] = fun
                else:
                    lastMember.Args['MotherMonitor'] = fun

        # Return a tuple of (linename, algorithsm, related) info if
        # that is present, otherwise just (linename, algorithms)
        if hasattr(self, "relatedInfo"):
            ri = self.relatedInfo()
            return [(nn, algs, self.__relatedInfoSequence(ri.get(nn, []), algs)) for nn, algs in sd.iteritems()]
        else:
            return sd.items()
