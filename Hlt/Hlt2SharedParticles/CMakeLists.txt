################################################################################
# Package: Hlt2SharedParticles
################################################################################
gaudi_subdir(Hlt2SharedParticles)

gaudi_depends_on_subdirs(Hlt/HltLine)

gaudi_install_python_modules()


gaudi_add_test(QMTest QMTEST)
