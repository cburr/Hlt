##
from Gaudi.Configuration import *
from HltLine.HltLine import bindMembers
from Configurables import PhotonMaker, ResolvedPi0Maker, MergedPi0Maker
from GaudiKernel.SystemOfUnits import MeV
#
from HltTracking.Hlt2TrackingConfigurations import Hlt2BiKalmanFittedForwardTracking
Hlt2BiKalmanFittedForwardTracking = Hlt2BiKalmanFittedForwardTracking()
##########################################################################
#
# Neutral protoparticles
#
neutralProtos = Hlt2BiKalmanFittedForwardTracking.hlt2NeutralProtos()

##########################################################################
# Make the pi0
#
Hlt2ResolvedPi0s 			= ResolvedPi0Maker("Hlt2ResolvedPi0s")
Hlt2ResolvedPi0s.Input      = neutralProtos.outputSelection()
Hlt2ResolvedPi0s.DecayDescriptor 	= "Pi0"
Hlt2ResolvedPi0s.Output 	= 'Hlt2/Hlt2ResolvedPi0s/Particles'
Hlt2ResolvedPi0s.addTool(PhotonMaker)
Hlt2ResolvedPi0s.PhotonMaker.Input 	= neutralProtos.outputSelection()
Hlt2ResolvedPi0s.MassWindow 		= 60.* MeV # was 30.* MeV
Hlt2ResolvedPi0s.PhotonMaker.PtCut 	= 200.*MeV
##########################################################################
# Make the pi0
#
Hlt2MergedPi0s 			= MergedPi0Maker("Hlt2MergedPi0s")
Hlt2MergedPi0s.Output        = 'Hlt2/Hlt2MergedPi0s/Particles'
Hlt2MergedPi0s.DecayDescriptor 	= "Pi0"
Hlt2MergedPi0s.Input		= neutralProtos.outputSelection()
Hlt2MergedPi0s.MassWindow 	= 60.* MeV # this is as in  offline. too tight? 
##########################################################################
# Make both
#
SeqHlt2Pi0 = GaudiSequencer('SeqHlt2Pi0',  ModeOR=True, ShortCircuit=False,
                            Members = [ Hlt2MergedPi0s, Hlt2ResolvedPi0s])

__all__ = ( 'MergedPi0s', 'ResolvedPi0s', 'AllPi0s' )

MergedPi0s    = bindMembers( None, [ neutralProtos, Hlt2MergedPi0s ] ).setOutputSelection(Hlt2MergedPi0s.Output)
ResolvedPi0s  = bindMembers( None, [ neutralProtos, Hlt2ResolvedPi0s ] ).setOutputSelection(Hlt2ResolvedPi0s.Output)
AllPi0s       = bindMembers( None, [ neutralProtos, SeqHlt2Pi0 ] ).setOutputSelection( [ Hlt2MergedPi0s.Output, Hlt2ResolvedPi0s.Output ] )
