#ifndef HLTBASE_HLTBASE_H
#define HLTBASE_HLTBASE_H 1

// Include files
#include <vector>
#include <map>
#include <string>
#include <iostream>
#include <type_traits>
#include "GaudiKernel/StatusCode.h"
#include "GaudiKernel/StringKey.h"
#include "GaudiKernel/HistoDef.h"
#include "GaudiKernel/Property.h"
#include "GaudiKernel/VectorMap.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiAlg/GaudiHistoAlg.h"
#include "Kernel/IANNSvc.h"
#include "HltBase/IHltRegister.h"
#include "HltBase/IHltData.h"

/** @class HltBaseAlg
 *
 *  Provide services to HLT algorithms and tools
 *
 *  Functionality:
 *       Access to HltSvc, Hlt::Data
 *       Retrieve, register Hlt::Selections
 *       Histogram booking via options
 *       Counters
 *
 *  Options
 *       HistogramDescriptor
 *
 *  @author Hugo Ruiz Perez
 *  @author Jose Angel Hernando Morata
 *  @date   2006-06-15
 */
namespace LHCb
{
class Track;
class RecVertex;
}

class ISvcLocator;

class HltBaseAlg : public GaudiHistoAlg
{
  public:
    // Algorithm constructor
    HltBaseAlg( std::string name, ISvcLocator* pSvcLocator );

    // initialize
    StatusCode initialize() override;

  protected:
    // initialize Histo
    AIDA::IHistogram1D* initializeHisto( const std::string& name, double min = 0.,
                                         double max = 100., int nBins = 100 );

    // print info of this container
    template <class CONT>
    void printInfo( const std::string& title, const CONT& cont ) const
    {
        info() << title << cont.size() << endmsg;
        for ( const auto& i : cont ) printInfo( title, *i );
    }

    // print info from track
    void printInfo( const std::string& title, const LHCb::Track& track ) const;

    // print info from vertex
    void printInfo( const std::string& title, const LHCb::RecVertex& ver ) const;

    // print info from extraInfo
    void printInfo( const std::string& title,
                    const GaudiUtils::VectorMap<int, double>& info ) const;

    IANNSvc& annSvc() const;
    Hlt::IRegister* regSvc() const;
    Hlt::IData* hltSvc() const;

    // returns the ID of the extraInfo by name
    int hltInfoID( const std::string& name ) const;

  private:
    // returns the ID of the extraInfo by name
    std::string hltInfoName( int id ) const;

    // Property to book histogram from options
    SimpleProperty<std::map<std::string, Gaudi::Histo1DDef>> m_histoDescriptor;

    // hlt data provider service
    mutable SmartIF<Hlt::IData> m_hltSvc;
    //
    // hlt data registration service
    mutable SmartIF<Hlt::IRegister> m_regSvc;

    // assigned names and numbers service...
    mutable SmartIF<IANNSvc> m_hltANNSvc;
};
#endif // HLTBASE_HLTBASE_H
