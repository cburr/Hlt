#ifndef IL0CONFEXTRAPOLATOR_H
#define IL0CONFEXTRAPOLATOR_H 1

// Include files

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

static const InterfaceID IID_IL0ConfExtrapolator( "IL0ConfExtrapolator", 2, 0 );

namespace LHCb {
class Track;
class State;
}
namespace GAUDI {
class XYZPoint;
}

/** @class IL0ConfExtrapolator IL0ConfExtrapolator.h
 *
 *  Interface for tool which does the extrapolation from the seed
 *  to the T-stations for muon, hadron and electron L0-T-confirmation.
 *
 *  @author Johannes Albrecht
 *  @date   2008-01-18
 */
class IL0ConfExtrapolator : virtual public IAlgTool
{
  public:
    // Return the interface ID
    static const InterfaceID& interfaceID()
    {
        return IID_IL0ConfExtrapolator;
    }

    // Extrapolates the seed track to the end of T3
    virtual void muon2T( const LHCb::Track& muonTrack,
                         LHCb::State& stateAtT ) const = 0;

    virtual void ecal2T( const LHCb::Track& ecalTrack, LHCb::State& statePosAtT,
                         LHCb::State& stateNegAtT ) const = 0;

    virtual void hcal2T( const LHCb::Track& ecalTrack, LHCb::State& statePosAtT,
                         LHCb::State& stateNegAtT ) const = 0;

    // provides the Calo region for a given position
    // 0: ECal IP, 1: ECal MP, 2: ECal OP
    // 3: HCal IP, 4: HCal OP
    virtual int getCaloRegion( double stateX, double stateY,
                               double stateZ ) const = 0;

    virtual double getBScale() = 0;
};
#endif // IL0CONFEXTRAPOLATOR_H
