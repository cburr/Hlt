// Include files

// local
#include "HltBase/HltBaseAlg.h"

#include "Event/RecVertex.h"
#include "Event/Track.h"
#include "Event/Particle.h"

//-----------------------------------------------------------------------------
// Implementation file for class : HltBaseAlg
//
// 2006-06-15 : Jose Angel Hernando Morata
//-----------------------------------------------------------------------------

HltBaseAlg::HltBaseAlg( std::string name, ISvcLocator* pSvcLocator )
    : GaudiHistoAlg( std::move(name), pSvcLocator) {
  declareProperty("HistoDescriptor", m_histoDescriptor);
}

StatusCode HltBaseAlg::initialize() {
  StatusCode sc = GaudiHistoAlg::initialize();
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  if (produceHistos()) {
      for (auto&  i : m_histoDescriptor.value()) {
          book(i.second);
          debug() << " booking histogram from descriptor "
                         << i.second << endmsg;
      }
  }
  return StatusCode::SUCCESS;
}

AIDA::IHistogram1D* HltBaseAlg::initializeHisto(const std::string& title,
                                            double x0, double xf, int nbins) {
  AIDA::IHistogram1D* histo = nullptr;
  if (!produceHistos()) return histo;

  const auto& values = m_histoDescriptor.value();
  auto i = values.find(title);
  Gaudi::Histo1DDef hd = (i!=values.end()) ? i->second
                                           : Gaudi::Histo1DDef{title,x0,xf,nbins};
  debug() << " booking histogram  " << hd << endmsg;
  return book(hd);
}

// ============================================================================
// accessor to Hlt Registration Service
// ============================================================================
Hlt::IRegister* HltBaseAlg::regSvc() const
{
  if ( UNLIKELY(!m_regSvc) ) m_regSvc = service( "Hlt::Service" , true ) ;
  return m_regSvc ;
}
// ============================================================================
// accessor to Hlt Data Service
// ============================================================================
Hlt::IData* HltBaseAlg::hltSvc() const
{
  if ( UNLIKELY(!m_hltSvc) ) m_hltSvc = service( "Hlt::Service" , true ) ;
  return m_hltSvc ;
}

IANNSvc& HltBaseAlg::annSvc() const {
  if ( UNLIKELY(!m_hltANNSvc) ) m_hltANNSvc = service("HltANNSvc",true);
  return *m_hltANNSvc;
}

int HltBaseAlg::hltInfoID(const std::string& infoname) const
{
  static const Gaudi::StringKey InfoID{"InfoID"};
  auto i = annSvc().value(InfoID,infoname);
  if (i) return i->second;
  info()<<"request for unknown Info id for name: "<<infoname <<endmsg;
  return 0;
}

std::string HltBaseAlg::hltInfoName(int id) const
{
  static const Gaudi::StringKey InfoID{"InfoID"};
  auto i = annSvc().value(InfoID,id);
  if(i) return i->first;
  info()<<"request for unknown Info name for id: "<<id << endmsg;
  return "UNKNOWN";
}

void HltBaseAlg::printInfo(const std::string& title,
                           const LHCb::Track& track) const {
  info() << title
               << " track  " << track.key()
               << " slopes " << track.slopes()
               << " pt "     << track.pt()
               << " q/p "    << track.firstState().qOverP()
               << " ids "    << track.lhcbIDs().size()
               << " states " << track.states().size()
               << endmsg;
  printInfo(title+" Info: ",track.extraInfo());
}

void HltBaseAlg::printInfo(const std::string& title,
                           const LHCb::RecVertex& vertex) const {
  info() << title << " vertex  " << vertex.key() << " position "
               << vertex.position()  << endmsg;
  printInfo(title+" Info: ",vertex.extraInfo());
  printInfo(title,vertex.tracks());
}

void HltBaseAlg::printInfo(const std::string& title,
                              const GaudiUtils::VectorMap<int,double>& vmap) const
{
  info() << title;
  for (auto& i : vmap ) info() << hltInfoName(i.first) << " = " << i.second << "; \t";
  info() << endmsg;
}
