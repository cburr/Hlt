// ============================================================================
#ifndef HLTCOMMON_HLTGEC_H
#define HLTCOMMON_HLTGEC_H 1
// ============================================================================
// Include files
// ============================================================================
// GaudiAlg
// ============================================================================
#include "GaudiAlg/GaudiTool.h"
// ============================================================================
// LHCbKernel
// ============================================================================
#include "Kernel/ICheckTool.h"
#include "Kernel/IAccept.h"
// ============================================================================
// OTDAQ
// ============================================================================
#include "OTDAQ/IOTRawBankDecoder.h"
// ============================================================================
// VeloDet
// ============================================================================
#include "VeloDet/DeVelo.h"
// ============================================================================
#include "TfKernel/IOTHitCreator.h"

namespace Hlt
{
// ==========================================================================
/** @class GEC HltGEC.h
 *  Simple tool to check the number of hits in OT
 *
 *  @author Jose Angel Hernando Morata
 *  @author Vanya Belyaev@nikhef.nl
 *  @date   2010-08-03
 */
class GEC : public extends<GaudiTool, IAccept, ICheckTool>
{
  public:
    // ========================================================================
    /** check
     *  @see ICheckTool
     *  @see ICheckTool::check
     */
    StatusCode check() override;
    // ========================================================================
    /** accept
     *  @see IAccept
     *  @see IAccept::accept
     */
    bool accept() const override;
    // ========================================================================
  public:
    // ========================================================================
    /// standard initialization
    StatusCode initialize() override;
    // ========================================================================
    /** Standard constructor
     *  @param type the tol type (???)
     *  @param name the tool instance name
     *  @param parent the tool parent
     */
    GEC( const std::string& type,    // the tool type
         const std::string& name,    // the tool instance name
         const IInterface* parent ); // the tool parent
    // ========================================================================
  private:
    // ========================================================================
    /// the default contructor is disabled
    GEC() = delete;
    /// the copy contructor is disabled
    GEC( const GEC& ) = delete;
    /// the assignement operator is disabled
    GEC& operator=( const GEC& ) = delete;
    // ========================================================================
  private:
    // ========================================================================
    double veloBalance() const;
    /// the maximum imbalance of VELO-hits
    double m_maxVeloBalance;
    /// the maximum number of OT-hits
    int m_maxOTHits; // the maximum number of OT-hits
    /// the maximum number of IT-hits
    int m_maxITHits; // the maximum number of IT-hits
    /// the maximum number of Velo-hits
    int m_maxVeloHits;
    /// the minimum number of OT-hits
    int m_minOTHits; // the minimum number of OT-hits
    /// the minimum number of IT-hits
    int m_minITHits; // the minimum number of IT-hits
    /// the minimum number of Velo-hits
    int m_minVeloHits; // the minimum number of Velo-hits
    /// the OT decoder tool
    const Tf::IOTHitCreator* m_otHitCreator;
    ///
    DeVelo* m_veloDet;
    // is it a upper GEC or an activity trigger
    bool m_isActivity;
    // ========================================================================
};
// ==========================================================================
} // end of namespace Hlt
// ============================================================================
// The END
// ============================================================================
#endif // HLTCOMMON_HLTGEC_H
// ============================================================================
