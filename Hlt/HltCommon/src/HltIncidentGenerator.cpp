// Include files
#include <string>

// from Gaudi
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiKernel/Incident.h"
#include "GaudiAlg/GaudiAlgorithm.h"

//-----------------------------------------------------------------------------
// Implementation file for class : HltIncidentGenerator
//
//-----------------------------------------------------------------------------

class HltIncidentGenerator : public GaudiAlgorithm
{
  public:
    HltIncidentGenerator( const std::string& name, ISvcLocator* pSvcLocator );
    StatusCode initialize() override;
    StatusCode execute() override;

  private:
    std::string m_incident;
    SmartIF<IIncidentSvc> m_incidentSvc;
};

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( HltIncidentGenerator )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
HltIncidentGenerator::HltIncidentGenerator( const std::string& name,
                                            ISvcLocator* pSvcLocator )
    : GaudiAlgorithm( name, pSvcLocator )
{
    declareProperty( "Incident", m_incident );
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode HltIncidentGenerator::initialize()
{
    StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
    if ( sc.isFailure() ) return sc; // error printed already by GaudiAlgorithm

    m_incidentSvc = service( "IncidentSvc" );

    return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode HltIncidentGenerator::execute()
{
  if ( !m_incident.empty() )
    m_incidentSvc->fireIncident( Incident( name(), m_incident ) );
  return StatusCode::SUCCESS;
}
