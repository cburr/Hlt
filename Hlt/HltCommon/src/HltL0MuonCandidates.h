#ifndef HLTCOMMON_HLTL0MUONPREPARE_H
#define HLTCOMMON_HLTL0MUONPREPARE_H 1

// Include files
// from Gaudi
#include "HltBase/HltSelectionContainer.h"
#include "HltBase/HltAlgorithm.h"
#include "HltBase/IMuonSeedTool.h"
#include "Event/L0MuonCandidate.h"

/** @class HltHadAlleyPreTrigger HltHadAlleyPreTrigger.h
 *
 *
 *  @author Jose Angel Hernando Morata
 *  @date   2006-07-28
 */
class HltL0MuonCandidates : public HltAlgorithm
{
  public:
    /// Standard constructor
    HltL0MuonCandidates( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode initialize() override; ///< Algorithm initialization
    StatusCode execute() override;    ///< Algorithm execution

  private:
    std::vector<int> generateCutList( const LHCb::L0DUChannel& channel );

    Hlt::SelectionContainer<LHCb::Track, LHCb::L0MuonCandidate> m_selection;

    std::string m_l0Location;
    std::string m_l0Channel;
    IMuonSeedTool* m_maker;
    AIDA::IHistogram1D* m_pt;
    AIDA::IHistogram1D* m_ptMax;

    bool checkClone( const LHCb::L0MuonCandidate* muon ) const;
};
#endif // HLTHADALLEYPRETRIGGER_H
