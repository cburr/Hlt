"""HLT Afterburner configuration."""
import types
from Gaudi.Configuration import *
from LHCbKernel.Configuration import *
from Configurables import GaudiSequencer as Sequence
from Configurables import ChargedProtoANNPIDConf
from HltTracking.Hlt2TrackingConfigurations import Hlt2BiKalmanFittedForwardTracking
from HltTracking.Hlt2TrackingConfigurations import Hlt2BiKalmanFittedDownstreamTracking

from HltPersistReco import HltPersistRecoConf

from ThresholdUtils import importLineConfigurables
import Hlt2Lines

__author__ = "R. Aaij roel.aaij@cern.ch"


class HltAfterburnerConf(LHCbConfigurableUser):
    # python configurables to be applied before me
    __queried_configurables__ = [
        Hlt2BiKalmanFittedForwardTracking,
        Hlt2BiKalmanFittedDownstreamTracking,
    ] + importLineConfigurables(Hlt2Lines)
    # We need the dependency on the Hlt2 lines above as we need to inspect
    # all of them for the PersistReco flag in order to create the filter

    # python configurables that I configure
    __used_configurables__ = [
        HltPersistRecoConf,
        ( ChargedProtoANNPIDConf, None )
    ]

    __slots__ = {"Sequence"                : None,
                 "EnableHltRecSummary"     : True,
                 "RecSummaryLocation"      : "Hlt2/RecSummary",
                 "AddAdditionalTrackInfos" : True,
                 "Hlt2Filter"              : "HLT_PASS_RE('Hlt2(?!Forward)(?!DebugEvent)(?!Lumi)(?!Transparent)(?!PassThrough).*Decision')"
                }

    def _persistRecoLines(self):
        from HltLine.HltLine import hlt2Lines
        return [line for line in hlt2Lines() if line.persistReco()]

    def _persistRecoFilterCode(self, lines):
        decisions = [line.decision() for line in lines]
        # code = ' | '.join(["HLT_TURBOPASS('{}')".format(i) for i in decisions])
        # if not code: code = 'HLT_NONE'
        # There is no HLT_TURBOPASS functor, so need to use regexps:
        code = "HLT_TURBOPASS_RE('^({})$')".format('|'.join(decisions))
        return code

    def _filterCode(self, lines):
        code = "HLT_TURBOPASS_RE('^({})Decision$')".format('|'.join(lines))
        return code

    def _persistRecoSeq(self):
        lines = self._persistRecoLines()
        print '# List of requested PersistReco lines: {}'.format(
            [line.name() for line in lines])

        # If no PR lines, do not configure HltPersistRecoConf
        if not lines:
            return []

        from DAQSys.Decoders import DecoderDB
        decoder = DecoderDB["HltDecReportsDecoder/Hlt2DecReportsDecoder"]
        from Configurables import LoKi__HDRFilter as HltFilter
        lineFilter = HltFilter(
            "Hlt2PersistRecoLineFilter",
            Code=self._persistRecoFilterCode(lines),
            Location=decoder.listOutputs()[0]
        )

        persistRecoSeq = Sequence("HltPersistRecoSeq",IgnoreFilterPassed=True)
        reconstructionSeq = Sequence("HltPersistRecoCreateProtos", ModeOR = True, ShortCircuit = False)
        persistSeq = Sequence("HltPersistReco")
        HltPersistRecoConf().Sequence = persistSeq
        persistRecoSeq.Members = [reconstructionSeq, persistSeq]
       
        # Configure the reconstruction
        from HltTracking.HltPVs import PV3D
        pvReco = PV3D("Hlt2")
        longProtos = Hlt2BiKalmanFittedForwardTracking().hlt2ChargedAllPIDsProtos()
        downstreamProtos = Hlt2BiKalmanFittedDownstreamTracking().hlt2ChargedAllPIDsProtos()
        neutralProtos = Hlt2BiKalmanFittedForwardTracking().hlt2NeutralProtos()
        from itertools import chain
        from Hlt2Lines.Utilities.Utilities import uniqueEverseen
        reconstructionSeq.Members  = list(uniqueEverseen(chain.from_iterable([longProtos, downstreamProtos, neutralProtos, pvReco])))
        
        return [Sequence("HltPersistRecoFilterSequence", Members=[lineFilter, persistRecoSeq])]

###################################################################################
#
# Main configuration
#
    def __apply_configuration__(self):
        """
        HLT Afterburner configuration
        """
        from HltTracking.Hlt2TrackingConfigurations import Hlt2BiKalmanFittedForwardTracking
        from HltTracking.Hlt2TrackingConfigurations import Hlt2BiKalmanFittedDownstreamTracking

        Afterburner = self.getProp("Sequence") if self.isPropertySet("Sequence") else None
        if not Afterburner:
            return
        AfterburnerFilterSeq = Sequence("HltAfterburnerFilterSequence" )
        Afterburner.Members += [AfterburnerFilterSeq]
        if self.getProp("Hlt2Filter"):
            from DAQSys.Decoders import DecoderDB
            decoder = DecoderDB["HltDecReportsDecoder/Hlt2DecReportsDecoder"]
            from Configurables import LoKi__HDRFilter   as HDRFilter
            hlt2Filter = HDRFilter('HltAfterburnerHlt2Filter', Code = self.getProp("Hlt2Filter"),
                                   Location = decoder.listOutputs()[0])
            AfterburnerFilterSeq.Members += [hlt2Filter]
        AfterburnerSeq = Sequence("HltAfterburnerSequence", IgnoreFilterPassed = True)
        AfterburnerFilterSeq.Members += [ AfterburnerSeq ]
        if self.getProp("EnableHltRecSummary"):
            from Configurables import RecSummaryAlg
            seq = Sequence("RecSummarySequence")
            
            tracks = Hlt2BiKalmanFittedForwardTracking().hlt2PrepareTracks()
            tracksDown = Hlt2BiKalmanFittedDownstreamTracking().hlt2PrepareTracks()
            muonID = Hlt2BiKalmanFittedForwardTracking().hlt2MuonID()

            from HltLine.HltDecodeRaw import DecodeVELO, DecodeIT, DecodeTT, DecodeSPD, DecodeMUON
            decoders = {"Velo"   : (DecodeVELO, "VeloLiteClustersLocation"),
                        "TT"     : (DecodeTT,   "clusterLocation"),
                        "IT"     : (DecodeIT,   "clusterLocation"),
                        "SPD"    : (DecodeSPD,  "DigitsContainer"),
                        'Muon'   : (DecodeMUON, "OutputLocation"),
                        'MuonTr' : (muonID,     "MuonTrackLocation")}
            decoders = {k : (bm.members(), bm.members()[-1].getProp(loc)) for (k, (bm, loc)) in decoders.iteritems()}

            from HltTracking.HltPVs import PV3D
            PVs = PV3D("Hlt2")
            from HltTracking.HltTrackNames import Hlt2TrackLoc
            recSeq = Sequence("RecSummaryRecoSequence", IgnoreFilterPassed = True)
            from itertools import chain
            from Hlt2Lines.Utilities.Utilities import uniqueEverseen
            recSeq.Members = list(uniqueEverseen(chain.from_iterable([dec[0] for dec in decoders.itervalues()]
                                                                     + [tracks, tracksDown, muonID, PVs])))
            from Configurables import Rich__Future__RawBankDecoder as RichDecoder
            recSeq.Members += [ RichDecoder( "RichFutureDecode" ) ]
            summary = RecSummaryAlg('Hlt2RecSummary', SummaryLocation = self.getProp("RecSummaryLocation"),
                                    HltSplitTracks = True,
                                    SplitLongTracksLocation = tracks.outputSelection(),
                                    SplitDownTracksLocation = tracksDown.outputSelection(),
                                    PVsLocation = PVs.output,
                                    VeloClustersLocation = decoders['Velo'][1],
                                    ITClustersLocation = decoders['IT'][1],
                                    TTClustersLocation = decoders['TT'][1],
                                    SpdDigitsLocation  = decoders['SPD'][1],
                                    MuonCoordsLocation = decoders['Muon'][1],
                                    MuonTracksLocation = decoders['MuonTr'][1])
            seq.Members = [recSeq, summary]
            AfterburnerSeq.Members += [seq]

        if self.getProp("AddAdditionalTrackInfos"):
            from GaudiKernel.SystemOfUnits import mm
            from Configurables import LoKi__VoidFilter as Filter
            trackLocations = [ Hlt2BiKalmanFittedForwardTracking().hlt2PrepareTracks().outputSelection(),
                               Hlt2BiKalmanFittedDownstreamTracking().hlt2PrepareTracks().outputSelection() ]
            infoSeq = Sequence("TrackInfoSequence", IgnoreFilterPassed = True)
            # I don't want to pull in reconstruction if not run before, then there should be also no candidates needing this information
            # This is anyhow done by the RecSummary above
            members = [Hlt2BiKalmanFittedForwardTracking().hlt2PrepareTracks().members() + Hlt2BiKalmanFittedDownstreamTracking().hlt2PrepareTracks().members()]
            infoSeq.Members += list(uniqueEverseen(chain.from_iterable(members)))
            prefix = "Hlt2"
            trackClones = Sequence(prefix + "TrackClonesSeq")
            #checkTracks =  Filter(prefix+"CheckTrackLoc",Code = "EXISTS('%(trackLocLong)s') & EXISTS('%(trackLocDown)s')" % {"trackLocLong" : trackLocations[0], "trackLocDown" : trackLocations[1]})
            #trackClones.Members += [checkTracks]
            from Configurables import TrackBuildCloneTable, TrackCloneCleaner
            cloneTable = TrackBuildCloneTable(prefix + "FindTrackClones")
            cloneTable.maxDz   = 500*mm
            cloneTable.zStates = [ 0*mm, 990*mm, 9450*mm ]
            cloneTable.klCut   = 5e3
            cloneTable.inputLocations = trackLocations
            cloneTable.outputLocation = trackLocations[0]+"Downstream" + "Clones"
            cloneCleaner = TrackCloneCleaner(prefix + "FlagTrackClones")
            cloneCleaner.CloneCut = 5e3
            cloneCleaner.inputLocations = trackLocations
            cloneCleaner.linkerLocation = cloneTable.outputLocation
            trackClones.Members += [ cloneTable, cloneCleaner ]

            infoSeq.Members += [trackClones]

            AfterburnerSeq.Members += [infoSeq]

            # Add VeloCharge to protoparticles for dedx
            veloChargeSeq = Sequence("VeloChargeSequence")
            from Configurables import ChargedProtoParticleAddVeloInfo
            protoLocation = Hlt2BiKalmanFittedForwardTracking().hlt2ChargedAllPIDsProtos().outputSelection()
            checkProto =  Filter("CheckProtoParticles",Code = "EXISTS('%(protoLoc)s')"% {"protoLoc" : protoLocation})
            addVeloCharge = ChargedProtoParticleAddVeloInfo("Hlt2AddVeloCharge")
            addVeloCharge.ProtoParticleLocation = protoLocation
            decodeVeloFullClusters = DecoderDB["DecodeVeloRawBuffer/createVeloClusters"].setup()
            veloChargeSeq.Members +=  [checkProto, decodeVeloFullClusters, addVeloCharge]
            AfterburnerSeq.Members += [veloChargeSeq]


        # Configure and add the persist reco
        AfterburnerSeq.Members += self._persistRecoSeq()
