"""
 Configurable to configure HLT monitoring

 @author R. Aaij
 @date 19-06-2015
"""
# =============================================================================
__author__  = "R. Aaij roel.aaij@cern.ch"
# =============================================================================
from functools import partial
from Gaudi.Configuration import *
from LHCbKernel.Configuration import *
from Configurables import GaudiSequencer as Sequence

def _recurse(c,fun) :
    fun(c)
    for p in [ 'Members','Filter0','Filter1' ] :
        if not hasattr(c,p) : continue
        x = getattr(c,p)
        if list is not type(x) : x = [ x ]
        for i in x : _recurse(i,fun)

def _enableMonitoring(c) :
    if c.getType() in (['FilterDesktop','CombineParticles' ]
                       + ['DaVinci::N%dBodyDecays' % i for i in range(3, 9)]):
        c.Monitor = True
        c.HistoProduce = True

def _disableHistograms(c,filter = lambda x : True) :
    if 'HistoProduce' in c.getDefaultProperties() and filter(c):
        c.HistoProduce = False
    for p in [ 'Members','Filter0','Filter1' ] :
        if not hasattr(c,p) : continue
        x = getattr(c,p)
        if list is not type(x) : x = [ x ]
        for i in x : _disableHistograms(i,filter)


class HltMonitoringConf(LHCbConfigurableUser):
    __used_configurables__ = []

    __slots__ = {"EnableAlgMonitoring"        : True,  # Enable monitoring for some algorithms
                 "EnableL0Monitor"            : True,
                 "EnableRateMonitor"          : True,
                 "EnableGlobalMonitor"        : True,
                 "EnableTrackMonitor"         : True,
                 "EnableMassMonitor"          : True,
                 'Hlt1TrackMonitorPrescale'   : 0.001,
                 'Hlt1TrackMonitorGEC'        : "Loose",
                 "HistogrammingLevel"         : "Line",  # One of 'None', 'Line' or 'NotLine'
                 "OutputFile"                 : "",
                 "MonitorSequence"            : None,
                 "ExtraName"                  : ""
                }

    def __globalMonitor(self, stage):
        def params(offset, width, nsigma, nbins):
            return [offset + width * nsigma, offset - width * nsigma, nbins]

        from Configurables import HltGlobalMonitor
        globalMon = HltGlobalMonitor(stage + self.getProp("ExtraName") + "GlobalMonitor")

        from DAQSys.Decoders import DecoderDB
        decRepLoc = DecoderDB["HltDecReportsDecoder/%sDecReportsDecoder" % stage].listOutputs()[0]
        globalMon.HltDecReports = decRepLoc
        globalMon.Stage = stage
        globalMon.BcidVertexParams = {
            'x': params(0.84, 0.035, 8, 64),
            'y': params(-0.18, 0.035, 8, 64),
            'z': params(0., 50., 8, 64),
        }

        from HltConf.Configuration import onlinePV
        globalMon.VertexLocations = onlinePV()
        return globalMon

    def __groupLines( self, lines , mapping ) :
        import re
        groups = {}
        taken = []
        for pos in range(len(mapping)):
            (name, pattern) = mapping[pos]
            expr = re.compile(pattern)
            groups[ name ] = [ i for i in lines if expr.match(i) and i not in taken ]
            taken += groups[ name ]
        #prune empty groups
        return dict( (k,v) for k,v in groups.iteritems() if v )


    def __l0_monitoring(self, stage):

        from Configurables import HltL0GlobalMonitor
        from DAQSys.Decoders import DecoderDB
        from DAQSys.DecoderClass import decodersForBank

        l0decoder = decodersForBank( DecoderDB, 'L0DU' )
        assert len(l0decoder)
        l0decoder = l0decoder[0].setup()

        decRepLoc = DecoderDB["HltDecReportsDecoder/%sDecReportsDecoder" % stage].listOutputs()[0]
        monitor = HltL0GlobalMonitor(stage + self.getProp('ExtraName') + 'L0GlobalMonitor', DecReports = decRepLoc,
                                     HltDecName = stage + "Global")

        if self.getProp("EnableL0Monitor"):
            return [l0decoder, monitor]
        else:
            return []

    def __hlt1_groups(self):
        return [('BeamGas',       "Hlt1BeamGas.*Decision"),
                ("Beauty",        "Hlt1B2.*Decision"),
                ("Bottomonium",   "Hlt1Bottomonium.*Decision"),
                ("CalibTracking", "Hlt1CalibTracking.*Decision"),
                ("Calib",         "Hlt1Calib.*Decision"),
                ("DiMuon",        "Hlt1.*DiMuon.*Decision"),
                ("DiProton",      "Hlt1DiProton.*Decision"),
                ("Electron",      "Hlt1.*Electron.*Decision"),
                ("Gamma",         "Hlt1.*Gamma.*Decision"),
                ("Global",        "Hlt1Global"),
                ("Phi",           "Hlt1IncPhiDecision"),
                ("L0",            "Hlt1L0.*Decision"),
                ("LowMult",       "Hlt1LowMult.*Decision"),
                ("Lumi",          "Hlt1Lumi.*Decision"),
                ("MicroBias",     "Hlt1MB.*Decision"),
                ("Multi",         "Hlt1Multi.*Decision"),
                ("MuonNoL0",      "Hlt1.*MuonNoL0.*Decision"),
                ("MVA",           "Hlt1.*MVADecision"),
                ("MVALoose",      "Hlt1.*MVALooseDecision"),
                ("MVATight",      "Hlt1.*MVATightDecision"),
                ("NoBias",        "Hlt1NoBias.*Decision"),
                ("NonPhysics",    "Hlt1(ODIN.*|Tell1Error|Incident)Decision"),
                ("RICH",          "Hlt1CalibRICH.*Decision"),
                ("SingleMuon",    "Hlt1(Single|Track)Muon.*Decision"),
                ("VeloClosing",   "Hlt1VeloClosing.*Decision"),
                ("Global",        ".*Global.*"),
                ("Other",         ".*") # add a "catch all" term to pick up all remaining decisions...
               ]

    def __hlt1_monitoring(self):
        monSeq = Sequence('Hlt1' + self.getProp('ExtraName') + 'MonitorSequence', IgnoreFilterPassed = True)

        l0Mon = self.__l0_monitoring("Hlt1")
        monSeq.Members += l0Mon

        # Tell the monitoring what it should expect..
        # the keys are the Labels for the Histograms in the GUI
        # the values are the Pattern Rules to for the Decisions contributing

        ## Global monitor
        from Configurables import HltGlobalMonitor
        globalMon = self.__globalMonitor("Hlt1")
        globalMon.DecToGroup = self.__hlt1_groups()
        if self.getProp("EnableGlobalMonitor"):
            monSeq.Members.append(globalMon)

        ## Mass monitor
        from Configurables import HltMassMonitor
        from DAQSys.Decoders import DecoderDB
        massMon = HltMassMonitor("Hlt1" + self.getProp('ExtraName') + "MassMonitor")
        massMon.DecReportsLocation = DecoderDB["HltDecReportsDecoder/Hlt1DecReportsDecoder"].listOutputs()[0]
        massMon.SelReportsLocation = DecoderDB["HltSelReportsDecoder/Hlt1SelReportsDecoder"].listOutputs()[0]
        massMon.Decisions  = {"Jpsi"         : "Hlt1DiMuonHighMassDecision",
                              "JpsiAlign"    : "Hlt1CalibMuonAlignJpsiDecision",
                              "D0->Kpi(det)" : 'Hlt1CalibTrackingKPiDetachedDecision',
                              "D0->Kpi"      : 'Hlt1CalibTrackingKPiDecision',
                              "D0->KK"       : 'Hlt1CalibTrackingKKDecision',
                              "D0->pipi"     : "Hlt1CalibTrackingPiPiDecision",
                              "phi->KK"      : "Hlt1IncPhiDecision"}
        massMon.DecisionStructure = {"Jpsi" : [105.658,105.658]}
        massMon.Histograms = {"Jpsi"         : [ 3010, 3190, 90 ],
                              "JpsiAlign"    : [ 3010, 3190, 90 ],
                              "D0->Kpi(det)" : [ 1815, 1915, 50 ],
                              "D0->Kpi"      : [ 1815, 1915, 50 ],
                              "D0->KK"       : [ 1815, 1915, 50 ],
                              "D0->pipi"     : [ 1815, 1915, 50 ],
                              "phi->KK"      : [ 1000, 1040, 80 ]}

        if self.getProp("EnableMassMonitor"):
            monSeq.Members.append(massMon)

        if self.getProp('EnableRateMonitor'):
            self.__rateMonitoring(('ODIN', 'L0', 'HLT1'), monSeq)

        from Configurables import HltRawBankMonitor
        rawBankMon = HltRawBankMonitor("Hlt1" + self.getProp('ExtraName') + "RawBankMonitor")
        monSeq.Members.append(rawBankMon)

        # Setup the track monitoring
        from Configurables        import Hlt1TrackMonitor
        import HltTracking
        from HltTracking.HltSharedTracking import MinimalVelo, VeloTTTracking, HltHPTTracking
        from Configurables import DeterministicPrescaler
        trackMon = Hlt1TrackMonitor("Hlt1" + self.getProp('ExtraName') + "TrackMonitor")
        trackMon.VeloTrackLocation = MinimalVelo.outputSelection()
        trackMon.VeloTTTrackLocation = VeloTTTracking.outputSelection()
        trackMon.ForwardTrackLocation = HltHPTTracking.outputSelection()

        # This is not so nice but currently unavoidable
        from HltTracking.Hlt1TrackNames import Hlt1TrackLoc
        from HltTracking.HltTrackNames import HltDefaultFitSuffix
        trackMon.FittedTrackLocation = Hlt1TrackLoc["FitTrack"]

        from Hlt1Lines.Hlt1GECs import Hlt1GECUnit
        gecUnit = Hlt1GECUnit( self.getProp("Hlt1TrackMonitorGEC") )
        prescaler = DeterministicPrescaler("Hlt1" + self.getProp('ExtraName') + "TrackMonitorPrescaler", AcceptFraction = self.getProp("Hlt1TrackMonitorPrescale") )
        trackMonSeq = Sequence('Hlt1' + self.getProp('ExtraName') + 'TrackMonitorSequence',
                               Members = [ gecUnit, prescaler ] + HltHPTTracking.members() + [ trackMon ])

        if self.getProp("EnableTrackMonitor"):
            monSeq.Members.append(trackMonSeq)

        return monSeq

    def __hlt2_groups(self):
        return [("B2HH",               "Hlt2B2HH.*Decision"),
                ("B2Kpi0",             "Hlt2B2K0?[pP]i0?.*Decision"),
                ("Bc2JpsiX",           "Hlt2Bc2JpsiX.*Decision"),
                ("BHad",               "Hlt2BHad.*Decision"),
                ("Bottomonium",        "Hlt2Bottomonium.*Decision"),
                ("CcDiHadron",         "Hlt2CcDiHadron.*Decision"),
                ("CharmHadDp2Eta",     "Hlt2CharmHadDp2Eta.*Decision"),
                ("CharmHadDp2KS0",     "Hlt2CharmHadDp2KS0.*Decision"),
                ("CharmHadDp2K",       "Hlt2CharmHadDp2[KP](m|p|Pi).*Decision"),
                ("CharmHadDp(To|Dsp)", "Hlt2CharmHadDp(To|Dsp).*Decision"),
                ("CharmHadDsp",        "Hlt2CharmHadDsp2.*Decision"),
                ("CharmHadDstp",       "Hlt2CharmHadDstp.*Decision"),
                ("CharmHadIncl",       "Hlt2CharmHadIncl.*Decision"),
                ("CharmHadLcp",        "Hlt2CharmHadLcp.*Decision"),
                ("CharmHadOmm",        "Hlt2CharmHadOmm.*Decision"),
                ("CharmHadXic",        "Hlt2CharmHadXic.*Decision"),
                ("CharmHadXim",        "Hlt2CharmHadXim.*Decision"),
                ("CharmHad",           "Hlt2CharmHad.*Decision"),
                ("Commissioning",      "Hlt2Commissioning.*Decision"),
                ("DPS",                "Hlt2DPS.*Decision"),
                ("DiMuon",             "Hlt2DiMuon.*Decision"),
                ("DisplVertices",      "Hlt2DisplVertices.*Decision"),
                ("EW",                 "Hlt2EW.*Decision"),
                ("Exotica",            "Hlt2Exotica.*Decision"),
                ("Jets",               "Hlt2Jets.*Decision"),
                ("LFV",                "Hlt2LFV.*Decision"),
                ("LowMult",            "Hlt2LowMult.*Decision"),
                ("Lumi",               "Hlt2Lumi.*Decision"),
                ("PID",                "Hlt2PID.*Decision"),
                ("Phi",                "Hlt2Phi.*Decision"),
                ("Radiative",          "Hlt2Radiative.*Decision"),
                ("RareCharm",          "Hlt2RareCharm.*Decision"),
                ("RareStrange",        "Hlt2RareStrange.*Decision"),
                ("RecoTest",           "Hlt2RecoTest.*Decision"),
                ("SingleMuon",         "Hlt2SingleMuon.*Decision"),
                ("Topo",               "Hlt2Topo.*Decision"),
                ("TrackEff",           "Hlt2TrackEff.*Decision"),
                ("TrackEffDiMuon",     "Hlt2TrackEffDiMuon.*Decision"),
                ("TriMuon",            "Hlt2TriMuon.*Decision"),
                ("XcMuXForTau",        "Hlt2XcMuXForTau.*Decision"),
                ("Global",             ".*Global.*"),
                ("Other",              ".*") # add a 'catch all' term to pick up all remaining decisions...
                ]

    def __hlt2_monitoring(self):
        monSeq = Sequence("Hlt2" + self.getProp('ExtraName') + "MonitorSequence", IgnoreFilterPassed = True)

        l0Mon = self.__l0_monitoring("Hlt2")
        monSeq.Members += l0Mon

        # Tell the monitoring what it should expect..
        # the keys are the Labels for the Histograms in the GUI
        # the values are the Pattern Rules to for the Decisions contributing

        from Configurables import HltGlobalMonitor
        globalMon = self.__globalMonitor("Hlt2")
        globalMon.DecToGroup = self.__hlt2_groups()

        if self.getProp("EnableGlobalMonitor"):
            monSeq.Members.append(globalMon)

        from Configurables import HltMassMonitor
        massMon = HltMassMonitor("Hlt2" + self.getProp('ExtraName') + "MassMonitor")
        from DAQSys.Decoders import DecoderDB
        massMon.DecReportsLocation = DecoderDB["HltDecReportsDecoder/Hlt2DecReportsDecoder"].listOutputs()[0]
        massMon.SelReportsLocation = DecoderDB["HltSelReportsDecoder/Hlt2SelReportsDecoder"].listOutputs()[0]
        massMon.Decisions  = {"Jpsi"                : "Hlt2DiMuonJPsiDecision",
                              "Psi2S"               : "Hlt2DiMuonPsi2STurboDecision",
                              "D+->Kpipi"           : "Hlt2CharmHadDpToKmPipPipTurboDecision",
                              "Ds+->KKpi"           : "Hlt2CharmHadDspToKmKpPipTurboDecision",
                              "Lambdac->pKpi"       : "Hlt2CharmHadLcpToPpKmPipTurboDecision",
                              "Omega->Lambda(LL)K"  : "Hlt2CharmHadOmm2LamKm_LLLDecision",
                              "Omega->Lambda(DD)K"  : "Hlt2CharmHadOmm2LamKm_DDLDecision",
                              "Xi->Lambda(LL)pi"    : "Hlt2CharmHadXim2LambPim_LLLDecision",
                              "Xi->Lambda(DD)pi"    : "Hlt2CharmHadXim2LambPim_DDLDecision",
                              "D*->(D0->KK)pi"      : "Hlt2CharmHadDstp2D0Pip_D02KmKpTurboDecision",
                              "D*->(D0->Kpi)pi"     : "Hlt2CharmHadDstp2D0Pip_D02KmPipTurboDecision",
                              "D*->(D0->Kpipipi)pi" : "Hlt2CharmHadDstp2D0Pip_D02KmPimPipPipTurboDecision",
                              "Xc->(D0->Kpi)pi"     : "Hlt2CharmHadSpec_D0ToKPi_PiTurboDecision",
                              "Xc->(D+->Kpipi)pi"   : "Hlt2CharmHadSpec_DpPiTurboDecision",
                              "D0->Kpi"             : "Hlt2RareCharmD02KPiDecision",
                              "phi->KK"             : "Hlt2IncPhiDecision"
                              }
        massMon.Histograms = {"Jpsi"                : [3005, 3186, 50],
                              "Psi2S"               : [3600, 3770, 50],
                              "D+->Kpipi"           : [1820.,1920., 100],
                              "Ds+->KKpi"           : [1920., 2020., 100],
                              "Lambdac->pKpi"       : [2235., 2335., 100],
                              "Omega->Lambda(LL)K"  : [1640., 1705., 65],
                              "Omega->Lambda(DD)K"  : [1640., 1705., 65],
                              "Xi->Lambda(LL)pi"    : [1290., 1355., 65],
                              "Xi->Lambda(DD)pi"    : [1290., 1355., 65],
                              "D*->(D0->KK)pi"      : [1990., 2040., 100],
                              "D*->(D0->Kpi)pi"     : [1990.,2040., 100],
                              "D*->(D0->Kpipipi)pi" : [1990.,2040., 100],
                              "Xc->(D0->Kpi)pi"     : [1975.,2975., 200],
                              "Xc->(D+->Kpipi)pi"   : [1995.,2995., 200],
                              "D0->Kpi"             : [1815.,1915., 100],
                              "phi->KK"             : [1000.,1040., 80]
                              }
        if self.getProp("EnableMassMonitor"):
            monSeq.Members.append(massMon)

        if self.getProp('EnableRateMonitor'):
            self.__rateMonitoring(('HLT2',), monSeq)

        return monSeq

    def __rateMonitoring(self, stages, seq):
        odin_rates = { 'Odin_Beam1OrCrossing'  : '( ODIN_BXTYP == LHCb.ODIN.Beam1 ) | ( ODIN_BXTYP == LHCb.ODIN.BeamCrossing )'
                     , 'Odin_Beam2OrCrossing'  : '( ODIN_BXTYP == LHCb.ODIN.Beam2 ) | ( ODIN_BXTYP == LHCb.ODIN.BeamCrossing )'
                     , 'Odin_True'             : 'ODIN_TRUE'
                     , 'Odin_Lumi'             : 'ODIN_PASS(LHCb.ODIN.Lumi)'
                     , 'Odin_NoNias'           : 'ODIN_PASS(LHCb.ODIN.NoBias)'
                     , 'Odin_SequencerTrigger' : 'ODIN_PASS(LHCb.ODIN.SequencerTrigger)'}

        l0_rates =   { 'L0_Physics'            : 'L0_DECISION_PHYSICS'
                     , 'L0_BeamGas'            : "L0_CHANNEL_RE('B?gas')"
                     , 'L0_CaloMuonMinBias'    : "L0_CHANNEL_RE('%s')" % '|'.join([chan for chan in ('CALO','MUON,minbias')])
                     , 'L0_PhysicsChannels'    : "L0_CHANNEL_RE('%s')" % '|'.join([chan for chan in ('Electron','Photon','Hadron','Muon','DiMuon',
                                                                                                     'Muon,lowMult','DiMuon,lowMult','Electron,lowMult',
                                                                                                     'Photon,lowMult','DiEM,lowMult','DiHadron,lowMult')])
                     , 'L0_Calo'               : "L0_CHANNEL_RE('CALO')" # note: need to take into account prescale in L0...
                     , 'L0_Hadron'             : "L0_CHANNEL_RE('Hadron')"
                     , 'L0_ElectronPhoton'     : "L0_CHANNEL_RE('Electron|Photon')"
                     , 'L0_MuonDiMuon'         : "L0_CHANNEL_RE('Muon|DiMuon')"
                     , 'L0_LowMult'            : "L0_CHANNEL_RE('.*,lowMult')"
                     , 'L0_Muon'               : "L0_CHANNEL_RE('Muon')"
                     , 'L0_DiMuon'             : "L0_CHANNEL_RE('DiMuon')"
                     , 'L0_HadronOrSumEt'      : "L0_CHANNEL_RE('Hadron|SumEt')"}

        hlt1_rates = { 'HLT1_' + k : "HLT_PASS_RE('%s')" % v for k, v in self.__hlt1_groups()}

        hlt2_rates = { 'HLT2_' + k : "HLT_PASS_RE('%s')" % v for k, v in self.__hlt2_groups()}

        rates = {'ODIN' : odin_rates, 'L0' : l0_rates,
                 'HLT1' : hlt1_rates, 'HLT2' : hlt2_rates}


        from DAQSys.Decoders import DecoderDB
        hlt1_decrep_loc = DecoderDB["HltDecReportsDecoder/Hlt1DecReportsDecoder"].listOutputs()[0]
        hlt2_decrep_loc = DecoderDB["HltDecReportsDecoder/Hlt2DecReportsDecoder"].listOutputs()[0]

        from Configurables import HltOnlineRateMonitor
        hltStage = 'Hlt2' if 'HLT2' in stages else 'Hlt1'
        for stage in stages:
            if stage not in rates:
                raise ValueError("Stage %s is not knows for rate monitoring." % stage)
            monitor = HltOnlineRateMonitor(hltStage + self.getProp('ExtraName') + '%sRateMonitor' % stage)
            monitor.Rates = rates[stage]
            monitor.Stage = stage
            monitor.Hlt1DecReportsLocation = hlt1_decrep_loc
            monitor.Hlt2DecReportsLocation = hlt2_decrep_loc
            seq.Members += [monitor]

    def __configureOutput(self):
        histoFile = self.getProp("OutputFile")
        if not histoFile:
            return

        from Configurables import HistogramPersistencySvc, RootHistCnv__PersSvc
        HistogramPersistencySvc().OutputFile = histoFile
        RootHistCnv__PersSvc().OutputFile = histoFile

    def configureHltMonitoring(self, hlt1lines, hlt2lines, fromTCK = False):
        """
        HLT Monitoring configuration
        """
        ## Only do things here that need to know the list of instantiated HLT
        ## lines. This function is called from the postConfigAction of
        ## HltConf.Configuration.

        monSeq = self.getProp("MonitorSequence")
        monSeq.IgnoreFilterPassed = True  # enforce execution of all sub-sequences
        if hlt1lines:
            hlt1Mon = self.__hlt1_monitoring()
            monSeq.Members += [hlt1Mon]
        if hlt2lines:
            hlt2Mon = self.__hlt2_monitoring()
            monSeq.Members += [hlt2Mon]

        if fromTCK:
            from GaudiConf.Manipulations import recurseConfigurables, setPropertiesAndAddTools
            props={"OutputLevel" : 4, "StatPrint" : False,
                   "ErrorsPrint" : False, "PropertiesPrint" : False}
            func = partial(setPropertiesAndAddTools, properties = props, force = True)
            recurseConfigurables(func, head = monSeq, descend_properties = ['Members'], descend_tools = True)
            return

        # Disable production of histograms for all (most) of the algorithms
        if   self.getProp('HistogrammingLevel') == 'None' :
            for i in hlt1lines + hlt2lines: _disableHistograms( i.configurable() )
        elif self.getProp('HistogrammingLevel') == 'Line' :
            for i in hlt1lines + hlt2lines: _disableHistograms( i.configurable(), lambda x: x.getType()!='Hlt::Line' )
        elif self.getProp('HistogrammingLevel') == 'NotLine' :
            for i in hlt1lines + hlt2lines: _disableHistograms( i.configurable(), lambda x: x.getType()=='Hlt::Line' )
        else:
            ValueError("HltMonitoringConf: HistogrammingLevel property must be set to 'None', 'Line' or 'NotLine'.")

        # Enable production of histograms for some algorithms, see _enableMonitoring()
        if self.getProp('EnableAlgMonitoring') :
            for i in hlt1lines + hlt2lines: _recurse( i.configurable(),_enableMonitoring )

    def __apply_configuration__(self):
        ## Only do things here that do not need to know the list of instantiated
        ## HLT lines.
        if not self.getProp("MonitorSequence"):
            raise ValueError("HltMonitoringConf: MonitorSequence property must be set.")
        self.__configureOutput()
