// stdlib
#include <exception>
#include <math.h>
#include <iostream>
#include <tuple>

// boost
#include <boost/optional.hpp>

// AIDA
#include <AIDA/IHistogram1D.h>
#include <AIDA/IHistogram2D.h>
#include <AIDA/IProfile1D.h>
#include <AIDA/IAxis.h>

// LHCb
#include <Event/ODIN.h>
#include <Event/Particle.h>
#include <Event/RecVertex.h>

#include <GaudiKernel/IIncidentSvc.h>
#include <GaudiUtils/QuasiRandom.h>

// local
#include "HistoWrapper.h"
#include "HltMassMonitor.h"

namespace {
   namespace QuasiRandom = Gaudi::Utils::QuasiRandom;
   using std::make_tuple;
   using std::string;
   using std::vector;
   using std::exception;
}

//-----------------------------------------------------------------------------
// Implementation file for class : HistoWrapper
//
// 2011-04-23 : Roel Aaij
//-----------------------------------------------------------------------------

//=============================================================================
HistoWrapper::HistoWrapper( HltMassMonitor* algo, const std::string& histoName,
                            const std::string& decision,
                            const std::vector< double >& massDef)
   : HistoWrapper(algo, histoName, decision, massDef, vector<double>{})
{

}


//=============================================================================
HistoWrapper::HistoWrapper( HltMassMonitor* algo, const string& histoName,
                            const string& decision,
                            const vector< double >& massDef,
                            const vector< double >& dauMass)
   :  m_name{histoName},
      m_decision{decision},
      m_massDef{massDef},
      m_dauMass{dauMass},
      m_algo{algo}
{
   if ( m_massDef.size() != 3 ) {
      throw exception();
   }

   auto initial = algo->name() + "/" + decision;
   m_initial = QuasiRandom::mixString(initial.size(), initial);

   auto monSvc = algo->hltMonSvc();
   HltHistogram* hltMass{nullptr};
   HltHistogram* hltPt{nullptr};

   if (monSvc.isValid()) {
      auto bins = boost::numeric_cast<size_t>(massDef[2]);
      HltHistogram& mass = monSvc->histogram(algo->name() + "/" + name() + "_mass",
                                             massDef[0], massDef[1], bins);
      hltMass = &mass;
      HltHistogram& pt = monSvc->histogram(algo->name() + "/" + name() + "_pT",
                                           0., 6000., 100);
      hltPt = &pt;
   }
   m_mass = Wrapper(algo->book1D(name() + "_mass", name() + " invariant mass",
                                 left(), right(), bins()), hltMass);
   m_pT   = Wrapper(algo->book1D(name() + "_pT",  0., 10000., 100), hltPt);
}

//=============================================================================
HistoWrapper::~HistoWrapper()
{

}

//=============================================================================
void HistoWrapper::fill(const LHCb::ODIN* odin, const LHCb::HltSelReports* selReports)
{
   const LHCb::HltObjectSummary* selReport = selReports->selReport(decision());
   if (!selReport) return;

   // Avoid hoarding memory
   if (m_masses.size() > 10) {
      m_masses.resize(10);
   }
   m_masses.clear();

   auto get = [](const GaudiUtils::VectorMap<string, float>& info, string key) -> boost::optional<float> {
      auto it = info.find(key);
      boost::optional<float> r;
      if (it != end(info)) {
         r = it->second;
      }
      return r;
   };
   
   auto PT = [&get](const LHCb::HltObjectSummary* cand, string stx, string sty, string sqop) {
      const auto& numInfo = cand->numericalInfo();
      auto tx = get(numInfo, std::move(stx));
      auto ty = get(numInfo, std::move(sty));
      auto qoverp = get(numInfo, std::move(sqop));
      boost::optional<float> q, p, pt;
      if (tx && ty && qoverp) {
         q = (*qoverp > 0) ? 1. : -1.;
         p = fabs(1. / *qoverp);
         auto normfactor = sqrt(*tx * *tx + *ty * *ty + 1);
         pt = sqrt(*tx * *tx + *ty * *ty) * *p / normfactor;
         return make_tuple(tx, ty, q, p, pt);
      } else {
         return make_tuple(boost::optional<float>{}, boost::optional<float>{}, q, p, pt);
      }
   };

   boost::optional<CandInfo> info;
   for (const auto& cand : selReport->substructure()) {
      const auto* candidate = cand.data();
      // dig through extra layers of substructure
      while (candidate->numericalInfo().size() == 0 && candidate->substructure().size() == 1) {
         candidate = candidate->substructure()[0].data();
      }

      boost::optional<float> tx, ty, q, p, pT, mass;
      
      //compute mass depending on type of object
      if (candidate->summarizedObjectCLID() == LHCb::Particle::classID()) {
         mass = get(candidate->numericalInfo(), "1#Particle.measuredMass");
         if (!mass) continue;
         std::tie(tx, ty, q, p, pT) = PT(candidate, "5#Particle.slopes.x", "6#Particle.slopes.y", "7#Particle.1/p");
         if (mass && pT) info = CandInfo{*mass, *pT};
      } else if (candidate->summarizedObjectCLID() == LHCb::RecVertex::classID()) {
         //We need to dig into the substructure
         float cand_E = 0., cand_px = 0.,cand_py = 0.,cand_pz = 0.;
         float charge = 1.;
         bool good = true;
         size_t i = 0;
         for (const auto& child : candidate->substructure()) {
            //Check that the child is a track or we need to dig deeper
            const auto* track = child.data();
            while (track->numericalInfo().size() == 0 && track->substructure().size() == 1) {
               track = track->substructure()[0].data();
            }
            std::tie(tx, ty, q, p, pT) = PT(track, "3#Track.firstState.tx", "4#Track.firstState.ty",
                                            "5#Track.firstState.qOverP");
            if (i > m_dauMass.size()) {
               good &= false;
               m_algo->warning() << "Daughter mass at index " << i << " requested, but not available for "
                                 <<  m_algo->name() + "/" + m_decision << endmsg;
               break;
            }
            mass = m_dauMass[i];

            if (!pT) {
               good &= false;
               m_algo->warning() << "Could not get pT for daughter at index " << i << " for "
                                 <<  m_algo->name() + "/" + m_decision << endmsg;
               break;
            }
            
            charge *= *q;
            auto normfactor = sqrt(*tx * *tx + *ty * *ty + 1);
            cand_px += *tx * *p / normfactor;
            cand_py += *ty * *p / normfactor;
            cand_pz += 1. * *p / normfactor;
            cand_E  += sqrt(*mass * *mass + *p * *p);
            ++i;
         }
         if (!good) continue;
         
         auto m2 = cand_E * cand_E - cand_px * cand_px - cand_py * cand_py - cand_pz * cand_pz;
         if (m2 > 0.) info = CandInfo{sqrt(m2), sqrt(cand_px * cand_px + cand_py * cand_py)};
      } else {
         m_algo->warning() << "Got candidate with unknown class ID: " << candidate->summarizedObjectCLID() << endmsg;
      }
      if (info) m_masses.push_back(*info);
   }

   if (m_masses.size() == 1) {
      m_mass.fill(m_masses[0].mass);
      m_pT.fill(m_masses[0].pT);
   } else if (m_masses.size() > 1) {
      // Randomly select a candidate
      uint32_t x = m_initial;
      x = QuasiRandom::mix64( x, odin->gpsTime() );
      x = QuasiRandom::mix32( x, odin->runNumber() );
      x = QuasiRandom::mix64( x, odin->eventNumber() );

      // scale to interval [0, size)
      auto scale = [](uint32_t x, uint32_t size) {
         const uint32_t denom = boost::integer_traits<uint32_t>::const_max / (size);
         return x / denom;
      };
      auto ci = m_masses[scale(x, m_masses.size())];
      m_mass.fill(ci.mass);
      m_pT.fill(ci.pT);
   }
}
