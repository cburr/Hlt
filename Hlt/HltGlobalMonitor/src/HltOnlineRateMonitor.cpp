// Include files
#include <map>

// from Boost
#include <boost/format.hpp>
#include <boost/algorithm/string.hpp>

// from Gaudi
#include "GaudiKernel/IIncidentSvc.h"
#include "AIDA/IHistogram1D.h"

// Event
#include "Event/RawEvent.h"
#include "Event/RawBank.h"
#include "Event/HltDecReports.h"
#include "Event/L0DUReport.h"
#include "Event/ODIN.h"

// Hlt Interfaces
#include "Kernel/RateCounter.h"

// local
#include "HltOnlineRateMonitor.h"

//-----------------------------------------------------------------------------
// Implementation file for class : HltOnlineRateMonitor
//
// 2016-05-01 : Roel Aaij
//-----------------------------------------------------------------------------

namespace {
   using std::string;
   using std::map;
}

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY(HltOnlineRateMonitor)

//=============================================================================
StatusCode HltOnlineRateMonitor::decode() {
   zeroEvaluators();

   // Create the right type of evaluator and build it
   auto build = [this](const string t, const string expr) -> StatusCode {
      if (expr.empty()) return StatusCode::SUCCESS;

      std::string title = boost::str(boost::format("%s:%s") % t % expr);
      std::string htitle = boost::str(boost::format("HltRate_%s") % t);

      decltype(m_evaluators)::iterator it; bool placed{false};
      if (m_stage == "ODIN") {
         std::tie(it, placed) = m_evaluators.emplace(title, ODINEval{m_odin_location});
      } else if (m_stage == "L0") {
         std::tie(it, placed) = m_evaluators.emplace(title, L0Eval{m_l0_location});
      } else if (m_stage == "HLT1") {
         std::tie(it, placed) = m_evaluators.emplace(title, HltEval{m_hlt_location[0]});
      } else if (m_stage == "HLT2") {
         std::tie(it, placed) = m_evaluators.emplace(title, HltEval{m_hlt_location[1]});
      } else {
         return Error(string{"Bad stage: "} + m_stage, StatusCode::FAILURE);
      }
      assert(placed && it->first == title);
      auto builder = Builder{this, expr, title, htitle};
      return boost::apply_visitor(builder, it->second);
   };

   // Build the routing bits
   for (const auto& entry : m_rates) {
      auto sc = build(entry.first, entry.second);
      if (!sc.isSuccess()) return sc;
   }

   m_evals_updated = false;
   m_preambulo_updated = false;
   return StatusCode::SUCCESS;
}

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
HltOnlineRateMonitor::HltOnlineRateMonitor( const std::string& name,
                                            ISvcLocator* pSvcLocator)
   : HltEvaluator( name , pSvcLocator )
{
   declareProperty("Rates", m_rates)->declareUpdateHandler( &HltOnlineRateMonitor::updateRates, this);
   declareProperty("Stage", m_stage, "ODIN, L0, HLT1 or HLT2");
}

//=============================================================================
HltOnlineRateMonitor::~HltOnlineRateMonitor()
{
   zeroEvaluators();
}

//=============================================================================
void HltOnlineRateMonitor::zeroEvaluators() {
   Deleter deleter;
   for (auto& entry : m_evaluators) {
      boost::apply_visitor(deleter, entry.second);
   }
}

//=============================================================================
// update handlers
//=============================================================================
void HltOnlineRateMonitor::updateRates( Property& /* p */ )
{
   /// mark as "to-be-updated"
   m_evals_updated = true;
   // no action if not yet initialized
   if (Gaudi::StateMachine::INITIALIZED > FSMState()) return;
   // postpone the action
   if ( !m_preambulo_updated ) return;
   // perform the actual immediate decoding
   StatusCode sc = decode();
   Assert(sc.isFailure(), "Error from HltOnlineRateMonitor::decode()", sc);
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode HltOnlineRateMonitor::initialize() {
   auto sc = HltEvaluator::initialize(); // must be executed first
   if (!sc.isSuccess()) return sc;

   // Check if the Stage property is either Hlt1 or Hlt2
   if (!std::set<std::string>{{"ODIN", "L0", "HLT1", "HLT2"}}.count(m_stage)) {
      sc = Error(string{"Stage property must be ODIN, L0, Hlt1 or Hlt2, while it is '"} +
                 m_stage + "'.", StatusCode::FAILURE);
   }

   return sc;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode HltOnlineRateMonitor::execute() {
   StatusCode sc = StatusCode::SUCCESS;
   if (m_evals_updated || m_preambulo_updated) {
      sc = decode();
      Error(" Unable to Decode ???? ", sc);
      if (!sc.isSuccess()) return sc;
   }

   // Get the fill time, weight and event time
   double t = 0, weight = 0, evt_time = 0;
   sc = times(t, weight, evt_time);
   if (!sc.isSuccess()) return sc;

   // Create the evaluator
   Evaluator evaluator{this, t, weight, evt_time};

   // Evaluate the routing bits
   for (auto& entry : m_evaluators) {
      boost::apply_visitor(evaluator, entry.second);
   }
   return sc;
}
