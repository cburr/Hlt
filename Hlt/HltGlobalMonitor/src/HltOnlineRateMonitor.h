#ifndef HLTGLOBALMONITOR_HLTONLINERATEMONITOR_H
#define HLTGLOBALMONITOR_HLTONLINERATEMONITOR_H 1

// Include files
#include <array>
#include <functional>

// boost
#include <boost/variant.hpp>

// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiKernel/IUpdateManagerSvc.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/GaudiException.h"
#include "DetDesc/Condition.h"

#include "Kernel/IHltMonitorSvc.h"

#include "LoKi/OdinTypes.h"
#include "LoKi/L0Types.h"
#include "LoKi/HLTTypes.h"

#include "HltDAQ/HltEvaluator.h"

/** @class HltOnlineRateMonitor HltOnlineRateMonitor.h
 *
 *
 *  @author Gerhard Raven
 *  @date   2008-07-29
 */
class HltOnlineRateMonitor : public HltEvaluator {
public:
   /// Standard constructor
   HltOnlineRateMonitor( const std::string& name, ISvcLocator* pSvcLocator );

   ~HltOnlineRateMonitor() override;

   StatusCode initialize() override;
   StatusCode execute   () override;

protected:

   /// Decode
   StatusCode decode() override;

private:

   std::string m_stage;
   std::map<std::string, std::string> m_rates;

   void zeroEvaluators();
   void updateRates( Property& /* p */ );

   std::unordered_map<std::string, EvalVariant> m_evaluators;


};
#endif // HLTGLOBALMONITOR_HLTONLINERATEMONITOR_H
