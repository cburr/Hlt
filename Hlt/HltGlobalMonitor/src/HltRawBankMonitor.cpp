// $Id: HltRawBankMonitor.cpp,v 1.1 2009-03-09 21:18:07 jvantilb Exp $
// Include files
#include <tuple>
#include <string>
#include <vector>
#include <unordered_set>
#include <unordered_map>

// From DAQEvent
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "Event/ODIN.h"

// For regular expressions
#include <boost/regex.hpp>

// AIDA
#include <AIDA/IHistogram1D.h>

// GaudiKernel
#include "GaudiKernel/IUpdateManagerSvc.h"
#include "GaudiKernel/IIncidentSvc.h"

// GaudiUtils
#include <GaudiUtils/HistoStats.h>
#include <GaudiUtils/HistoLabels.h>

// local
#include "HltRawBankMonitor.h"

namespace {
   using namespace LHCb;
   using std::string;
   using std::make_pair;
   using std::make_tuple;
   using std::vector;
   using namespace Gaudi::Utils::Histos;
}

//-----------------------------------------------------------------------------
// Implementation file for class : HltRawBankMonitor
//
// 2016-10-18 : Roel Aaij
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT(HltRawBankMonitor)

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
HltRawBankMonitor::HltRawBankMonitor(const std::string& name,
                                     ISvcLocator* pSvcLocator)
: base_class(name, pSvcLocator),
   m_runpars{nullptr},
   m_binWidth{10}, // in seconds!!!
   m_timeSpan{4000} // in seconds!!!
{
   declareProperty("ODINLocation", m_odin_location = LHCb::ODINLocation::Default);
   declareProperty("InputLocation", m_inputLocation = RawEventLocation::Default);
   declareProperty("BankNames", m_bankNames = {".*Error"} );
   declareProperty("GetStartOfRunFromCondDB", m_useCondDB = true);
   declareProperty("TrendTimeSpan", m_timeSpan = 4000 );
   declareProperty("TrendBinWidth", m_binWidth = 10 );
}

//=============================================================================
StatusCode HltRawBankMonitor::initialize()
{
   // Gaudi initialize
   StatusCode sc = base_class::initialize(); // must be executed first
   if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

   // Incidents and conditions
   auto incSvc = service("IncidentSvc").as<IIncidentSvc>();
   if (m_useCondDB) {
      auto updMgrSvc = service("UpdateManagerSvc").as<IUpdateManagerSvc>();
      if(!updMgrSvc) {
         error()<< "Could not retrieve UpdateManagerSvc" << endmsg;
         return StatusCode::FAILURE;
      }
      updMgrSvc->registerCondition(this, "Conditions/Online/LHCb/RunParameters",
                                     &HltRawBankMonitor::i_updateConditions, m_runpars);
      sc = updMgrSvc->update(this);
      if(!sc.isSuccess()) return sc;
   } else {
      // reset m_startOfRun to zero at start of run....
      incSvc->addListener(this, IncidentType::BeginRun, 0, false, false);
      incSvc->addListener(this, "RunChange",0, false, false);
   }
   incSvc->addListener(this, IncidentType::BeginEvent, 0, false, false);

   // Loop over the list of possible BankTypes
   info() << "Monitoring bank types: ";

   int nBins = int(m_timeSpan / m_binWidth + 0.5);

   for(unsigned int iBank = 0; iBank < RawBank::LastType; ++iBank) {
      // make an enum vector from the string vector of bank names
      std::vector<std::string>::const_iterator iBankName = m_bankNames.begin();
      for( ; iBankName != m_bankNames.end(); ++iBankName ) {
         // Use the regular expression
         boost::regex e(*iBankName);
         std::string bankName = RawBank::typeName( RawBank::BankType(iBank));
         if( boost::regex_match( bankName, e) ) {
            m_bankTypes.emplace( iBank );
            auto histo = book1D(bankName + "_Rate", 0, nBins * m_binWidth, nBins);
            declareInfo(bankName + "_Rate", histo, bankName + "_Rate");
            m_counters.emplace(iBank, make_tuple(m_counters.size(), &counter(bankName), histo));
            info() << bankName << "(" << iBank << ") ";
         }
      }
   }
   info() << endmsg;

   // Declare counters
   vector<string> labels(m_counters.size());
   for (auto& entry : m_counters) {
      auto bankName = RawBank::typeName(RawBank::BankType(entry.first));
      labels[std::get<0>(entry.second)] = bankName;
      declareInfo(string{"COUNTER_TO_RATE["} + bankName + "]", *std::get<1>(entry.second), bankName);
   }

   // Book and declare histogram
   m_histogram = book1D("RawBankTypes", -0.5, m_counters.size() - 0.5, m_counters.size());
   setBinLabels(m_histogram, labels);
   declareInfo("RawBankTypes", m_histogram, "RawBankTypes");

   return StatusCode::SUCCESS;
}

//=============================================================================
StatusCode HltRawBankMonitor::execute()
{
   StatusCode sc = StatusCode::SUCCESS;
   // Get the raw data
   RawEvent* raw = get<RawEvent>( m_inputLocation );
   if (!raw) {
      return sc;
   }

   // Get the fill time, weight and event time
   double t = 0, weight = 0, evt_time = 0;
   sc = times(t, weight, evt_time);
   if (!sc.isSuccess()) return sc;

   // Loop over the bank types
   for (auto bankType : m_bankTypes) {

      // Get the bank in the RawEvent
      const std::vector<RawBank*>& bank = raw->banks( RawBank::BankType(bankType) );

      // If bank exist mark the event
      if ( bank.size() > 0 ) {
         // Make some printout if bank is found
         if (UNLIKELY(msgLevel(MSG::DEBUG))) {
            debug() << "Found " << bank.size() << " bank(s) of type "
                    << bankType << " (" <<  (bank.front())->typeName() << ")."
                    << endmsg;
         }
         // Fill histogram
         auto it = m_counters.find(bankType);
         if (it != end(m_counters)) {
            m_histogram->fill(std::get<0>(it->second));
            ++(*(std::get<1>(it->second)));
            std::get<2>(it->second)->fill(t, weight);
         }
      }
   }

   setFilterPassed(true);
   return sc;
}

//=============================================================================
StatusCode HltRawBankMonitor::times(double& t, double& w, double& et) const
{
   // go from microseconds to seconds
   auto odin = get<LHCb::ODIN>(m_odin_location);
   if (!odin) return StatusCode::FAILURE;

   et = odin->gpsTime() / 1e6;
   t = odin->gpsTime() >= m_startOfRun ? double(odin->gpsTime() - m_startOfRun) : -double(m_startOfRun - odin->gpsTime());
   // t in seconds
   t /= 1e6;

   // m_binWidth is in seconds, need rate in Hz
   w = 1. / m_binWidth;

   return StatusCode::SUCCESS;
}

//=============================================================================
void HltRawBankMonitor::handle(const Incident& incident) {
   if (!m_useCondDB && (incident.type() == IncidentType::BeginRun
                        || incident.type() == "RunChange")) {
      m_startOfRun = 0;
   }
}

//=============================================================================
StatusCode HltRawBankMonitor::i_updateConditions()
{
   if (m_runpars==nullptr) {
      error() << "Could not obtain Condition for run parameters from conditions DB" << endmsg;
      return StatusCode::FAILURE;
   }
   if (!m_runpars->exists("RunStartTime")) {
      error() << "Condition does not contain RunStartTime " << endmsg;
      return StatusCode::FAILURE;
   }

   //from seconds -> microseconds since 1/1/1970
   m_startOfRun = (long long unsigned int) (m_runpars->param<int>("RunStartTime") * 1e6);
   return StatusCode::SUCCESS;
}
