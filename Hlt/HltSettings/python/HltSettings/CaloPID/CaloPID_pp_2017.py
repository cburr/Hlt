# =============================================================================
# @file   CaloPID_pp_2017.py
# @author Carla Marin (carla.marin@cern.ch)
# @author Regis Lefevre (rlefevre@cern.ch)
# @author Max Chefdeville (chefdevi@lapp.in2p3.fr)
# @date   24.02.2017
# =============================================================================
"""Threshold settings for Hlt2 CaloPID lines for 2017.
"""


class CaloPID_pp_2017(object):
    """Threshold settings for Hlt2 CaloPID lines for 2017."""
    __all__ = ('ActiveHlt2Lines', 'Thresholds')

    def ActiveHlt2Lines(self):
        """Returns a list of active lines."""

        lines = [# High PT photons
                 'Hlt2CaloPIDBd2KstGammaTurboCalib',
                 'Hlt2CaloPIDBs2PhiGammaTurboCalib',
                 # Soft photons
                 'Hlt2CaloPIDDsst2DsGammaTurboCalib',
                 'Hlt2CaloPIDD2EtapPiTurboCalib',
                 # Pi0s
                 'Hlt2CaloPIDDstD02KPiPi0_MergedPi0TurboCalib',
                 'Hlt2CaloPIDDstD02KPiPi0_ResolvedPi0TurboCalib',
                 ]

        return lines

    def Thresholds(self):
        """Set thresholds for the lines."""

        from GaudiKernel.SystemOfUnits import MeV,mm,picosecond

        thresholds = {'Prescale'    : {'Hlt2CaloPIDDstD02KPiPi0_MergedPi0TurboCalib'   : 1.00,
                                       'Hlt2CaloPIDDstD02KPiPi0_ResolvedPi0TurboCalib' : 1.00,
                                       'Hlt2CaloPIDDsst2DsGammaTurboCalib' : 1.00,
                                       'Hlt2CaloPIDD2EtapPiTurboCalib'     : 1.00,},
                      'Common'      : {# The following is for the DstD02KPiPi0 lines
                                       'Track_CHI2DOF_MAX'        :  5. ,
                                       'Track_GHOSTPROB_MAX'      :  0.3,
                                       'Track_MIPCHI2DV_MIN'      : 25. ,
                                       'SoftPion_MIPCHI2DV_MAX'   : 16. ,
                                       'Kaon_PIDK_MIN'            :  0. ,
                                       'Pion_PIDK_MAX'            :  0. ,
                                       'ResolvedPi0_GammaCL_MIN'  : -1000., # i.e. no cut
                                       'D0_PT_MIN'                : 4000. * MeV,
                                       'D0_AM_MIN'                : 1400. * MeV,
                                       'D0_AM_MAX'                : 2300. * MeV,
                                       'D0_M_MIN'                 : 1600. * MeV,
                                       'D0_M_MAX'                 : 2100. * MeV,
                                       'D0_VCHI2PDOF_MAX'         :   9.    ,
                                       'D0_VVDCHI2_MIN'           : 100.    ,
                                       'D0_VIPCHI2_MAX'           :   9.    ,
                                       'D0_DIRA_MIN'              :   0.9999,
                                       'Dst_PT_MIN'               : 4000. * MeV,
                                       'Dst_dAM_MIN'              : (145.421-50.) * MeV,
                                       'Dst_dAM_MAX'              : (145.421+50.) * MeV,
                                       'Dst_dM_MIN'               : (145.421-10.) * MeV,
                                       'Dst_dM_MAX'               : (145.421+10.) * MeV,
                                       'Dst_VCHI2PDOF_MAX'        :   9.    ,
                                       'Dst_VVDCHI2_MAX'          :  16.    ,
                                       'Dst_VIPCHI2_MAX'          :   9.    ,
                                       },
                      
                      'Bd2KstGamma' : {'Photon_PT'         : 2600. * MeV,
                                       'Track_IP_Chi2'     : 55.,
                                       'Track_Chi2ndof'    : 5.,
                                       'Track_P'           : 0. * MeV, # no cut
                                       'Track_PT'          : 0. * MeV, # tight config: 500 * MeV
                                       'HH_Particle'       : 'K*(892)0',
                                       'HH_Vtx_Chi2ndof'   : 9.,
                                       'HH_Mass_Range'     : 100. * MeV,
                                       'B_Particle'        : 'B0',
                                       'B_Mass_Range'      : 1500. * MeV,
                                       'B_DIRA'            : 0.02,
                                       'B_IP_Chi2'         : 15.,
                                       'B_PT'              : 3000. * MeV,
                                       'B_FD_Chi2'         : 0.,
                                       },
                      
                      'Bs2PhiGamma' : {'Photon_PT'         : 2600. * MeV,
                                       'Track_IP_Chi2'     : 55.,
                                       'Track_Chi2ndof'    : 5.,
                                       'Track_P'           : 3000. * MeV,
                                       'Track_PT'          : 0. * MeV, # tight config: 500 * MeV
                                       'HH_Particle'       : 'phi(1020)',
                                       'HH_Vtx_Chi2ndof'   : 9.,
                                       'HH_Mass_Range'     : 15. * MeV,
                                       'B_Particle'        : 'B_s0',
                                       'B_Mass_Range'      : 1500. * MeV,
                                       'B_DIRA'            : 99999., # no cut
                                       'B_IP_Chi2'         : 15.,
                                       'B_PT'              : 3000. * MeV,
                                       'B_FD_Chi2'         : 0.,
                                       },
                      
                      'Dsst2DsGamma' : {'Dsst_Particle'         : 'D*_s+',
                                        'Dsst_Mass_Min'         : 2050. * MeV,
                                        'Dsst_Mass_Max'         : 2250. * MeV,
                                        'Dsst_DIRA'             : 0.999975,
                                        'Dsst_IP'               : 0.05 * mm,
                                        'Dsst_IP_Chi2'          : 10.,
                                        'gamma_PT'              : 500. * MeV,                                        
                                        },

                      'D2EtapPi' : {'Pi_PT'         : 500. * MeV,
                                    'Pi_MIPCHI2DV'  : 16.,
                                    'Pi_Track_Chi2ndof' : 5.,
                                    'Pi_TRGHOSTPROB' : 0.5,
                                    'Pi_P' : 1000 * MeV,
                                    'Pi_PID' : 0,
                                    'gamma_PT' : 1000. * MeV,
                                    'Etap_Mass_Min' : 900. * MeV,
                                    'Etap_Mass_Max' : 1020. * MeV,
                                    'Pi_BACH_PT'      : 600. * MeV,
                                    'D_APT' : 2000. * MeV,
                                    'D_Mass_Min' : 1700. * MeV,
                                    'D_Mass_Max' : 2200. * MeV,
                                    'D_Vtx_Chi2ndof' : 4.,
                                    'D_BPVLTIME' : 0.25 * picosecond ,
                                    'D_DIRA'             : 0.99995,
                                    'D_IP'               : 0.05 * mm,
                                    'D_IP_Chi2'          : 10.,
                                    },

                      'D02KPiPi0_MergedPi0'  : {'Pi0_PT_MIN'  : 2000. * MeV,
                                                'Track_PT_MIN':  300. * MeV,
                                                },

                      'D02KPiPi0_ResolvedPi0': {'Pi0_PT_MIN'  : 1000. * MeV,
                                                'Track_PT_MIN':  600. * MeV,
                                                },
                      
                      }

        ##########################################################################
        # Return
        ##########################################################################
        from Hlt2Lines.CaloPID.Lines import CaloPIDLines

        return {CaloPIDLines: thresholds}

# EOF

