from GaudiKernel.SystemOfUnits import GeV, mm, MeV 

class DiMuon_pPb2016(object) :
    """
    Threshold settings for Hlt2 DiMuon lines

    WARNING :: DO NOT EDIT WITHOUT PERMISSION OF THE AUTHORS

    @author F. Dettori
    @date 2015-07-019
    """

    __all__ = ( 'ActiveHlt2Lines' )


    def ActiveHlt2Lines(self) :
        """
        Returns a list of active lines
        """

        lines = [
            # control rate via: 1) mass: 120 --> 70,
            # DO NOT INCREASE PT!
            'Hlt2DiMuonJPsi',
            'Hlt2DiMuonJPsiHighPT',
            'Hlt2DiMuonPsi2S',
            'Hlt2DiMuonPsi2SHighPT',

            # Do not change 
            'Hlt2DiMuonB',
            'Hlt2DiMuonZ',

            # Control rate via IP cut
            'Hlt2DiMuonSoft',

            # control rate via: DLS cut, dimuon PT
            'Hlt2DiMuonDetached',
            'Hlt2DiMuonDetachedJPsi',
            'Hlt2DiMuonDetachedHeavy',
            'Hlt2DiMuonDetachedPsi2S',

            # Turbo lines 
            'Hlt2DiMuonJPsiTurbo',
            'Hlt2DiMuonPsi2STurbo',
            #'Hlt2DiMuonPsi2SLowPTTurbo',
            'Hlt2DiMuonBTurbo'
            ]

        return lines


    def Thresholds(self) :
        """
        Returns a dictionary of cuts
        """

        # keep pass through thresholds
        d = { }

        from Hlt2Lines.DiMuon.Lines     import DiMuonLines            
        d.update({DiMuonLines :
                  {
                 'PersistReco' : {'JPsiTurbo'                          : True,
                                  'Psi2SLowPTTurbo'                    : True,
                                  'Psi2STurbo'                         : True,
                                  'BTurbo'                             : True},

                   'Common' :              {'TrChi2'      :   10,
                                            'TrChi2Tight' :    5},
                   'DiMuon' :              {'MinMass'     :  2700 * MeV,
                                            'Pt'          :     0 * MeV,
                                            'MuPt'        :     0 * MeV,
                                            'VertexChi2'  :    25},
                   'JPsi' :                {'MassWindow'  :   150 * MeV,
                                            'Pt'          :     0 * MeV,
                                            'MuPt'        :     0 * MeV,
                                            'VertexChi2'  :    25},
                   'JPsiHighPT' :          {'Pt'          :  2000 * MeV,
                                            'MuPt'        :     0 * MeV,
                                            'VertexChi2'  :    25,
                                            'MassWindow'  :   120 * MeV},
                   'Psi2S' :               {'MassWindow'  :   150 * MeV,
                                            'Pt'          :     0 * MeV,
                                            'MuPt'        :     0 * MeV,
                                            'VertexChi2'  :    25},
                   'Psi2SLowPT' :          {'MassWindow' :   150 * MeV,
                                            'PtMax'      :  2500 * MeV,
                                            'MuPt'       :     0 * MeV,
                                            'VertexChi2' :    25},
                   'Psi2SHighPT' :         {'Pt'          :  2000 * MeV,
                                            'MassWindow'  :   150 * MeV,
                                            'MuPt'        :     0 * MeV,
                                            'VertexChi2'  :    25},
                   'B' :                   {'MinMass'     :  4700 * MeV,
                                            'VertexChi2'  :    25},
                   'Z' :                   {'MinMass'     : 40000 * MeV,
                                            'Pt'          :     0 * MeV},
                   'Detached' :            {'IPChi2'      :     4,
                                            'DLS'         :     3},
                   'DetachedHeavy' :       {'MinMass'     :  2500 * MeV,
                                            'Pt'          :     0 * MeV,
                                            'MuPt'        :   300 * MeV,
                                            'VertexChi2'  :    25,
                                            'IPChi2'      :     0,
                                            'DLS'         :     3},
                   'Soft' :          {'IP'         :   0.3 * mm ,
                                      'IPChi2Min'  :   9,
                                      'IPChi2Max'  :   1000000000000,
                                      'TTHits'     :      -1,
                                      'TRACK_TRGHOSTPROB_MAX': 0.4,
                                      'MaxMass'    :   1000 * MeV,
                                      'VertexChi2' :    25,
                                      'MuProbNNmu' :    0.05,
                                      'Rho'        :     3,
                                      'SVZ'        :   650,
                                      'doca'       :   0.3,
                                      'MinVDZ'     :     0,
                                      'MinBPVDira' :     0,
                                      'MaxIpDistRatio':  1./60,
                                      'cosAngle'   : 0.999998
                                    },                    
                   'DetachedJPsi' :        {'DLS'         :     3},
                   'DetachedPsi2S' :       {'DLS'         :     3},
                   'DetachedHeavy' : {'MinMass'    :  2500 * MeV,
                                      'Pt'         :     0 * MeV,
                                      'MuPt'       :   300 * MeV,
                                      'VertexChi2' :    25,
                                      'IPChi2'     :     0,
                                      'DLS'        :     3},

                   # Turbo lines
                   'JPsiTurbo' :          {'MassWindow' :   150 * MeV,
                                              'Pt'         :     0 * MeV,
                                              'MuPt'       :   500 * MeV,
                                              'VertexChi2' :    25,
                                              'PIDCut'     : "MINTREE('mu-' == ABSID, TRCHI2DOF) <10" },  #overwrite the PID, keeps the interface
                   'Psi2STurbo' :         {'MassWindow' :   150 * MeV,
                                              'Pt'         :     0 * MeV,
                                              'MuPt'       :   500 * MeV,
                                              'VertexChi2' :    25, 
                                              'PIDCut'     : "MINTREE('mu-' == ABSID, TRCHI2DOF) <10" }, #overwrite the PID
                   'Psi2SLowPTTurbo' :    {'MassWindow' :   150 * MeV,
                                              'PtMax'      :  2500 * MeV,
                                              'MuPt'       :   500 * MeV,
                                              'VertexChi2' :    25,
                                              'PIDCut'     : "MINTREE('mu-' == ABSID, TRCHI2DOF) <10" }, #overwrite the PID
                   'BTurbo' :             {'MinMass'    :   4700 * MeV,
                                           'VertexChi2' :    25},
                   
                   # Prescales #To do
                   'Prescale'   : {'Hlt2DiMuon'        :  1.,   
                                   'Hlt2DiMuonJPsi'    :  1.,
                                   'Hlt2DiMuonPsi2S'   :  1.}
                   }
                  })
        return d
    
    

