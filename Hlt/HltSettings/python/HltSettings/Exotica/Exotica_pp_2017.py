from GaudiKernel.SystemOfUnits import GeV, MeV, picosecond, mm

class Exotica_pp_2017:
    """
    Threshold settings for Hlt2 exotica lines for 2016 25 ns data-taking.

    WARNING :: DO NOT EDIT WITHOUT PERMISSION OF THE AUTHORS

    @author P. Ilten and M. Williams (minor tweaks K. Dreimanis and C. Weisser)
    @date 2017-05-01
    """


    __all__ = ( 'ActiveHlt2Lines' )

    def ActiveHlt2Lines(self) :
        """Returns a list of active lines."""

        lines = [
                 'Hlt2ExoticaDisplPhiPhi',
                 'Hlt2ExoticaDiMuonNoIPTurbo',
                 'Hlt2ExoticaDiMuonNoIPSSTurbo',
                 'Hlt2ExoticaQuadMuonNoIP',
                 'Hlt2ExoticaDisplDiMuon',
                 'Hlt2ExoticaDisplDiMuonNoPoint',
                 'Hlt2ExoticaPrmptDiMuonTurbo',
                 #'Hlt2ExoticaPrmptDiMuonJpsiMassTurbo',
                 'Hlt2ExoticaPrmptDiMuonSSTurbo',
                 #'Hlt2ExoticaPrmptDiMuonSSJpsiMassTurbo',
                 #'Hlt2ExoticaPrmptDiMuonHighMass',
                 'Hlt2ExoticaDisplDiE',
                 'Hlt2ExoticaRHNu',
                 'Hlt2ExoticaLFVPrmptTurbo',
                 'Hlt2ExoticaLFVPrmptSSTurbo',
                 'Hlt2ExoticaLFVPrmpt'                 
                 #'Hlt2ExoticaRHNuHighMass',
                 #'Hlt2ExoticaDiRHNu',
                 #'Hlt2ExoticaD2PiEta_Eta2PiPiXee', # this line under development
                ]

        return lines
    
    def Thresholds(self) :
        """Return the trigger thresholds."""

        d = {}

        from Hlt2Lines.Exotica.Lines import ExoticaLines
        d.update( 
            { ExoticaLines : {
                'Prescale' : {'Hlt2ExoticaDiMuonNoIPTurbo'   : 0.01,
                              'Hlt2ExoticaDiMuonNoIPSSTurbo' : 0.01,
                              'Hlt2ExoticaPrmptDiMuonJpsiMassTurbo':  1.0,
                              'Hlt2ExoticaLFVPrmptSSTurbo'   :  0.1},

                'Common' : {'GhostProb' : 0.3},
                'ExoticaDisplPhiPhi' : {'TisTosSpec' : "Hlt1IncPhi.*Decision%TOS" ,
                                        'KPT'  : 500*MeV,
                                        'KIPChi2' : 16,
                                        'KProbNNk' : 0.1,
                                        'PhiPT'  : 1000*MeV,
                                        'PhiMassWindow' : 20*MeV,
                                        'VChi2' : 10,
                                        'FDChi2' : 45},
                'ExoticaSharedDiMuonNoIP' : {'MuPT' : 500*MeV,
                                             'MuP' : 10000*MeV,
                                             'MuProbNNmu' : 0.5,
                                             'DOCA' : 0.5*mm,
                                             'VChi2' : 10},
                'ExoticaDiMuonNoIPTurbo' : {'TisTosSpec' : None,
                                            'PT' : 1000*MeV},
                'ExoticaDiMuonNoIPSSTurbo' : {'TisTosSpec' : [],
                                               'MuPT' : 500*MeV,
                                               'MuP' : 10000*MeV,
                                               'MuProbNNmu' : 0.5,
                                               'DOCA' : 0.5*mm,
                                               'VChi2' : 10,
                                               'PT' : 1000*MeV},
                'ExoticaQuadMuonNoIP' : {'TisTosSpec' : None,
                                         'PT' : 0,
                                         'VChi2' : 10},       
                'ExoticaDisplDiMuon' : {'TisTosSpec' : None,
                                        'MSwitch' : 500*MeV,
                                        'MuProbNNmu_lowmass' : 0.5,
                                        'MuProbNNmu_highmass' : 0.8,
                                        'MuIPChi2_lowmass' : 4,
                                        'MuIPChi2_highmass' : 9,
                                        'PT' : 1000*MeV,                                   
                                        'IPChi2' : 16, 
                                        'FDChi2' : 4},
                'ExoticaDisplDiMuonNoPoint' : {'TisTosSpec' : None,
                                               'MSwitch' : 500*MeV,
                                               'MuProbNNmu_lowmass' : 0.5,
                                               'MuProbNNmu_highmass' : 0.8,
                                               'MuIPChi2_lowmass' : 4,
                                               'MuIPChi2_highmass' : 25,
                                               'PT' : 1000*MeV,                                   
                                               'FDChi2' : 16},
                'ExoticaPrmptDiMuonTurbo' : {'TisTosSpec' : "Hlt1DiMuonNoIPDecision%TOS",
                                             'MuPT' : 500*MeV,
                                             'MuPTPROD' : 1*GeV*GeV,
                                             'MuP' : 10000*MeV,
                                             'M_switch_ab' : 740*MeV,
                                             'M_switch_bc' : 1100*MeV,
                                             'M_switch_cd' : 3000*MeV,
                                             'M_switch_de' : 3200*MeV,
                                             'M_switch_ef' : 9000*MeV,
                                             'MuProbNNmu_a' : 0.8,
                                             'MuProbNNmu_b' : 0.8,
                                             'MuProbNNmu_c' : 0.95,
                                             'MuProbNNmu_d' : 2.0, #Always False
                                             'MuProbNNmu_e' : 0.95,
                                             'MuProbNNmu_f' : 0.9,
                                             'MuIPChi2' : 6,
                                             'PT' : 1000*MeV,
                                             'FDChi2' : 45},
                'ExoticaPrmptDiMuonJpsiMassTurbo' : {'TisTosSpec' : "Hlt1DiMuonNoIPDecision%TOS",
                                             'MuPT' : 500*MeV,
                                             'MuPTPROD' : 1*GeV*GeV,
                                             'MuP' : 10000*MeV,
                                             'M_switch_ab' : 740*MeV,
                                             'M_switch_bc' : 1100*MeV,
                                             'M_switch_cd' : 3000*MeV,
                                             'M_switch_de' : 3200*MeV,
                                             'M_switch_ef' : 9000*MeV,
                                             'MuProbNNmu_a' : 2.0, #Always False
                                             'MuProbNNmu_b' : 2.0, #Always False
                                             'MuProbNNmu_c' : 2.0, #Always False
                                             'MuProbNNmu_d' : 0.9,
                                             'MuProbNNmu_e' : 2.0, #Always False
                                             'MuProbNNmu_f' : 2.0, #Always False
                                             'MuIPChi2' : 6,
                                             'PT' : 1000*MeV,
                                             'FDChi2' : 45},
                'ExoticaPrmptDiMuonSSTurbo' : {'TisTosSpec' : "Hlt1DiMuonNoIPSSDecision%TOS",
                                               'MuPT' : 500*MeV,
                                               'MuPTPROD' : 1*GeV*GeV,
                                               'MuP' : 10000*MeV,
                                               'M_switch_ab' : 740*MeV,
                                               'M_switch_bc' : 1100*MeV,
                                               'M_switch_cd' : 3000*MeV,
                                               'M_switch_de' : 3200*MeV,
                                               'M_switch_ef' : 9000*MeV,
                                               'MuProbNNmu_min' : 0.8,
                                               'MuProbNNmu_a' : 0.8,
                                               'MuProbNNmu_b' : 0.8,
                                               'MuProbNNmu_c' : 0.95,
                                               'MuProbNNmu_d' : 2.0, #Always False
                                               'MuProbNNmu_e' : 0.95,
                                               'MuProbNNmu_f' : 0.9,
                                               'MuIPChi2' : 6,
                                               'DOCA' : 0.5*mm,
                                               'VChi2' : 10,
                                               'PT' : 1000*MeV,
                                               'FDChi2' : 45},
                'ExoticaPrmptDiMuonSSJpsiMassTurbo' : {'TisTosSpec' : "Hlt1DiMuonNoIPSSDecision%TOS",
                                               'MuPT' : 500*MeV,
                                               'MuPTPROD' : 1*GeV*GeV,
                                               'MuP' : 10000*MeV,
                                               'M_switch_ab' : 740*MeV,
                                               'M_switch_bc' : 1100*MeV,
                                               'M_switch_cd' : 3000*MeV,
                                               'M_switch_de' : 3200*MeV,
                                               'M_switch_ef' : 9000*MeV,
                                               'MuProbNNmu_min' : 0.9,
                                               'MuProbNNmu_a' : 2.0, #Always False
                                               'MuProbNNmu_b' : 2.0, #Always False
                                               'MuProbNNmu_c' : 2.0, #Always False
                                               'MuProbNNmu_d' : 0.9,
                                               'MuProbNNmu_e' : 2.0, #Always False
                                               'MuProbNNmu_f' : 2.0, #Always False
                                               'MuIPChi2' : 6,
                                               'DOCA' : 0.5*mm,
                                               'VChi2' : 10,
                                               'PT' : 1000*MeV,
                                               'FDChi2' : 45},
                'ExoticaPrmptDiMuonHighMass' : {'TisTosSpec' : "Hlt1DiMuonHighMassDecision%TOS",
                                                'MuPT' : 500*MeV,
                                                'MuP' : 10000*MeV,
                                                'M'          : 3200*MeV,
                                                'M_switch_ab' : 740*MeV,
                                                'M_switch_bc' : 1100*MeV,
                                                'M_switch_cd' : 3000*MeV,
                                                'M_switch_de' : 3200*MeV,
                                                'M_switch_ef' : 9000*MeV,
                                                'MuProbNNmu_a' : 0.8,
                                                'MuProbNNmu_b' : 0.8,
                                                'MuProbNNmu_c' : 0.95,
                                                'MuProbNNmu_d' : 2.0, #Always False
                                                'MuProbNNmu_e' : 0.95,
                                                'MuProbNNmu_f' : 0.9,
                                                'MuIPChi2' : 6,
                                                'PT' : 1000*MeV,
                                                'FDChi2' : 45},

                'ExoticaSharedLFVPrmpt' : {'TisTosSpec' : None,
                                           'PT' : 500*MeV,
                                           'P' : 10000*MeV,

                                           'ProbNNmu' : 0.5,
                                           'ProbNNe' : 0.25,
                                           'IPChi2' : 1,
                                           'VChi2' : 5,
                                           'XIPChi2' : 1,
                                           'M' : 0,
                                           'FDChi2' : 1},

                'ExoticaLFVPrmptTurbo' : {'TisTosSpec' :      None,
                                          'M'          : 2000*MeV},

                'ExoticaLFVPrmptSSTurbo' : {'TisTosSpec' :      None,
                                            'PT'         :   500*MeV,
                                            'P'          : 10000*MeV,
                                            'ProbNNmu'   :       0.5,
                                            'ProbNNe'    :      0.25,
                                            'IPChi2'     :         1,
                                            'VChi2'      :         5,
                                            'XIPChi2'    :         1,
                                            'M'          :  2000*MeV,
                                            'FDChi2'     :         1},

                'ExoticaLFVPrmpt' : {'TisTosSpec' : None,
                                     'M' : 5000*MeV},              
                
                'ExoticaSharedDiENoIP' : {'EPT' : 500*MeV,
                                              'EP' : 5000*MeV,
                                              'EProbNNe' : 0.1,
                                              'DOCA' : 0.5*mm,
                                              'MM' : 0*MeV,
                                              'VChi2' : 25},
                'ExoticaDisplDiE' : {'TisTosSpec' : "Hlt1TrackMVA.*Decision%TOS",
                                         'EProbNNe' : 0.1,
                                         'EIPChi2' : 4,
                                         'PT' : 500*MeV,
                                         'IPChi2' : 32,
                                         'FDChi2' : 0},
                'ExoticaSharedRHNu' : {'TisTosSpec' : None,
                                       'PT' : 500*MeV,
                                       'P' : 10000*MeV,                                              
                                       'ProbNNmu' : 0.7,
                                       'IPChi2' : 16,
                                       'VChi2' : 10,
                                       'XIPChi2' : 16,
                                       'M' : 0,
                                       'TAU' : 1*picosecond,
                                       'FDChi2' : 45},
                'ExoticaRHNu' : {'TisTosSpec' : None,                 
                                 'M' : 0,
                                'TAU' : 1*picosecond},
                'ExoticaRHNuHighMass' : {'TisTosSpec' : None,
                                         'ProbNNmu' : 0.5,
                                         'M' : 5000*MeV,
                                         'TAU' : 1*picosecond},
                'ExoticaDiRHNu' : {'TisTosSpec' : None,                 
                                   'PT' : 0,
                                   'VChi2' : 10},
                'ExoticaSharedDisplacedDiE' : {'EPT' : 200*MeV,
                                               'EP' : 2000*MeV,
                                               'EIPChi2' : 9,
                                               'EProbNNe' : 0.1,
                                               'M' : 0*MeV,
                                               'PT' : 0*MeV,                                   
                                               'FDChi2' : 30,
                                               'VChi2' : 100},
                'ExoticaSharedDisplacedEta2PiPiXee' :  {'PiPT' : 200*MeV,
                                                        'PiP' : 2000*MeV,
                                                        'PiIPChi2' : 9,
                                                        'PiPIDK' : 0,
                                                        'DM' : 200*MeV,
                                                        'PT' : 500*MeV,                                   
                                                        'FDChi2' : 30,
                                                        'VChi2' : 100},
                'ExoticaD2PiEta_Eta2PiPiXee' : {'TisTosSpec' : None,
                                                'PiPT' : 500*MeV,
                                                'PiP'  : 5000*MeV,
                                                'PiIPChi2' : 9,
                                                'PiPIDK' : 0,
                                                'PT' : 1000*MeV,
                                                'MMIN' : 1700*MeV,
                                                'MMAX' : 2100*MeV,
                                                'VChi2' : 10,
                                                'FDChi2' : 30}                                                 
            }
          }
        )

        return d
