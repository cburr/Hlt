class HeavyIons_pHe_2016(object):
    def ActiveHlt2Lines(self):
        lines = [
            'Hlt2SMOGDiMuon',
            'Hlt2SMOGPassThrough',
        ]
        return lines

    def Thresholds(self):
        from Hlt2Lines.HeavyIons.Lines import HeavyIonsLines
        thresholds = {
            HeavyIonsLines: {
                'SMOGDiMuon': {
                    'HLT1': "HLT_PASS_RE('^Hlt1SMOGDiMuon.*Decision$')",
                    'VoidFilter': '',
                },
                'SMOGPassThrough': {
                    'HLT1': "HLT_PASS_RE('Hlt1(BE|EB).*Decision')",
                    'VoidFilter': '',
                },
            },
        }
        return thresholds
