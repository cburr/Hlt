from GaudiKernel.SystemOfUnits import MeV, GeV, picosecond, mm

class Jets_25ns_Draft2016:
   """
   Threshold settings for Hlt2 jets lines for 2016 25 ns data-taking.
   
   WARNING :: DO NOT EDIT WITHOUT PERMISSION OF THE AUTHORS
   
   @author P. Ilten and M. Williams
   @date 2016-02-13
   """
   
   __all__ = ( 'ActiveHlt2Lines' )
   
   def ActiveHlt2Lines(self) :
      """Returns a list of active lines."""
      
      lines = [
         'Hlt2JetsDiJetLowPt'    ,
         'Hlt2JetsDiJetMVLowPt'  ,
         'Hlt2JetsDiJetMuMuLowPt',
         'Hlt2JetsDiJetSVLowPt'  ,
         'Hlt2JetsDiJetSVMuLowPt',
         'Hlt2JetsDiJetSVSVLowPt',
         'Hlt2JetsJetLowPt'      ,
         #'Hlt2JetsJetDHLowPt'    ,
         'Hlt2JetsJetMuLowPt'    ,
         'Hlt2JetsJetSVLowPt'    ,
         'Hlt2JetsDiJet'    , #May
         #'Hlt2JetsDiJetMV'  , 
         'Hlt2JetsDiJetMuMu', #May
         'Hlt2JetsDiJetSV'  , #May
         'Hlt2JetsDiJetSVMu', #May
         'Hlt2JetsDiJetSVSV', #May
         #'Hlt2JetsJet'      ,
         #'Hlt2JetsJetDH'    ,
         #'Hlt2JetsJetMu'    ,
         #'Hlt2JetsJetSV'    ,
         #'Hlt2JetsDiJetHighPt'  ,
         #'Hlt2JetsDiJetSVHighPt',
         #'Hlt2JetsJetHighPt'    ,
         #'Hlt2JetsJetDHHighPt'  ,
         #'Hlt2JetsJetMuHighPt'  ,
         #'Hlt2JetsJetSVHighPt'  
         ]
      
      return lines
   
   def Thresholds(self) :
      """Return the trigger thresholds."""
      
      d = {}
      
      from Hlt2Lines.Jets.Lines import JetsLines
      d.update({
            JetsLines : {
        'Prescale': {
            'Hlt2JetsDiJetLowPt'    : 1e-04,
            'Hlt2JetsDiJetMVLowPt'  : 1e-02,
            'Hlt2JetsDiJetMuMuLowPt': 1e-01,
            'Hlt2JetsDiJetSVLowPt'  : 1e-03,
            'Hlt2JetsDiJetSVMuLowPt': 1e-02,
            'Hlt2JetsDiJetSVSVLowPt': 1e-01,
            'Hlt2JetsJetLowPt'      : 1e-04,
            'Hlt2JetsJetDHLowPt'    : 1e-03,
            'Hlt2JetsJetMuLowPt'    : 1e-03,
            'Hlt2JetsJetSVLowPt'    : 1e-03,
            'Hlt2JetsDiJet'         : 1e-03, #May
            'Hlt2JetsDiJetMV'       : 1e-04,
            'Hlt2JetsDiJetMuMu'     : 1.0,  #May
            'Hlt2JetsDiJetSV'       : 1e-02,#May
            'Hlt2JetsDiJetSVMu'     : 1.0,  #May
            'Hlt2JetsDiJetSVSV'     : 1.0,  #May
            'Hlt2JetsJet'           : 1e-03, 
            'Hlt2JetsJetDH'         : 1e-04, 
            'Hlt2JetsJetMu'         : 1e-03,
            'Hlt2JetsJetSV'         : 1e-03,
            
            'Hlt2JetsDiJetHighPt'   : 1e-04,
            'Hlt2JetsDiJetSVHighPt' : 1e-04,
            'Hlt2JetsJetHighPt'     : 1e-04,
            'Hlt2JetsJetDHHighPt'   : 1e-04,
            'Hlt2JetsJetMuHighPt'   : 1e-04,
            'Hlt2JetsJetSVHighPt'   : 1e-04 
            },
        'Postscale': {},
        'Common': {
            'D_TOS'        : 'Hlt1((Two)?Track(Muon)?MVA(Loose)?|TrackMuon)Decision%TOS',
            'D_MASS'       : 50*MeV,
            'GHOSTPROB'    : 0.2,
            'DPHI'         : 0,
            'SV_VCHI2'     : 10,
            'SV_TRK_PT'    : 500*MeV,
            'SV_TRK_IPCHI2': 16,
            'SV_FDCHI2'    : 25,
            'MU_PT'        : 1000*MeV,
            'MU_PROBNNMU'  : 0.5,
            'JET_PT'       : 17*GeV,
            },
        'JetBuilder': {
            'JetPtMin'  : 5*GeV,
            'JetInfo'   : False,
            'JetEcPath' : ''
            },
        'JetsDiJetLowPt'    : {'JET_PT': 10*GeV},
        'JetsDiJetMVLowPt'  : {'JET_PT': 10*GeV},
        'JetsDiJetMuMuLowPt': {'JET_PT': 10*GeV},
        'JetsDiJetSVLowPt'  : {'JET_PT': 10*GeV},
        'JetsDiJetSVMuLowPt': {'JET_PT': 10*GeV},
        'JetsDiJetSVSVLowPt': {'JET_PT': 10*GeV},
        'JetsJetLowPt'      : {'JET_PT': 10*GeV},
        'JetsJetDHLowPt'    : {'JET_PT': 10*GeV},
        'JetsJetMuLowPt'    : {'JET_PT': 10*GeV},
        'JetsJetSVLowPt'    : {'JET_PT': 10*GeV},
        'JetsDiJetHighPt'   : {'JET_PT': 60*GeV},
        'JetsDiJetSVHighPt' : {'JET_PT': 50*GeV},
        'JetsJetHighPt'     : {'JET_PT': 100*GeV},
        'JetsJetDHHighPt'   : {'JET_PT': 40*GeV},
        'JetsJetMuHighPt'   : {'JET_PT': 60*GeV},
        'JetsJetSVHighPt'   : {'JET_PT': 70*GeV}
        }})
      
      return d
