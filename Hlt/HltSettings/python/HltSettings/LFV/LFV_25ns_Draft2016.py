# =============================================================================
# @file   LFV_25ns_Draft2016.py
# @author Carla Marin (carla.marin@cern.ch)
# @date   15.05.2016
# =============================================================================
"""Threshold settings for Hlt2 LFV lines for 2016.
"""


class LFV_25ns_Draft2016(object):
    """Threshold settings for Hlt2 LFV lines for 2016."""
    __all__ = ('ActiveHlt2Lines', 'Thresholds')

    def ActiveHlt2Lines(self):
        """Returns a list of active lines."""

        lines = ['Hlt2LFVJpsiMuETurbo',
                 'Hlt2LFVPhiMuETurbo',
                 'Hlt2LFVPromptPhiMuETurbo',
                 'Hlt2LFVUpsilonMuETurbo']

        return lines

    def Thresholds(self):
        """Set thresholds for the lines."""

        from GaudiKernel.SystemOfUnits import MeV
        m_jpsi = 3096
        m_phi = 1020
        
        thresholds = {'Prescale': {},
                      'Common': {},
                      'JpsiMuE': {'CombMassHigh': (m_jpsi + 550) * MeV,
                                  'CombMassLow': (m_jpsi - 1100) * MeV,
                                  'ElectronProbNn': 0.93,
                                  'ElectronTrChi2DoF': 3,
                                  'ElectronTrGhostProb': 0.2,
                                  'MassHigh': (m_jpsi + 500) * MeV,
                                  'MassLow': (m_jpsi - 1000) * MeV,
                                  'MuonProbNn': 0.9,
                                  'MuonTrChi2DoF': 3,
                                  'MuonTrGhostProb': 0.1,
                                  'VertexChi2DoF': 3},

                      'PhiMuE' : {'CombMassHigh': (m_phi + 550) * MeV,
                                  'CombMassLow': (m_phi - 850) * MeV,
                                  'ElectronProbNn': 0.97,
                                  'ElectronTrChi2DoF': 3,
                                  'ElectronTrGhostProb': 0.2, 
                                  'MassHigh': (m_phi + 500) * MeV,
                                  'MassLow': (m_phi - 800) * MeV,
                                  'MuonProbNn': 0.95,
                                  'MuonTrChi2DoF': 3,
                                  'MuonTrGhostProb': 0.15,
                                  'VertexChi2DoF': 3,
                                  'IPCHI2_Min': 16,
                                  'BPVVDCHI2_Min': 100},
        
                      'PromptPhiMuE' : {'CombMassHigh': (m_phi + 550) * MeV,
                                        'CombMassLow': (m_phi - 850) * MeV,
                                        'ElectronProbNn': 0.97,
                                        'ElectronTrChi2DoF': 3,
                                        'ElectronTrGhostProb': 0.2, 
                                        'MassHigh': (m_phi + 500) * MeV,
                                        'MassLow': (m_phi - 800) * MeV,
                                        'MuonProbNn': 0.95,
                                        'MuonTrChi2DoF': 3,
                                        'MuonTrGhostProb': 0.15,
                                        'VertexChi2DoF': 3},
        
                      'UpsilonMuE'  : {'CombMassHigh': (13000) * MeV,
                                       'CombMassLow': (7000) * MeV,
                                       'ElectronProbNn': 0.2,
                                       'ElectronTrChi2DoF': 3,
                                       'ElectronTrGhostProb': 0.3,
                                       'MassHigh': (13000) * MeV,
                                       'MassLow': (7000) * MeV,
                                       'MuonProbNn': 0.2,
                                       'MuonTrChi2DoF': 3,
                                       'MuonTrGhostProb': 0.2,
                                       'VertexChi2DoF': 3},
        
                      'SpdCut'      : {'NSPD': 200},
                      'PromptSpdCut': {'NSPD': 100}
                      }
        

        from Hlt2Lines.LFV.Lines import LFVLines
        
        return {LFVLines: thresholds}

# EOF
