from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from Utilities.Utilities import update_thresholds


def _get_conf(folder, suffix):
    import importlib
    conf = folder + suffix
    module = importlib.import_module("HltSettings.{0}.{1}".format(folder, conf))
    return getattr(module, conf)()


class Physics_pp_2017(object):
    """Settings for 25ns physics in 2017."""

    def __init__(self):
        from Hlt1Lines.Hlt1SharedParticles import Hlt1SharedParticles
        Hlt1SharedParticles(**{
            'PreFitPT'        : 570,
            'PreFitP'         : 4750,
            'PT'              : 600,     # MeV
            'P'               : 5000,
            'TrackChi2DOF'    : 4.,
            'TrGP'            : 999.,  # ghost prob
        })

    def verifyType(self, ref):
        # verify self.ActiveLines is still consistent with
        # our types self.ActiveHlt2Lines and self.ActiveHlt1Lines...
        # so we can force that classes which inherit from us
        # and overrule either ActiveHlt.Lines also overrule
        # HltType...
        if (self.ActiveHlt1Lines() != ref.ActiveHlt1Lines(self) or
           self.ActiveHlt2Lines() != ref.ActiveHlt2Lines(self)):
            raise RuntimeError('Must update HltType when modifying ActiveHlt.Lines()')

    def L0TCK(self):
        return '0x1609'

    def HltType(self):
        self.verifyType(Physics_pp_2017)
        return 'Physics_pp_2017'

    def SubDirs(self):
        return {'pp_2017': [
            'CcDiHadron', 'DPS', 'Bc2JpsiX', 'Bottomonium', 'B2HH',
            'B2Kpi0', 'BHad', 'CharmHad', 'DiElectron', 'Exotica',
            'Jets', 'LFV', 'LowMult', 'PID', 'Phi', 'Radiative',
            'RareCharm', 'EW', 'Topo', 'TrackEffDiMuon', 'DisplVertices',
            'XcMuXForTau', 'Technical', 'TrackEff', 'DiMuon',
            'SingleMuon', 'Majorana', 'RareStrange', 'TriMuon',
            'TrackEffElectron','Strange', 'SLB', 'StrangeLFV', 
            'CaloPID','Hb2V0V0']}
    
    def Thresholds(self):
        """Return a dictionary of cuts."""

        from Hlt1Lines.Hlt1TrackLines           import Hlt1TrackLinesConf
        from Hlt1Lines.Hlt1MuonLines            import Hlt1MuonLinesConf
        from Hlt1Lines.Hlt1ElectronLines        import Hlt1ElectronLinesConf
        from Hlt1Lines.Hlt1L0Lines              import Hlt1L0LinesConf
        from Hlt1Lines.Hlt1MBLines              import Hlt1MBLinesConf
        from Hlt1Lines.Hlt1CommissioningLines   import Hlt1CommissioningLinesConf
        from Hlt1Lines.Hlt1BeamGasLines         import Hlt1BeamGasLinesConf
        from Hlt1Lines.Hlt1MVALines             import Hlt1MVALinesConf
        from Hlt1Lines.Hlt1CalibTrackingLines   import Hlt1CalibTrackingLinesConf
        from Hlt1Lines.Hlt1LowMultLines         import Hlt1LowMultLinesConf
        from Hlt1Lines.Hlt1CalibRICHMirrorLines import Hlt1CalibRICHMirrorLinesConf
        from Hlt1Lines.Hlt1BottomoniumLines     import Hlt1BottomoniumLinesConf
        from Hlt1Lines.Hlt1ProtonLines          import Hlt1ProtonLinesConf

        thresholds = { Hlt1TrackLinesConf :   {  'Muon_TrNTHits'     : 0 #OFF
                                               , 'Muon_Velo_NHits'   : 0 #OFF
                                               , 'Muon_Velo_Qcut'    : 999 #OFF
                                               , 'Muon_PreFitPT'     : 1045.
                                               , 'Muon_PreFitP'      : 5700.
                                               , 'Muon_PT'           : 1100.
                                               , 'Muon_P'            : 6000.
                                               , 'Muon_TrGP'         : 0.2
                                               , 'Muon_IPChi2'       : 35.
                                               , 'Muon_TrChi2'       : 3.
                                               , 'Muon_GEC'          : 'Loose'
                                               , 'L0Channels'        : {'Muon'   : ('Muon', 'DiMuon')}
                                               , 'Priorities'        : { 'Muon'   : 5 }
                                               }
                       , Hlt1MVALinesConf :     {'DoTiming'                     : False,
                                                 'TrackMVA' :    {'TrChi2'      :     2.5,
                                                                'TrGP'        :  0.2,
                                                                'MinPT'       :  1000.  * MeV,
                                                                'MaxPT'       : 25000.  * MeV,
                                                                'MinIPChi2'   :     7.4,
                                                                'Param1'      :     1.0,
                                                                'Param2'      :     1.0,
                                                                'Param3'      :     1.1,
                                                                'GEC'         : 'Loose'},
                                               'TrackMVATight' :    {'TrChi2'      :     2.5,
                                                                     'TrGP'        :  0.2,
                                                                'MinPT'       :  1000.  * MeV,
                                                                'MaxPT'       : 25000.  * MeV,
                                                                'MinIPChi2'   :     7.4,
                                                                'Param1'      :     1.0,
                                                                'Param2'      :     1.0,
                                                                'Param3'      :     2.3,
                                                                'GEC'         : 'Loose'},
                                               'TrackMuonMVA' : {'MinPT'       :  1000.  * MeV,
                                                                 'MaxPT'       : 25000.  * MeV,
                                                                 'MinIPChi2'   :     7.4,
                                                                 'TrChi2'      :     2.5,
                                                                 'TrGP'        :     0.2,
                                                                 'Param1'      :     1.0,
                                                                 'Param2'      :     1.0,
                                                                 'Param3'      :     1.1,
                                                                 'GEC'         : 'Loose'},
                                               'TwoTrackMVA' : {'P'           :  5000. * MeV,
                                                                'PT'          :   600. * MeV,
                                                                'TrGP'        :  0.2,
                                                                'TrChi2'      :     2.5,
                                                                'IPChi2'      :     4.,
                                                                'MinMCOR'     :  1000. * MeV,
                                                                'MaxMCOR'     :   1e9  * MeV,
                                                                'MinETA'      :     2.,
                                                                'MaxETA'      :     5.,
                                                                'MinDirA'     :     0.,
                                                                'V0PT'        :  2000. * MeV,
                                                                'VxChi2'      :    10.,
                                                                'Threshold'   :     0.95,
                                                                'MvaVars'     : {'chi2'   : 'VFASPF(VCHI2)',
                                                                                 'fdchi2' : 'BPVVDCHI2',
                                                                                 'sumpt'  : 'SUMTREE(PT, ISBASIC, 0.0)',
                                                                                 'nlt16'  : 'NINTREE(ISBASIC & (BPVIPCHI2() < 16))'},
                                                                'Classifier'  : {'Type'   : 'MatrixNet',
                                                                                 'File'   : '$PARAMFILESROOT/data/Hlt1TwoTrackMVA.mx'},
                                                                'GEC'         : 'Loose'},
                                               'TwoTrackMVATight' : {'P'           :  5000. * MeV,
                                                                'PT'          :   500. * MeV,
                                                                     'TrGP'        :  0.2,
                                                                'TrChi2'      :     2.5,
                                                                'IPChi2'      :     4.,
                                                                'MinMCOR'     :  1000. * MeV,
                                                                'MaxMCOR'     :   1e9  * MeV,
                                                                'MinETA'      :     2.,
                                                                'MaxETA'      :     5.,
                                                                'MinDirA'     :     0.,
                                                                'V0PT'        :  2000. * MeV,
                                                                'VxChi2'      :    10.,
                                                                'Threshold'   :     0.97,
                                                                'MvaVars'     : {'chi2'   : 'VFASPF(VCHI2)',
                                                                                 'fdchi2' : 'BPVVDCHI2',
                                                                                 'sumpt'  : 'SUMTREE(PT, ISBASIC, 0.0)',
                                                                                 'nlt16'  : 'NINTREE(ISBASIC & (BPVIPCHI2() < 16))'},
                                                                'Classifier'  : {'Type'   : 'MatrixNet',
                                                                                 'File'   : '$PARAMFILESROOT/data/Hlt1TwoTrackMVA.mx'},
                                                                'GEC'         : 'Loose'},

                                               'L0Channels'  : {'TrackMVA'         : 'L0_DECISION_PHYSICS',
                                                                'TrackMuonMVA'     :  {'Muon','DiMuon'},
                                                                'TwoTrackMVA'      : 'L0_DECISION_PHYSICS',
                                                                'TrackMVATight'    : 'L0_DECISION_PHYSICS',
                                                                'TwoTrackMVATight' : 'L0_DECISION_PHYSICS' },
                                               'Priorities'  : {'TrackMVA'         : 1,
                                                                'TwoTrackMVA'      : 2,
                                                                'TrackMVATight'    : 3,
                                                                'TwoTrackMVATight' : 4,
                                                                'TrackMuonMVA'     : 5},
                                               }
                     , Hlt1ElectronLinesConf : { 'SingleElectronNoIP_P'          : 20000
                                               , 'SingleElectronNoIP_PT'         : 10000
                                               , 'SingleElectronNoIP_TrChi2'     :     3
                                               , 'SingleElectronNoIP_TrNTHits'   :     0 #OFF
                                               , 'SingleElectronNoIP_Velo_NHits' :     0 #OFF
                                               , 'SingleElectronNoIP_Velo_Qcut'  :   999 #OFF
                                               , 'SingleElectronNoIP_GEC'        : 'Loose'
                                               , 'L0Channels': { 'SingleElectronNoIP' : ( 'Electron', ) }
                                               }
                     , Hlt1MuonLinesConf :     { 'SingleMuonHighPT_P'       : 6000
                                               , 'SingleMuonHighPT_PT'      : 6000
                                               , 'SingleMuonHighPT_TrChi2'  :    3.
                                               , 'SingleMuonHighPT_TrGP'    :  0.2
                                               , 'SingleMuonHighPT_MuID'    : 'IsMuon'
                                               , 'SingleMuonHighPT_GEC'     : 'Loose'
                                               , 'SingleMuonHighPTNoMUID_P'       : 6000
                                               , 'SingleMuonHighPTNoMUID_PT'      : 15000
                                               , 'SingleMuonHighPTNoMUID_TrChi2'  :    3.
                                               , 'SingleMuonHighPTNoMUID_TrGP'    :  0.2
                                               , 'SingleMuonHighPTNoMUID_MuID'    : ''
                                               , 'SingleMuonHighPTNoMUID_GEC'     : 'Loose'
                                               , 'SingleMuonNoIP_P'         : 6000
                                               , 'SingleMuonNoIP_PT'        : 1400
                                               , 'SingleMuonNoIP_TrChi2'    :    3.
                                               , 'SingleMuonNoIP_TrGP'      :  0.2
                                               , 'SingleMuonNoIP_MuID'      : 'IsMuon'
                                               , 'SingleMuonNoIP_GEC'       : 'Loose'
                                               , 'DiMuonLowMass_VxDOCA'     :  0.2
                                               , 'DiMuonLowMass_VxChi2'     :   25
                                               , 'DiMuonLowMass_P'          : 3000
                                               , 'DiMuonLowMass_PT'         :    0
                                               , 'DiMuonLowMass_TrChi2'     :    4
                                               , 'DiMuonLowMass_TrGP'       :  0.2
                                               , 'DiMuonLowMass_M'          :    0.
                                               , 'DiMuonLowMass_IPChi2'     :    4.
                                               , 'DiMuonLowMass_GEC'        : 'Loose'
                                               , 'DiMuonLowMass_CFTracking' : False
                                               , 'DiMuonHighMass_VxDOCA'    :  0.2
                                               , 'DiMuonHighMass_VxChi2'    :   25
                                               , 'DiMuonHighMass_P'         : 6000
                                               , 'DiMuonHighMass_PT'        :  300
                                               , 'DiMuonHighMass_TrChi2'    :    4
                                               , 'DiMuonHighMass_TrGP'    :    999.
                                               , 'DiMuonHighMass_M'         : 2700
                                               , 'DiMuonHighMass_GEC'       : 'Loose'
                                               , 'DiMuonHighMass_CFTracking': False
                                               , 'DiMuonNoIPSS_VxDOCA'    :  0.3
                                               , 'DiMuonNoIPSS_VxChi2'    :    9.
                                               , 'DiMuonNoIPSS_P'         : 10000.
                                               , 'DiMuonNoIPSS_TrChi2'    :  3.
                                               , 'DiMuonNoIPSS_TrGP'      :  0.2
                                               , 'DiMuonNoIPSS_M'         :  0.
                                               , 'DiMuonNoIPSS_ShHits'    :  1
                                               , 'DiMuonNoIPSS_PTPROD'    :  1.*GeV*GeV
                                               , 'DiMuonNoIPSS_GEC'       : 'Loose'
                                               , 'DiMuonNoIPSS_CFTracking': False
                                               , 'DiMuonNoIP_VxDOCA'    :  0.3
                                               , 'DiMuonNoIP_VxChi2'    :    9.
                                               , 'DiMuonNoIP_P'         : 10000.
                                               , 'DiMuonNoIP_TrChi2'    :  3.
                                               , 'DiMuonNoIP_TrGP'      :  0.2
                                               , 'DiMuonNoIP_M'         :  0.
                                               , 'DiMuonNoIP_ShHits'    :  1
                                               , 'DiMuonNoIP_PTPROD'    :  1.*GeV*GeV
                                               , 'DiMuonNoIP_GEC'       : 'Loose'
                                               , 'DiMuonNoIP_CFTracking': False
                                               , 'DiMuonNoL0_VxDOCA'     :  0.2
                                               , 'DiMuonNoL0_VxChi2'     :   25.
                                               , 'DiMuonNoL0_P'          : 3000.
                                               , 'DiMuonNoL0_PT'         :   80.
                                               , 'DiMuonNoL0_IP'         :   0.3
                                               , 'DiMuonNoL0_TrChi2'     :    4.
                                               , 'DiMuonNoL0_TrGP'       :  0.2
                                               , 'DiMuonNoL0_M'          :    0.
                                               , 'DiMuonNoL0_IPChi2'     :    9.
                                               , 'DiMuonNoL0_GEC'        : 'Tight'
                                               , 'MultiMuonNoL0_P'          : 6000
                                               , 'MultiMuonNoL0_PT'         :  500
                                               , 'MultiMuonNoL0_TrChi2'     :    3.
                                               , 'MultiMuonNoL0_TrGP'       :  0.2
                                               , 'MultiMuonNoL0_GT'         :    2.5
                                               , 'MultiMuonNoL0_GEC'        : 'Loose'
                                               , 'MultiDiMuonNoIP_VxDOCA'     :  0.1
                                               , 'MultiDiMuonNoIP_VxChi2'     :    9.
                                               , 'MultiDiMuonNoIP_P'          : 10000.
                                               , 'MultiDiMuonNoIP_PT'         :  500.
                                               , 'MultiDiMuonNoIP_TrChi2'     :    3.
                                               , 'MultiDiMuonNoIP_TrGP'       :  0.2
                                               , 'MultiDiMuonNoIP_M'          :    0.
                                               , 'MultiDiMuonNoIP_MSS'        : 200000.
                                               , 'MultiDiMuonNoIP_IPChi2'     :    -1
                                               , 'MultiDiMuonNoIP_NMinDiMu'   :    1.
                                               , 'MultiDiMuonNoIP_GEC'        : 'Loose'
                                               , 'MultiDiMuonNoIP_CFTracking' : False
                                               ,'L0Channels'               : {'SingleMuonHighPT'       : ( 'Muon', 'MuonEW' ),
                                                                              'SingleMuonNoIP'         : ( 'Muon', ),
                                     									      'SingleMuonHighPTNoMUID' : 'L0_DECISION_PHYSICS',
                                                                              'DiMuonLowMass'          : ( 'Muon', 'DiMuon' ),
                                                                              'DiMuonHighMass'         : ( 'Muon', 'DiMuon' ),
                                                                              'DiMuonNoIP'             : ( 'Muon', 'DiMuon' ),
                                                                              'DiMuonNoIPSS'           : ( 'Muon', 'DiMuon' ),
                                                                              'MultiDiMuonNoIP'        : ( 'Muon', 'DiMuon' ),
                                                                              'DiMuonNoL0'             : 'L0_DECISION_PHYSICS',
                                                                              'MultiMuonNoL0'          : 'L0_DECISION_PHYSICS'
                                                                             }
                                               , 'Prescale'                 : { 'Hlt1NoPVPassThrough'        : 1.0,
                                                                                'Hlt1SingleMuonNoIP'         : 0.01,
                                                                                'Hlt1DiMuonNoL0'             : 1.0,
                                                                                'Hlt1DiMuonNoIPSS'           : 0.25,
                                                                                'Hlt1MultiMuonNoL0'          : 0.01,
                                                                                'Hlt1SingleMuonHighPTNoMUID' : 0.0005
                                                                                }
                                               , 'Priorities'               : { 'SingleMuonHighPT' : 8,
                                                                                'DiMuonLowMass'    : 7,
                                                                                'DiMuonHighMass'   : 6,
                                                                                'DiMuonNoL0'       : 9
                                                  }
                                               }
                       , Hlt1L0LinesConf :     {  'Postscale' : { 'Hlt1L0AnyRateLimited'       : 'RATE(1)'
                                                                , 'Hlt1L0AnyNoSPDRateLimited'  : 'RATE(1)'
                                                                }
                                               ,  'Prescale' : { 'Hlt1L0HighSumETJet' : 1
                                                               , 'Hlt1L0AnyNoSPD'     : 0.01
                                                               }
                                               }
                       , Hlt1BeamGasLinesConf: {
                            'Postscale': {
                                'Hlt1BeamGasBeam1': 1.,
                                'Hlt1BeamGasBeam2': 1.,
                                'Hlt1BeamGasBeam1VeloOpen': 1.,
                                'Hlt1BeamGasBeam2VeloOpen': 1.,
                                'Hlt1BeamGasHighRhoVertices': 'RATE(4)',
                            }
                                                }
                       , Hlt1CalibTrackingLinesConf :  { 'ParticlePT'            : 600     # MeV
                                                        ,'ParticleP'             : 4000    # MeV
                                                        ,'ParticlePT_LTUNB'      : 800     # MeV
                                                        ,'ParticleP_LTUNB'       : 4000    # MeV
                                                        ,'L0_DHH_LTUNB'          : ['Photon', 'Electron','Hadron','Muon']
                                                        ,'L0_DHH_LTUNB_Lines'    : ['CalibTrackingPiPi','CalibTrackingKPi','CalibTrackingKK']
                                                        ,'TrackCHI2DOF'          : 2       # dimensionless
                                                        ,'CombMaxDaughtPT'       : 900     # MeV 900
                                                        ,'CombAPT'               : 1800    # MeV 1200
                                                        ,'CombDOCA'              : 0.1     # mm
                                                        ,'CombVCHI2DOF'          : 10      # dimensionless
                                                        ,'CombVCHI2DOFLoose'     : 15      # dimensionless
                                                        ,'CombDIRA'              : 0.99    # dimensionless
                                                        ,'CombTAU'               : 0.25    # ps
                                                        ,'D0MassWinLoose'        : 100     # MeV
                                                        ,'D0MassWin'             : 60      # MeV
                                                        ,'B0MassWinLoose'        : 200     # MeV
                                                        ,'B0MassWin'             : 150     # MeV
                                                        ,'D0DetachedDaughtsIPCHI2': 9      # dimensionless
                                                        ,'D0DetachedIPCHI2'       : 9      # dimensionless
                                                        ,'BsPhiGammaMassMinLoose': 3350    # MeV
                                                        ,'BsPhiGammaMassMaxLoose': 6900    # MeV
                                                        ,'BsPhiGammaMassMin'     : 3850    # MeV
                                                        ,'BsPhiGammaMassMax'     : 6400    # MeV
                                                        ,'PhiMassWinLoose'       : 50      # MeV
                                                        ,'PhiMassWin'            : 30      # MeV
                                                        ,'PhiMassWinTight'       : 20      # MeV
                                                        ,'PhiPT'                 : 1800    # MeV
                                                        ,'PhiPTLoose'            : 800     # MeV
                                                        ,'PhiSumPT'              : 3000    # MeV
                                                        ,'PhiIPCHI2'             : 16      # dimensionless
                                                        ,'B0SUMPT'               : 4000    # MeV
                                                        ,'B0PT'                  : 1000    # MeV
                                                        ,'GAMMA_PT_MIN'          : 2000    # MeV
                                                        ,'Velo_Qcut'             : 999     # OFF
                                                        ,'TrNTHits'              : 0       # OFF
                                                        ,'ValidateTT'            : False

                                                        ,'Prescale'  : { 'Hlt1CalibTrackingKPi': 0.2 }
                                                       }
                       , Hlt1CommissioningLinesConf : { 'Postscale' : { 'Hlt1Incident'     : 'RATE(1)'
                                                                      , 'Hlt1Tell1Error'   : 'RATE(10)'
                                                                      , 'Hlt1ErrorEvent'   : 'RATE(0.01)'
                                                                      , 'Hlt1NZSVelo'      : 'RATE(1)'
                                                                      , 'Hlt1GEC.*'        : 'RATE(1)'
                                                                      , 'Hlt1VeloClosingMicroBias' : 'RATE(500)'
                                                                      }
                                                      }
                       # micro bias lines switched off for high mu physics running
                       , Hlt1MBLinesConf :     { 'Prescale' : { 'Hlt1MBMicroBiasVelo'                : 0
                                                              , 'Hlt1MBMicroBiasTStation'            : 0
                                                              , 'Hlt1MBMicroBiasVeloRateLimited'     : 0
                                                              , 'Hlt1MBMicroBiasTStationRateLimited' : 0 
                                                              , 'Hlt1MBNoBias'                       : 1 }
                                               , 'MaxNoBiasRate' : 1000000.
                                               }
                       , Hlt1CalibRICHMirrorLinesConf : { 'Prescale' : { 'Hlt1CalibHighPTLowMultTrks'     : 0.0001,
                                                                          'Hlt1CalibRICHMirrorRICH1'       : 0.093,
                                                                          'Hlt1CalibRICHMirrorRICH2'       : 0.331}
                                                          , 'DoTiming' : False
                                                          , 'R2L_PreFitPT'    : 475. * MeV
                                                          , 'R2L_PreFitP'     : 38000. * MeV
                                                          , 'R2L_PT'       : 500. * MeV
                                                          , 'R2L_P'        : 40000. * MeV
                                                          , 'R2L_MinETA'   : 2.65
                                                          , 'R2L_MaxETA'   : 2.80
                                                          , 'R2L_Phis'     : [(-2.59, -2.49), (-0.65, -0.55), (0.55, 0.65), (2.49, 2.59)]
                                                          , 'R2L_TrChi2'   : 2.
                                                          , 'R2L_MinTr'    : 0.5
                                                          , 'R2L_GEC'      : 'Loose'
                                                          , 'R1L_PreFitPT' : 475. * MeV
                                                          , 'R1L_PreFitP'  : 19000. * MeV
                                                          , 'R1L_PT'       : 500. * MeV
                                                          , 'R1L_P'        : 20000. * MeV
                                                          , 'R1L_MinETA'   : 1.6
                                                          , 'R1L_MaxETA'   : 2.04
                                                          , 'R1L_Phis'     : [(-2.65, -2.30 ), (-0.80, -0.50), (0.50, 0.80), (2.30, 2.65)]
                                                          , 'R1L_TrChi2'   : 2.
                                                          , 'R1L_MinTr'    : 0.5
                                                          , 'R1L_GEC'      : 'Loose'
                                                          , 'LM_PreFitPT'  : 475. * MeV
                                                          , 'LM_PreFitP'   : 950. * MeV
                                                          , 'LM_PT'    : 500. * MeV
                                                          , 'LM_P'     : 1000. * MeV
                                                          , 'LM_TrChi2': 2.
                                                          , 'LM_MinTr' : 1
                                                          , 'LM_MaxTr' : 40
                                                          , 'LM_GEC'   : 'Loose'
                                                          }
                       , Hlt1BottomoniumLinesConf : { 'PhiDauPT'             : 650     # MeV
                                                    , 'PhiDauP'              : 3000    # MeV
                                                    , 'PhiDauCHI2DOF'        : 3       # dimensionless
                                                    , 'PhiDauIPCHI2'         : 12
                                                    , 'PhiDauTrGP'           : 0.2
                                                    , 'PhiCombMaxDaughtPT'   : 1500    # MeV
                                                    , 'PhiCombAPT'           : 1000    # MeV
                                                    , 'PhiCombDOCA'          : 0.1     # mm
                                                    , 'PhiCombVCHI2DOFLoose' : 10      # dimensionless
                                                    , 'PhiMassWinLoose'      : 50      # MeV
                                                    , 'PhiMassWin'           : 30      # MeV
                                                    , 'PhiMassWinTight'      : 30      # MeV
                                                    , 'PhiPT'                : 1200    # MeV
                                                    , 'PhiPTLoose'           : 1000    # MeV
                                                    , 'PhiSumPT'             : 1000    # MeV
                                                    , 'Bb2PhiPhiAMassMin'    :  8500    # MeV
                                                    , 'Bb2PhiPhiAMassMax'    : 11000    # MeV
                                                    , 'Bb2PhiPhiMassMin'     :  8900    # MeV
                                                    , 'Bb2PhiPhiMassMax'     : 10500    # MeV
                                                    , 'KstDauPT'             : 1500    # MeV
                                                    , 'KstDauP'              : 5000    # MeV
                                                    , 'KstDauCHI2DOF'        : 2       # dimensionless
                                                    , 'KstDauIPCHI2'         : 12
                                                    , 'KstDauTrGP'           : 0.2
                                                    , 'KstCombMaxDaughtPT'   : 2000    # MeV
                                                    , 'KstCombAPT'           : 2000    # MeV
                                                    , 'KstCombDOCA'          : 0.1     # mm
                                                    , 'KstCombVCHI2DOFLoose' : 9       # dimensionless
                                                    , 'KstMassWinLoose'      : 100      # MeV
                                                    , 'KstMassWin'           : 75       # MeV
                                                    , 'KstMassWinTight'      : 75       # MeV
                                                    , 'KstPT'                : 2000     # MeV
                                                    , 'KstPTLoose'           : 2000     # MeV
                                                    , 'KstSumPT'             : 5000     # MeV
                                                    , 'Bb2KstKstAMassMin'    :  8500    # MeV
                                                    , 'Bb2KstKstAMassMax'    : 11000    # MeV
                                                    , 'Bb2KstKstMassMin'     :  8900    # MeV
                                                    , 'Bb2KstKstMassMax'     : 10000    # MeV
                                                    , 'l0'                   : 'L0_DECISION_PHYSICS'

                                                    }
                        , Hlt1LowMultLinesConf : {   'Prescale' : { 'Hlt1LowMultPassThrough'             : 0.01,
                                                                    'Hlt1NoBiasNonBeamBeam'              : 0.07,
                                                                    #
                                                                    'Hlt1LowMult'                        : 0.00, #off
                                                                    'Hlt1LowMultMuon'                    : 1.00,
                                                                    'Hlt1LowMultVeloCut_Hadrons'         : 1.00, #0.10 Heschel moved to L0
                                                                    'Hlt1LowMultVeloCut_Leptons'         : 1.00, #0.10 Heschel moved to L0
                                                                    'Hlt1LowMultMaxVeloCut'              : 1.00, #0.10 Heschel moved to L0
                                                                    #
                                                                    'Hlt1LowMultHerschel'                : 0.00, #off
                                                                    'Hlt1LowMultVeloAndHerschel_Hadrons' : 1.00,
                                                                    'Hlt1LowMultVeloAndHerschel_Leptons' : 1.00,
                                                                    'Hlt1LowMultMaxVeloAndHerschel'      : 1.00
                                                                    }
                                                     #
                                                     , 'SpdMult'                                 :   100.   # dimensionless
                                                     , 'VeloCut_Hadrons_MinVelo'                 :     2    # dimensionless
                                                     , 'VeloCut_Hadrons_MaxVelo'                 :     8    # dimensionless
                                                     , 'VeloCut_Hadrons_SpdMult'                 :    20    # dimensionless
                                                     , 'VeloCut_Leptons_MinVelo'                 :     0    # dimensionless
                                                     , 'VeloCut_Leptons_MaxVelo'                 :    10    # dimensionless
                                                     , 'MaxVeloCut_MaxVelo'                      :     8    # dimensionless
                                                     , 'MaxNVelo'                                :    10    # dimensionless for generic Hlt1LowMult
                                                     , 'MinNVelo'                                :     0    # dimensionless for generic Hlt1LowMult
                                                     #
                                                     , 'MaxHRCADC_B2'                            :   750   # dimensionless,
                                                     , 'MaxHRCADC_B1'                            :   750   # dimensionless,
                                                     , 'MaxHRCADC_B0'                            :   750   # dimensionless,
                                                     , 'MaxHRCADC_F1'                            :   750   # dimensionless,
                                                     , 'MaxHRCADC_F2'                            :   750   # dimensionless,
                                                     #
                                                     , 'TrChi2'                                  :     5.   # dimensionless,
                                                     , 'PT'                                      :   500.   # MeV
                                                     , 'P'                                       :     0.   # MeV
                                                     #
                                                     , 'LowMultLineL0Dependency'                 : "L0_CHANNEL_RE('.*lowMult')"   #"( L0_CHANNEL_RE('.*(?<!Photon),lowMult') )"
                                                     , 'LowMultVeloCut_HadronsLineL0Dependency'  : "( L0_CHANNEL_RE('DiHadron,lowMult') | L0_CHANNEL_RE('HRCDiHadron,lowMult') )"
                                                     , 'LowMultVeloCut_LeptonsLineL0Dependency'  : "( L0_CHANNEL_RE('Muon,lowMult') | L0_CHANNEL_RE('DiMuon,lowMult') | L0_CHANNEL_RE('Electron,lowMult') )"
                                                     , 'LowMultMaxVeloCutLineL0Dependency'       : "( L0_CHANNEL_RE('Photon,lowMult') | L0_CHANNEL_RE('DiEM,lowMult') )"
                                                     , 'LowMultPassThroughLineL0Dependency'      : "L0_CHANNEL_RE('.*lowMult')"
                                                     , 'NoBiasNonBeamBeamODIN'                   : "ODIN_PASS(LHCb.ODIN.Lumi) & ~(ODIN_BXTYP == LHCb.ODIN.BeamCrossing)"
                                                     }
                       , Hlt1ProtonLinesConf :   { 'DiProton_SpdMult'    :   300.   # dimensionless, Spd Multiplicy cut
                                                 , 'DiProton_PreFitPT'   :  1800.   # MeV
                                                 , 'DiProton_PreFitP'    : 12000.   # MeV
                                                 , 'DiProton_PT'         :  1900.   # MeV
                                                 , 'DiProton_P'          : 12500.   # MeV
                                                 , 'DiProton_theta'      : 0.0366   # Proton eta>4
                                                 , 'DiProton_TrChi2'     :     2.5
                                                 , 'DiProton_TrGP'       :     0.2 # ghost prob 
                                                 , 'DiProton_MassMin'    :  2800.   # MeV, after Vtx fit
                                                 , 'DiProton_MassMax'    :  3300.   # MeV, after Vtx fit
                                                 , 'DiProton_VtxDOCA'    :     0.1
                                                 , 'DiProton_VtxChi2'    :     4.   # dimensionless
                                                 , 'DiProton_JpsiPT'     :  6500.   # MeV
                                                 , 'DiProtonLowMult_SpdMult'    :    10.   # dimensionless, Spd Multiplicy cut
                                                 , 'DiProtonLowMult_PT'         :   500.   # MeV, same as LooseForward
                                                 , 'DiProtonLowMult_P'          :  6000.   # MeV, same as LooseForward
                                                 , 'DiProtonLowMult_MassMin'    :  2800.   # MeV, after Vtx fit
                                                 , 'DiProtonLowMult_VtxDOCA'    :     0.3
                                                 , 'DiProtonLowMult_VtxChi2'    :    25.   # dimensionless
                                                 }
                       }

        # HLT2 thresholds from individual files
        sds = self.SubDirs()
        for version, subdirs in self.SubDirs().iteritems():
            for subdir in subdirs:
                conf = _get_conf(subdir, "_" + version)
                update_thresholds(thresholds, conf.Thresholds())

        return thresholds

    def ActiveHlt2Lines(self):
        """Return a list of active lines."""
        hlt2 = []
        sds = self.SubDirs()
        for version, subdirs in self.SubDirs().iteritems():
            for subdir in subdirs:
                conf = _get_conf(subdir, "_" + version)
                hlt2.extend(conf.ActiveHlt2Lines())
        return hlt2

    def ActiveHlt1Lines(self):
        """Return a list of active lines."""
        lines = [
            'Hlt1TrackMuon',
            'Hlt1SingleMuonHighPT',
            'Hlt1SingleMuonHighPTNoMUID',
            'Hlt1SingleMuonNoIP',
            'Hlt1DiMuonNoL0',
            'Hlt1DiMuonLowMass', 'Hlt1DiMuonHighMass',
            'Hlt1MultiMuonNoL0',
            'Hlt1DiMuonNoIPSS', 'Hlt1DiMuonNoIP', 'Hlt1MultiDiMuonNoIP',
            'Hlt1B2GammaGamma',
            'Hlt1B2PhiPhi_LTUNB',
            'Hlt1SingleElectronNoIP',
            'Hlt1TrackMVA', 'Hlt1TwoTrackMVA', 'Hlt1TrackMuonMVA',
            'Hlt1TrackMVATight', 'Hlt1TwoTrackMVATight',
            'Hlt1CalibTrackingKPi', 'Hlt1CalibTrackingKK', 'Hlt1CalibTrackingPiPi',
            'Hlt1CalibHighPTLowMultTrks', 'Hlt1CalibTrackingKPiDetached',
            'Hlt1CalibMuonAlignJpsi',
            'Hlt1B2HH_LTUNB_KPi', 'Hlt1B2HH_LTUNB_KK', 'Hlt1B2HH_LTUNB_PiPi',
            'Hlt1IncPhi',
            'Hlt1B2PhiGamma_LTUNB',
            'Hlt1CalibRICHMirrorRICH1',
            'Hlt1CalibRICHMirrorRICH2',
            'Hlt1DiProton',
            'Hlt1DiProtonLowMult',
            #
            'Hlt1LowMultMuon',
            'Hlt1LowMultVeloCut_Hadrons',
            'Hlt1LowMultVeloCut_Leptons',
            'Hlt1LowMultMaxVeloCut',
            'Hlt1LowMultVeloAndHerschel_Hadrons',
            'Hlt1LowMultVeloAndHerschel_Leptons',
            'Hlt1LowMultMaxVeloAndHerschel',
            'Hlt1LowMultPassThrough',
            'Hlt1NoBiasNonBeamBeam',
            #
            'Hlt1Bottomonium2PhiPhi',
            'Hlt1Bottomonium2KstarKstar',
        ]

        from Hlt1TechnicalLines import Hlt1TechnicalLines
        lines.extend(Hlt1TechnicalLines().ActiveHlt1Lines())

        return lines
