from GaudiKernel.SystemOfUnits import GeV, mm, MeV 

class SLB_pp_2017(object) :
    """
    Threshold settings for Hlt2 SLB lines

    @author Sascha Stahl
    """

    __all__ = ( 'ActiveHlt2Lines' )

    def ActiveHlt2Lines(self) :
        """
        Returns a list of active lines
        """

        lines = [
            # D0 mu decays
            'Hlt2SLB_B2D0Mu_D02KmPipTurbo',
            'Hlt2SLB_B2D0Mu_D02PimPipTurbo',
            'Hlt2SLB_B2D0Mu_D02KmKpTurbo',
            # Dstar mu decays
            'Hlt2SLB_B2DstMu_D02KmPipTurbo',
            'Hlt2SLB_B2DstMu_D02PimPipTurbo',
            'Hlt2SLB_B2DstMu_D02KmKpTurbo'
            ]
        return lines


    def Thresholds(self) :
        """
        Returns a dictionary of cuts
        """

        # keep pass through thresholds
        d = { }
        
        from Hlt2Lines.SLB.Lines import SLBLines
        _local_m_pip = 139.57018 * MeV
        d.update({SLBLines : {
            'Prescale'  : {},
            'Postscale' : {},
            'Common'    : { 'TisTosSpec'        : [],
                            'Had_GHOSTPROB_MAX' :  1.0,
                            'Had_PT_MIN'        : 200.0*MeV,
                            'Had_P_MIN'         : 2000.0*MeV,
                            'Had_MIPCHI2DV_MIN' : 9.0,
                            'Mu_GHOSTPROB_MAX'  : 1.0,
                            'Mu_PT_MIN'         : 1000.0*MeV,
                            'Mu_P_MIN'          : 3000.0*MeV,
                            'Mu_MIPCHI2DV_MIN'  : 9.0,
                            'Mu_PIDMU_MIN'      : 0.0,
                            'D0_VCHI2PDOF_MAX'  : 25.0,
                            'D0_AMassWin'       : 100.0 *MeV,
                            'D0_MassWin'        : 90.0 *MeV,
                            'D0_BPVVDCHI2_MIN'  : 9.0,
                            'D0_VCHI2PDOF_MAX'  : 9.0,
                            'B_Mass_MIN'        : 2.3*GeV,
                            'B_Mass_MAX'        : 10.0*GeV,
                            'B_CorrMass_MIN'    : 2.8*GeV,
                            'B_CorrMass_MAX'    : 8.5*GeV,
                            'B_DocaChi2_MAX'    : 10.0,
                            'B_DIRA_MIN'        : 0.999,
                            'B_D_DZ_MIN'        : -0.05*mm,
                            'B_VCHI2PDOF_MAX'   : 9.0
                            },
            'SLB_SlowPionTag': { "Pi_PT_MIN"          : 150.0*MeV,
                                 "Pi_P_MIN"           : 2000.0*MeV,
                                 "Pi_PIDK_MAX"        : 2.0,
                                 "Pi_GHOSTPROB_MAX"   : 0.3,
                                 "Tag_VCHI2PDOF_MAX"  : 16.0,
                                 'Q_AM_MIN'           : 130.0 * MeV - _local_m_pip,
                                 'Q_M_MIN'            : 130.0 * MeV - _local_m_pip,
                                 'Q_AM_MAX'           : 165.0 * MeV - _local_m_pip,
                                 'Q_M_MAX'            : 160.0 * MeV - _local_m_pip
                                 },
            'SLB_D02KmPip'   : { "K_PIDK_MIN"  : 5.,
                                 "Pi_PIDK_MAX" : 0.
                                 },
            'SLB_D02KmKp'    : { "K_PIDK_MIN"  : 5.},
            'SLB_D02PimPip'  : { "Pi_PIDK_MAX" : 0.},
            'PersistReco'    : { 'B2D0Mu_D02KmPipTurbo'    : False,
                                 'B2D0Mu_D02KmKpTurbo'     : False,
                                 'B2D0Mu_D02PimPipTurbo'   : False,
                                 }
            }})

        return d
