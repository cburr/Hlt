from GaudiKernel.SystemOfUnits import GeV, mm, MeV

class TrackEffElectron_pp_2017 :
    """
    Threshold settings for Hlt2 Hadronic TrackEff lines: 25ns data taking, 2017

    WARNING :: DO NOT EDIT WITHOUT PERMISSION OF THE AUTHORS

    @author V. V. Gligorov
    @date 2017-03-29
    """

    __all__ = ( 'ActiveHlt2Lines' )


    def ActiveHlt2Lines(self) :
        """
        Returns a list of active lines
        """

        lines = [
            'Hlt2TrackEffElectronDetachedEKTurboCalib',
            'Hlt2TrackEffElectronDetachedEPiTurboCalib',
            'Hlt2TrackEffElectronDetachedMuKTurboCalib',
            'Hlt2TrackEffElectronDetachedMuPiTurboCalib',
        ]


        return lines

    def Thresholds(self) :

        d = {}

        from Hlt2Lines.TrackEffElectron.Lines     import TrackEffElectronLines
        d.update ({TrackEffElectronLines : {'Prescale'             : {'Hlt2TrackEffElectronDetachedMuKTurboCalib'  : 0.1,
                                                                      'Hlt2TrackEffElectronDetachedMuPiTurboCalib' : 0.1},
                                            'TrackGEC'             : {'NTRACK_MAX'           : 80}, 
                                            'SharedChild'          : {'TrChi2Mu'   :   5,  
                                                                      'TrChi2Ele'  :   5,  
                                                                      'TrChi2Kaon' :   5,  
                                                                      'TrChi2Pion' :   5,  
                                                                      'IPMu'       :   0.0 * mm, 
                                                                      'IPEle'      :   0.0 * mm, 
                                                                      'IPKaon'     :   0.0 * mm, 
                                                                      'IPPion'     :   0.0 * mm, 
                                                                      'IPChi2Mu'   :   16, 
                                                                      'IPChi2Ele'  :   16, 
                                                                      'IPChi2Kaon' :   16,  
                                                                      'IPChi2Pion' :   36,  
                                                                      'EtaMinMu'   :   1.8,
                                                                      'EtaMinEle'  :   1.8,
                                                                      'EtaMaxMu'   :   3.5,
                                                                      'EtaMaxEle'  :   3.5,
                                                                      'ProbNNe'    :   0.2,
                                                                      'ProbNNmu'   :   0.5,    
                                                                      'ProbNNk'    :   0.2,
                                                                      'ProbNNpi'   :   0.8,
                                                                      'PtMu'       :   3500 * MeV,
                                                                      'PtEle'      :   3500 * MeV,
                                                                      'PtKaon'     :   750 * MeV,
                                                                      'PtPion'     :   1000 * MeV },
                                            'DetachedEK'           : {'AM'         :   5000*MeV,
                                                                      'VCHI2'      :   10,
                                                                      'VDCHI2'     :   100,
                                                                      'DIRA'       :   0.95,
                                                                      'TisTosSpec' :   "Hlt1TrackMVA.*Decision%TOS"
                                                                     },  
                                            'DetachedEPi'          : {'AM'         :   2000*MeV,
                                                                      'VCHI2'      :   10,
                                                                      'VDCHI2'     :   100, 
                                                                      'DIRA'       :   0.95,
                                                                      'TisTosSpec' :   "Hlt1TrackMVA.*Decision%TOS"
                                                                     },  
                                            'DetachedMuK'          : {'AM'         :   5000*MeV,
                                                                      'VCHI2'      :   10, 
                                                                      'VDCHI2'     :   100,
                                                                      'DIRA'       :   0.95,
                                                                      'TisTosSpec' :   "Hlt1Track(Muon)?MVA.*Decision%TOS"
                                                                     },  
                                            'DetachedMuPi'         : {'AM'         :   2000*MeV,
                                                                      'VCHI2'      :   10, 
                                                                      'VDCHI2'     :   100,
                                                                      'DIRA'       :   0.95,
                                                                      'TisTosSpec' :   "Hlt1Track(Muon)?MVA.*Decision%TOS"
                                                                     },  
                                            'L0Req'                : {'DetachedEK'   : "L0_CHANNEL('Electron')",
                                                                      'DetachedEPi'  : "L0_CHANNEL('Electron')",
                                                                      'DetachedMuK'  : "L0_CHANNEL('Muon')",
                                                                      'DetachedMuPi' : "L0_CHANNEL('Muon')" },
                                            'Hlt1Req'              : {'DetachedEK'   : "HLT_PASS_RE('Hlt1TrackMVA.*Decision')",
                                                                      'DetachedEPi'  : "HLT_PASS_RE('Hlt1TrackMVA.*Decision')",
                                                                      'DetachedMuK'  : "HLT_PASS_RE('Hlt1Track(Muon)?MVA.*Decision')",
                                                                      'DetachedMuPi' : "HLT_PASS_RE('Hlt1Track(Muon)?MVA.*Decision')" }
                           
                 }
                 })
        return d

