from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from TrackEffDiMuon.TrackEffDiMuon_pHe_2016 import TrackEffDiMuon_pHe_2016
from PID.PID_pHe_2016 import PID_pHe_2016
from HeavyIons.HeavyIons_pHe_2016 import HeavyIons_pHe_2016
from Utilities.Utilities import update_thresholds


class protonHelium_2016(object):
    """Settings for proton-helium 2016 data taking.

    @author P. Robbe, R. Matev, Y. Zhang
    @date 2016-02-08
    @date 2016-10-05
    """

    def __init__(self):
        # No TURBO => no aferburner
        from Configurables import HltConf
        HltConf().EnableHltAfterburner = False

        # Extend velo tracking search range in z direction to [-2000,2000]
        from Configurables import HltRecoConf
        HltRecoConf().BeamGasMode = True
        HltRecoConf().VeloTrackingZMin = -2000. * mm
        HltRecoConf().VeloTrackingZMax = 2000. * mm

        # Loosen the radial cut for standard Hlt PVs
        HltRecoConf().PVOptions = {
            "UseBeamSpotRCut": True,
            "BeamSpotRCut": 4 * mm,
            "TrackErrorScaleFactor": 1.,
            "ResolverBound": 5 * mm,
            "MinTracks": 4.,
            "trackMaxChi2": 12.,
            "BeamSpotRHighMultiplicityCut": 4 * mm,
            "BeamSpotRMultiplicityTreshold": 10,
        }

        # Lower the pt and p cut on tracks for the SMOG lines
        from Hlt1Lines.Hlt1SharedParticles import Hlt1SharedParticles
        Hlt1SharedParticles().PreFitPT = 380. * MeV
        Hlt1SharedParticles().PreFitP = 2850. * MeV
        Hlt1SharedParticles().PT = 400. * MeV
        Hlt1SharedParticles().P = 3000. * MeV
        # TODO do you also want to lower the PT cut for the DiMuonHighMass lines?

        # Remove implicit GECs in the decoding
        from HltLine.HltDecodeRaw import DecodeVELO
        DecodeVELO.members()[0].MaxVeloClusters = 99999
        from DAQSys.Decoders import DecoderDB
        DecoderDB["DecodeVeloRawBuffer/createVeloClusters"].Properties["MaxVeloClusters"] = 99999
        from HltTracking.HltRecoConf import HltRecoConf
        HltRecoConf().Forward_MaxOTHits = 99999

        # Exclude Hlt1 lines unprotected by GECs (BBHighMult, (BB|BE|EB)NoBias)
        from HltConf.Hlt2 import Hlt2Conf
        Hlt2Conf().DefaultHlt1Filter = "HLT_PASS_RE('Hlt1(?!Lumi)(?!Velo)(?!BeamGas)(?!NoPV)(?!MB)(?!BBHighMult)(?![BE]{2}NoBias).*Decision')"

        # Set the lines for the data quality
        from HltConf.HltOutput import HltOutputConf
        HltOutputConf().Hlt2LinesForDQ.append(
            '(Hlt2SMOG(?!PassThrough).*)'
        )

    def verifyType(self, ref):
        # verify self.ActiveLines is still consistent with
        # our types self.ActiveHlt2Lines and self.ActiveHlt1Lines...
        # so we can force that classes which inherit from us
        # and overrule either ActiveHlt.Lines also overrule
        # HltType...
        if (self.ActiveHlt1Lines() != ref.ActiveHlt1Lines(self) or
            self.ActiveHlt2Lines() != ref.ActiveHlt2Lines(self)):
            raise RuntimeError('Must update HltType when modifying ActiveHlt.Lines()')

    def L0TCK(self):
        return '0x1608'

    def HltType(self):
        self.verifyType(protonHelium_2016)
        return 'protonHelium_2016'

    def ActiveHlt1Lines(self):
        """Return a list of active Hlt1 lines."""
        lines = [
            # Technical lines
            'Hlt1Lumi',  # => Hlt2Lumi
            'Hlt1VeloClosingMicroBias',  # disk buffer only
            'Hlt1VeloClosingPV',  # disk buffer only
            # BE+EB lines => SMOGPHYS stream
            # - trivial lines => Hlt2SMOGPassThrough
            'Hlt1BENoBias',
            'Hlt1EBNoBias',
            'Hlt1BEMicroBiasVelo',
            'Hlt1BEMicroBiasLowMultVelo',
            # - muon lines => Hlt2SMOGDiMuon
            'Hlt1SMOGDiMuonHighMass',
            # - SMOG lines => Hlt2SMOG*
            'Hlt1SMOGKPi',
            'Hlt1SMOGKPiPi',
            'Hlt1SMOGKKPi',
            'Hlt1SMOGpKPi',
            'Hlt1SMOGGeneric',
            'Hlt1SMOGSingleTrack',
            # BB lines => FULL stream
            # - trivial lines => Hlt2PassThrough
            'Hlt1BBNoBias',
            'Hlt1BBMicroBiasVelo',
            'Hlt1BBHighMult',
            # - muon lines => Hlt2PassThrough
            'Hlt1DiMuonHighMass',
            # - SMOG lines => Hlt2BBSMOG*
            'Hlt1BBSMOGKPi',
            'Hlt1BBSMOGKPiPi',
            'Hlt1BBSMOGKKPi',
            'Hlt1BBSMOGpKPi',
            'Hlt1BBSMOGGeneric',
            'Hlt1BBSMOGSingleTrack',
            # Calibration lines for alignment, disk buffer only
            'Hlt1CalibHighPTLowMultTrks',
            'Hlt1CalibRICHMirrorRICH1',
            'Hlt1CalibRICHMirrorRICH2',
            'Hlt1CalibTrackingKPiDetached',
            'Hlt1CalibMuonAlignJpsi',
        ]
        return lines

    def ActiveHlt2Lines(self):
        """Return a list of active Hlt2 Lines."""
        lines = [
            'Hlt2Lumi',
            # FULL stream
            'Hlt2PassThrough',
            'Hlt2BBSMOGD02KPi',
            'Hlt2BBSMOGDpm2KPiPi',
            'Hlt2BBSMOGDs2KKPi',
            'Hlt2BBSMOGLc2KPPi',
            'Hlt2BBSMOGB2PiMu',
            # SMOGPHYS stream
            'Hlt2SMOGD02KPi',
            'Hlt2SMOGDpm2KPiPi',
            'Hlt2SMOGDs2KKPi',
            'Hlt2SMOGLc2KPPi',
            'Hlt2SMOGB2PiMu',
        ]

        # Passthrough lines Hlt2SMOGDiMuon, Hlt2SMOGPassThrough
        lines.extend(HeavyIons_pHe_2016().ActiveHlt2Lines())

        # TrackEffDiMuon Calib lines
        lines.extend(TrackEffDiMuon_pHe_2016().ActiveHlt2Lines())
        # PID Calib lines
        lines.extend(PID_pHe_2016().ActiveHlt2Lines())

        return lines

    def Thresholds(self):
        """Return a dictionary of cuts."""

        from Hlt1Lines.Hlt1LumiLines import Hlt1LumiLinesConf
        from Hlt1Lines.Hlt1IFTLines import Hlt1IFTLinesConf
        from Hlt1Lines.Hlt1MuonLines import Hlt1MuonLinesConf
        from Hlt1Lines.Hlt1SMOGLines import Hlt1SMOGLinesConf
        from Hlt1Lines.Hlt1CalibTrackingLines import Hlt1CalibTrackingLinesConf
        from Hlt1Lines.Hlt1CalibRICHMirrorLines import Hlt1CalibRICHMirrorLinesConf
        from Hlt1Lines.Hlt1CommissioningLines import Hlt1CommissioningLinesConf

        thresholds = {
            Hlt1LumiLinesConf: {
                'Prescale': {'Hlt1Lumi': 1.},
                'Postscale': {'Hlt1Lumi': 1.},
            },
            Hlt1CommissioningLinesConf: {
                'Prescale': {
                    'Hlt1VeloClosingMicroBias': 1,
                    'Hlt1VeloClosingPV': 1,
                },
                'Postscale': {
                    'Hlt1VeloClosingMicroBias': 'RATE(500)',
                    'Hlt1VeloClosingPV': 'RATE(500)',
                },
                'ODINVeloClosing': '(ODIN_BXTYP == LHCb.ODIN.BeamCrossing) & ODIN_PASS(LHCb.ODIN.VeloOpen)',
                'ODIN': {
                    'VeloClosingPV': 'ODIN_BXTYP == LHCb.ODIN.BeamCrossing',
                },
                'L0': {
                    'VeloClosingPV': "L0_DECISION(LHCb.L0DUDecision.Any)",
                },
                'VeloClosingPV': {
                    'ZMin': -150 * mm,
                    'ZMax': 150 * mm,
                    'MinBackwardTracks': 1,
                    'MinForwardTracks': 1,
                },
            },
            Hlt1IFTLinesConf: {
                'ODIN': {
                    'BENoBias': 'ODIN_PASS(LHCb.ODIN.Lumi)',
                    'EBNoBias': 'ODIN_PASS(LHCb.ODIN.Lumi)',
                    'BBNoBias': 'ODIN_PASS(LHCb.ODIN.NoBias)',
                },
                'L0': {
                    'BBMicroBiasVelo': "L0_CHANNEL('SPD')",
                },
                'GEC': {
                    'BEMicroBiasVelo': "HeavyIons",
                    'BBMicroBiasVelo': "HeavyIons",
                    'BEMicroBiasLowMultVelo': "HeavyIons",
                    'BBHighMult': "HeavyIons",
                },
                'MaxVeloTracks': {
                    'BEMicroBiasLowMultVelo': 10,
                },
                'MinVeloTracks': {
                    'BBMicroBiasVelo': 1,
                    'BEMicroBiasVelo': 1,
                    'BEMicroBiasLowMultVelo': 1,
                },
                'Prescale': {
                    # NOTE: The prescales below expect x:1:1:y kHz lumi rate,
                    # which is what we should use during BGI/VDM
                    'Hlt1BBNoBias':               1,  # ODIN NoBias to be set to 100 Hz in trigger config
                    'Hlt1BENoBias':               1,  # 1 x 1000 Hz = 1000 Hz, NOTE multiple prescales needed
                    'Hlt1EBNoBias':               0.01,  # 0.01 x 1000 Hz = 10 Hz
                    'Hlt1BBMicroBiasVelo':        0.1,
                    'Hlt1BEMicroBiasVelo':        1,  # NOTE multiple prescales needed
                    'Hlt1BEMicroBiasLowMultVelo': 1,
                },
            },
            Hlt1MuonLinesConf: {
                'L0Channels': {
                    'DiMuonHighMass':     ('Muon',),
                    'SMOGDiMuonHighMass': ('Muon',),
                    'CalibMuonAlignJpsi': ('Muon',),
                },
                'ODINFilter': {
                    'DiMuonHighMass':     'ODIN_BXTYP == LHCb.ODIN.BeamCrossing',
                    # TODO should the DiMuonHighMass trigger only on NoBias like Hlt1BBSMOG*?
                    'SMOGDiMuonHighMass': '(ODIN_BXTYP == LHCb.ODIN.Beam1) | (ODIN_BXTYP == LHCb.ODIN.Beam2)',
                    'CalibMuonAlignJpsi': 'ODIN_BXTYP == LHCb.ODIN.BeamCrossing',
                },
                'DiMuonHighMass_VxDOCA'    :  0.2,
                'DiMuonHighMass_VxChi2'    :   25,
                'DiMuonHighMass_P'         : 3000,
                'DiMuonHighMass_PT'        :  500,
                'DiMuonHighMass_TrChi2'    :    3,
                'DiMuonHighMass_M'         : 2500,  # 2700->2500 MeV,  #...
                'DiMuonHighMass_GEC'       : 'HeavyIons',
                'SMOGDiMuonHighMass_VxDOCA'    :  0.2,
                'SMOGDiMuonHighMass_VxChi2'    :   25,
                'SMOGDiMuonHighMass_P'         : 3000,
                'SMOGDiMuonHighMass_PT'        :  500,
                'SMOGDiMuonHighMass_TrChi2'    :    3,
                'SMOGDiMuonHighMass_M'         : 2500,  # 2700->2500 MeV,  #...
                'SMOGDiMuonHighMass_GEC'       : 'HeavyIons',
                'CalibMuonAlignJpsi_ParticlePT'             : 800,     # MeV
                'CalibMuonAlignJpsi_ParticleP'              : 6000,    # MeV
                'CalibMuonAlignJpsi_TrackCHI2DOF'           : 2,       # dimensionless
                'CalibMuonAlignJpsi_CombMaxDaughtPT'        : 800,     # MeV
                'CalibMuonAlignJpsi_CombAPT'                : 1500,    # MeV
                'CalibMuonAlignJpsi_CombDOCA'               : 0.2,     # mm
                'CalibMuonAlignJpsi_CombVCHI2DOF'           : 10,     # dimensionless
                'CalibMuonAlignJpsi_CombVCHI2DOFLoose'      : 10,      # dimensionless
                'CalibMuonAlignJpsi_CombDIRA'               : 0.9,     # dimensionless
                'CalibMuonAlignJpsi_CombTAU'                : 0.,     # ps
                'CalibMuonAlignJpsi_JpsiMassWinLoose'       : 150,     # MeV
                'CalibMuonAlignJpsi_JpsiMassWin'            : 100,     # MeV
            },
            Hlt1SMOGLinesConf: {
                'ParticlePT':          400,  # MeV  # 500 -> 400 #...
                'ParticleP':           3000,  # MeV
                'TrackCHI2DOF':        4,
                'CombMaxDaughtPT':     400,  # MeV  500 -> 400 #...
                'CombDOCA':            1.0,  # mm
                'CombVCHI2DOF':        25,
                'MassWinLoose':        250,  # MeV
                'MassWin':             150,  # MeV
                'GenericMassMinLoose': 0,  # MeV
                'GenericMassMin':      0,  # MeV
                'GenericMaxDaughtPT':  800,  # MeV
                'SingleTrackPT':       800,  # MeV
                'GEC':                 'HeavyIons',
                'L0':    'L0_ALL',
                'L0_BB': 'L0_ALL',
                # TODO should we explicitly trigger on channels or L0_DECISION_PHYSICS?
                #      With no L0 filter, you get mix of L0 triggered, NoBias, Lumi...)
                'ODIN':    '(ODIN_BXTYP == LHCb.ODIN.Beam1) | (ODIN_BXTYP == LHCb.ODIN.Beam2)',
                'ODIN_BB': 'ODIN_BXTYP == LHCb.ODIN.BeamCrossing',
            },
            # TODO The Hlt1CalibRICHMirrorLines and Hlt1CalibTrackingLinesConf trigger on L0_DECISION_PHYSICS,
            #      which in the SMOG L0/ODIN configuration means bb+be+eb. Is this fine?
            Hlt1CalibRICHMirrorLinesConf: {
                'Prescale': {
                    'Hlt1CalibHighPTLowMultTrks': 0.0001,
                    'Hlt1CalibRICHMirrorRICH1':   0.281,
                    'Hlt1CalibRICHMirrorRICH2':   1.0,
                },
                # TODO are the Hlt1CalibRICHMirrorLines prescales ok for the SMOG run?
                'DoTiming'   : False,
                'R2L_PT'     : 500.   * MeV,
                'R2L_P'      : 40000. * MeV,
                'R2L_MinETA' : 2.59,
                'R2L_MaxETA' : 2.97,
                'R2L_Phis'   : [ ( -2.69, -2.29 ), ( -0.85, -0.45 ), ( 0.45, 0.85 ), ( 2.29, 2.69 ) ],
                'R2L_TrChi2' : 2.,
                'R2L_MinTr'  : 0.5,
                'R2L_GEC'    : 'Loose',
                'R1L_PT'     : 500.   * MeV,
                'R1L_P'      : 10000. * MeV,
                'R1L_MinETA' : 1.6,
                'R1L_MaxETA' : 2.04,
                'R1L_Phis'   : [ ( -2.65, -2.30 ), ( -0.80, -0.50 ), ( 0.50, 0.80 ), ( 2.30, 2.65 ) ],
                'R1L_TrChi2' : 2.,
                'R1L_MinTr'  : 0.5,
                'R1L_GEC'    : 'Loose',
                'LM_PT'      : 500.   * MeV,
                'LM_P'       : 1000.  * MeV,
                'LM_TrChi2'  : 2.,
                'LM_MinTr'   : 1,
                'LM_MaxTr'   : 40,
                'LM_GEC'     : 'Loose'
            },
            Hlt1CalibTrackingLinesConf: {
                'ParticlePT'             : 600,     # MeV
                'ParticleP'              : 4000,    # MeV
                'TrackCHI2DOF'           : 2,       # dimensionless
                'CombMaxDaughtPT'        : 900,     # MeV 900
                'CombAPT'                : 1800,    # MeV 1200
                'CombDOCA'               : 0.1,     # mm
                'CombVCHI2DOF'           : 10,      # dimensionless
                'CombVCHI2DOFLoose'      : 15,      # dimensionless
                'CombDIRA'               : 0.99,    # dimensionless
                'CombTAU'                : 0.25,    # ps
                'D0MassWinLoose'         : 100,     # MeV
                'D0MassWin'              : 60,      # MeV
                'B0MassWinLoose'         : 200,     # MeV
                'B0MassWin'              : 150,     # MeV
                'D0DetachedDaughtsIPCHI2': 9,       # dimensionless
                'D0DetachedIPCHI2'       : 9,       # dimensionless
                'BsPhiGammaMassMinLoose' : 3350,    # MeV
                'BsPhiGammaMassMaxLoose' : 6900,    # MeV
                'BsPhiGammaMassMin'      : 3850,    # MeV
                'BsPhiGammaMassMax'      : 6400,    # MeV
                'PhiMassWinLoose'        : 50,      # MeV
                'PhiMassWin'             : 30,      # MeV
                'PhiMassWinTight'        : 20,      # MeV
                'PhiPT'                  : 1800,    # MeV
                'PhiPTLoose'             : 800,     # MeV
                'PhiSumPT'               : 3000,    # MeV
                'PhiIPCHI2'              : 16,      # dimensionless
                'B0SUMPT'                : 4000,    # MeV
                'B0PT'                   : 1000,    # MeV
                'GAMMA_PT_MIN'           : 2000,    # MeV
                'Velo_Qcut'              : 999,     # OFF
                'TrNTHits'               : 0,       # OFF
                'ValidateTT'             : False,
            }
        }

        from Hlt2Lines.SMOG.Lines import SMOGLines
        thresholds.update({
            SMOGLines: {
                'Prescale': {},
                'Postscale': {},
                'D02HH': {
                    'TisTosSpec'               : "Hlt1SMOG.*Decision%TOS",
                    'TisTosSpecBB'             : "Hlt1BBSMOG.*Decision%TOS",
                    'Pair_AMINDOCA_MAX'        : 0.10 * mm,
                    'Trk_Max_APT_MIN'          : 400.0 * MeV,  # 500->400
                    'D0_VCHI2PDOF_MAX'         : 10.0,       # neuter
                    'Comb_AM_MIN'              : 1775.0 * MeV,
                    'Comb_AM_MAX'              : 1955.0 * MeV,
                    'Trk_ALL_PT_MIN'           : 250.0 * MeV,
                    'Trk_ALL_P_MIN'            : 2.0  * GeV,
                    'Mass_M_MIN'               : 1784.0 * MeV,
                    'Mass_M_MAX'               : 1944.0 * MeV,
                },
                'Dpm2HHH': {
                    'TisTosSpec'               : "Hlt1SMOG.*Decision%TOS",
                    'TisTosSpecBB'             : "Hlt1BBSMOG.*Decision%TOS",
                    'Trk_ALL_PT_MIN'           : 200.0 * MeV,
                    'Trk_2OF3_PT_MIN'          : 400.0 * MeV,
                    'Trk_1OF3_PT_MIN'          : 1000.0 * MeV,
                    'VCHI2PDOF_MAX'            : 25.0,
                    'ASUMPT_MIN'               : 0 * MeV,
                    'AM_MIN'                   : 1779 * MeV,
                    'AM_MAX'                   : 1959 * MeV,
                    'Mass_M_MIN'               : 1789.0 * MeV,
                    'Mass_M_MAX'               : 1949.0 * MeV,
                    },
                'Ds2HHH': {
                    'TisTosSpec'               : "Hlt1SMOG.*Decision%TOS",
                    'TisTosSpecBB'             : "Hlt1BBSMOG.*Decision%TOS",
                    'Trk_ALL_PT_MIN'           : 200.0 * MeV,
                    'Trk_2OF3_PT_MIN'          : 400.0 * MeV,
                    'Trk_1OF3_PT_MIN'          : 1000.0 * MeV,
                    'VCHI2PDOF_MAX'            : 25.0,
                    'ASUMPT_MIN'               : 0 * MeV,
                    'AM_MIN'                   : 1879 * MeV,
                    'AM_MAX'                   : 2059 * MeV,
                    'Mass_M_MIN'               : 1889.0 * MeV,
                    'Mass_M_MAX'               : 2049.0 * MeV,
                },
                'Lc2HHH': {
                    'TisTosSpec'               : "Hlt1SMOG.*Decision%TOS",
                    'TisTosSpecBB'             : "Hlt1BBSMOG.*Decision%TOS",
                    'Trk_ALL_PT_MIN'           : 200.0 * MeV,
                    'Trk_2OF3_PT_MIN'          : 400.0 * MeV,
                    'Trk_1OF3_PT_MIN'          : 1000.0 * MeV,
                    'VCHI2PDOF_MAX'            : 25.0,
                    'ASUMPT_MIN'               : 0 * MeV,
                    'AM_MIN'                   : 2201 * MeV,
                    'AM_MAX'                   : 2553 * MeV,
                    'Mass_M_MIN'               : 2211.0 * MeV,
                    'Mass_M_MAX'               : 2543.0 * MeV,
                },
                'B2PiMu': {
                    'TisTosSpec'               : "Hlt1SMOG.*Decision%TOS",
                    'TisTosSpecBB'             : "Hlt1BBSMOG.*Decision%TOS",
                    'AM_MIN'                   : 0 * MeV,
                    'AM_MAX'                   : 1000.0 * GeV,
                    'ASUMPT_MIN'               : 4.0 * GeV,
                    'VCHI2PDOF_MAX'            : 30.0,
                    'Mass_M_MIN'               : 0 * MeV,
                    'Mass_M_MAX'               : 1000.0 * GeV
                },
            }
        })

        # Hlt2 pass through lines (beam-beam)
        from Hlt2Lines.Technical.Lines import TechnicalLines
        thresholds.update({
            TechnicalLines: {
                'PassThrough': {
                    'VoidFilter': '',
                    'HLT1':
                        "HLT_PASS_RE('Hlt1BB(?!SMOG).*Decision') | " +
                        "HLT_PASS('Hlt1DiMuonHighMassDecision')",
                },
                'Prescale': {
                    'Hlt2PassThrough': 1.,
                },
            }
        })

        # Hlt2 SMOG pass through lines
        update_thresholds(thresholds, HeavyIons_pHe_2016().Thresholds())
        # TrackEffDiMuon Calib thresholds
        update_thresholds(thresholds, TrackEffDiMuon_pHe_2016().Thresholds())
        # PID Calib thresholds
        update_thresholds(thresholds, PID_pHe_2016().Thresholds())

        return thresholds

    def Streams(self):
        return {
            # Deliberately turn off the LUMI stream to not double the large
            # rate of lumi during VDM (45 kHz) to storage
            'FULL': ' | '.join([
                "HLT_PASS_RE('Hlt2BBSMOG.*Decision')",
                "HLT_PASS('Hlt2PassThroughDecision')",
                "HLT_PASS_RE('Hlt2TrackEffDiMuonMuon.*Decision')",
                # TODO is really only Hlt2TrackEffDiMuonMuon* needed? (note the MuonMuon)
                "HLT_PASS_RE('Hlt2PID.*Decision')",
                # Hlt2Lumi is added by HltOutput
            ]),
            'SMOGPHYS': ' | '.join([
                "HLT_PASS_RE('Hlt2SMOG.*Decision')",
                "HLT_PASS_RE('Hlt2TrackEffDiMuonMuon.*Decision')",
                # TODO is really only Hlt2TrackEffDiMuonMuon* needed? (note the MuonMuon)
                "HLT_PASS_RE('Hlt2PID.*Decision')",
                # Hlt2Lumi is added by HltOutput
            ]),
            # TODO note that there is one set of Hlt2TrackEffDiMuon and Hlt2PID
            #      lines, which means that some bb events will go to the SMOGPHYS
            #      and some be/eb into FULL unless we change something.
            'VELOCLOSING': "HLT_PASS_RE('Hlt1Velo.*Decision')",
        }

    def StreamsWithBanks(self):
        return [(["FULL"], 'KILL', []),
                (["SMOGPHYS"], 'KILL', [])]
