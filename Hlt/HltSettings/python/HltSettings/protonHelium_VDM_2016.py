from protonHelium_2016 import protonHelium_2016
from VanDerMeerScan_2016 import VanDerMeerScan_2016
from Utilities.Utilities import update_thresholds


class protonHelium_VDM_2016(object):
    """Settings for combined proton-helium physics and VDM/BGI in 2016.

    @author R. Matev
    @date 2016-04-27
    """

    def __init__(self):
        self.physics = protonHelium_2016()
        self.lumi = VanDerMeerScan_2016()

    def verifyType(self, ref):
        # verify self.ActiveLines is still consistent with
        # our types self.ActiveHlt2Lines and self.ActiveHlt1Lines...
        # so we can force that classes which inherit from us
        # and overrule either ActiveHlt.Lines also overrule
        # HltType...
        if (self.ActiveHlt1Lines() != ref.ActiveHlt1Lines(self) or
            self.ActiveHlt2Lines() != ref.ActiveHlt2Lines(self)):
            raise RuntimeError('Must update HltType when modifying ActiveHlt.Lines()')

    def L0TCK(self):
        return '0x1608'

    def HltType(self):
        self.verifyType(protonHelium_VDM_2016)
        return 'protonHelium_VDM_2016'

    def ActiveHlt1Lines(self):
        """Return a list of active Hlt1 lines."""
        lines = self.physics.ActiveHlt1Lines() + self.lumi.ActiveHlt1Lines()
        return list(set(lines))

    def ActiveHlt2Lines(self):
        """Return a list of active Hlt2 Lines."""
        lines = self.physics.ActiveHlt2Lines() + self.lumi.ActiveHlt2Lines()
        return list(set(lines))

    def Thresholds(self):
        """Return a dictionary of cuts."""
        thresholds = {}
        update_thresholds(thresholds, self.physics.Thresholds())
        update_thresholds(thresholds, self.lumi.Thresholds())
        return thresholds

    def Streams(self):
        streams = {}
        streams.update(self.physics.Streams())
        streams.update(self.lumi.Streams())
        # TODO check that we're not overriding something
        return streams

    def NanoBanks(self):
        return self.lumi.NanoBanks()

    def StreamsWithLumi(self):
        streams = set(self.Streams().keys())
        streams.discard('VELOCLOSING')
        return list(streams)
