from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from Utilities.Utilities import update_thresholds


def __get_conf__(folder, suffix):
    import importlib
    conf = folder + suffix
    module = importlib.import_module("HltSettings.{0}.{1}".format(folder, conf))
    return getattr(module, conf)()


class protonLead_2016( object ):
    """Settings for proton-lead 2016 data taking, sqrt(s) = 8TeV, beam-beam physics.


    @author P. Robbe, R. Matev, S. Stahl, Y. Zhang
    @date 2016-07-19
    """

    def __init__(self):
        from HltLine.HltDecodeRaw import DecodeVELO
        DecodeVELO.members()[0].MaxVeloClusters = 99999
        from DAQSys.Decoders import DecoderDB
        DecoderDB["DecodeVeloRawBuffer/createVeloClusters"].Properties["MaxVeloClusters"] = 99999

        from HltTracking.HltRecoConf import HltRecoConf
        HltRecoConf().Forward_MaxOTHits = 99999

        from Hlt1Lines.Hlt1SharedParticles import Hlt1SharedParticles
        Hlt1SharedParticles().PT = 500
        Hlt1SharedParticles().PreFitPT = 475

        # Exclude Hlt1 lines unprotected by GECs (BBHighMult)
        from HltConf.Hlt2 import Hlt2Conf
        Hlt2Conf().DefaultHlt1Filter = "HLT_PASS_RE('Hlt1(?!Lumi)(?!Velo)(?!BeamGas)(?!NoPV)(?!MB)(?!BBHighMult)(?!HighVeloMult).*Decision')"
        # default for pp is "HLT_PASS_RE('Hlt1(?!Lumi)(?!Velo)(?!BeamGas)(?!NoPV)(?!MB).*Decision')"

        # Exclude Hlt1BBHighMultDecision from calling the reconstruction in the Afterburner
        from HltConf.HltAfterburner import HltAfterburnerConf
        HltAfterburnerConf().Hlt2Filter = "HLT_PASS_RE('Hlt2(?!Forward)(?!DebugEvent)(?!Lumi)(?!Transparent)(?!PassThrough)(?!MBHighMult).*Decision')"
        
        from HltConf.HltOutput import HltOutputConf
        HltOutputConf().Hlt2LinesForDQ.append(
            '(' +
            'Hlt2(?!MB)(?!HighVeloMultTurbo)(?!PassThrough)(?!Lumi)' +
            '(?!Debug)(?!Forward)(?!ErrorEvent)(?!Transparent)(?!BeamGas).*' +
            ')'
        )

    def verifyType(self,ref) :
        # verify self.ActiveLines is still consistent with
        # our types self.ActiveHlt2Lines and self.ActiveHlt1Lines...
        # so we can force that classes which inherit from us
        # and overrule either ActiveHlt.Lines also overrule
        # HltType...
        if ( self.ActiveHlt1Lines() != ref.ActiveHlt1Lines(self)  or self.ActiveHlt2Lines() != ref.ActiveHlt2Lines(self) ) :
            raise RuntimeError( 'Must update HltType when modifying ActiveHlt.Lines()' )

    def L0TCK(self) :
        return '0x1621'

    def HltType(self) :
        self.verifyType( protonLead_2016 )
        return          'protonLead_2016'

    def SubDirs(self):
        #Load configurations
        return {'pPb2016': ['DPS','CharmHad','LowMult','PID','EW',
                            'Topo','TrackEffDiMuon','Technical','TrackEff',
                            'DiMuon','SLB','HeavyIons',
                            'CcDiHadron',"SingleMuon" ,'XcMuXForTau','Jets'
                            ]}

    def Thresholds(self) :
        """
        Returns a dictionary of cuts
        """

        from Hlt1Lines.Hlt1TrackLines           import Hlt1TrackLinesConf
        from Hlt1Lines.Hlt1MuonLines            import Hlt1MuonLinesConf
        from Hlt1Lines.Hlt1ElectronLines        import Hlt1ElectronLinesConf
        from Hlt1Lines.Hlt1MBLines              import Hlt1MBLinesConf
        from Hlt1Lines.Hlt1CommissioningLines   import Hlt1CommissioningLinesConf
        from Hlt1Lines.Hlt1BeamGasLines         import Hlt1BeamGasLinesConf
        from Hlt1Lines.Hlt1MVALines             import Hlt1MVALinesConf
        from Hlt1Lines.Hlt1CalibTrackingLines   import Hlt1CalibTrackingLinesConf
        from Hlt1Lines.Hlt1HighMultLines        import Hlt1HighMultLinesConf
        from Hlt1Lines.Hlt1LowMultLines         import Hlt1LowMultLinesConf
        from Hlt1Lines.Hlt1CalibRICHMirrorLines import Hlt1CalibRICHMirrorLinesConf
        from Hlt1Lines.Hlt1IFTLines             import Hlt1IFTLinesConf
        from Hlt1Lines.Hlt1ProtonLines          import Hlt1ProtonLinesConf
        from Hlt1Lines.Hlt1HighMultLines        import Hlt1HighMultLinesConf

        thresholds = { 
                Hlt1IFTLinesConf: {
                    'ODIN': {
                        'BENoBias': 'ODIN_PASS(LHCb.ODIN.NoBias)',
                        'EBNoBias': 'ODIN_PASS(LHCb.ODIN.NoBias)',
                        'BBNoBias': 'ODIN_PASS(LHCb.ODIN.NoBias)',
                        'BBMicroBiasVelo': 'ODIN_PASS(LHCb.ODIN.NoBias)',
                        #'BBHighMult': 'ODIN_PASS(LHCb.ODIN.NoBias)',
                        },
                    'MinVeloTracks': {
                        'BBMicroBiasVelo': 1,
                        },
                    'GEC':{
                        'BBMicroBiasVelo': 'HeavyIons', 
                        'BBHighMult':  'HeavyIons', 
                        },
                    'Prescale': {
                        #To do, set it to one 
                        'Hlt1BBMicroBiasVelo': 1., 
                        'Hlt1BBHighMult': 1, 
                        },
                    }
                ,Hlt1HighMultLinesConf: {
                    'VeloClustersRanges' : [3000, 3600, 4000, 5400, 99999],
                    'ODIN'               : '',
                    'L0'                 : 'L0_DECISION_PHYSICS',
                    'GEC'                : 'HeavyIons' ,
                    'Prescale' : {
                                  "Hlt1HighVeloMult1": 0.0025,
                                  "Hlt1HighVeloMult2": 0.006,
                                  "Hlt1HighVeloMult3": 0.02,
                                  "Hlt1HighVeloMult4": 0.2
                                 }

                    }
                ,Hlt1TrackLinesConf :   {  'Muon_TrNTHits'     : 0 #OFF
                    , 'Muon_Velo_NHits'   : 0 #OFF
                    , 'Muon_Velo_Qcut'    : 999 #OFF
                    , 'Muon_PreFitPT'     : 475.
                    , 'Muon_PreFitP'     : 1500.
                    , 'Muon_PT'           : 500   #<- 910., minimum is 500
                    , 'Muon_P'            : 3000. #<- 6000.
                    , 'Muon_TrGP'         : 0.5 
                    , 'Muon_IPChi2'       : 2.    #10.
                    , 'Muon_TrChi2'       : 4.
                    , 'Muon_GEC'          : 'HeavyIons'    #'Loose'
                    , 'L0Channels'        : {'Muon'   : ('Muon',)}
                    , 'Priorities'        : { 'Muon'   : 5 }
                    }
                , Hlt1MVALinesConf :     {
                    'DoTiming'                     :  False
                    ,'TrackMVA' :    {
                        'TrChi2'      :  4   #    2.5,
                        ,'TrGP'        :  0.5 
                        ,'MinPT'       :  500 *MeV   # 1000.  * MeV,
                        ,'MaxPT'       :  10000.*MeV   #25000.  * MeV,
                        ,'MinIPChi2'   :  6.0   #    7.4,
                        ,'Param1'      :  0.3   #    1.0,
                        ,'Param2'      :  0.0   #    1.0,
                        ,'Param3'      :  0.2   #    1.1,
                        ,'GEC'         :  'HeavyIons'  #'Loose',
                        }  
                    ,'TrackMVALoose' :    {
                        'TrChi2'      :     4
                        ,'TrGP'        :  0.5
                        ,'MinPT'       :  500.  * MeV
                        ,'MaxPT'       : 10000.  * MeV
                        ,'MinIPChi2'   :     6.0
                        ,'Param1'      :     0.3
                        ,'Param2'      :     0.0
                        ,'Param3'      :     0.2
                        ,'GEC'         : 'HeavyIons'
                        }
                    ,'TrackMuonMVA' : {
                        'MinPT'       :  500.  * MeV
                        ,'MaxPT'       : 10000.  * MeV
                        ,'TrChi2'      :     4.0
                        ,'TrGP'        :     0.5
                        ,'MinIPChi2'   :     6.0
                        ,'Param1'      :     0.3
                        ,'Param2'      :     0.0
                        ,'Param3'      :     0.2
                        ,'GEC'         : 'HeavyIons'
                        }
                    ,'TwoTrackMVA' : {
                        'P'           :  2000. * MeV,
                        'PT'          :   300. * MeV,
                        'TrGP'        :     0.5,
                        'TrChi2'      :     4,
                        'IPChi2'      :     4.,
                        'MinMCOR'     :   500. * MeV,
                        'MaxMCOR'     :   1e9  * MeV,
                        'MinETA'      :     2.,
                        'MaxETA'      :     5.,
                        'MinDirA'     :     0.,
                        'V0PT'        :     0. * MeV,
                        'VxChi2'      :    10.,
                        'Threshold'   :     0.95,
                        'MvaVars'     : {
                            'chi2'   : 'VFASPF(VCHI2)',
                            'fdchi2' : 'BPVVDCHI2',
                            'sumpt'  : 'SUMTREE(PT, ISBASIC, 0.0)',
                            'nlt16'  : 'NINTREE(ISBASIC & (BPVIPCHI2() < 16))'
                            },
                        'Classifier'  : {
                            'Type'   : 'MatrixNet',
                            'File'   : '$PARAMFILESROOT/data/Hlt1TwoTrackMVA.mx'
                            },
                        'GEC'         : 'HeavyIons'
                        },
                    'TwoTrackMVALoose' : {
                            'P'           :  2000. * MeV,
                            'PT'          :   300. * MeV,
                            'TrGP'        :     0.5,
                            'TrChi2'      :     4.,
                            'IPChi2'      :     4.,
                            'MinMCOR'     :   500. * MeV,
                            'MaxMCOR'     :   1e9  * MeV,
                            'MinETA'      :     2.,
                            'MaxETA'      :     5.,
                            'MinDirA'     :     0.,
                            'V0PT'        :     0. * MeV,
                            'VxChi2'      :    10.,
                            'Threshold'   :     0.95,
                            'MvaVars'     : {
                                'chi2'   : 'VFASPF(VCHI2)',
                                'fdchi2' : 'BPVVDCHI2',
                                'sumpt'  : 'SUMTREE(PT, ISBASIC, 0.0)',
                                'nlt16'  : 'NINTREE(ISBASIC & (BPVIPCHI2() < 16))'
                                },
                            'Classifier'  : {
                                'Type'   : 'MatrixNet',
                                'File'   : '$PARAMFILESROOT/data/Hlt1TwoTrackMVA.mx'
                                },
                            'GEC'         : 'HeavyIons'
                            },

                    'L0Channels'  : {
                            'TrackMVA'         : 'L0_DECISION_PHYSICS',
                            'TrackMuonMVA'     : {'Muon',},
                            'TwoTrackMVA'      : 'L0_DECISION_PHYSICS',
                            'TrackMVALoose'    : '',
                            'TwoTrackMVALoose' : '' 
                            },
                    'Priorities'  : {
                            'TrackMVA'         : 1,
                            'TwoTrackMVA'      : 2,
                            'TrackMVALoose'    : 3,
                            'TwoTrackMVALoose' : 4,
                            'TrackMuonMVA'     : 5
                            },
                    'Prescale' : {   
                            #for hlt2 open charm, note that there is not l0 requirements
                            #'Hlt1TrackMVALoose'    : 0.0,
                            #'Hlt1TwoTrackMVALoose' : 0.0
                            }
                    }
                , Hlt1ElectronLinesConf : { 'SingleElectronNoIP_P'          : 20000
                        , 'SingleElectronNoIP_PT'         : 10000
                        , 'SingleElectronNoIP_TrChi2'     :     3
                        , 'SingleElectronNoIP_TrNTHits'   :     0 #OFF
                        , 'SingleElectronNoIP_Velo_NHits' :     0 #OFF
                        , 'SingleElectronNoIP_Velo_Qcut'  :   999 #OFF
                        , 'SingleElectronNoIP_GEC'        : 'HeavyIons'
                        , 'L0Channels': { 'SingleElectronNoIP' : ( 'SPD', ) }
                        }
                , Hlt1MuonLinesConf :     { 
                        'SingleMuonHighPT_P'       : 6000
                        , 'SingleMuonHighPT_PT'      : 1300
                        , 'SingleMuonHighPT_TrChi2'  :    4.
                        , 'SingleMuonHighPT_TrGP'    :  0.5
                        , 'SingleMuonHighPT_GEC'     : 'HeavyIons'

                        , 'DiMuonNoIP_VxDOCA'    :  1.0
                        , 'DiMuonNoIP_VxChi2'    :    25.
                        , 'DiMuonNoIP_P'         : 3000.
                        , 'DiMuonNoIP_PT'        : 0.
                        , 'DiMuonNoIP_TrChi2'    :  4.
                        , 'DiMuonNoIP_TrGP'      :  0.5
                        , 'DiMuonNoIP_M'         :  0.
                        , 'DiMuonNoIP_GEC'       : 'HeavyIons'
                        , 'DiMuonNoIP_MuID'  :    'IsMuon'  
                        , 'DiMuonNoIP_CFTracking': False

                        , 'DiMuonLowMass_VxDOCA'     :  1.
                        , 'DiMuonLowMass_VxChi2'     :   25
                        , 'DiMuonLowMass_P'          : 3000
                        , 'DiMuonLowMass_PT'         :    0
                        , 'DiMuonLowMass_TrChi2'     :    4
                        , 'DiMuonLowMass_TrGP'       :  0.5  
                        , 'DiMuonLowMass_M'          :    0.
                        , 'DiMuonLowMass_IPChi2'     :    0.
                        , 'DiMuonLowMass_GEC'        : 'HeavyIons'
                        , 'DiMuonLowMass_CFTracking' : False

                        , 'DiMuonHighMass_VxDOCA'    :  1.
                        , 'DiMuonHighMass_VxChi2'    :   25
                        , 'DiMuonHighMass_P'         : 3000
                        , 'DiMuonHighMass_PT'        :  300
                        , 'DiMuonHighMass_TrGP'       :  999.  
                        , 'DiMuonHighMass_TrChi2'    :    4
                        , 'DiMuonHighMass_M'         : 2500
                        , 'DiMuonHighMass_GEC'       : 'HeavyIons'
                        , 'DiMuonHighMass_CFTracking': False 

                        , 'CalibMuonAlignJpsi_ParticlePT'             : 800     # MeV
                        , 'CalibMuonAlignJpsi_ParticleP'              : 6000    # MeV
                        , 'CalibMuonAlignJpsi_TrackCHI2DOF'           : 2       # dimensionless
                        , 'CalibMuonAlignJpsi_CombMaxDaughtPT'        : 800     # MeV
                        , 'CalibMuonAlignJpsi_CombAPT'                : 1500    # MeV
                        , 'CalibMuonAlignJpsi_CombDOCA'               : 0.2     # mm
                        , 'CalibMuonAlignJpsi_CombVCHI2DOF'           : 10      # dimensionless
                        , 'CalibMuonAlignJpsi_CombVCHI2DOFLoose'      : 10      # dimensionless
                        , 'CalibMuonAlignJpsi_CombDIRA'               : 0.9     # dimensionless
                        , 'CalibMuonAlignJpsi_CombTAU'                : 0.     # ps
                        , 'CalibMuonAlignJpsi_JpsiMassWinLoose'         : 150     # MeV
                        , 'CalibMuonAlignJpsi_JpsiMassWin'              : 100     # MeV
                        , 'CalibMuonAlignJpsi_GEC'                     : 'Loose'     # TODO: Do we want a different one?
                        ,'L0Channels'               : {
                                'SingleMuonHighPT' : ( 'Muon',),
                                'DiMuonLowMass'    : ( 'Muon',),
                                'DiMuonNoIP'    : ( 'Muon',),
                                'DiMuonHighMass'   : ( 'Muon',),
                                'CalibMuonAlignJpsi'    : ( 'Muon',),
                                }
                        , 'Prescale'                 : { 
                                'Hlt1SingleMuonHighPT': 1.0,
                                'Hlt1DiMuonLowMass' : 1.0,
                                'Hlt1DiMuonHighMass' : 1.0,
                                'Hlt1DiMuonNoIP' : 1.0,
                                }
                        , 'Priorities'               : { 
                                'SingleMuonHighPT' : 8,
                                'DiMuonLowMass'    : 7,
                                'DiMuonHighMass'   : 6,
                                }
                        }
            ,Hlt1BeamGasLinesConf: {
                    # Global behaviour settings
                    'FitTracks'             : True,
                    'SplitVertices'         : True,  # implies creation of selections (stored in SelReports)
                    'FullZVetoLumiTriggers' : False,
                    'UseGEC'                : 'HeavyIons',

                    # Minimum number of tracks for the produced vertices (#tr/vtx > X)
                    'VertexMinNTracks'          : 9,  # strictly greater than
                    # 'FullZVertexMinNTracks'     : 9,  # strictly greater than
                    'Beam1VtxMaxBwdTracks'      : -1,  # less or equal than, negative to switch off
                    'Beam2VtxMaxFwdTracks'      : -1,  # less or equal than, negative to switch off

                    # z-ranges for Vertexing
                    'Beam1VtxRangeLow'        : -2000.,
                    'Beam1VtxRangeUp'         :  2000.,
                    'Beam2VtxRangeLow'        : -2000.,
                    'Beam2VtxRangeUp'         :  2000.,
                    # 'BGVtxExclRangeMin'       :  -250.,
                    # 'BGVtxExclRangeMax'       :   250.,

                    'L0Filter' : {
                        'NoBeamBeam1' : "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                        'NoBeamBeam2' : "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                        'Beam1'       : "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                        'Beam2'       : "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                        # 'BB'          : "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                        },
                    'L0RateLimit' : {
                        'NoBeamBeam1'     : None,
                        'NoBeamBeam2'     : None,
                        'Beam1'           : None,
                        'Beam2'           : None,
                        # 'ForcedReco'      : None,
                        # 'ForcedRecoFullZ' : None,
                        },
                    'Prescale' : {
                        'Hlt1BeamGasNoBeamBeam1'             : 1.,
                        'Hlt1BeamGasNoBeamBeam2'             : 1.,
                        'Hlt1BeamGasBeam1'                   : 1.,
                        'Hlt1BeamGasBeam2'                   : 1.,
                        # 'Hlt1BeamGasCrossingForcedReco'      : 1.,
                        # 'Hlt1BeamGasCrossingForcedRecoFullZ' : 1.,
                        },
                    'Postscale' : {
                        'Hlt1BeamGasNoBeamBeam1'             : 1.,
                        'Hlt1BeamGasNoBeamBeam2'             : 1.,
                        'Hlt1BeamGasBeam1'                   : 1.,
                        'Hlt1BeamGasBeam2'                   : 1.,
                        # 'Hlt1BeamGasCrossingForcedReco'      : 1.,
                        # 'Hlt1BeamGasCrossingForcedRecoFullZ' : 1.,
                        },                                                
                    }
                    , Hlt1CalibTrackingLinesConf :  { 'ParticlePT'            : 600     # MeV
                            ,'ParticleP'             : 4000    # MeV
                            ,'ParticlePT_LTUNB'      : 800     # MeV 
                            ,'ParticleP_LTUNB'       : 4000    # MeV
                            ,'L0_DHH_LTUNB'          : ['SPD','Muon', 'CALO']
                            ,'L0_DHH_LTUNB_Lines'    : ['CalibTrackingPiPi','CalibTrackingKPi','CalibTrackingKK']
                            ,'TrackCHI2DOF'          : 2       # dimensionless
                            ,'CombMaxDaughtPT'       : 900     # MeV 900
                            ,'CombAPT'               : 1800    # MeV 1200
                            ,'CombDOCA'              : 0.1     # mm
                            ,'CombVCHI2DOF'          : 10      # dimensionless
                            ,'CombVCHI2DOFLoose'     : 15      # dimensionless
                            ,'CombDIRA'              : 0.99    # dimensionless
                            ,'CombTAU'               : 0.25    # ps
                            ,'D0MassWinLoose'        : 100     # MeV
                            ,'D0MassWin'             : 60      # MeV
                            ,'B0MassWinLoose'        : 200     # MeV
                            ,'B0MassWin'             : 150     # MeV
                            ,'D0DetachedDaughtsIPCHI2': 9      # dimensionless
                            ,'D0DetachedIPCHI2'       : 9      # dimensionless
                            ,'BsPhiGammaMassMinLoose': 3350    # MeV
                            ,'BsPhiGammaMassMaxLoose': 6900    # MeV
                            ,'BsPhiGammaMassMin'     : 3850    # MeV
                            ,'BsPhiGammaMassMax'     : 6400    # MeV
                            ,'PhiMassWinLoose'       : 50      # MeV
                            ,'PhiMassWin'            : 30      # MeV
                            ,'PhiMassWinTight'       : 20      # MeV
                            ,'PhiPT'                 : 1800    # MeV
                            ,'PhiPTLoose'            : 800     # MeV
                            ,'PhiSumPT'              : 3000    # MeV
                            ,'PhiIPCHI2'             : 16      # dimensionless
                            ,'B0SUMPT'               : 4000    # MeV
                            ,'B0PT'                  : 1000    # MeV
                            ,'GAMMA_PT_MIN'          : 2000    # MeV
                            ,'Velo_Qcut'             : 999     # OFF
                            ,'TrNTHits'              : 0       # OFF
                            ,'ValidateTT'            : False
                            ,'GEC'            : "HeavyIons"
                            ,'Prescale'  : { 'Hlt1CalibTrackingKPi': 0.2 }
                            }
                    , Hlt1CommissioningLinesConf : { 
                            'Postscale' : { 
                                'Hlt1Incident'     : 'RATE(1)'
                                , 'Hlt1Tell1Error'   : 'RATE(10)'
                                , 'Hlt1ErrorEvent'   : 'RATE(0.01)'
                                , 'Hlt1NZSVelo'      : 'RATE(1)'
                                , 'Hlt1GEC.*'        : 'RATE(1)'
                                , 'Hlt1VeloClosingMicroBias' : 'RATE(500)'
                                }
                            ,'GEC': 'Loose'             
                            }
                    # micro bias lines switched off for high mu physics running
                       , Hlt1MBLinesConf :     { 
                               'Prescale' : { 
                                   'Hlt1MBMicroBiasVelo'                : 0
                                   , 'Hlt1MBMicroBiasTStation'            : 0
                                   , 'Hlt1MBMicroBiasVeloRateLimited'     : 0
                                   , 'Hlt1MBMicroBiasTStationRateLimited' : 0 
                                   , 'Hlt1MBNoBias'                       : 0.003
                                   }
                               , 'MaxNoBiasRate' : 1000000.
                               }
                       , Hlt1CalibRICHMirrorLinesConf : { 
                               'Prescale' : { 
                                   'Hlt1CalibHighPTLowMultTrks'     : 0.00001,  #prescale further
                                   'Hlt1CalibRICHMirrorRICH1'       : 0.281,
                                   'Hlt1CalibRICHMirrorRICH2'       : 1.0
                                   }
                               , 'DoTiming' : False
                               , 'R2L_PreFitPT'    : 475. * MeV
                               , 'R2L_PreFitP'     : 38000. * MeV 
                               , 'R2L_PT'       : 500. * MeV
                               , 'R2L_P'        : 40000. * MeV
                               , 'R2L_MinETA'   : 2.65
                               , 'R2L_MaxETA'   : 2.80
                               , 'R2L_Phis'     : [(-2.59, -2.49), (-0.65, -0.55), (0.55, 0.65), (2.49, 2.59)]
                               , 'R2L_TrChi2'   : 2.
                               , 'R2L_MinTr'    : 0.5
                               , 'R2L_GEC'      : 'Loose'
                               , 'R1L_PreFitPT' : 475. * MeV
                               , 'R1L_PreFitP'  : 19000. * MeV 
                               , 'R1L_PT'       : 500. * MeV
                               , 'R1L_P'        : 20000. * MeV
                               , 'R1L_MinETA'   : 1.6
                               , 'R1L_MaxETA'   : 2.04
                               , 'R1L_Phis'     : [(-2.65, -2.30 ), (-0.80, -0.50), (0.50, 0.80), (2.30, 2.65)]
                               , 'R1L_TrChi2'   : 2.
                               , 'R1L_MinTr'    : 0.5
                               , 'R1L_GEC'      : 'Loose'
                               , 'LM_PreFitPT' : 475. * MeV
                               , 'LM_PreFitP'  : 950. * MeV 
                               , 'LM_PT'    : 500. * MeV
                               , 'LM_P'     : 1000. * MeV
                               , 'LM_TrChi2': 2.
                               , 'LM_MinTr' : 1
                               , 'LM_MaxTr' : 40
                               , 'LM_GEC'   : 'Loose'
                               }
                       , Hlt1LowMultLinesConf : {   
                               'Prescale' : { 
                                   'Hlt1LowMultPassThrough'             : 0.01,#0.01,  # To do
                                   'Hlt1NoBiasNonBeamBeam'              : 0.07,
                                   #
                                   'Hlt1LowMultMuon'                    : 1.00,
                                   'Hlt1LowMultVeloCut_Hadrons'         : 1.,#0.10,
                                   'Hlt1LowMultVeloCut_Leptons'         : 1.,#0.10,
                                   'Hlt1LowMultMaxVeloCut'              : 1.,#0.10,
                                   #
                                   'Hlt1LowMult'                        : 0.00, #off
                                   #
                                   'Hlt1LowMultHerschel'                : 0.00, #off
                                   'Hlt1LowMultVeloAndHerschel_Hadrons' : 1.00,
                                   'Hlt1LowMultVeloAndHerschel_Leptons' : 1.00,
                                   'Hlt1LowMultMaxVeloAndHerschel'      : 1.00
                                   }
                               #
                               , 'SpdMult'                                 :   100.   # dimensionless
                               , 'VeloCut_Hadrons_MinVelo'                 :     2    # dimensionless
                               , 'VeloCut_Hadrons_MaxVelo'                 :     8    # dimensionless
                               , 'VeloCut_Hadrons_SpdMult'                 :    20    # dimensionless
                               , 'VeloCut_Leptons_MinVelo'                 :     0    # dimensionless
                               , 'VeloCut_Leptons_MaxVelo'                 :    10    # dimensionless
                               , 'MaxVeloCut_MaxVelo'                      :     8    # dimensionless
                               , 'MaxNVelo'                                :    10    # dimensionless for generic Hlt1LowMult
                               , 'MinNVelo'                                :     0    # dimensionless for generic Hlt1LowMult
                               #
                               , 'MaxHRCADC_B2'                            :   750    # dimensionless,
                               , 'MaxHRCADC_B1'                            :   750    # dimensionless,
                               , 'MaxHRCADC_B0'                            :   750    # dimensionless,
                               , 'MaxHRCADC_F1'                            :   750    # dimensionless,
                               , 'MaxHRCADC_F2'                            :   750    # dimensionless,
                               #
                               , 'TrChi2'                                  :     5.   # dimensionless,
                               , 'PT'                                      :   500.   # MeV
                               , 'P'                                       :     0.   # MeV
                               # To Do

                               , 'LowMultLineL0Dependency'                 : "L0_CHANNEL_RE('.*lowMult')"   #"( L0_CHANNEL_RE('.*(?<!Photon),lowMult') )"
                               , 'LowMultVeloCut_HadronsLineL0Dependency'  : "( L0_CHANNEL_RE('DiHadron,lowMult') )"
                               , 'LowMultVeloCut_LeptonsLineL0Dependency'  : "( L0_CHANNEL_RE('Muon,lowMult') | L0_CHANNEL_RE('DiMuon,lowMult') | L0_CHANNEL_RE('Electron,lowMult') )"
                               , 'LowMultMaxVeloCutLineL0Dependency'       : "( L0_CHANNEL_RE('Photon,lowMult') | L0_CHANNEL_RE('DiEM,lowMult') )"
                               , 'LowMultPassThroughLineL0Dependency'      : "L0_CHANNEL_RE('.*lowMult')"
                               , 'NoBiasNonBeamBeamODIN'                   : "ODIN_PASS(LHCb.ODIN.Lumi) & ~(ODIN_BXTYP == LHCb.ODIN.BeamCrossing)"


                               }
                       , Hlt1ProtonLinesConf :   { 
                               'DiProton_SpdMult'    :   99999.   # removed, otherwise useless
                               , 'DiProton_PreFitPT'   :  1800.   # MeV
                               , 'DiProton_PreFitP'    : 12000.   # MeV
                               , 'DiProton_PT'         :  1900.   # MeV
                               , 'DiProton_P'          : 12500.   # MeV
                               , 'DiProton_theta'      : 0.0366   # Proton eta>4
                               , 'DiProton_TrChi2'     :     2.5
                               , 'DiProton_TrGP'       :     0.2 # ghost prob 
                               , 'DiProton_MassMin'    :  2800.   # MeV, after Vtx fit
                               , 'DiProton_MassMax'    :  3300.   # MeV, after Vtx fit
                               , 'DiProton_VtxDOCA'    :     0.1
                               , 'DiProton_VtxChi2'    :     4.   # dimensionless
                               , 'DiProton_JpsiPT'     :  6500.   # MeV
                               , 'DiProtonLowMult_SpdMult'    :    10.   # dimensionless, Spd Multiplicy cut
                               , 'DiProtonLowMult_PT'         :   500.   # MeV, same as LooseForward
                               , 'DiProtonLowMult_P'          :  6000.   # MeV, same as LooseForward
                               , 'DiProtonLowMult_MassMin'    :  2800.   # MeV, after Vtx fit
                               , 'DiProtonLowMult_VtxDOCA'    :     0.3
                               , 'DiProtonLowMult_VtxChi2'    :    25.   # dimensionless
                               , 'GEC'    :    'HeavyIons'
                               , 'L0Channels'   :    {   
                                   'DiProton' : ['SPD']
                                   }   
                               }
                       #,Hlt1HighMultLinesConf{
                       #       Prescale:{
                       #           "Hlt1HighVeloMult":1.
                       #        },
                       #       'MinVeloHits'        : 2400,
                       #       'MaxVeloHits'        : 99999,
                       #       'nPVs'               :  1 ,
                       #       'MinVeloHits_PV'     : 2400,
                       #       'ODIN'               : '',
                       #       'L0'                 : 'L0_DECISION_PHYSICS',
                       #       'GEC'                : 'HeavyIons',
                       #        }
                } # Thresholds

        # HLT2 thresholds from individual files
        sds = self.SubDirs()
        for monthyear, subdirs in self.SubDirs().iteritems():
            for subdir in subdirs:
                conf = __get_conf__(subdir, "_%s" %(monthyear))
                update_thresholds(thresholds, conf.Thresholds())

        return thresholds

    def ActiveHlt2Lines(self) :
        """
        Returns a list of active lines
        """

        hlt2 = []
        sds = self.SubDirs()
        for monthyear, subdirs in self.SubDirs().iteritems():
            for subdir in subdirs:
                conf = __get_conf__(subdir, "_%s" %(monthyear))
                hlt2.extend(conf.ActiveHlt2Lines())

        return hlt2

    def ActiveHlt1Lines(self) :
        """
        Returns a list of active lines
        """
        lines =  [ 
                # MB lines
                "Hlt1BBHighMult" ,'Hlt1BBMicroBiasVelo'
                #High mult line for ridge Analysis
                , "Hlt1HighVeloMult1", "Hlt1HighVeloMult2", "Hlt1HighVeloMult3", "Hlt1HighVeloMult4"
                #Muon lines
                , 'Hlt1SingleMuonHighPT' , 'Hlt1DiMuonLowMass', 'Hlt1DiMuonHighMass', 'Hlt1DiMuonNoIP'
                #Electron lines for DY
                , 'Hlt1SingleElectronNoIP'
                #Track (MVA) lines
                ,'Hlt1TrackMuon', 'Hlt1TrackMuonMVA'
                ,'Hlt1TrackMVA', 'Hlt1TwoTrackMVA' , 'Hlt1TrackMVALoose', 'Hlt1TwoTrackMVALoose'
                #Calib lines 
                , 'Hlt1CalibTrackingKPi' , 'Hlt1CalibHighPTLowMultTrks', 'Hlt1CalibTrackingKPiDetached' 
                , 'Hlt1CalibMuonAlignJpsi' , 'Hlt1B2HH_LTUNB_PiPi'
                #, 'Hlt1CalibTrackingKPi' , 'Hlt1CalibTrackingKK' , 'Hlt1CalibTrackingPiPi' , 'Hlt1CalibHighPTLowMultTrks', 'Hlt1CalibTrackingKPiDetached' 
                #, 'Hlt1CalibMuonAlignJpsi' , 'Hlt1B2HH_LTUNB_KPi' , 'Hlt1B2HH_LTUNB_KK' , 'Hlt1B2HH_LTUNB_PiPi'
                #, 'Hlt1B2PhiGamma_LTUNB'
                , 'Hlt1IncPhi'
                , 'Hlt1CalibRICHMirrorRICH1'
                , 'Hlt1CalibRICHMirrorRICH2'
                #DiProton lines
                , 'Hlt1DiProton'
                , 'Hlt1DiProtonLowMult'
                #CEP lines
                , 'Hlt1LowMultMuon' 
                , 'Hlt1LowMultVeloCut_Hadrons'
                , 'Hlt1LowMultVeloAndHerschel_Hadrons'
                , 'Hlt1LowMultVeloAndHerschel_Leptons' 
                , 'Hlt1LowMultMaxVeloAndHerschel'
                , 'Hlt1LowMultVeloCut_Leptons'
                , 'Hlt1LowMultMaxVeloCut'
                , 'Hlt1LowMultPassThrough'
                , 'Hlt1NoBiasNonBeamBeam'
                ]




        #provide beam gas lines
        from Hlt1TechnicalLines import Hlt1TechnicalLines
        lines.extend( Hlt1TechnicalLines().ActiveHlt1Lines() )

        return lines

    def Streams(self):
        #TURBO(++)       J-psi and open charm lines
        #NOBIAS          NB events
        #TURCAL          calibration lines
        #FULL            all the rest
        return {
                # None means use default
                'FULL': "HLT_NONTURBOPASS_RE('Hlt2(?!BeamGas)(?!Lumi)(?!MBMicroBiasVelo).*Decision')",
                # FULL default is "HLT_NONTURBOPASS_RE('Hlt2(?!BeamGas)(?!Lumi).*Decision')"
                'TURBO': None,
                'TURCAL': None,
                # NOBIAS redefined to be BBMicroBiasVelo
                'NOBIAS': "HLT_PASS('Hlt2MBMicroBiasVeloDecision') | HLT_PASS('Hlt2MBHighMultDecision')",
                # NOBIAS default is "HLT_PASS('Hlt2MBNoBiasDecision')"
                'BEAMGAS': None,
                'LUMI': None,
                'EXPRESS': None,
                'HLT1NOBIAS' : None,
                'VELOCLOSING': None,
                }

    def StreamsWithBanks(self):
            return [(["FULL"], 'KILL', ['DstData']),
                    (["BEAMGAS"], 'KEEP', ['ODIN', 'HltRoutingBits', 'DAQ', 'L0DU', 'Velo', 'HC', 'HltDecReports', 'HltSelReports', ]),
                    (["TURCAL"], 'KILL', ['DstData']),
                    (["TURBO"], 'KILL', []),
                    (["NOBIAS"], 'KILL', ['DstData']),
                    ]

    def StreamsWithLumi(self):
                return ['FULL', 'TURCAL', 'TURBO', 'NOBIAS']
