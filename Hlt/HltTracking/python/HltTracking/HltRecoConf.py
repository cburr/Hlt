#!/usr/bin/env gaudirun.py
# =============================================================================
# $Id: $
# =============================================================================
## @file
#  Configuration of Hlt Reconstruction Algorithms
#  @author Sebastian Neubert, sebastian.neubert@cern.ch
#  @date 2014-11-24
# =============================================================================
"""
 Parameters for the HLT reconstruction algorithms
 Note: the actual definition of the tracking sequences can be found in
 HltSharedTracking.py
 Hlt1Tracking.py
 and
 Hlt2Tracking.py
 Line-specific parameters should go into the respectiv HltLine configurables
"""
# =============================================================================
__author__  = "Sebastian Neubert, sebastian.neubert@cern.ch"
__version__ = ""
# =============================================================================

from Gaudi.Configuration import *
from LHCbKernel.Configuration import *

from GaudiKernel.SystemOfUnits import MeV, mm, m
from Configurables import TrackSys
class HltRecoConf(LHCbConfigurableUser):
    __used_configurables__ = [ TrackSys ]
    __slots__ = { "DataType"                    : "2016"
                , "Forward_HPT_MinPt"           :  500. * MeV
                , "Forward_HPT_MinP"            : 3000. * MeV
                , "Forward_LPT_Muon_MinPt"      :  300. * MeV
                , "Forward_LPT_Muon_MinP"       : 3000. * MeV
                , "Forward_ULPT_Muon_MinPt"     :  80. * MeV
                , "Forward_ULPT_Muon_MinP"      : 3000. * MeV
                , "Forward_LPT_MinPt"           :   80. * MeV  # was 200
                , "Forward_LPT_MinP"            : 1000. * MeV # was 3000
                , "Forward_MaxOTHits"           : 15000
                , "MatchVeloMuon_MinP"          : 4000. * MeV
                , "GoodTrCHI2PDOF"              : 4.0  # This TrCHI2PDOF is used in the sequence to mark hits of good tracks.
                , "MaxTrCHI2PDOF"               : 4.0  # This TrCHI2PDOF is used in the making of fitted tracks.
                , "ApplyGHOSTPROBCut"           : False  # Enable cut on ghost probability in the making of protoparticles.
                , "ApplyGHOSTPROBCutInTBTC"     : True  # Enable cut on ghost probability in the creation of fitted tracks.
                , "ApplyGHOSTPROBCutInHLT1"     : True  # Enable cut on ghost probability in the fitting of Hlt1 tracks
                , "MaxTrGHOSTPROB"              : 0.4  # Cut value of ghost probability for above options.
                , "VeloSelectionCut"            : "(~TrBACKWARD) & ( TrNVELOMISS < 100 )"
                , "FitVelo"                     : True
                , "FastFitVelo"                 : True
                , "OfflineSeeding"              : True
                , "CalculateProbNN"             : True
                , "AddGhostProb"                : True
                , "InitFits"                    : True
                , "BeamGasMode"                 : False
                , "VeloTrackingZMin"            : -2000.   #minimum velo tracking range
                , "VeloTrackingZMax"            : +2000.   #maximum velo tracking range
                , "MoreOfflineLikeFit"          : True   # This runs a fit in HLT1 and HLT2 which uses the default offline fit
                                                         # with a simplified material description
                , "FitIterationsInHltFit"       : 1      # Increases the number of fit iterations in the current Hlt fit.
                , "NewMSinFit"                  : True   # Use the new description of the multiple scattering term
                , "PVOptions"                   : { "UseBeamSpotRCut"       : True,
                                                    "BeamSpotRCut"          : 0.2 * mm,
                                                    "TrackErrorScaleFactor" : 1.,
                                                    "ResolverBound"         : 5 * mm,
                                                    "MinTracks"             : 4.,
                                                    "trackMaxChi2"          : 12.,
                                                    "BeamSpotRHighMultiplicityCut": 0.4*mm,
                                                    "BeamSpotRMultiplicityTreshold": 10}
                }


    def __apply_configuration__(self):
        """
        Apply Hlt configuration
        """

        # Configure the PV refitting correctly
        from Configurables import LoKi__PVReFitter
        LoKi__PVReFitter("LoKi::PVReFitter").CheckTracksByLHCbIDs = True

        # Configure TrackSys
        self.setOtherProps(TrackSys(),[ "DataType" ])
        TrackSys().HltMode = True
        
        # Configure the position tool for lite (and full) clusters
        from STTools import STOfflineConf
        STOfflineConf.DefaultConf().configureTools()

MatchVeloMuonOptions = {"MaxChi2DoFX" : 10.,
                        "XWindow" : 200.,
                        "YWindow" : 400.
                        }

MatchVeloTTMuonOptions = {"MaxChi2DoFX" : 2.,
                          "HistoProduce" : False,
                          "Window" : {1 : (100, 200),
                                      2 : (200, 400),
                                      3 : (300, 500),
                                      4 : (400, 600)},
                          "ErrorCorrections" : {'mx' : [9.55, -0.5, -143],
                                                'my' : [7.46, -0.5, -112],
                                                'x'  : [1., 1., 1., 1.],
                                                'y'  : [1., 1., 1., 1.]}
                          }

VeloTTToolOptions = {
    "PassTracks": True,
}

VeloTTOptions = { }

CommonForwardOptions = { "MaxITHits" : 999999 ,
                         "MaxNVelo" : 999999 }

#Sascha: Rename this to tool,
CommonForwardTrackingOptions = { "MinHits" : 12,
                                 "MinOTHits" : 14,
                                 "AddTTClusterName" : "PatAddTTCoord" }

#Sascha: Rename this to tool and hlt1
ForwardTrackingOptions_MomEstimate = { "UseMomentumEstimate" : True,
                                       "UseWrongSignWindow"  : True,
                                       "WrongSignPT"         : 2000.,
                                       "Preselection"        : True,
                                       "PreselectionPT"      : 400.,
                                       "SecondLoop"          : False,
                                       # optimized settings for 2016 Data taking
                                       "MaxXCandidateSize"   : [60,30],
                                       "MaxSpreadX"          : 0.65,
                                       "MaxSpreadSlopeX"     : 0.0075,
                                       "MaxChi2X"            : 36,
                                       "MaxChi2Y"            : 28,
                                       "NNAfterStereoFit"    : True,
                                       "NNAfterStereoFitCut" : 0.08,
                                       "NextBestResponse"    : 0.65 }

ComplementForwardToolOptions = { "SecondLoop"          : True,
                                 "MaxXCandidateSize"   : [80,32,18],
                                 "MaxSpreadX"          : 0.8,
                                 "MaxSpreadSlopeX"     : 0.007,
                                 "MaxChi2X"            : 48,
                                 "MaxChi2Y"            : 38,
                                 "NNBeforeXFit"        : True,
                                 "NNAfterStereoFit"    : True,
                                 "NNBeforeXFitCut"     : 0.10,
                                 "NNAfterStereoFitCut" : 0.06,
                                 "NextBestResponse"    : 0.65
                                 }

OnlineSeedingToolOptions = { "NDblOTHitsInXSearch" : 2,
                             "MinMomentum" : 1000.}

OfflineSeedingToolOptions = { "NDblOTHitsInXSearch" : 0,
                              "MinMomentum" : 500.}


DownstreamOptions = { "MinMomentum": 0.,
                      "MinPt": 0.}

MuonTTOptions = {
               "SeedStation"  : 2,
               "YTolSlope"    : 40000.,
               "XTolSlope"    : 40000.,
               "XTol"         : 25.,
               "MaxChi2Tol"   : 7.,
               "MinAxProj"    : 5.5,
               "MaxAxProj"    : 25.
               }

