// Include files 

//from Event
#include "Event/Track.h"

// local
#include "HltTrackFit.h"

//-----------------------------------------------------------------------------
// Implementation file for class : HltTrackFit
//
// 2008-10-31 : Johannes Albrecht
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_TOOL_FACTORY( HltTrackFit )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
HltTrackFit::HltTrackFit( const std::string& type,
                          const std::string& name,
                          const IInterface* parent )
  : base_class ( type, name , parent )
{
  declareInterface<ITracksFromTrack>(this);
  declareProperty("FitterName", m_fitName = "TrackMasterFitter/Fit");
  declareProperty("FailureWarnThreshold", m_failureWarnThreshold = 0.0005);
}

//=============================================================================
StatusCode HltTrackFit::initialize() 
{
  StatusCode sc = GaudiTool::initialize();
  if (sc.isSuccess()) m_fit = tool<ITrackFitter>(m_fitName, this);
  m_fitFailureCounter = &counter("FitFailed");
  return sc;
}

//=============================================================================
StatusCode HltTrackFit::tracksFromTrack( const LHCb::Track& seed,
		                                 std::vector<LHCb::Track*>& tracks ) const
{
  std::unique_ptr<LHCb::Track> tr( seed.clone() );
  StatusCode sc = m_fit->fit( *tr );
  *m_fitFailureCounter += sc.isFailure();
  if( sc.isFailure() ) {
    if ( m_failureWarnThreshold * m_fitFailureCounter->nEntries() > 1. && m_fitFailureCounter->mean() > m_failureWarnThreshold )
      warning() << "Fit failure rate high. " << endmsg;
  } else {
    tracks.push_back( tr.release() );
  }
  return sc;
}
