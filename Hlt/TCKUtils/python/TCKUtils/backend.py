import os
import weakref
from pprint import pprint, pformat
from functools import wraps
from multiprocessing.managers import BaseManager

import GaudiPython
import cppyy
from Gaudi.Configuration import *
from Configurables import ConfigTreeEditor, PropertyConfigSvc
from Configurables import (ConfigStackAccessSvc,
                           ConfigDBAccessSvc,
                           ConfigZipFileAccessSvc,
                           ConfigTarFileAccessSvc,
                           ConfigFileAccessSvc,
                           ConfigCDBAccessSvc)


# Decorate the MD5 object
MD5 = cppyy.gbl.Gaudi.Math.MD5
MD5.__str__ = MD5.str
MD5.__hash__ = lambda x: x.str().__hash__()
MD5.__eq__ = lambda self, other: self.str() == str(other)
MD5.__ne__ = lambda self, other: self.str() != str(other)
digest = MD5.createFromStringRep

ConfigTreeNodeAlias = cppyy.gbl.ConfigTreeNodeAlias
ConfigTreeNodeAlias.alias_type.__str__ = ConfigTreeNodeAlias.alias_type.str
alias = ConfigTreeNodeAlias.alias_type
topLevelAlias = ConfigTreeNodeAlias.createTopLevel
TCK = ConfigTreeNodeAlias.createTCK


def _appMgr():
    # print 'starting appMgr @ pid = %s' % os.getpid()
    ApplicationMgr().AppName = ""
    ApplicationMgr().OutputLevel = ERROR
    appMgr = GaudiPython.AppMgr()
    appMgr.initialize()
    return appMgr


def _tck(x):
    if isinstance(x, basestring) and x.upper().startswith('0X'):
        return int(x, 16)
    return int(x)


def _digest(x):
    if type(x) == str:
        x = digest(x)
    return x


def cdb_file(filename):
    return os.path.join(os.environ['HLTTCKROOT'], filename)


def cdb_file_exists(filename):
    return os.path.isfile(cdb_file(filename))

ConfigAccessSvc = ConfigCDBAccessSvc if cdb_file_exists('config.cdb') \
                  else ConfigTarFileAccessSvc


def get_default_cas():
    from Gaudi.Configuration import allConfigurables
    if 'ConfigAccessSvc' not in allConfigurables:
        filename = cdb_file('config.cdb') if cdb_file_exists('config.cdb') \
                   else cdb_file('config.tar')
        print 'Using {} as input.'.format(filename)
        return ConfigAccessSvc('ConfigAccessSvc', File=filename)
    else:
        return allConfigurables['ConfigAccessSvc']
default_cas = get_default_cas()


def getConfigTree(id, cas=default_cas):
    if not hasattr(getConfigTree, 'forest'):
        getConfigTree.forest = dict()
    if id not in getConfigTree.forest:
        tree = AccessProxy().access(cas).rgetConfigTree(id)
        getConfigTree.forest[id] = tree
        if tree.digest != id:
            # in case we got a TCK, the remote side resolves this to an ID
            # and we mark this ID in our cache. Unfortunately, it doesn't work
            # the other way around (i.e. we first get an ID, then a TCK )
            # and we must rely on the remote to cache as much as possible...
            getConfigTree.forest[tree.digest] = tree
    return getConfigTree.forest[id]


def accept_id(f):
    @wraps(f)
    def decorated(tree, *args, **kwargs):
        cas = kwargs.pop('cas', default_cas)
        if not isinstance(tree, Tree):
            tree = getConfigTree(tree, cas=cas)
        return f(tree, *args, **kwargs)
    return decorated


class AccessSvcSingleton(object):
    __app = None
    __pcs = None
    __cas = None
    __cte = None

    def reset(self):
        if self._app():
            self._app().finalize()
        AccessSvcSingleton.__pcs = None
        AccessSvcSingleton.__cas = None
        AccessSvcSingleton.__cte = None
        AccessSvcSingleton.__app = None

    def _app(self):
        return AccessSvcSingleton.__app

    def _pcs(self):
        return AccessSvcSingleton.__pcs

    def _cas(self):
        return AccessSvcSingleton.__cas

    def _cte(self):
        if callable(AccessSvcSingleton.__cte):
            AccessSvcSingleton.__cte = AccessSvcSingleton.__cte(self)
        return AccessSvcSingleton.__cte

    def __init__(self, create=False, createConfigTreeEditor=None, cas=None):
        if create:
            if (AccessSvcSingleton.__pcs or AccessSvcSingleton.__cas):
                raise LogicError('re-entry of singleton creation')
            pcs = PropertyConfigSvc(ConfigAccessSvc=cas.getFullName())
            cte = ConfigTreeEditor(
                PropertyConfigSvc=pcs.getFullName(), ConfigAccessSvc=cas.getFullName())
            AccessSvcSingleton.__app = _appMgr()
            self._app().createSvc(cas.getFullName())
            AccessSvcSingleton.__cas = self._app().service(
                cas.getFullName(), 'IConfigAccessSvc')
            self._app().createSvc(pcs.getFullName())
            AccessSvcSingleton.__pcs = self._app().service(
                pcs.getFullName(), 'IPropertyConfigSvc')
            AccessSvcSingleton.__cte = lambda x: self._app().toolsvc().create(
                cte.getFullName(), interface='IConfigTreeEditor')

    def resolveTCK(self, tck):
        """Return the id (digest) for a TCK."""
        tck = _tck(tck)
        for i in self._cas().configTreeNodeAliases(alias('TCK/')):
            if _tck(i.alias().str().split('/')[-1]) == tck:
                return digest(i.ref().str())
        raise KeyError('Not a valid TCK: {:#x}'.format(tck))

    def _2id(self, id):
        if type(id) is int:
            id = '0x%08x' % id
        if type(id) is str and len(id) == 32:
            id = _digest(id)
        #  if we're not a valid id at this point, maybe we're a TCK...
        if type(id) is not MD5:
            id = self.resolveTCK(id)
        return id

    def resolveConfigTreeNode(self, id):
        if type(id) is not MD5:
            id = self._2id(id)
        return self._pcs().resolveConfigTreeNode(id) if (id and id.valid()) else None

    def writeConfigTreeNode(self, node):
        return self._cas().writeConfigTreeNode(node)

    def resolvePropertyConfig(self, id):
        return self._pcs().resolvePropertyConfig(id) if (id and id.valid()) else None

    def writePropertyConfig(self, node):
        return self._cas().writePropertyConfig(node)

    def collectLeafRefs(self, id):
        if type(id) is not MD5:
            id = self._2id(id)
        for ids in self._pcs().collectLeafRefs(id):
            yield PropCfg(ids)

    def resolveConfigTreeNodeAliases(self, a):
        if type(a) is not type(alias):
            a = alias(a)
        return self._pcs().configTreeNodeAliases(a)

    def configTreeNodeAliases(self, alias):
        return self._cas().configTreeNodeAliases(alias)

    def writeConfigTreeNodeAlias(self, alias):
        return self._cas().writeConfigTreeNodeAlias(alias)

    def updateAndWrite(self, id, updates, label, copy=False):
        # For copy we still need the next bit, but the updates are empty
        if not updates and not copy:
            return
        if updates:
            print 'updateAndWrite updates:'
            pprint(dict(updates))
        mod_map = cppyy.gbl.std.multimap(
            'std::string', 'std::pair<std::string, std::string>')
        value_pair = cppyy.gbl.std.pair('std::string', 'std::string')
        insert_pair = cppyy.gbl.std.pair(
            'const std::string', 'std::pair<std::string, std::string>')
        mods = mod_map()
        for algname, props in updates.iteritems():
            for k, v in props.iteritems():
                vp = value_pair(k, v if type(v) == str else repr(v))
                ip = insert_pair(algname, vp)
                mods.insert(ip)

        return self._cte().updateAndWrite(id, mods, label)


def createAccessSvcSingleton(cas, createConfigTreeEditor=False):
    return AccessSvcSingleton(True, createConfigTreeEditor, cas)


class Configuration:
    " A class representing a configuration entry "

    def __init__(self, alias, svc):
        self.info = {'id': alias.ref().str(), 'TCK': [], 'label': '<NONE>'}
        self.info.update(zip(['release', 'hlttype'],
                             alias.alias().str().split('/')[1:3]))
        x = svc.resolveConfigTreeNode(alias.ref())
        if not x:
            print 'Configuration: failed to resolve configtreenode %s - %s ' % (alias.alias().str(), alias.ref())
        label = x.label()
        self.info.update({'label': label})

    def __getitem__(self, label):
        return self.info[label]

    def update(self, d):
        self.info.update(d)

    def printSimple(self, prefix='      '):
        if not self.info['TCK']:
            print prefix + '%10s : %s : %s' % ('<NONE>', self.info['id'], self.info['label'])
        else:
            for tck in self.info['TCK']:
                if type(tck) == int:
                    tck = '0x%08x' % tck
                print prefix + '%10s : %s : %s' % (tck, self.info['id'], self.info['label'])

    def PVSS(self):
        for tck in self.info['TCK']:
            if type(tck) == str and not tck.startswith('0x'):
                tck = int(tck)
            if type(tck) != str:
                tck = '0x%08x' % tck
            return '%20s : %10s : %s : %s\n' % (self.info['hlttype'], tck, self.info['id'], self.info['label'])


class TreeNode(object):
    " A class representing a ConfigTreeNode "
    #  use flyweight pattern, and use digest to identify objects...
    import weakref
    _pool = weakref.WeakValueDictionary()
    _nreq = 0
    _noid = 0
    _cm = 0
    #  TODO: add functionality to flush _pool

    def __new__(cls, id=None):
        TreeNode._nreq = TreeNode._nreq + 1
        if not id:
            TreeNode._noid = TreeNode._noid + 1
            return object.__new__(cls)
        if type(id) != MD5 and type(id) != int:
            print id, type(id)
            id = digest(id)
        obj = TreeNode._pool.get(id)
        if not obj:
            TreeNode._cm = TreeNode._cm + 1
            obj = AccessSvcSingleton().resolveConfigTreeNode(id)
            TreeNode._pool[id] = obj
        if not obj:
            print 'failed to resolve ConfigTreeNode %s' % id
        return obj


class PropCfg(object):
    " A class representing a PropertyConfig "
    #  use flyweight pattern, and use digest to identify objects...
    __slots__ = ('__weakref__', 'name', 'type', 'kind', 'digest', 'props')

    import weakref
    _pool = weakref.WeakValueDictionary()
    _nreq = 0
    _noid = 0
    _cm = 0
    #  TODO: make a singleton svc which we use to resolve IDs if not existent yet...
    #  TODO: add functionality to flush _pool

    def __new__(cls, id=None):
        PropCfg._nreq = PropCfg._nreq + 1
        if not id:
            PropCfg._noid = PropCfg._noid + 1
            # added to make it possible to recv in parent process...
            return object.__new__(cls)
        if type(id) != MD5:
            print id, type(id)
            id = digest(id)
        obj = PropCfg._pool.get(id)
        if not obj:
            PropCfg._cm = PropCfg._cm + 1
            x = AccessSvcSingleton().resolvePropertyConfig(id)
            if not x:
                print 'failed to resolve PropertyConfig ' + id
            obj = object.__new__(cls)
            obj.name = x.name()
            obj.type = x.type()
            obj.kind = x.kind()
            obj.digest = id
            obj.props = dict((i.first, i.second) for i in x.properties())
            PropCfg._pool[id] = obj
        if not obj:
            print 'invalid PropCfg %s' % id
        return obj

    def properties(self): return self.props

    def fqn(self): return self.type + '/' + self.name + ' (' + self.kind + ')'

    def fmt(self):
        return ["'%s':%s\n" % (k, v) for k, v in self.props.iteritems()]

    def evaled_props(self):
        def try_eval(x):
            try:
                return eval(x, {}, {})
            except (SyntaxError, NameError):
                return x
            except:
                print x
                raise
        return {k: try_eval(v) for k, v in self.props.iteritems()}


class Tree(object):
    __slots__ = ('__weakref__', 'digest', 'label', 'leaf', 'nodes')
    _pool = weakref.WeakValueDictionary()

    def __new__(cls, id=None):
        if not id:
            return object.__new__(cls)
        if not isinstance(id, (MD5, int)):
            print id, type(id)
            id = digest(id)
        obj = cls._pool.get(id)
        if not obj:
            obj = object.__new__(cls)
            cls._pool[id] = obj
            node = TreeNode(id)
            if not node:
                print 'invalid TreeNode for ' + id
            obj.digest = node.digest().str()
            obj.label = node.label()
            leaf = node.leaf()
            obj.leaf = PropCfg(leaf) if leaf.valid() else None
            obj.nodes = [cls(id) for id in node.nodes()]
        return obj

    def prnt(self, depth=0, file=None):
        s = ' --> ' + str(self.leaf.digest) if self.leaf else ''
        indent = depth * '   '
        print >>file, indent + str(self.digest) + (30 - len(indent)) * ' ' + s
        for tree in self.nodes:
            tree.prnt(depth + 1, file)

    def _inorder(self):
        yield self
        for i in self.nodes:
            for x in i._inorder():
                yield x

    def _inorder_fast(self, visited):
        if self in visited:
            return
        visited.add(self)
        yield self
        for i in self.nodes:
            for x in i._inorder_fast(visited):
                yield x

    def iter(self, duplicates=False):
        return self._inorder() if duplicates else self._inorder_fast(set())

    def __iter__(self):
        return self._inorder_fast(set())

    def iterleafs(self, duplicates=False):
        """Return a generator yielding leafs.

        Args:
            duplicates (bool): Whether to traverse the full tree.
                Can be O(10-100) times slower!

        Yields:
            leaf objects of type PropCfg

        """
        return (i.leaf for i in self.iter(duplicates) if i.leaf)

    def leafs(self):
        return {i.name: i for i in self.iterleafs()}


class RemoteAccess(object):
    _svc = None

    def __init__(self, cas):
        # print 'remote(%s) created at pid=%s and cas %s' %
        # (self,os.getpid(),cas.getType())
        RemoteAccess._svc = createAccessSvcSingleton(cas=cas)

    def __del__(self):
        RemoteAccess._svc.reset()

    def rgetConfigTree(self, id):
        # maybe prefetch all leafs by invoking
        # RemoteAccess._svc.collectLeafRefs(id)
        # benchmark result: makes no difference whatsoever...

        # from time import clock
        # x = clock()
        t = Tree(id)
        # print '<< remote(%s) at pid=%s: rgetConfigTree(%s) : lookup time: %s s.' % (self, os.getpid(), id, clock() - x)
        return t

    def rgetConfigurations(self):
        # print 'remote(%s) at pid=%s: rgetConfigurations()' %
        # (self,os.getpid())
        svc = RemoteAccess._svc
        info = dict()
        # print 'reading TOPLEVEL'
        for i in svc.configTreeNodeAliases(alias('TOPLEVEL/')):
            x = Configuration(i, svc)
            info[i.alias().str()] = x
        # print 'reading TCK'
        for i in svc.configTreeNodeAliases(alias('TCK/')):
            tck = _tck(i.alias().str().split('/')[-1])
            id = i.ref().str()
            for k in info.values():
                if k.info['id'] == id:
                    k.info['TCK'].append(tck)
        # print 'reading TAG'
        for i in svc.configTreeNodeAliases(alias('TAG/')):
            tag = i.alias().str().split('/')[1:]
            id = i.ref().str()
            for k in info.values():
                if k.info['id'] == id:
                    k.update({'TAG': tag})
        return info

    def rupdateProperties(self, id, updates, label, copy=False, hlttype=None):
        if not label:
            print 'please provide a reasonable label for the new configuration'
            return None
        svc = RemoteAccess._svc
        #  either we got (some form of) an ID, and we reverse engineer back the alias (provided it is unique!)
        #  or we got an alias directly...

        if type(id) == ConfigTreeNodeAlias:
            a = id.alias().str()
            id = id.ref()
            # this is fine for TOPLEVEL, but not TCK...
        else:
            id = svc._2id(id)
            if not id.valid():
                raise RuntimeWarning('not a valid id : %s' % id)
            a = [i.alias().str() for i in svc.configTreeNodeAliases(
                alias('TOPLEVEL/')) if i.ref() == id]
            if len(a) != 1:
                print 'something went wrong: no unique toplevel match for ' + str(id)
                return
            a = a[0]
        (release, old_hlttype) = a.split('/', 3)[1:3]
        hlttype = hlttype or old_hlttype
        newId = svc.updateAndWrite(id, updates, label, copy)
        noderef = svc.resolveConfigTreeNode(newId)
        top = topLevelAlias(release, hlttype, noderef)
        svc.writeConfigTreeNodeAlias(top)
        print 'wrote ' + str(top.alias())
        return str(newId)

    def rupdateL0TCK(self, id, l0config, label, extra):
        svc = RemoteAccess._svc
        id = svc._2id(id)
        if not id.valid():
            raise RuntimeWarning('not a valid id : %s' % id)
        a = [i.alias().str() for i in svc.configTreeNodeAliases(
            alias('TOPLEVEL/')) if i.ref() == id]
        if len(a) != 1:
            print 'something went wrong: no unique toplevel match for ' + str(id)
            return
        (release, hlttype) = a[0].split('/', 3)[1:3]

        from collections import defaultdict
        updates = defaultdict(dict)
        # check L0 config in source config
        for cfg in svc.collectLeafRefs(id):
            #  check for either a MultiConfigProvider with the right setup,
            #  or for a template with the right TCK in it...
            if cfg.name == 'ToolSvc.L0DUConfig':
                if cfg.type != 'L0DUConfigProvider':
                    raise KeyError(
                        "Can only update configuration which use L0DUConfigProvider, not  %s" % cfg.type)
                #  check that all specified properties exist in cfg
                for (k, v) in l0config.iteritems():
                    if k not in cfg.props:
                        raise KeyError(
                            'Specified property %s not in store' % k)
                    updates['ToolSvc.L0DUConfig'].update({k: v})

        if extra:
            updates.update(extra)

        newId = svc.updateAndWrite(id, updates, label)
        noderef = svc.resolveConfigTreeNode(newId)
        if not noderef:
            print 'oops, could not find node for %s ' % newId
        top = topLevelAlias(release, hlttype, noderef)
        svc.writeConfigTreeNodeAlias(top)
        print 'wrote ' + str(top.alias())
        return str(newId)

    def rresolveTCK(self, tck):
        svc = RemoteAccess._svc
        return svc.resolveTCK(tck)

    def rcreateTCKEntries(self, d):
        svc = RemoteAccess._svc
        for tck, id in d.iteritems():
            id = _digest(id)
            tck = _tck(tck)
            # check whether L0 part of the TCK is specified
            l0tck = tck & 0xffff
            hlt1, hlt2 = (False, False)
            masks = {(True, False): 1 << 28,
                     (False, True): 1 << 29,
                     (True, True): 0}
            if l0tck:
                l0tck = '0x%04X' % l0tck
            for cfg in svc.collectLeafRefs(id):
                if l0tck and cfg.name == 'ToolSvc.L0DUConfig':
                    #  check for either a MultiConfigProvider with the right setup,
                    #  or for a template with the right TCK in it...
                    if cfg.type not in ['L0DUMultiConfigProvider', 'L0DUConfigProvider']:
                        raise KeyError(
                            "not a valid L0DU config provider: %s" % cfg.type)
                    if cfg.type == 'L0DUMultiConfigProvider' and l0tck not in cfg.props['registerTCK']:
                        raise KeyError('requested L0TCK %s not known by L0DUMultiConfigProvider in config %s; known L0TCKs: %s' % (
                            l0tck, id, cfg.props['registerTCK']))
                    elif cfg.type == 'L0DUConfigProvider' and l0tck != cfg.props['TCK']:
                        raise KeyError('requested L0TCK %s not known by L0DUConfigProvider in config %s; known L0TCK: %s' % (
                            l0tck, id, cfg.props['TCK']))
                if cfg.type == 'GaudiSequencer' and cfg.name == 'HltDecisionSequence':
                    hlt1 = 'GaudiSequencer/Hlt1' in cfg.props['Members']
                    hlt2 = 'GaudiSequencer/Hlt2' in cfg.props['Members']

            # Check and set HLT1/HLT2 bits
            # If most significant bit is set check if it is really a techincal TCK or a PID-less TCK
            # this check is moved from
            # Kernel/HltInterfaces/src/ConfigTreeNodeAlias.cpp
            if (tck & (1 << 31)):
                if (tck & (3 << 30)):
                    print 'Creating PID-less TCK %x' % (tck)
                elif (tck & 0xFFFF):
                        # check if the technical TCK respect the rules
                    raise ValeError(
                        'The requested TCK does not match the rules: technical TCK must have L0 bits set to zero')
            else:
                mask = masks[(hlt1, hlt2)]
                # In case bits were already assigned, check if they are correct
                if ((tck & (3 << 28)) | mask) != mask:
                    raise ValueError('Wrong HLT1 and HLT2 specific bits set in tck: %x instead of %x for highest four bits.' % (
                        tck >> 28, mask >> 28))
                # Apply HLT1/HLT2 mask
                tck |= mask

            print 'creating mapping TCK: 0x%08x -> ID: %s' % (tck, id)
            ref = svc.resolveConfigTreeNode(id)
            alias = TCK(ref, tck)
            svc.writeConfigTreeNodeAlias(alias)

    def rcopyTree(self, nodeRef):
        svc = RemoteAccess._svc

        def __nodes(n):
            node = svc.resolveConfigTreeNode(n)
            from itertools import chain
            # depth first traversal -- this insures we do not write
            # incomplete data objects...
            for j in chain.from_iterable(__nodes(i) for i in node.nodes()):
                yield j
            yield node
        for i in __nodes(nodeRef):
            leafRef = i.leaf()
            if leafRef.valid():
                leaf = svc.resolvePropertyConfig(leafRef)
                assert leaf
                newRef = svc.writePropertyConfig(leaf)
                assert leafRef == newRef
            n = svc.writeConfigTreeNode(i)
            assert n == i.digest()

    def rcopy(self, glob=None):
        svc = RemoteAccess._svc
        from copy import deepcopy
        from itertools import chain
        aliases = [deepcopy(i) for label in ('TOPLEVEL/', 'TCK/')
                   for i in svc.configTreeNodeAliases(alias(label))]
        if glob:
            from fnmatch import fnmatch
            aliases = filter(lambda i: fnmatch(i.alias().str(), glob), aliases)
        # Now, split them into
        #    TOPLEVEL (just copy)
        top = filter(lambda i: i.alias().str(
        ).startswith('TOPLEVEL/'), aliases)
        # The rest: find corresponding TOPLEVEL, add it to the toplevel list,
        # re-create alias afterwards
        other = filter(lambda i: not i.alias().str(
        ).startswith('TOPLEVEL/'), aliases)
        assert len(top) + len(other) == len(aliases)
        toplevelaliases = svc.configTreeNodeAliases(alias('TOPLEVEL/'))
        for i in other:
            top += [deepcopy(j) for j in toplevelaliases if j.ref() == i.ref() and j not in top]
        l = len(top)
        print '# of TOPLEVEL items to copy: %s' % l
        # TODO: sort on the (integer) numbers appearing in the string...
        for k, i in enumerate(sorted(top, key=lambda x: x.alias().str()), 1):
            print '[%s/%s] copying tree %s' % (k, l, i.alias())
            empty = dict()
            node = svc.resolveConfigTreeNode(i.ref())
            newid = self.rupdateProperties(i, empty, node.label(), True)
            assert newid == i.ref().str()
        for i in other:
            print 'copying alias %s -> %s ' % (i.alias().str(), i.ref())
            svc.writeConfigTreeNodeAlias(i)
        print 'done copying... '


class AccessMgr(BaseManager):
    pass

AccessMgr.register('Access', RemoteAccess)


class AccessProxy(object):
    _manager = None
    _access = None
    _cas = None

    def __init__(self):
        # print 'creating proxy in pid = %s' % os.getpid()
        pass
    # TODO: access should be seperately for each cas instance...
    #  worse: since Configurables are singletons, they may have
    #         changed since the last time used. Hence, have to
    #         check that that hasn't happend, so we need a local
    #         copy of the state of cas, and compare to that....
    #         and if different, let's just shutdown the remote
    #         and start again...
    #         (shouldn't have to flush PropertyConfig/ConfigTreeNode)

    def access(self, cas):
        if not self._valid(cas):
            self.flush()
        if not AccessProxy._manager:
            AccessProxy._manager = AccessMgr()
            AccessProxy._manager.start()
            # print 'proxy started manager'
        if not AccessProxy._access:
            # print 'proxy requesting access to remote -- cas =
            # %s'%(cas.getType())
            AccessProxy._access = AccessProxy._manager.Access(cas)
            AccessProxy._cas = cas
            AccessProxy._properties = cas.getProperties()

        return AccessProxy._access

    def _valid(self, cas):
        if not AccessProxy._access or not AccessProxy._cas:
            return True
        if cas != AccessProxy._cas:
            return False  # different configurable!
        return cas.getProperties() == AccessProxy._properties

    # make flush visible such that eg. createTCKEntries can flush the remote
    # and force re-reading...
    def flush(self):
        AccessProxy._cas = None
        AccessProxy._properties = None
        AccessProxy._access = None
        AccessProxy._manager.shutdown()
        AccessProxy._manager = None
