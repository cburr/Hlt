import re
import contextlib
import trees


@contextlib.contextmanager
def smart_open(filename=None):
    if not filename:
        import sys
        fh, close = sys.stdout, False
    elif isinstance(filename, basestring):
        fh, close = open(filename, 'w'), True
    else:
        fh, close = filename, False

    try:
        yield fh
    finally:
        if close:
            fh.close()


def findHybridFilterFactory(tree, hfilter):
    """Return the factory of a LoKi hybrid filter.

    Args:
        hfilter (obj): Hybrid filter leaf.

    """
    factory = hfilter.properties()['Factory']
    type_, name = re.match(r'^(.*)/(.*):PUBLIC$', factory).group(1, 2)
    return trees.findInTree(tree, type=r"^{}$".format(type_),
                            name=r"^ToolSvc\.{}$".format(name)).leaf


def defaultFunctorOverrides():
    from LoKiNumbers.decorators import FALL, FNONE
    return {
        'SCALE': lambda x: FALL if x > 0 else FNONE,
        'RATE': lambda x: FALL if x > 0 else FNONE,
    }


def functorFromHybridFilter(hfilter, tree, overrides=None):
    """Return an evaluatable functor corresponding to the hybrid filter.

    Args:
        hfilter (obj): Hybrid filter leaf.
        tree (obj): Configuration tree where factories are searched.
        overrides (dict): Override variables in the functors' environment.

    """
    if not hfilter:
        return lambda x: True
    locals_ = defaultFunctorOverrides()
    if overrides:
        locals_.update(overrides)
    globals_ = {}

    factory = findHybridFilterFactory(tree, hfilter)
    modules = eval(factory.properties()['Modules'])
    for module in modules:
        exec('from {} import *'.format(module), globals_)
    lines = eval(factory.properties()['Lines'])
    exec('\n'.join(lines), globals_)

    preambulo = eval(hfilter.properties()['Preambulo'])
    exec('\n'.join(preambulo), globals_)

    return eval(hfilter.properties()['Code'], globals_, locals_)
