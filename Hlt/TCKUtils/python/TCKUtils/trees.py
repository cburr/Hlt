__all__ = [
    'matchInTree',
    'findInTree',
    'isSubTree',
]

import re

from backend import accept_id, default_cas, getConfigTree


# TODO support returning/yielding duplicate leafs?

@accept_id
def matchInTree(tree, name='', type='', kind='', fqn='', regex=True):
    """Yield nodes whose leaf matches the given crieteria.

    This function expects regular expressions, which are compiled and
    .match() is called.

    Args:
        tree (obj): Configuration tree.
        name (str): Component's name.
        type (str): Component's type (e.g. "LoKi::ODINFilter").
        kind (str): Component's kind (e.g. "IAlgorithm").
        fqn (str): Component's fully qualified name (i.e. "type/name (kind)").
        regex (bool): If False, requires exact string matches.
    """

    def rcompile(expr):
        return re.compile('^{}$'.format(re.escape(expr)) if expr and not regex else expr)

    name = rcompile(name)
    type = rcompile(type)
    kind = rcompile(kind)
    fqn = rcompile(fqn)
    for i in tree.iter():
        if not i.leaf:
            continue
        if name.match(i.leaf.name) and \
           type.match(i.leaf.type) and \
           kind.match(i.leaf.kind) and \
           fqn.match(i.leaf.fqn()):
            yield i


@accept_id
def findInTree(tree, name='', type='', kind='', fqn='', regex=True):
    """Return the sole node whose leaf matches the given crieteria.

    If no or multiple matches are found, a RunTime exception is raised.
    This function expects regular expressions, which are compiled and
    .match() is called.

    Args:
        tree (obj): Configuration tree.
        name (str): Component's name.
        type (str): Component's type (e.g. "LoKi::ODINFilter").
        kind (str): Component's kind (e.g. "IAlgorithm").
        fqn (str): Component's fully qualified name (i.e. "type/name (kind)").
        regex (bool): If False, requires exact string matches.

    """
    matches = matchInTree(tree, name, type, kind, fqn, regex)
    try:
        match = next(matches)
    except StopIteration:
        raise KeyError('No matches found in tree (name={!r}, type={!r}, '
                       'kind={!r}, fqn={!r}, regex={!r}).'
                       .format(name, type, kind, fqn, regex))
    try:
        multiple = [match, next(matches)]  # expect this to normally raise
        multiple += list(matches)
    except StopIteration:
        pass
    else:
        print [i.leaf.fqn() for i in multiple]
        raise RuntimeError('Multiple ({}) matches found in tree'.format(len(multiple)))
    return match


def isSubTree(tree, subtree):
    """Return whether a tree contains another tree.

    The subtree is matched by fqn, i.e. "type/name (kind)".

    Args:
        tree (obj): Configuration tree.
        subtree (obj): Tree to be searched for.

    """
    matches = matchInTree(tree, fqn=subtree.leaf.fqn(), regex=False)
    try:
        next(matches)
        return True
    except StopIteration:
        return False
