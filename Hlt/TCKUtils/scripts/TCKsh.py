import os
import argparse

parser = argparse.ArgumentParser(description='Explore TCK database')
parser.add_argument('files', nargs='*', help='config.cdb/tar files (use - for the default)')
parser.add_argument('--write', action='store_true', help='Open the first file for writing')

args = parser.parse_args()
if not args.files and args.write:
    parser.error('"--write" requires at least one file to be specified')

from Configurables import (ConfigStackAccessSvc,
                           ConfigZipFileAccessSvc,
                           ConfigTarFileAccessSvc,
                           ConfigCDBAccessSvc)


def __create_file_cas(name, path, mode):
    if path == '-':
        return ConfigCDBAccessSvc(name, Mode=mode)
    ext = os.path.splitext(path)[1]
    try:
        ConfigAccessSvc = {
            '.zip': ConfigZipFileAccessSvc,
            '.tar': ConfigTarFileAccessSvc,
            '.cdb': ConfigCDBAccessSvc,
        }[ext]
    except KeyError:
        raise ValueError("Extension '{}' not recognized".format(ext))
    if 'Write' not in mode and not os.path.isfile(path):
        raise IOError("File not found '{}'".format(path))
    return ConfigCDBAccessSvc(name, File=path, Mode=mode)

if len(args.files) == 1:
    fn = args.files[0]
    mode = 'ReadWrite' if args.write else 'ReadOnly'
    __create_file_cas('ConfigAccessSvc', fn, mode)
elif len(args.files) > 1:
    svcs = []
    for i, fn in enumerate(args.files):
        mode = 'ReadWrite' if i == 0 and args.write else 'ReadOnly'
        svc = __create_file_cas('ConfigAccessSvc{}'.format(i), fn, mode)
        svcs.append(svc)
    ConfigStackAccessSvc('ConfigAccessSvc', ConfigAccessSvcs=svcs)


# The only essential thing:
from TCKUtils.utils import *
