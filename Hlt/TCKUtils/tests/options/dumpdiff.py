import os
from TCKUtils.utils import *

dumpL0(0x11291600, file='dump_0x1600.txt')

dump(0x11291600, file='dump_0x11291600.txt')
dump(0x21291600, file='dump_0x21291600.txt')

diff(0x212c1605, 0x212c1606, file='diff_0x212c1605_0x212c1606.diff')
diff(0x21291600, 0x212c1600, file='diff_0x21291600_0x212c1600.diff')

diff(0x212c1605, 0x212c1606, human=True, file='hdiff_0x212c1605_0x212c1606.diff')
diff(0x21291600, 0x212c1600, human=True, file='hdiff_0x21291600_0x212c1600.diff')

# Split TCKs
dump_flow(0x11291600, file='flow_0x11291600.txt')
dump_flow(0x21291600, 0x11291600, file='flow_0x21291600_0x11291600.txt')

# Combined TCKs (before 2016)
dump_flow(0x01260243, file='flow_0x01260243.txt')
dump_flow(0x01260243, 0x01230243, file='flow_0x01260243_0x01230243.txt')
