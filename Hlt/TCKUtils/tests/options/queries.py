import unittest
import itertools
from TCKUtils.utils import *


class TestGenericMethods(unittest.TestCase):

    def test_getComponents(self):
        expected = [
            (7, 'Hlt1ForwardHPT', 'b7aab5b0114f4dc8378148c6148ef46a'),
            (8, 'Hlt1ForwardHPT.PatForwardTool', 'a9df2f268ba78325058b7184aa3a43c3'),
            (9, 'Hlt1ForwardHPT.PatForwardTool.PatFwdTool', '3d9ba410de73d72172ba775099b28df1')
        ]
        result = getComponents(0x11291600)
        result = list(itertools.islice(result, 298, 301))
        self.assertEqual(result, expected)

    def test_getAlgorithms(self):
        expected = [
            (1, 'Hlt', 'c87b1cfc29bb0f766a03a346c15af9c6'),
            (2, 'HltDecisionSequence', '70aab61e02f643b518a0cb5cde49296f'),
            (3, 'Hlt1', 'bf20bdf0ddee88643841d87002eaed30'),
            (4, 'Hlt1TrackMVA', 'f3a75204403fa8914f6243ec3d19eeca')
        ]
        result = getAlgorithms(0x11291600)
        result = list(itertools.islice(result, 0, 4))
        self.assertEqual(result, expected)

    def test_getProperties(self):
        expected = {'PatPV3DHltBeamGas.PVOfflineTool': {'BeamSpotRCut': '0.20000000'}}
        result = getProperties(0x11291600, '.*PatPV3D.*', 'BeamSpotRCut')
        self.assertEqual(result, expected)

    def test_getTCKInfo(self):
        expected = ('Physics_pp_May2016', 'MOORE_v25r2')
        result = getTCKInfo(0x11291600)
        self.assertEqual(result, expected)

    def test_getReleases(self):
        releases = getReleases()
        self.assertTrue('MOORE_v25r2' in releases)

    def test_getTCKs(self):
        expected = ('0x11291600', 'Hlt1, Physics_pp_May2016, 0x1600')
        tcks = getTCKs()
        tcks2 = getTCKs(release='MOORE_v25r2', hlttype='Physics_pp_May2016')
        self.assertTrue(expected in tcks)
        self.assertTrue(expected in tcks2)

    def test_getTCKList(self):
        tcks = getTCKList()
        self.assertTrue('0x11291600' in tcks)

    def test_getHlt1Lines(self):
        expected = ['Hlt1TrackMVA',
                    'Hlt1TwoTrackMVA',
                    'Hlt1TrackMVALoose']
        result = getHlt1Lines(0x11291600)
        self.assertEqual(result[:len(expected)], expected)
        self.assertEqual(getHlt1Lines(0x21291600), [])

    def test_getHlt2Lines(self):
        expected = ['Hlt2B2HH_B2HH',
                    'Hlt2B2HH_B2KK',
                    'Hlt2B2HH_B2KPi']
        result = getHlt2Lines(0x21291600)
        self.assertEqual(result[:len(expected)], expected)
        self.assertEqual(getHlt2Lines(0x11291600), [])

    def test_getHlt1Decisions(self):
        expected0 = ['Hlt1TrackMVADecision',
                     'Hlt1TwoTrackMVADecision',
                     'Hlt1TrackMVALooseDecision']
        expected1 = 'Hlt1Global'
        result = getHlt1Decisions(0x11291600)
        self.assertEqual(result[:len(expected0)], expected0)
        self.assertEqual(result[-1], expected1)

    def test_isTurboLine(self):
        self.assertTrue(isTurboLine(0x21291600, 'Hlt2DiMuonJPsiTurbo'))
        self.assertFalse(isTurboLine(0x21291600, 'Hlt2DiMuonJPsi'))

    def test_isTurboPPLine(self):
        self.assertTrue(isTurboPPLine(0x21291600, 'Hlt2DiMuonJPsiTurbo'))
        self.assertFalse(isTurboPPLine(0x21291600, 'Hlt2DiMuonJPsi'))
        self.assertFalse(isTurboPPLine(0x21291600, 'Hlt2BottomoniumDiKstarTurbo'))


class TestFlowMethods(unittest.TestCase):
    TCK1 = 0x1138160f
    TCK2 = 0x2139160f

    def test_getLineInputFilters(self):
        expected_keys = set(['HLT', 'HLT1', 'HLT2', 'L0DU', 'ODIN', 'TISTOS'])
        expected_fqn1 = 'LoKi::HDRFilter/Hlt2Topo2BodyHlt1Filter (IAlgorithm)'
        expected_fqn2 = 'TisTosParticleTagger/Hlt2Topo2BodyTosTisTosTagger (IAlgorithm)'

        filters = getLineInputFilters(self.TCK2, 'Hlt2Topo2Body')
        self.assertEqual(set(filters), expected_keys)
        self.assertIsInstance(filters['TISTOS'], list)
        for k in expected_keys - set(['TISTOS']):
            self.assertIsInstance(filters[k], (backend.PropCfg, type(None)))
        self.assertEqual(filters['HLT1'].fqn(), expected_fqn1)
        self.assertEqual(filters['TISTOS'][0].fqn(), expected_fqn2)

    def test_getHltLineInputs_Hlt1(self):
        expected1 = {'ODIN': {'Beam1Gas:be:VeloOpen'}, 'L0DU': {'B1gas'}}
        inputs1 = getHltLineInputs(self.TCK1, 'Hlt1BeamGasBeam1VeloOpen')
        self.assertDictEqual(inputs1, expected1)

        expected11 = {'ODIN': {'Beam1Gas:be:VeloOpen',
                               'Beam2Gas:be:VeloOpen',
                               'Lumi:be:VeloOpen',
                               'NoBias:be:VeloOpen',
                               'Physics:be:VeloOpen',
                               'SequencerTrigger:be:VeloOpen'},
                      'L0DU': {'B1gas'}}
        inputs11 = getHltLineInputs(self.TCK1, 'Hlt1BeamGasBeam1VeloOpen', use_l0_masks=False)
        self.assertDictEqual(inputs11, expected11)

        expected2 = {'ODIN': {'Lumi:ee,be,eb,bb:'}}
        inputs2 = getHltLineInputs(self.TCK1, 'Hlt1Lumi')
        self.assertDictEqual(inputs2, expected2)

        expected3 = {'ODIN': {'Beam1Gas:ee,be,eb,bb:VeloOpen',
                              'Beam2Gas:ee,be,eb,bb:VeloOpen',
                              'Lumi:ee,be,eb,bb:VeloOpen',
                              'NoBias:ee,be,eb,bb:VeloOpen',
                              'Physics:ee,be,eb,bb:VeloOpen',
                              'SequencerTrigger:ee,be,eb,bb:VeloOpen'}}
        inputs3 = getHltLineInputs(self.TCK1, 'Hlt1VeloClosingMicroBias')
        self.assertDictEqual(inputs3, expected3)

        expected4 = {'ODIN': {'Lumi:ee,be,eb:'}}
        inputs4 = getHltLineInputs(self.TCK1, 'Hlt1NoBiasNonBeamBeam')
        self.assertDictEqual(inputs4, expected4)

    def test_getHltLineInputs_Hlt2(self):
        inputs1 = getHltLineInputs(self.TCK2, 'Hlt2Topo2Body', self.TCK1)
        expected1 = {'HLT1': {'Hlt1TrackMVA',
                              'Hlt1TrackMVALoose',
                              'Hlt1TwoTrackMVA',
                              'Hlt1TwoTrackMVALoose'}}
        self.assertDictEqual(inputs1, expected1)
        inputs2 = getHltLineInputs(self.TCK2, 'Hlt2Lumi', self.TCK1)
        expected2 = {'HLT1': {'Hlt1Lumi'}}
        self.assertDictEqual(inputs2, expected2)
        inputs3 = getHltLineInputs(self.TCK2, 'Hlt2PassThrough', self.TCK1)
        expected3 = {'HLT1': (set(getHlt1Lines(self.TCK1)) -
                              set(['Hlt1Global', 'Hlt1Lumi']))}
        self.assertDictEqual(inputs3, expected3)


class TestL0Methods(unittest.TestCase):
    TCK1 = 0x1138160f

    def test_getL0Config(self):
        l0 = getL0Config(self.TCK1)
        self.assertEqual(l0['TCK'], '0x160F')
        expected_muon = {'MASK': '001',
                         'conditions': ['Spd_Had(Mult)<450', 'Muon1(Pt)>36'],
                         'name': 'Muon',
                         'rate': '100'}
        self.assertDictEqual(l0['Channels']['Muon'], expected_muon)
        expected_cond = {'bx': '-1',
                         'comparator': '<',
                         'data': 'Sum(Et)',
                         'name': 'SumEtPrev<250',
                         'threshold': '250'}
        self.assertDictEqual(l0['Conditions']['SumEtPrev<250'], expected_cond)

    def test_getL0Channels(self):
        expected = [
            'B1gas', 'B2gas', 'CALO', 'DiEM,lowMult', 'DiHadron,lowMult',
            'DiMuon', 'DiMuon,lowMult', 'Electron', 'Electron,lowMult',
            'Hadron', 'JetEl', 'JetPh', 'Muon', 'Muon,lowMult', 'MuonEW',
            'Photon', 'Photon,lowMult'
        ]
        result = getL0Channels(self.TCK1)
        self.assertListEqual(result, expected)

    def test_getL0Prescales(self):
        prescales = getL0Prescales(self.TCK1)
        self.assertEqual(prescales['0x160F']['Muon'], '100')


if __name__ == '__main__':
    unittest.main()
