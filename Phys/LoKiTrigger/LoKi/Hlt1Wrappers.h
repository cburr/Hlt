// ============================================================================
#ifndef LOKI_HLT1WRAPPERS_H
#define LOKI_HLT1WRAPPERS_H 1
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/Kernel.h"
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/TrackTypes.h"
#include "LoKi/TriggerTypes.h"
#include "LoKi/Interface.h"
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/TrackTypes.h"
#include "LoKi/HltTool.h"
// ============================================================================
/** @file LoKi/Hlt1Wrappers.h
 *  Collection of specific wrappers for Hl1 implementation
 *
 *  This file is part of LoKi project:
 *   ``C++ ToolKit for Smart and Friendly Physics Analysis''
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
 *  @date   2009-03-31
 */
// ============================================================================
namespace LoKi
{
  // ==========================================================================
  namespace Tracks
  {
  } //                                            end of namespace LoKi::Tracks
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
namespace LoKi
{
  // ==========================================================================
  namespace Cuts
  {
  } //                                              end of namespace LoKi::Cuts
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
// The END
// ============================================================================
#endif // LOKI_HLT1WRAPPERS_H
// ============================================================================
