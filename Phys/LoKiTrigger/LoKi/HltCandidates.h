// ============================================================================
#ifndef LOKI_HLTCANDIDATES_H
#define LOKI_HLTCANDIDATES_H 1
// ============================================================================
// Include files
// ============================================================================
#include<iostream>
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/Kernel.h"
// ============================================================================
// HltBase
// ============================================================================
#include "Event/HltCandidate.h"
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/BasicFunctors.h"
#include "LoKi/TriggerTypes.h"
#include "LoKi/Streamers.h"
#include "LoKi/AlgUtils.h"
// ============================================================================
// Local
// ============================================================================
#include "LoKi/HltTool.h"
// ============================================================================
/** @file  LoKi/HltCandidates.h
 *
 *  Hlt-Candidates functors
 *
 *  This file is part of LoKi project:
 *   ``C++ ToolKit for Smart and Friendly Physics Analysis''
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
 *  @date 2010-08-01
 *
 */
// ============================================================================
namespace LoKi
{
  // ==========================================================================
  /** @namespace LoKi::Candidates
   *  collection of functors that deal with Hlt::Candidate objects
   *  @see Hlt::Candidate
   *  @author Vanya Belyaev Ivan.BElyaev@nikhef.nl
   *  @date 2010-08-01
   */
  namespace Candidates
  {
    // ========================================================================
    /** @class TotalStages trivial functor to get number of stages
     *  @see LoKi::Cuts::TC_TOTSTAGES
     *  @author Vanya Belyaev Ivan.BElyaev@nikhef.nl
     *  @date 2010-08-01
     */
    class GAUDI_API TotalStages
      : public LoKi::BasicFunctors<const Hlt::Candidate*>::Function
    {
    public:
      // ======================================================================
      /// MANDATORY: clone method ("virtual constructor")
      TotalStages* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument a ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    } ;
    // ========================================================================
    /** trivial functor to counts certain of stages
     *  @see LoKi::Cuts::TC_NSTAGES
     *  @author Vanya Belyaev Ivan.BElyaev@nikhef.nl
     *  @date 2010-08-01
     */
    class GAUDI_API NStages
      : public LoKi::BasicFunctors<const Hlt::Candidate*>::Function
    {
    public:
      // ======================================================================
      /// constructor
      NStages  ( const  LoKi::TriggerTypes::TS_Cuts& cut ) ;
      /// MANDATORY: clone method ("virtual constructor")
      NStages* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument a ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      /// the actual predicate
      LoKi::TriggerTypes::TS_Cut m_cut ;                // the actual predicate
      // ======================================================================
    } ;
    // ========================================================================
    /** trivial functor to check if Candidate is 'branch', e.g. resutl
     *  of split of another candidate
     *  @see LoKi::Cuts::TC_BRANCH
     *  @author Vanya Belyaev Ivan.BElyaev@nikhef.nl
     *  @date 2010-08-01
     */
    class GAUDI_API Branch
      : public LoKi::BasicFunctors<const Hlt::Candidate*>::Predicate
    {
    public:
      // ======================================================================
      /// MANDATORY: clone method ("virtual constructor")
      Branch* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument a ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    } ;
    // ========================================================================
    /** @class StageFun
     *  simple meta-functor for stage functors
     *  @see LoKi::Cuts::TC_StFUN
     *  @author Vanya Belyaev Ivan.BElyaev@nikhef.nl
     *  @date 2010-08-01
     */
    class GAUDI_API StageFun
      : public LoKi::BasicFunctors<const Hlt::Candidate*>::Function
    {
    public:
      // ======================================================================
      /** constructor
       *  @param fun  (INPUT) the function
       *  @param slot (INPUT) the slot:
       *     - 0 corresponds to current stage ,
       *     - negative value corresponds to initianor stage
       *     - positive value corresponds to step-back in history
       */
      StageFun ( const LoKi::TriggerTypes::TS_Func& fun           ,
                 const int                          slot  = 0     ) ;
      /// MANDATORY: clone method ("virtual constructor")
      StageFun* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument a ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      /// the actual function
      LoKi::TriggerTypes::TS_Fun m_cut  ;               // the actual function
      /// the slot
      int                        m_slot ;               // the slot
      // ======================================================================
    } ;
    // ========================================================================
    /** @class StageCut
     *  simple meta-functor for stage functors
     *  @see LoKi::Cuts::TC_StCUT
     *  @author Vanya Belyaev Ivan.BElyaev@nikhef.nl
     *  @date 2010-08-01
     */
    class GAUDI_API StageCut
      : public LoKi::BasicFunctors<const Hlt::Candidate*>::Predicate
    {
    public:
      // ======================================================================
      /** constructor
       *  @param fun the predicate
       *  @param slot the slot:
       *     - 0 corresponds to current stage ,
       *     - negative value corresponds to initiator stage
       *     - positive value corresponds to step-back in history
       */
      StageCut ( const LoKi::TriggerTypes::TS_Cuts& fun           ,
                 const int                          slot  = 0     ) ;
      /// MANDATORY: clone method ("virtual constructor")
      StageCut* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument a ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      /// the actual function
      LoKi::TriggerTypes::TS_Cut m_cut ;                // the actual function
      /// the slot
      int                        m_slot ;               // the slot
      // ======================================================================
    } ;
    // ========================================================================
    /** @class SlotFun
     *  generalized function that applies the function to the content
     *  of the actual slot/stage
     *  @thanks Alexander MAZUROV
     *  @author Vanya BELYAEV Ivan.Belyaev@cern.ch
     *  @date 2010-11-21
     */
    class GAUDI_API SlotFun
      : public LoKi::BasicFunctors<const Hlt::Candidate*>::Function
    {
      // ======================================================================
    public :
      // ======================================================================
      /** constructor
       *  @param fun the function
       *  @param slot the slot:
       *  @param bad  the bad value
       *  @see Hlt::Candidate::get
       */
      SlotFun
      ( const LoKi::BasicFunctors<const LHCb::Track*>::Function& fun ,
        const int     slot = 0     ,
        const double  bad  = 0     ) ;
      /** constructor
       *  @param fun the function
       *  @param slot the slot:
       *  @see Hlt::Candidate::get
       */
      SlotFun
      ( const LoKi::BasicFunctors<const LHCb::VertexBase*>::Function& fun ,
        const int     slot = 0 ,
        const double  bad  = 0 ) ;
      /** constructor
       *  @param fun the function
       *  @param slot the slot:
       *  @see Hlt::Candidate::get
       */
      SlotFun
      ( const LoKi::BasicFunctors<const LHCb::ProtoParticle*>::Function& fun ,
        const int    slot = 0 ,
        const double bad  = 0 ) ;
      /** constructor
       *  @param fun the function
       *  @param slot the slot:
       *  @see Hlt::Candidate::get
       */
      SlotFun
      ( const LoKi::BasicFunctors<const LHCb::Particle*>::Function& fun ,
        const int    slot = 0 ,
        const double bad  = 0 ) ;
      /** constructor
       *  @param fun the function
       *  @param slot the slot:
       *  @see Hlt::Candidate::get
       */
      SlotFun
      ( const LoKi::BasicFunctors<const Hlt::Stage*>::Function& fun ,
        const int    slot = 0 ,
        const double bad  = 0 ) ;
      /** constructor
       *  @param fun the function
       *  @param slot the slot:
       *  @see Hlt::Candidate::get
       */
      SlotFun
      ( const LoKi::BasicFunctors<const LHCb::Track*>::FunVal& fun ,
        const int    slot = 0 ,
        const double bad  = 0 ) ;
      // =====================================================================
      /// MANDATORY: clone method ("virtual constructor")
      SlotFun* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument a ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    public:
      // ======================================================================
      int     slot () const { return m_slot ; }
      double  bad  () const { return m_bad  ; }
      // ======================================================================
    private:
      // ======================================================================
      /// the actual function
      LoKi::FunctorFromFunctor<const Hlt::Stage*,double> m_fun  ; // the cut
      /// the slot
      int                                                m_slot ; // the slot
      /// bad value
      double                                             m_bad  ; // bad value
      // ======================================================================
    } ;
    // ========================================================================
    /** @class SlotCut
     *  generalized predicate that applies the predicate to the content
     *  of the actual slot/stage
     *  @thanks Alexander MAZUROV
     *  @author Vanya BELYAEV Ivan.Belyaev@cern.ch
     *  @date 2010-11-21
     */
    class GAUDI_API SlotCut
      : public LoKi::BasicFunctors<const Hlt::Candidate*>::Predicate
    {
      // ======================================================================
    public :
      // ======================================================================
      /** constructor
       *  @param fun the predicate
       *  @param slot the slot:
       *     - 0 corresponds to current stage ,
       *     - negative value corresponds to initiator stage
       *     - positive value corresponds to step-back in history
       */
      SlotCut
      ( const LoKi::BasicFunctors<const LHCb::L0MuonCandidate*>::Predicate& cut ,
        const int slot  = -1 ) ;
      /** constructor
       *  @param fun the predicate
       *  @param slot the slot:
       *     - 0 corresponds to current stage ,
       *     - negative value corresponds to initiator stage
       *     - posiitve value corresponds to step-back in history
       */
      SlotCut
      ( const LoKi::BasicFunctors<const LHCb::L0CaloCandidate*>::Predicate& cut ,
        const int slot  = -1 ) ;
      /** constructor
       *  @param fun the predicate
       *  @param slot the slot:
       *     - 0 corresponds to current stage ,
       *     - negative value corresponds to initiator stage
       *     - posiitve value corresponds to step-back in history
       */
      SlotCut
      ( const LoKi::BasicFunctors<const LHCb::Track*>::Predicate& cut ,
        const int slot  =  0 ) ;
      /** constructor
       *  @param fun the predicate
       *  @param slot the slot:
       *     - 0 corresponds to current stage ,
       *     - negative value corresponds to initiator stage
       *     - posiitve value corresponds to step-back in history
       */
      SlotCut
      ( const LoKi::BasicFunctors<const LHCb::VertexBase*>::Predicate& cut ,
        const int slot  =  0 ) ;
      /** constructor
       *  @param fun the predicate
       *  @param slot the slot:
       *     - 0 corresponds to current stage ,
       *     - negative value corresponds to initiator stage
       *     - posiitve value corresponds to step-back in history
       */
      SlotCut
      ( const LoKi::BasicFunctors<const LHCb::ProtoParticle*>::Predicate& cut ,
        const int slot  =  0 ) ;
      /** constructor
       *  @param fun the predicate
       *  @param slot the slot:
       *     - 0 corresponds to current stage ,
       *     - negative value corresponds to initiator stage
       *     - posiitve value corresponds to step-back in history
       */
      SlotCut
      ( const LoKi::BasicFunctors<const LHCb::Particle*>::Predicate& cut ,
        const int slot  =  0 ) ;
      /** constructor
       *  @param fun the predicate
       *  @param slot the slot:
       *     - 0 corresponds to current stage ,
       *     - negative value corresponds to initiator stage
       *     - positive value corresponds to step-back in history
       */
      SlotCut
      ( const LoKi::BasicFunctors<const Hlt::Stage*>::Predicate& cut ,
        const int slot  = 0 ) ;
      /** constructor
       *  @param fun the predicate
       *  @param slot the slot:
       *     - 0 corresponds to current stage ,
       *     - negative value corresponds to initiator stage
       *     - posiitve value corresponds to step-back in history
       */
      SlotCut
      ( const LoKi::BasicFunctors<const LHCb::Track*>::CutVal& cut ,
        const int slot  = 0 ) ;
      // =====================================================================
      /// MANDATORY: clone method ("virtual constructor")
      SlotCut* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument a ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      /// the actual function
      LoKi::FunctorFromFunctor<const Hlt::Stage*,bool> m_cut  ; // the cut
      /// the slot
      int                                              m_slot ; // the slot
      // ======================================================================
    } ;
    // =========================================================================
    /** @class StageFilter
     *  simple meta-functor for stage filters
     *  @author Alexander Mazurov alexander.mazurov@gmail.com
     *  @date 2010-10-22
     */
    class GAUDI_API SlotFilter
      : public LoKi::BasicFunctors<const Hlt::Candidate*>::Pipe
    {
    public:
      // ======================================================================
      /// constructor from the selection
      /** constructor
       *  @param fun the predicate
       *  @param slot the slot:
       *     - 0 corresponds to current stage ,
       *     - negative value corresponds to initiator stage
       *     - positive value corresponds to step-back in history
       */
    	SlotFilter
      ( const LoKi::BasicFunctors<const LHCb::L0MuonCandidate*>::Predicate& cut,
    		const int slot = -1 );
      /** constructor
       *  @param fun the predicate
       *  @param slot the slot:
       *  @see Hlt::Candidate::get
       */
    	SlotFilter
      ( const LoKi::BasicFunctors<const LHCb::L0CaloCandidate*>::Predicate& cut,
        const int slot = -1 );
      /** constructor
       *  @param fun the predicate
       *  @param slot the slot:
       *  @see Hlt::Candidate::get
       */
    	SlotFilter
      ( const LoKi::BasicFunctors<const LHCb::Track*>::Predicate& cut,
    		const int slot =  0 );
      /** constructor
       *  @param fun the predicate
       *  @param slot the slot:
       *  @see Hlt::Candidate::get
       */
    	SlotFilter
      ( const LoKi::BasicFunctors<const LHCb::VertexBase*>::Predicate& cut,
    		const int slot =  0 );
      /** constructor
       *  @param fun the predicate
       *  @param slot the slot:
       *  @see Hlt::Candidate::get
       */
    	SlotFilter
      ( const LoKi::BasicFunctors<const LHCb::Particle*>::Predicate& cut,
    		const int slot =  0 );
      /** constructor
       *  @param fun the predicate
       *  @param slot the slot:
       *  @see Hlt::Candidate::get
       */
    	SlotFilter
      ( const LoKi::BasicFunctors<const LHCb::ProtoParticle*>::Predicate& cut,
    		const int slot =  0 );
      /** constructor
       *  @param fun the predicate
       *  @param slot the slot:
       *  @see Hlt::Candidate::get
       */
    	SlotFilter
      ( const LoKi::BasicFunctors<const Hlt::Stage*>::Predicate& cut,
        const int slot =  0 );
      /// constructor
      SlotFilter ( const SlotCut& cut ) ;
      /// MANDATORY: clone method ("virtual constructor")
      SlotFilter* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument a )  const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream ( std::ostream& s ) const override;
      // =======================================================================
    private:
      // ========================================================================
      SlotCut m_cut;
      // ========================================================================
    };
    // ==========================================================================
    /** @class SlotMap
     *  generalized function that applies the function to the content
     *  of the actual slot/stage
     *  @thanks Vanya Belyaev
     *  @author Alexander MAZUROV alexander.mazurov@gmail.com
     *  @date 2010-11-21
     */
    class GAUDI_API SlotMap
      : public LoKi::BasicFunctors<const Hlt::Candidate*>::Map
    {
    public:
      // ======================================================================
      /** constructor
       *  @param fun the function
       *  @param slot the slot:
       *  @see Hlt::Candidate::get
       */
    	SlotMap
      ( const LoKi::BasicFunctors<const LHCb::Track*>::Function& fun,
    		const int    slot =  0 ,
        const double bad  =  0 ) ;
      /** constructor
       *  @param fun the function
       *  @param slot the slot:
       *  @see Hlt::Candidate::get
       */
    	SlotMap
      ( const LoKi::BasicFunctors<const LHCb::VertexBase*>::Function& fun,
    		const int    slot =  0 ,
        const double bad  =  0 ) ;
      /** constructor
       *  @param fun the function
       *  @param slot the slot:
       *  @see Hlt::Candidate::get
       */
    	SlotMap
      ( const LoKi::BasicFunctors<const LHCb::Particle*>::Function& fun,
    		const int    slot =  0 ,
        const double bad  =  0 ) ;
      /** constructor
       *  @param fun the function
       *  @param slot the slot:
       *  @see Hlt::Candidate::get
       */
    	SlotMap
      ( const LoKi::BasicFunctors<const LHCb::ProtoParticle*>::Function& fun,
    		const int    slot =  0 ,
        const double bad  =  0 ) ;
      /** constructor
       *  @param fun the function
       *  @param slot the slot:
       *  @see Hlt::Candidate::get
       */
    	SlotMap
      ( const LoKi::BasicFunctors<const Hlt::Stage*>::Function& fun,
    		const int    slot = 0 ,
        const double bad  = 0 ) ;
      /** constructor
       *  @param fun the function
       *  @param slot the slot:
       *  @see Hlt::Candidate::get
       */
    	SlotMap
      ( const LoKi::BasicFunctors<const LHCb::Track*>::FunVal& fun,
    		const int    slot = 0 ,
        const double bad  = 0 ) ;
      /// constructor
      SlotMap ( const SlotFun& fun ) ;
      /// MANDATORY: clone method ("virtual constructor")
      SlotMap* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument a )  const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream ( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      SlotFun m_fun;
      // ======================================================================
    };
    // ========================================================================
  } //                                        end of namespace LoKi::Candidates
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_HLTCANDIDATES_H
// ============================================================================
