// ============================================================================
#ifndef LOKI_HLTSTAGES_H
#define LOKI_HLTSTAGES_H 1
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/Kernel.h"
// ============================================================================
// HltBase
// ============================================================================
#include "Event/HltStage.h"
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/BasicFunctors.h"
#include "LoKi/TrackTypes.h"
#include "LoKi/AlgUtils.h"
// ============================================================================
// Boost
// ============================================================================
#include "boost/regex.hpp"
// ============================================================================
/** @file  LoKi/HltStages.h
 *
 *  Hlt-Stages functors
 *
 *  This file is part of LoKi project:
 *   ``C++ ToolKit for Smart and Friendly Physics Analysis''
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
 *  @date 2010-08-01
 *
 */
// ============================================================================
namespace LoKi
{
  // ==========================================================================
  /** @namespace LoKi::Stages
   *  collection of functors that deal with Hlt::Stage objects
   *  @see Hlt::Stage
   *  @author Vanya Belyaev Ivan.BElyaev@nikhef.nl
   *  @date 2010-08-01
   */
  namespace Stages
  {
    // ========================================================================
    /** @class IsTrack
     *  trivial predicate to check if the stage is a track
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @see LoKi::Cuts::TS_ISTRACK
     *  @date 2010-08-01
     */
    class GAUDI_API IsTrack
      : public LoKi::BasicFunctors<const Hlt::Stage*>::Predicate
    {
    public:
      // ======================================================================
      /// MANDATORY: clone method ("virtual constructor")
      IsTrack* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument a ) const override;
      /// OPTIONAL: the ince printout
      std::ostream& fillStream ( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
    /** @class IsL0Muon
     *  trivial predicate to check if the stage is a L0-muon stage
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @see LoKi::Cuts::TS_ISL0MUON
     *  @date 2010-08-01
     */
    class GAUDI_API IsL0Muon : public IsTrack
    {
    public:
      // ======================================================================
      /// MANDATORY: clone method ("virtual constructor")
      IsL0Muon* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument a ) const override;
      /// OPTIONAL: the ince printout
      std::ostream& fillStream ( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
    /** @class IsL0DiMuon
     *  trivial predicate to check if the stage is a L0-dimuon candidate
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @see LoKi::Cuts::TS_ISL0DIMUON
     *  @date 2010-08-01
     */
    class GAUDI_API IsL0DiMuon : public IsL0Muon
    {
    public:
      // =======================================================================
      /// MANDATORY: clone method ("virtual constructor")
      IsL0DiMuon* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument a ) const override;
      /// OPTIONAL: the ince printout
      std::ostream& fillStream ( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
    /** @class IsL0Calo
     *  trivial predicate to check if the stage is a L0-calo candidate
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @see LoKi::Cuts::TS_ISL0CALO
     *  @date 2010-08-01
     */
    class GAUDI_API IsL0Calo : public IsL0DiMuon
    {
    public:
      // ======================================================================
      /// Default Constructor
      IsL0Calo() { }
      /// MANDATORY: clone method ("virtual constructor")
      IsL0Calo* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument a ) const override;
      /// OPTIONAL: the ince printout
      std::ostream& fillStream ( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
    /** @class IsVertex
     *  trivial predicate to check if the stage is a RecVertex
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @see LoKi::Cuts::TS_ISVERTEX
     *  @date 2010-08-01
     */
    class GAUDI_API IsVertex : public IsL0Calo
    {
    public:
      // ======================================================================
      /// MANDATORY: clone method ("virtual constructor")
      IsVertex* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument a ) const override;
      /// OPTIONAL: the ince printout
      std::ostream& fillStream ( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
    /** @class IsMultiTrack
     *  trivial predicate to check if the stage is a Multitrack
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @see LoKi::Cuts::TS_ISMULTITRACK
     *  @date 2010-08-01
     */
    class GAUDI_API IsMultiTrack : public IsVertex
    {
    public:
      // ======================================================================
      /// MANDATORY: clone method ("virtual constructor")
      IsMultiTrack* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument a ) const override;
      /// OPTIONAL: the ince printout
      std::ostream& fillStream ( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
    /** @class IsParticle
     *  trivial predicate to check if the stage is a Particle
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @see LoKi::Cuts::TS_ISPARTICLE
     *  @date 2014-12-03
     */
    class GAUDI_API IsParticle : public IsVertex
    {
    public:
      // ======================================================================
      /// MANDATORY: clone method ("virtual constructor")
      IsParticle* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument a ) const override;
      /// OPTIONAL: the ince printout
      std::ostream& fillStream ( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
    /** @class IsProtoP
     *  trivial predicate to check if the stage is a ProtoParticle
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @see LoKi::Cuts::TS_ISPROTOP
     *  @date 2014-12-03
     */
    class GAUDI_API IsProtoP : public IsVertex
    {
    public:
      // ======================================================================
      /// MANDATORY: clone method ("virtual constructor")
      IsProtoP* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument a ) const override;
      /// OPTIONAL: the ince printout
      std::ostream& fillStream ( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
    /** @class IsStage
     *  trivial predicate to check if the stage is a Stage
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @see LoKi::Cuts::TS_ISSTAGE
     *  @date 2010-08-01
     */
    class GAUDI_API IsStage : public IsMultiTrack
    {
    public:
      // ======================================================================
      /// MANDATORY: clone method ("virtual constructor")
      IsStage* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument a ) const override;
      /// OPTIONAL: the ince printout
      std::ostream& fillStream ( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
    /** @class Type
     *  trivial function to get stage type
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @see LoKi::Cuts::TS_TYPE
     *  @date 2010-08-01
     */
    class GAUDI_API Type
      : public LoKi::BasicFunctors<const Hlt::Stage*>::Function
    {
    public:
      // ======================================================================
      /// MANDATORY: clone method ("virtual constructor")
      Type* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument a ) const override;
      /// OPTIONAL: the ince printout
      std::ostream& fillStream ( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
    /** @class TrFun
     *  trivial adaptor for track-stage
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @see LoKi::Cuts::TS_TrFUN
     *  @date 2010-08-01
     */
    class TrFun
      : public LoKi::BasicFunctors<const Hlt::Stage*>::Function
    {
    public:
      // ======================================================================
      /// constructor
      TrFun ( const LoKi::TrackTypes::TrFunc& fun ,
              double                          bad ) ;
      /// constructor
      TrFun ( const LoKi::TrackTypes::TrFunc& fun ) ;
      /// MANDATORY: clone method ("virtual constructor")
      TrFun* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument a ) const override;
      /// OPTIONAL: the ince printout
      std::ostream& fillStream ( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      LoKi::TrackTypes::TrFun m_fun ;
      double                  m_bad ;
      // ======================================================================
    };
    // ========================================================================
    /** @class TrCut
     *  trivial adaptor for track-stage
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @see LoKi::Cuts::TS_TrCUT
     *  @date 2010-08-01
     */
    class GAUDI_API TrCut
      : public LoKi::BasicFunctors<const Hlt::Stage*>::Predicate
    {
    public:
      // ======================================================================
      /// consttuctor
      TrCut ( const LoKi::TrackTypes::TrCuts& fun ,
              bool                            bad ) ;
      /// constructor
      TrCut ( const LoKi::TrackTypes::TrCuts& fun ) ;
      /// MANDATORY: clone method ("virtual constructor")
      TrCut* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument a ) const override;
      /// OPTIONAL: the ince printout
      std::ostream& fillStream ( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      LoKi::TrackTypes::TrCut m_fun ;
      bool                    m_bad ;
      // ======================================================================
    };
    // ========================================================================
    /** @class Locked
     *  trivial predicate to check Hlt::Stage::locked
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @see LoKi::Cuts::TS_LOCKED
     *  @date 2010-08-01
     */
    class GAUDI_API Locked
      : public LoKi::BasicFunctors<const Hlt::Stage*>::Predicate
    {
    public:
      // ======================================================================
      /// MANDATORY: clone method ("virtual destructor")
      Locked* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument a ) const override;
      /// OPTIONAL: the ince printout
      std::ostream& fillStream ( std::ostream& s ) const override;
      // ======================================================================
    } ;
    // ========================================================================
    /** @class History
     *  simple predicate to check the presence of algorithm in history
     *  @see LoKi::Cuts::TS_HISTORY
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2010-08-02
     */
    class GAUDI_API History
      : public LoKi::BasicFunctors<const Hlt::Stage*>::Predicate
    {
    public:
      // ======================================================================
      /// constructor from the algorithm name
      History ( const std::string& alg ) ;
      /// MANDATORY: clone method ("virtual destructor")
      History* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument a ) const override;
      /// OPTIONAL: the ince printout
      std::ostream& fillStream ( std::ostream& s ) const override;
      // ======================================================================
    protected:
      // ======================================================================
      /// the algorithm to be checked
      std::string m_algorithm ;                  // the algorithm to be checked
      // ======================================================================
    } ;
    // ========================================================================
    /** @class HistorySub
     *  simple predicate to check the presence of algorithm in history
     *  @see LoKi::Cuts::TS_HISTORY_SUB
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2010-08-02
     */
    class GAUDI_API HistorySub : public History
    {
    public:
      // ======================================================================
      /// constructor from the algorithm name substring
      HistorySub ( const std::string& alg ) ;
      /// MANDATORY: clone method ("virtual destructor")
      HistorySub* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument a ) const override;
      /// OPTIONAL: the ince printout
      std::ostream& fillStream ( std::ostream& s ) const override;
      // ======================================================================
    } ;
    // ========================================================================
    /** @class HasCache
     *  check the existence of some 'cached'-information
     *  @see TS_HASCACHE
     *  @author Vanya BELYAEV Ivan.Belyaev@cern.ch
     *  @date 2010-12-06
     */
    class GAUDI_API HasCache
      : public LoKi::BasicFunctors<const Hlt::Stage*>::Predicate
    {
    public:
      // ======================================================================
      /// constructor from the key and data type
      HasCache ( const std::string& key ,
                 Hlt::Cache::Values typ ) ;
      /// MANDATORY: clone method ("virtual constructor")
      HasCache* clone () const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument a ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream ( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      /// the key
      Gaudi::StringKey   m_key ;                                   // the key
      /// data type
      Hlt::Cache::Values m_typ ;                                   // data type
      // ======================================================================
    } ;
    // ========================================================================
    /** @class Cache1
     *  get some 'cached'-information
     *  @see TS_CACHE_DOUBLE
     *  @author Vanya BELYAEV Ivan.Belyaev@cern.ch
     *  @date 2010-12-06
     */
    class GAUDI_API Cache1
      : public LoKi::BasicFunctors<const Hlt::Stage*>::Function
    {
    public:
      // ======================================================================
      /// constructor from the key and data type
      Cache1 ( const std::string& key ,
               const double       def ) ;
      /// MANDATORY: clone method ("virtual constructor")
      Cache1* clone () const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument a ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream ( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      /// the key
      Gaudi::StringKey   m_key ;                               // the key
      /// default value
      double             m_def ;                               // default value
      // ======================================================================
    } ;
    // ========================================================================
    /** @class Cache2
     *  get some 'cached'-information
     *  @see TS_CACHE_BOOL
     *  @author Vanya BELYAEV Ivan.Belyaev@cern.ch
     *  @date 2010-12-06
     */
    class GAUDI_API Cache2
      : public LoKi::BasicFunctors<const Hlt::Stage*>::Predicate
    {
    public:
      // ======================================================================
      /// constructor from the key and data type
      Cache2 ( const std::string& key ,
               const bool         def ) ;
      /// MANDATORY: clone method ("virtual constructor")
      Cache2* clone () const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument a ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream ( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      /// the key
      Gaudi::StringKey   m_key ;                               // the key
      /// default value
      bool               m_def ;                               // default value
      // ======================================================================
    } ;
    // ========================================================================
    /** @class HistoryRegex
     *  simple predicate to check the presence of algorithm in history
     *  @see LoKi::Cuts::TS_HISTORY_RE
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2010-08-02
     */
    class GAUDI_API HistoryRegex : public HistorySub
    {
    public:
      // ======================================================================
      /// constructor from the algorithm name pattern
      HistoryRegex ( const std::string& alg ) ;
      /// MANDATORY: clone method ("virtual destructor")
      HistoryRegex* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument a ) const override;
      /// OPTIONAL: the ince printout
      std::ostream& fillStream ( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      /// the regular expression
      boost::regex  m_expression ;                    // the regular expression
      // ======================================================================
    } ;
    // ========================================================================
    /** @class Cut_
     *  Helper class for implementation of various filters
     *  @thanks Alexander Mazurov
     *  @author Vanya BELYAEV Ivan.BElyaev@cern.ch
     *  @date 2010-11-21
     */
    template <class TYPE>
    class Cut_ : public LoKi::BasicFunctors<const Hlt::Stage*>::Predicate
    {
    public:
      // ==================================================================
      /// constructor from the predicate and the fake integer argument
      Cut_
      ( const typename LoKi::BasicFunctors<const TYPE*>::Predicate& cut ,
        const int                                                   a   )
        : LoKi::AuxFunBase ( std::tie ( cut , a ) )
        , LoKi::BasicFunctors<const Hlt::Stage*>::Predicate ()
        , m_cut  ( cut  )
      {}
      /// MANDATORY: clone method ("virtual constructor")
      Cut_* clone() const override { return new  Cut_ ( *this ) ; }
      /// MANDATORY: the only one essential method
      typename LoKi::BasicFunctors<const Hlt::Stage*>::Predicate::result_type
      operator() ( typename LoKi::BasicFunctors<const Hlt::Stage*>::Predicate::argument a )  const override
      { return filterStage ( a ) ; }
      /// OPTIONAL: the nice printout
      std::ostream& fillStream ( std::ostream& s ) const override
      { return s << m_cut ; }
      // ======================================================================
    public:
      // ======================================================================
      /** perform the actual evaluation
       *  @attention predicate is appleid only for VALID stages
       *  @param stage (INPUT) the input stage
       *  @return result of predicate evaluation for VALID stages
       */
      bool filterStage ( const Hlt::Stage* stage ) const
      {
        //
        if ( !stage ) {
          Error ( "Invalid Stage, return false" ) ;
          return false ;                                         // RETURN
        }
        //
        // get the object from the stage
        //
        auto obj = stage->get<TYPE>() ;
        //
        // use the actual predicate for VALID stage :
        //
        return  obj && m_cut.fun ( obj ) ;
      }
      // ======================================================================
    private:
      // ======================================================================
      LoKi::FunctorFromFunctor<const TYPE*, bool> m_cut;
      // ======================================================================
    };
    // ========================================================================
    template <>
    class Cut_<Hlt::MultiTrack> :
      public LoKi::BasicFunctors<const Hlt::Stage*>::Predicate
    {
    public:
      // ==================================================================
      /// constructor from the "cutval"
      Cut_
      ( const LoKi::BasicFunctors<const LHCb::Track*>::CutVal& cut ) ;
      /// MANDATORY: clone method ("virtual constructor")
      Cut_* clone() const override { return new  Cut_ ( *this ) ; }
      /// MANDATORY: the only one essential method
      LoKi::BasicFunctors<const Hlt::Stage*>::Predicate::result_type
      operator() ( LoKi::BasicFunctors<const Hlt::Stage*>::Predicate::argument a )  const override
      { return filterStage ( a ) ; }
      /// OPTIONAL: the nice printout
      std::ostream& fillStream ( std::ostream& s ) const override;
      // ======================================================================
    public:
      // ======================================================================
      /** perform the actual evaluation
       *  @attention predicate is appleid only for VALID stages
       *  @param stage (INPUT) the input stage
       *  @return result of predicate evaluation for VALID stages
       */
      bool filterStage ( const Hlt::Stage* stage ) const ;
      // ======================================================================
    private:
      // ======================================================================
      LoKi::Assignable<LoKi::BasicFunctors<const LHCb::Track*>::CutVal>::Type m_cut;
      // ======================================================================
    };
    // ========================================================================
    /// helper function to create the predicate
    template <class TYPE>
    inline Cut_<TYPE> cut_
    ( const LoKi::Functor<const TYPE*,bool>& cut , int fake )
    { return Cut_<TYPE> ( cut , fake ) ; }
    /// helper function to create the predicate
    inline
    Cut_<Hlt::MultiTrack> cut_
    ( const LoKi::BasicFunctors<const LHCb::Track*>::CutVal&    cut     ,
      int                                                    /* fake */ )
    { return LoKi::Stages::Cut_<Hlt::MultiTrack> ( cut ) ; }
    // ========================================================================
    /** @class Fun_
     *  Helper class for implementation
     *  @thanks Alexander Mazurov
     *  @author Vanya BELYAEV Ivan.BElyaev@cern.ch
     *  @date 2010-11-21
     */
    template <class TYPE>
    class Fun_ : public LoKi::BasicFunctors<const Hlt::Stage*>::Function
    {
    public:
      // ==================================================================
      /// constructor from the function and "bad"-value
      Fun_
      ( const typename LoKi::BasicFunctors<const TYPE*>::Function& fun ,
        double                                                     bad )
        : LoKi::AuxFunBase ( std::tie ( fun , bad ) )
        , LoKi::BasicFunctors<const Hlt::Stage*>::Function ()
        , m_fun  ( fun )
        , m_bad  ( bad )
      {}
      /// MANDATORY: clone method ("virtual constructor")
      Fun_* clone() const override { return new  Fun_ ( *this ) ; }
      /// MANDATORY: the only one essential method
      typename LoKi::BasicFunctors<const Hlt::Stage*>::Function::result_type
      operator() ( typename LoKi::BasicFunctors<const Hlt::Stage*>::Function::argument a )  const override
      { return evalStage ( a ) ; }
      /// OPTIONAL: the nice printout
      std::ostream& fillStream ( std::ostream& s ) const override
      { return s << m_fun ; }
      // ======================================================================
    public:
      // ======================================================================
      /** perform the actual evaluation
       *  @attention function is applied only for VALID stages
       *  @param stage (INPUT) the input stage
       *  @return result of predicate evaluation for VALID stages
       */
      double evalStage ( const Hlt::Stage* stage ) const
      {
        //
        if ( !stage )
        {
          Error ( "Invalid Stage, return 'bad'" ) ;
          return m_bad ;                                         // RETURN
        }
        // get the object from the stage
        auto obj = stage->get<TYPE>() ;
        // use the actual predicate for VALID stage :
        return obj ? m_fun.fun ( obj ) : m_bad ;
      }
      // ======================================================================
    private:
      // ======================================================================
      /// the function
      LoKi::FunctorFromFunctor<const TYPE*, double> m_fun ; // the function
      /// bad-value
      double                                        m_bad ; // bad-value
      // ======================================================================
    };
    // ========================================================================
    template <>
    class Fun_<Hlt::MultiTrack>: public LoKi::BasicFunctors<const Hlt::Stage*>::Function
    {
    public:
      // ==================================================================
      /// constructor from the function and "bad"-value
      Fun_
      ( const LoKi::BasicFunctors<const LHCb::Track*>::FunVal& fun ,
        double                                                 bad ) ;
      /// MANDATORY: clone method ("virtual constructor")
      Fun_* clone() const override { return new  Fun_ ( *this ) ; }
      /// MANDATORY: the only one essential method
      LoKi::BasicFunctors<const Hlt::Stage*>::Function::result_type
      operator() ( LoKi::BasicFunctors<const Hlt::Stage*>::Function::argument a )  const override
      { return evalStage ( a ) ; }
      /// OPTIONAL: the nice printout
      std::ostream& fillStream ( std::ostream& s ) const override;
      // ======================================================================
    public:
      // ======================================================================
      /** perform the actual evaluation
       *  @attention function is applied only for VALID stages
       *  @param stage (INPUT) the input stage
       *  @return result of predicate evaluation for VALID stages
       */
      double evalStage ( const Hlt::Stage* stage ) const ;
      // ======================================================================
    private:
      // ======================================================================
      /// the function
      LoKi::Assignable<LoKi::BasicFunctors<const LHCb::Track*>::FunVal>::Type m_fun;
      /// bad-value
      double                                        m_bad ; // bad-value
      // ======================================================================
    };
    // ========================================================================
    /// helper function to create the function
    template <class TYPE>
    inline Fun_<TYPE> fun_
    ( const LoKi::Functor<const TYPE*,double>& fun , const double bad )
    { return LoKi::Stages::Fun_<TYPE> ( fun , bad ) ; }
    /// helper function to create the function
    inline Fun_<Hlt::MultiTrack> fun_
    ( const LoKi::BasicFunctors<const LHCb::Track*>::FunVal& fun , const double bad )
    { return LoKi::Stages::Fun_<Hlt::MultiTrack> ( fun , bad ) ; }
    // ========================================================================
  } //                                            end of namespace LoKi::Stages
  // ==========================================================================
  namespace Cuts
  {
    // ========================================================================
    /** @var TS_ISTRACK
     *  trivial predicate to check the type of Hlt::Stage
     *  @see Hlt::Stage
     *  @see Hlt::Stage::is<LHCb::Track>
     *  @see LHCb::Track
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2010-08-02
     */
    const auto TS_ISTRACK = LoKi::Stages::IsTrack{};
    // ========================================================================
    /** @var TS_ISL0MUON
     *  trivial predicate to check the type of Hlt::Stage
     *  @see Hlt::Stage
     *  @see Hlt::Stage::is<LHCb::L0MuonCandidate>
     *  @see LHCb::L0MuonCandidate
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2010-08-02
     */
    const auto TS_LSL0MUON = LoKi::Stages::IsL0Muon{};
    // ========================================================================
    /** @var TS_ISDI0MUON
     *  trivial predicate to check the type of Hlt::Stage
     *  @see Hlt::Stage
     *  @see Hlt::Stage::is<Hlt::L0DiMuonCandidate>
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2010-08-02
     */
    const auto TS_ISL0DIMUON = LoKi::Stages::IsL0DiMuon{};
    // ========================================================================
    /** @var TS_ISL0CALO
     *  trivial predicate to check the type of Hlt::Stage
     *  @see Hlt::Stage
     *  @see Hlt::Stage::is<LHCb::L0CaloCandidate>
     *  @see LHCb::L0CaloCandidate
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2010-08-02
     */
    const auto TS_ISL0CALO = LoKi::Stages::IsL0Calo{};
    // ========================================================================
    /** @var TS_ISVERTEX
     *  trivial predicate to check the type of Hlt::Stage
     *  @see Hlt::Stage
     *  @see Hlt::Stage::is<LHCb::RecVertex>
     *  @see LHCb::RecVertex
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2010-08-02
     */
    const auto TS_ISVERTEX = LoKi::Stages::IsVertex{};
    // ========================================================================
    /** @var TS_ISMULTITRACK
     *  trivial predicate to check the type of Hlt::Stage
     *  @see Hlt::Stage
     *  @see Hlt::Stage::is<Hlt::MultiTrack>
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2010-08-02
     */
    const auto TS_ISMULTITRACK = LoKi::Stages::IsMultiTrack{};
    // ========================================================================
    /** @var TS_ISPARTICLE
     *  trivial predicate to check the type of Hlt::Stage
     *  @see Hlt::Stage
     *  @see Hlt::Stage::is<LHCb::Particle>
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2014-12-03
     */
    const auto TS_ISPARTICLE = LoKi::Stages::IsParticle{};
    // ========================================================================
    /** @var TS_ISPROTOP
     *  trivial predicate to check the type of Hlt::Stage
     *  @see Hlt::Stage
     *  @see Hlt::Stage::is<LHCb::ProtoParticle>
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2014-12-03
     */
    const auto TS_ISPROTOP = LoKi::Stages::IsProtoP{};
    // ========================================================================
    /** @var TS_ISSTAGE
     *  trivial predicate to check the type of Hlt::Stage
     *  @see Hlt::Stage
     *  @see Hlt::Stage::is<Hlt::Stage>
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2010-08-02
     */
    const auto TS_ISSTAGE = LoKi::Stages::IsStage{};
    // ========================================================================
    /** @var TS_TYPE
     *  trivial functor to check the type of Hlt::Stage
     *  @see Hlt::Stage
     *  @see Hlt::Stage::stageType
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2010-08-02
     */
    const auto TS_TYPE = LoKi::Stages::Type{};
    // ========================================================================
    /** @typedef TS_TrFUN
     *  trivial adapter for Track-function
     *
     *  @code
     *
     *   const TS_TrFUN pt ( TrPT ) ;
     *
     *   const Hlt::Stage* stage = ... ;
     *
     *   const double result = pt ( stage ) ;
     *
     *  @endcode
     *
     *  @see Hlt::Stage
     *  @see LHCb::Track
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2010-08-02
     */
    typedef LoKi::Stages::TrFun                                      TS_TrFUN ;
    // ========================================================================
    /** @typedef TS_TrCUT
     *  trivial adapter for Track-predicate
     *
     *  @code
     *
     *   const TS_TrCUT ok ( TrPT > 500 * MeV  ) ;
     *
     *   const Hlt::Stage* stage = ... ;
     *
     *   const bool ok = has ( stage ) ;
     *
     *  @endcode
     *
     *  @see Hlt::Stage
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2010-08-02
     */
    typedef LoKi::Stages::TrCut                                      TS_TrCUT ;
    // ========================================================================
    /** @var TS_LOCKED
     *  trivial predicate to check if Hlt::Stage is locked
     *  @see Hlt::Stage
     *  @see Hlt::Stage::locked
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2010-08-02
     */
    const auto TS_LOCKED = LoKi::Stages::Locked{};
    // ========================================================================
    /** @typedef TS_HISTORY
     *  trivial predicate to checkcertain algorithm in history
     *
     *  @code
     *
     *   const TS_HISTORY ok ( "MyAlgorithm" ) ;
     *
     *   const Hlt::Stage* stage = ... ;
     *
     *   const bool ok = has ( stage ) ;
     *
     *  @endcode
     *
     *  @see Hlt::Stage
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2010-08-02
     */
    typedef LoKi::Stages::History                                  TS_HISTORY ;
    // ========================================================================
    /** @typedef TS_HISTORY_SUB
     *  trivial predicate to check certain sub-string algorithm in history
     *
     *  @code
     *
     *   const TS_HISTORY_SUB ok ( "DiMuon" ) ;
     *
     *   const Hlt::Stage* stage = ... ;
     *
     *   const bool ok = has ( stage ) ;
     *
     *  @endcode
     *
     *  @see Hlt::Stage
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2010-08-02
     */
    typedef LoKi::Stages::HistorySub                           TS_HISTORY_SUB ;
    // ========================================================================
    /** @typedef TS_HISTORY_RE
     *  trivial predicate to check certain sub-string algorithm in history
     *
     *  @code
     *
     *   const TS_HISTORY_RE ok ( "Hlt1.*MuonDecision" ) ;
     *   const Hlt::Stage* stage = ... ;
     *   const bool ok = has ( stage ) ;
     *
     *  @endcode
     *
     *  @see Hlt::Stage
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2010-08-02
     */
    typedef LoKi::Stages::HistoryRegex                          TS_HISTORY_RE ;
    // ========================================================================
    /** @typedef TS_HASCACHE
     *  check the existence of certain key in the the cache
     *
     *  @code
     *
     *   const TS_HASHCACHE has ( "MyPT" , Hlt::Cache::Double ) ;
     *   const Hlt::Stage* stage = ... ;
     *   const bool ok = has ( stage ) ;
     *
     *  @endcode
     *
     *  @see Hlt::Stage
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2010-12-06
     */
    typedef LoKi::Stages::HasCache                                TS_HASCACHE ;
    // ========================================================================
    /** @typedef TS_CACHE_DOUBLE
     *  get the certain value for the cache :
     *
     *  @code
     *
     *   const TS_CACHE_DOUBLE fun ( "MyPT" , -1 * GeV ) ;
     *   const Hlt::Stage* stage = ... ;
     *   const double pt = fun ( stage ) ;
     *
     *  @endcode
     *
     *  @see Hlt::Stage
     *  @author Vanya BELYAEV Ivan.Belyaev@cern.ch
     *  @date 2010-12-06
     */
    typedef LoKi::Stages::HasCache                                TS_HASCACHE ;
    // ========================================================================
    /** @typedef TS_CACHE_DOUBLE
     *  get the certain value for the cache :
     *
     *  @code
     *
     *   const TS_CACHE_DOUBLE fun ( "MyPT" , -1 * GeV ) ;
     *   const Hlt::Stage* stage = ... ;
     *   const double pt = fun ( stage ) ;
     *
     *  @endcode
     *
     *  @see Hlt::Stage
     *  @author Vanya BELYAEV Ivan.Belyaev@cern.ch
     *  @date 2010-12-06
     */
    typedef LoKi::Stages::Cache1                              TS_CACHE_DOUBLE ;
    // ========================================================================
    /** @typedef TS_CACHE_BOOL
     *  get the certain value for the cache :
     *
     *  @code
     *
     *   const TS_CACHE_BOOL fun ( "MyPT" , -1 * GeV ) ;
     *   const Hlt::Stage* stage = ... ;
     *   const bool ok = fun ( stage ) ;
     *
     *  @endcode
     *
     *  @see Hlt::Stage
     *  @author Vanya BELYAEV Ivan.Belyaev@cern.ch
     *  @date 2010-12-06
     */
    typedef LoKi::Stages::Cache2                                TS_CACHE_BOOL ;
    // ========================================================================
  } //                                              end of namespace LoKi::Cuts
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_HLTSTAGES_H
// ============================================================================
