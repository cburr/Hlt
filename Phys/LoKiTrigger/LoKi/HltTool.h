// ============================================================================
#ifndef LOKI_HLTTOOL_H
#define LOKI_HLTTOOL_H 1
// ============================================================================
// Include files
#include <string>
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/Kernel.h"
// ============================================================================
/** @file  LoKi/HltTool.h
 *
 *  This file is part of LoKi project:
 *   ``C++ ToolKit for Smart and Friendly Physics Analysis''
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
 *  @date   2009-03-31
 *
 */
namespace LoKi
{
  // ==========================================================================
  namespace Hlt1
  {
    // ========================================================================
    /** @class Tool
     *  Helper class to hold the configuration of varios tools
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date   2009-03-31
     */
    class GAUDI_API Tool
    {
    public:
      // ======================================================================
      /** constructor from the tool type/name & "public" flag
       *  @param tool the tool type/name
       *  @param flag public?
       */
      Tool
        ( const std::string& tool          ,
          const bool         flag  = false ) ;
      /// virtual destructor
      virtual ~Tool() = default;
      // ======================================================================
    public:
      // ======================================================================
      /// get the tool
      const std::string& tool () const { return m_tool    ; }
      /// public?
      inline bool        pub  () const { return m_public  ; }
      /// private
      inline bool        priv () const { return !m_public ; }
      /// flag
      inline bool        flag () const { return m_public  ; }
      // ======================================================================
    public:
      // ======================================================================
      /// output operator
      virtual std::ostream& fillStream  ( std::ostream& s ) const = 0 ;
      /// conversion to string
      virtual std::string   toString    () const ;
      // ======================================================================
    private:
      // ======================================================================
      /// the default constructor is disabled
      Tool () ;                          // the default constructor is disabled
      // ======================================================================
    private:
      // ======================================================================
      /// tool type/name
      std::string m_tool   ;                                  // tool type/name
      /// public tool ?
      bool        m_public ;                                   // public tool ?
      // ======================================================================
    };
    // ========================================================================
    /// output operator
    // ========================================================================
    inline std::ostream& operator<<
      ( std::ostream&           s ,
        const LoKi::Hlt1::Tool& t ) { return t.fillStream ( s ) ; }
    // ========================================================================
  } //                                              end of namespace LoKi::Hlt1
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
namespace Gaudi
{
  // ==========================================================================
  namespace Utils
  {
    // ========================================================================
    GAUDI_API std::string toCpp ( const LoKi::Hlt1::Tool&                o ) ;
    // ========================================================================
  }
  // ==========================================================================
}
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_HLTTOOL_H
// ============================================================================
