// ============================================================================
// Include files
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/TrackTool.h"
#include "LoKi/ToCpp.h"
// ============================================================================
/** @file
 *  Implementation file for class LoKi::Tracks::TrackTool
 *
 *  This file is part of LoKi project:
 *   ``C++ ToolKit for Smart and Friendly Physics Analysis''
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @date 2011-02-07
 *  @author Vanya  BELYAEV Ivan.Belyaev@cern.ch
 *
 */
// ============================================================================
std::string Gaudi::Utils::toCpp ( const LoKi::Hlt1::TrackTool& t ) 
{
  std::string s = " LoKi::Hlt1::TrackTool(" ;
  if ( t.hasFilter() ) { s += toCpp( t.filter() ) ; }
  return s +  ") " ;
}


// ============================================================================
// The END
// ============================================================================

